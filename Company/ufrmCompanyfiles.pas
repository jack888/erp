unit ufrmCompanyfiles;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, DBGridEhGrouping, ToolCtrlsEh,
  DBGridEhToolCtrls, DynVarsEh, cxGraphics, cxLookAndFeels,
  cxLookAndFeelPainters, Vcl.Menus, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, cxControls, cxContainer, cxEdit, cxTextEdit,
  Vcl.StdCtrls, cxButtons, Vcl.ComCtrls, RzTabs, RzButton, RzPanel,
  Vcl.FileCtrl, RzFilSys, Vcl.Mask, RzEdit, RzLabel, EhLibVCL, GridsEh,
  DBAxisGridsEh, DBGridEh, Vcl.ExtCtrls, Vcl.OleCtrls, RzSplit,
  cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage,
  cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, cxLabel,
  Winapi.ShlObj, cxShellCommon, cxListView, cxShellListView,ufrmBaseController, UtilsTree,FillThrdTree,
  Datasnap.DBClient,UnitADO, cxCalendar, RzListVw, RzShellCtrls, cxTreeView,
  cxShellTreeView, dxCore, cxDateUtils;

type
  TfrmCompanyfiles = class(TfrmBaseController)
    RzPanel1: TRzPanel;
    Left: TRzPanel;
    tv: TTreeView;
    RzToolbar1: TRzToolbar;
    RzSpacer2: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer5: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzSpacer6: TRzSpacer;
    RzToolButton5: TRzToolButton;
    RzSpacer7: TRzSpacer;
    cxGrid1: TcxGrid;
    tvCompany: TcxGridDBTableView;
    tvCompanyColumn1: TcxGridDBColumn;
    tvCompanyColumn2: TcxGridDBColumn;
    lv1: TcxGridLevel;
    Splitter3: TSplitter;
    Grid: TcxGrid;
    tvFiles: TcxGridDBTableView;
    tvFilesColumn1: TcxGridDBColumn;
    tvFilesColumn2: TcxGridDBColumn;
    tvFilesColumn3: TcxGridDBColumn;
    tvFilesColumn4: TcxGridDBColumn;
    tvFilesColumn5: TcxGridDBColumn;
    Lv: TcxGridLevel;
    RzToolbar2: TRzToolbar;
    RzSpacer9: TRzSpacer;
    RzToolButton7: TRzToolButton;
    RzSpacer10: TRzSpacer;
    RzToolButton8: TRzToolButton;
    RzSpacer11: TRzSpacer;
    RzToolButton9: TRzToolButton;
    RzSpacer14: TRzSpacer;
    RzToolButton10: TRzToolButton;
    RzSpacer15: TRzSpacer;
    RzToolButton11: TRzToolButton;
    RzLabel4: TRzLabel;
    RzSpacer16: TRzSpacer;
    cxTextEdit2: TcxTextEdit;
    Splitter1: TSplitter;
    RzSpacer18: TRzSpacer;
    Master: TClientDataSet;
    DataMaster: TDataSource;
    RzSpacer19: TRzSpacer;
    RzToolButton6: TRzToolButton;
    DataCompany: TDataSource;
    RzToolbar4: TRzToolbar;
    RzSpacer20: TRzSpacer;
    RzToolButton15: TRzToolButton;
    RzSpacer21: TRzSpacer;
    RzToolButton16: TRzToolButton;
    RzSpacer22: TRzSpacer;
    RzToolButton17: TRzToolButton;
    RzSpacer23: TRzSpacer;
    RzToolButton18: TRzToolButton;
    Company: TClientDataSet;
    RzPanel3: TRzPanel;
    cxLabel1: TcxLabel;
    search: TcxTextEdit;
    cxButton1: TcxButton;
    tvFilesColumn6: TcxGridDBColumn;
    RzSpacer1: TRzSpacer;
    pm: TPopupMenu;
    N1: TMenuItem;
    cxLabel3: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel7: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    RzToolButton19: TRzToolButton;
    RzSpacer8: TRzSpacer;
    RzSpacer12: TRzSpacer;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzToolButton9Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure tvChange(Sender: TObject; Node: TTreeNode);
    procedure tvEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure tvDragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure tvDragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure RzToolButton4Click(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
    procedure RzToolButton7Click(Sender: TObject);
    procedure RzToolButton6Click(Sender: TObject);
    procedure MasterAfterInsert(DataSet: TDataSet);
    procedure tvFilesColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvFilesEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvCompanyDblClick(Sender: TObject);
    procedure RzToolButton8Click(Sender: TObject);
    procedure RzToolButton10Click(Sender: TObject);
    procedure RzToolButton15Click(Sender: TObject);
    procedure tvCompanyEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzToolButton16Click(Sender: TObject);
    procedure RzToolButton18Click(Sender: TObject);
    procedure RzToolButton17Click(Sender: TObject);
    procedure RzToolButton11Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure searchKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CompanyAfterInsert(DataSet: TDataSet);
    procedure tvKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvCompanyColumn2PropertiesCloseUp(Sender: TObject);
    procedure MasterAfterOpen(DataSet: TDataSet);
    procedure MasterAfterClose(DataSet: TDataSet);
    procedure tvFilesEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure cxTextEdit2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure N1Click(Sender: TObject);
    procedure RzToolButton19Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
  private
    { Private declarations }
    g_TreeView : TNewUtilsTree;
    dwCurrentPath : WideString;
    dwPostCode : string;
    dwIsSelectCompany : Boolean;
    dwSQLtext : string;
    dwCompanySqlText : string;
    dwNodeId : Integer;
    dwCompayInfoIndex : Integer;
    procedure TreeAddName(Tv : TTreeView); //树增加名称
    procedure UpDateIndex(lpNode : TTreeNode);
    procedure ReadData();
    function IstreeEdit(S : string ; Node: TTreeNode):Boolean;
  public
    { Public declarations }
    dwADO : TADO;
  end;

var
  frmCompanyfiles: TfrmCompanyfiles;
  g_Table_Company_filesTree  : string = 'sk_Company_filesTree';
  g_Table_Company_fileslist  : string = 'sk_Company_fileslist';
  g_Dir_Company_fileslist    : string = 'Companyfileslist';


implementation

uses
   uDataModule,ufunctions,global;

{$R *.dfm}

procedure TfrmCompanyfiles.UpDateIndex(lpNode : TTreeNode);
var
  s : string;
begin
  if lpNode <> nil then
  begin
    S := 'UPDATE ' + g_Table_Company_filesTree + ' SET TreeIndex=''' + IntToStr(lpNode.Index) + ''' WHERE SignName="' + lpNode.Text + '"';
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      ExecSQL;
    end;
  end;
end;

procedure TfrmCompanyfiles.TreeAddName(Tv : TTreeView); //树增加名称
var
  Node , ChildNode : TTreeNode;
  szCode : string;

begin
  with Tv do
  begin
    Node := Selected;
    if Assigned(Node) then
    begin  
      if Node.Level = 0 then
      begin
        MessageBox(handle, PChar('不可以在顶级节点新建?'),'提示',
               MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2)
      end else
      begin
        szCode := 'FS-' + GetRowCode( g_Table_Company_filesTree ,'Code','100007',170000);
        g_TreeView.AddChildNode(szCode,'',Date,m_OutText);
      end;    
      
    end;

  end;

end;

function TfrmCompanyfiles.IstreeEdit(S : string ; Node: TTreeNode):Boolean;
var
  szCodeList : string;
  PrevNode , NextNode : TTreeNode;

begin
  if s = m_OutText then
  begin
    treeCldnode(Node,szCodeList);
    g_TreeView.DeleteTree(Node);
  end else
  begin
    g_TreeView.ModifyNodeSignName(s,Node);
    UpDateIndex(Node);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    if PrevNode.Index <> -1 then UpDateIndex(PrevNode);
    NextNode   := Tv.Selected.getNextSibling;//下
    if NextNode.Index <> -1 then UpDateIndex(NextNode);
  end;
end;

procedure TfrmCompanyfiles.MasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton6.Enabled := False;
  Self.RzToolButton7.Enabled := False;
  Self.RzToolButton8.Enabled := False;
  Self.RzToolButton10.Enabled:= False;
  Self.RzToolButton11.Enabled:= False;
  Self.cxTextEdit2.Enabled   := False;
  Self.RzToolButton19.Enabled:= False;
  Self.cxDateEdit1.Enabled := False;
  Self.cxDateEdit2.Enabled := False;
end;

procedure TfrmCompanyfiles.MasterAfterInsert(DataSet: TDataSet);
var
  szCode : string;
  dwPrefix , dwSuffix  : string;

begin
  dwPrefix := 'DA';
  dwSuffix := '100009';
  with DataSet do
  begin
    szCode := dwPrefix + GetRowCode(g_Table_Company_fileslist,Self.tvFilesColumn2.DataBinding.FieldName,dwSuffix,200000);
    Self.tvFilesColumn4.EditValue := Date;
    Self.tvFilesColumn2.EditValue := szCode;
    FieldByName('TreeId').Value:= dwNodeId;
    FieldByName('Code').Value  := dwPostCode;
  end;
end;

procedure TfrmCompanyfiles.MasterAfterOpen(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton6.Enabled := True;
  Self.RzToolButton7.Enabled := True;
  Self.RzToolButton8.Enabled := True;
  Self.RzToolButton10.Enabled:= True;
  Self.RzToolButton11.Enabled:= True;
  Self.cxTextEdit2.Enabled := True;
  Self.RzToolButton19.Enabled:= True;
  Self.cxDateEdit1.Enabled := True;
  Self.cxDateEdit2.Enabled := True;
end;

procedure TfrmCompanyfiles.N1Click(Sender: TObject);
var
  dir: WideString;
  s : string;
  szDir : string;
  szNumbers : string;

begin
  inherited;
  szNumbers := VarToStr( Self.tvFilesColumn2.EditValue );
  CreateOpenDir(Handle,Concat(dwCurrentPath,'\' + szNumbers),True);
end;

procedure TfrmCompanyfiles.N2Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton7.Click;
end;

procedure TfrmCompanyfiles.N3Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton10.Click;
end;

procedure TfrmCompanyfiles.N4Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton8.Click;
end;

procedure TfrmCompanyfiles.N6Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton6.Click;
end;

procedure TfrmCompanyfiles.N7Click(Sender: TObject);
begin
  inherited;
  GetMaintainInfo;
  ShowMessage('信息刷新完成!');
end;

procedure TfrmCompanyfiles.tvChange(Sender: TObject; Node: TTreeNode);
var
  s : string;
  PData: PNodeData;
  szRowCount : Integer;
begin
  inherited;
  if Node.Level >= 1 then
  begin
    PData := PNodeData(Node.Data);
    if Assigned(PData) then
    begin
      dwNodeId := pData^.Index;

      with DM do
      begin
        ADOQuery1.Close;
        ADOQuery1.SQL.Clear;
        dwSQLtext := 'Select * from ' +  g_Table_Company_fileslist + ' WHERE TreeId="' + IntToStr(dwNodeId) +'"';
        ADOQuery1.SQL.Text := dwSQLtext;
        Self.Master.Data := DataSetProvider1.Data;
        Self.Master.Open;
      end;

    end;

  end else
  begin
    Self.Master.Close;
  end;  

end;

procedure TfrmCompanyfiles.tvCompanyColumn2PropertiesCloseUp(Sender: TObject);
var
  szRowIndex : Integer;
  szCol1Name : string;
  DD1 : string;
  i : Integer;
  szIsExist:Boolean;
  
begin
  inherited;
  with (Sender as TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szCol1Name := VarToStr( DataController.Values[szRowIndex, 0] ); //姓名
      ////////////////////////////////////////////////////////////////////////

      szIsExist  := False;
      with Self.tvCompany do
      begin
        for I := 0 to ViewData.RowCount-1 do
        begin
          DD1 := VarToStr( ViewData.Rows[i].Values[Self.tvCompanyColumn2.Index] ); //姓名
          if ( DD1 = VarToStr(szCol1Name)) and (i <> Controller.FocusedRowIndex) then
          begin
            szIsExist := True;
            Self.tvCompanyColumn2.EditValue := null;
            Self.tvCompanyColumn2.Editing;
            Application.MessageBox(PWideChar(VarToStr( szCol1Name ) + ' 已经存在' ), '？', MB_OKCANCEL + MB_ICONWARNING);
            Break;
          end;
      
        end;

      end;
    end;
  end;
end;

procedure TfrmCompanyfiles.tvCompanyDblClick(Sender: TObject);
var
  szRowIndex : Integer;
  s : string;

begin
  inherited;

  if (Self.Master.Active) and (dwIsSelectCompany) then
  begin
     if MessageBox(handle, PChar('是否切换公司?'),'提示',
               MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDNO then
    begin
      Exit;
    end;
  end;

  szRowIndex := Self.tvCompany.Controller.FocusedRowIndex;
  if szRowIndex >= 0 then
  begin
    with Self.Company do
    begin
      if State <> dsInactive then
      begin
        dwPostCode :=FieldByName('Code').AsString;
        g_TreeView.GetTreeTable( FieldByName('parent').AsInteger );
        dwIsSelectCompany := True;

        Self.tv.Enabled := True;
        Self.RzToolButton1.Enabled := True;
        Self.RzToolButton2.Enabled := True;
        Self.RzToolButton3.Enabled := True;
        Self.RzToolButton4.Enabled := True;
        Self.RzToolButton5.Enabled := True;
        Self.cxButton1.Enabled := True;
        Self.search.Enabled := True;
        
      end;
    end;
    
  end;
end;

procedure TfrmCompanyfiles.tvCompanyEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Self.Company,AAllow);
end;

procedure TfrmCompanyfiles.tvDragDrop(Sender, Source: TObject; X, Y: Integer);
begin
//
end;

procedure TfrmCompanyfiles.tvDragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
//
end;

procedure TfrmCompanyfiles.tvEdited(Sender: TObject; Node: TTreeNode;
  var S: string);
begin
  IstreeEdit(s,Node);
end;

procedure TfrmCompanyfiles.tvFilesColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

function StringToWidestring(Source:string):WideString;
var
  Len  : Integer;
begin
  Len := MultiByteToWideChar(936,0,PAnsiChar(Source),Length(Source),nil,0);
  SetLength(Result,Len);
  MultiByteToWideChar(936,0,PAnsiChar(Source),Length(Source),PWideChar(Result),Len);//936为简体中文页码
end;

//widestring转成string
function WidestringToString(source:WideString):string;
var
  Len  : Integer;
begin
  Len := WideCharToMultiByte(936,0,PWideChar(source),-1,nil,0,nil,nil);
  SetLength(Result,Len-1);
  WideCharToMultiByte(936,0,PWideChar(source),Length(source),PAnsiChar(Result),Len-1,nil,nil);
end;

procedure TfrmCompanyfiles.tvFilesEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  dwADO.ADOIsEditing(Self.Master,AAllow);

  if (AItem.Index = Self.tvFilesColumn1.Index) or (Aitem.Index = Self.tvFilesColumn2.Index) then AAllow := False;

end;

procedure TfrmCompanyfiles.tvFilesEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = 13) and (AItem.Index = Self.tvFilesColumn5.Index) then
  begin
    if IsNewRow then
    begin
      dwADO.ADOAppend(Self.Master);
      Self.tvFilesColumn3.FocusWithSelection;
    end;
  end;
end;

procedure TfrmCompanyfiles.tvKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
    13:
      begin  
        if IsNewRow then
        begin
          Self.tv.Selected.Parent.Selected := True;
          Self.RzToolButton1.Click;
        end;   
        
      end; 
        
  end;
  
end;

procedure TfrmCompanyfiles.CompanyAfterInsert(DataSet: TDataSet);
var
  parent : Integer;
begin
  inherited;
  with DataSet do
  begin
    parent := DM.getLastId(g_Table_Company_filesTree);
    FieldByName('parent').Value := Parent;
    FieldByName('Code').Value   := GetRowCode(g_Table_Company_filesTree,'Code');
  end;
end;

procedure TfrmCompanyfiles.cxButton1Click(Sender: TObject);
  var
  i : integer;
begin
  inherited;
  with Self.tv do
  begin
    for i := 0 to Items.Count -1 do
    begin
     {如果和文本框匹配}
      if Items[i].Text = Self.search.Text then
      begin
        {有父类的时候通过关闭展开父类让Tv自动定位}
        if Items[i].Parent <> nil then
        begin
          Items[i].Parent.Expanded := False;
          Items[i].Parent.Expanded := True;
        end
        else
        begin
         {通过关闭展开父类让Tv自动定位}
          Items[i].Expanded := False;
          Items[i].Expanded := True;
        end;
       {选择当前循环项,并退出循环,不然他会循环到最后浪费}
        Items[i].Selected := True;
        Break;
      end;
    end;
  end;

end;

procedure TfrmCompanyfiles.cxTextEdit2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then Self.RzToolButton11.Click;
end;

procedure TfrmCompanyfiles.ReadData();
begin
  with DM do
  begin
    dwCompanySqlText := 'Select * from ' + g_Table_Company_filesTree + ' Where PID=0' ;
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := dwCompanySqlText;
    ADOQuery1.Open;
    Self.Company.Data := DataSetProvider1.Data;
    Self.Company.Open;
  end;
end;

procedure TfrmCompanyfiles.FormActivate(Sender: TObject);
begin
  inherited;
  dwCurrentPath :=  Concat(g_Resources + '\', g_Dir_Company_fileslist ) ;
  if not DirectoryExists( dwCurrentPath ) then
     CreateOpenDir(Handle, dwCurrentPath ,False);


  g_TreeView := TNewUtilsTree.Create(Self.tv,
                                     DM.ADOconn,
                                     g_Table_Company_filesTree,
                                     '公司档案');
  dwIsSelectCompany := False;
  ReadData;
  Self.tv.Items.Clear;
  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
end;

procedure TfrmCompanyfiles.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmCompanyfiles.RzToolButton10Click(Sender: TObject);
begin
  inherited;
  dwADO.ADOIsEdit(Self.Master);
end;

procedure TfrmCompanyfiles.RzToolButton11Click(Sender: TObject);
begin
  inherited;
  with DM do
  begin
    ADOQuery1.Close;
    ADOQuery1.SQL.Clear;
    dwSQLtext := 'Select * from ' +  g_Table_Company_fileslist +
                 ' WHERE TreeId="' + IntToStr(dwNodeId) +
                 '" AND ' + Self.tvFilesColumn5.DataBinding.FieldName +
                 ' Like "%' + Self.cxTextEdit2.Text + '%"';
    ADOQuery1.SQL.Text := dwSQLtext;
    Self.Master.Data := DataSetProvider1.Data;
    Self.Master.Open;
  end;

end;

procedure TfrmCompanyfiles.RzToolButton15Click(Sender: TObject);
begin
  inherited;
  dwADO.ADOAppend(Self.Company);
end;

procedure TfrmCompanyfiles.RzToolButton16Click(Sender: TObject);
begin
  inherited;
  dwADO.ADOIsEdit(Self.Company);

end;

procedure TfrmCompanyfiles.RzToolButton17Click(Sender: TObject);
var
  Node : TTreeNode;
  szCodeList : string;
  szColName  : string;
  szTableList: array[0..1] of TTableList;
  szCode : string;
  szParent: Integer;
begin
  inherited;
  //删除
  if MessageBox(handle, '是否删除公司节点？', '提示', MB_ICONQUESTION
     + MB_YESNO) = IDYES then
  begin
    with Self.Company do
    begin
      if not IsEmpty then
      begin
        szCode := FieldByName('Code').AsString;
        szParent := FieldByName('parent').AsInteger;
      end;

    end;
    
    DM.getTreeCode(g_Table_Company_filesTree, szParent,szCodeList); //获取要删除的id表
   
    szTableList[0].ATableName := g_Table_Company_fileslist;    //档案管理
    szTableList[0].ADirectory := g_Dir_Company_fileslist;

    DeleteTableFile(szTableList,szCodeList);
    DM.InDeleteData(g_Table_Company_filesTree,szCodeList);
    
    Self.FormActivate(Sender);
    Self.tv.Items.Clear;
 
  end;
end;

procedure TfrmCompanyfiles.RzToolButton18Click(Sender: TObject);
begin
  inherited;
  dwADO.ADOSaveData(Self.Company,dwCompanySqlText);
end;

procedure TfrmCompanyfiles.RzToolButton19Click(Sender: TObject);
var
  szColName : string;
begin
  inherited;
  //日期查询
  szColName   := Self.tvFilesColumn4.DataBinding.FieldName;
  dwSQLtext := 'Select * from ' +  g_Table_Company_fileslist +
                 ' WHERE TreeId="' + IntToStr(dwNodeId) + '" AND ' + szColName +' between :t1 and :t2';
  dwADO.ADOSearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text, dwSQLtext ,Self.Master);

end;

procedure TfrmCompanyfiles.RzToolButton1Click(Sender: TObject);
var
  Node : TTreeNode;
  szCodeList : string;
  szColName  : string;
  szTableList : array[0..1] of TTableList;

begin
  if sender  = Self.RzToolButton1 then
  begin
    //添加
    TreeAddName(Self.tv);
  end else
  if Sender = Self.RzToolButton2 then
  begin
    //编辑
    Node := Self.tv.Selected;
    if Node.Level <= 1 then
    begin
      MessageBox(handle, PChar('不可以在顶级节点新建?'),'提示',
             MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2)
    end else
    begin
      Node.EditText;
    end;
  end else
  if Sender = Self.RzToolButton3 then
  begin
    //删除
    Node := Self.Tv.Selected;
    if Assigned(Node)  then
    begin
      if Node.Level <= 1 then
      begin
        MessageBox(handle, '顶级节点不可以删除？', '提示', MB_ICONQUESTION + MB_YESNO)
      end else
      begin
        if MessageBox(handle, '是否删除档案节点？', '提示', MB_ICONQUESTION + MB_YESNO) = IDYES then
        begin
          GetTreeIndexList(Node,szCodeList);
          szTableList[0].ATableName := g_Table_Company_fileslist;    //档案管理
          szTableList[0].ADirectory := g_Dir_Company_fileslist;

          DeleteTableFile(szTableList,szCodeList);
          DM.InDeleteData(g_Table_Company_filesTree,szCodeList);
          g_TreeView.DeleteTree(Node);
        end;

      end;

    end;

  end;

end;

procedure TfrmCompanyfiles.RzToolButton4Click(Sender: TObject);
var
  PrevNode , NextNode , SourceNode , TargetNode : TTreeNode;
begin
  inherited;
  PrevNode   := Tv.Selected.getPrevSibling;
  NextNode   := Tv.Selected.getNextSibling;

  SourceNode := Tv.Selected;
  if (PrevNode.Index <> -1) then
  begin
    if NextNode <> nil then
      PrevNode.MoveTo(NextNode, naInsert)
    else
      PrevNode.MoveTo(SourceNode, naAdd);
  end;

  SourceNode := Tv.Selected; //中
  UpDateIndex(SourceNode);
//  PrevNode   := Tv.Selected.getPrevSibling;//上
//  UpDateIndex(PrevNode);
  NextNode   := Tv.Selected.getNextSibling;//下
  UpDateIndex(NextNode);
end;

procedure TfrmCompanyfiles.RzToolButton5Click(Sender: TObject);
var
  OldNode , Node , SourceNode ,PrevNode: TTreeNode;
begin
  inherited;
  Node := Self.Tv.Selected;
  OldNode := Node.getNextSibling;
  if OldNode.Index <> -1 then
  begin

    OldNode.MoveTo(Node, naInsert);

    SourceNode := Tv.Selected; //中
    UpDateIndex(SourceNode);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    UpDateIndex(PrevNode);
  //  NextNode   := Tv.Selected.getNextSibling;//下
  //  UpDateIndex(NextNode);
  end;
end;

procedure TfrmCompanyfiles.RzToolButton6Click(Sender: TObject);
begin
  dwADO.ADOSaveData(Self.Master,dwSQLtext);
end;

procedure TfrmCompanyfiles.RzToolButton7Click(Sender: TObject);
begin
  dwADO.ADOAppend(Self.Master);
  Self.cxGrid1.SetFocus;
  Self.tvFilesColumn1.FocusWithSelection;
//  keybd_event(VK_RETURN,0,0,0);
end;

procedure TfrmCompanyfiles.RzToolButton8Click(Sender: TObject);
begin
  inherited;
  dwADO.ADODeleteSelection(Self.tvFiles,
                           dwCurrentPath,
                           g_Table_Company_fileslist,
                           Self.tvFilesColumn2.DataBinding.FieldName);
end;

procedure TfrmCompanyfiles.RzToolButton9Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmCompanyfiles.searchKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    Self.cxButton1.Click;
  end;
end;

end.
