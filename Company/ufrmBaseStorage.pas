unit ufrmBaseStorage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, Vcl.ComCtrls, dxCore,
  cxDateUtils, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxTextEdit, cxCheckBox,
  cxCalendar, cxDBLookupComboBox, cxDropDownEdit, cxMemo, cxCurrencyEdit,
  cxSpinEdit, Vcl.ExtCtrls, cxDBEdit, Vcl.StdCtrls, cxGroupBox, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxMaskEdit, cxLabel, RzButton, RzPanel, Vcl.Menus,
  frxClass, frxDBSet,ufrmBaseController, Datasnap.DBClient, Data.Win.ADODB;

type
  TfrmBaseStorage = class(TfrmBaseController)
    frxStorageReport: TfrxDBDataset;
    pm: TPopupMenu;
    N1: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    RzPanel1: TRzPanel;
    RzToolbar13: TRzToolbar;
    RzSpacer30: TRzSpacer;
    RzToolButton85: TRzToolButton;
    RzSpacer113: TRzSpacer;
    RzToolButton87: TRzToolButton;
    RzToolButton88: TRzToolButton;
    RzToolButton90: TRzToolButton;
    RzSpacer120: TRzSpacer;
    Grid: TcxGrid;
    tvView: TcxGridDBTableView;
    tvViewColumn24: TcxGridDBColumn;
    tvViewColumn28: TcxGridDBColumn;
    tvViewColumn1: TcxGridDBColumn;
    tvViewColumn2: TcxGridDBColumn;
    tvViewColumn3: TcxGridDBColumn;
    tvViewColumn15: TcxGridDBColumn;
    tvViewColumn17: TcxGridDBColumn;
    tvViewColumn4: TcxGridDBColumn;
    tvViewColumn5: TcxGridDBColumn;
    tvViewColumn21: TcxGridDBColumn;
    tvViewColumn11: TcxGridDBColumn;
    tvViewColumn7: TcxGridDBColumn;
    tvViewColumn6: TcxGridDBColumn;
    tvViewColumn8: TcxGridDBColumn;
    tvViewColumn9: TcxGridDBColumn;
    tvViewColumn10: TcxGridDBColumn;
    tvViewColumn12: TcxGridDBColumn;
    tvViewColumn14: TcxGridDBColumn;
    tvViewColumn25: TcxGridDBColumn;
    tvViewColumn26: TcxGridDBColumn;
    tvViewColumn27: TcxGridDBColumn;
    tvViewColumn13: TcxGridDBColumn;
    Lv: TcxGridLevel;
    RzPanel3: TRzPanel;
    Bevel2: TBevel;
    cxGroupBox4: TcxGroupBox;
    Label23: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label3: TLabel;
    cxSpinEdit1: TcxSpinEdit;
    cxSpinEdit2: TcxSpinEdit;
    cxSpinEdit5: TcxSpinEdit;
    cxComboBox1: TcxComboBox;
    cxGroupBox2: TcxGroupBox;
    Label4: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    cxDBMemo2: TcxDBMemo;
    cxDBSpinEdit2: TcxDBSpinEdit;
    cxDBSpinEdit3: TcxDBSpinEdit;
    cxLabel1: TcxLabel;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxLabel2: TcxLabel;
    cxComboBox2: TcxComboBox;
    RzPanel4: TRzPanel;
    Bevel1: TBevel;
    cxDBTextEdit3: TcxDBTextEdit;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxDBComboBox1: TcxDBComboBox;
    RzPanel2: TRzPanel;
    Splitter1: TSplitter;
    Storage: TClientDataSet;
    ds: TDataSource;
    ADOMaking: TADOQuery;
    DataMaking: TDataSource;
    RzSpacer1: TRzSpacer;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzToolButton88Click(Sender: TObject);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvViewStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvViewColumn4PropertiesCloseUp(Sender: TObject);
    procedure tvViewColumn24GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBaseStorage: TfrmBaseStorage;

implementation

uses
   global,uDataModule,ufunctions;

{$R *.dfm}

procedure TfrmBaseStorage.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmBaseStorage.RzToolButton88Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmBaseStorage.tvViewColumn24GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmBaseStorage.tvViewColumn4PropertiesCloseUp(Sender: TObject);
var
  szColContentA : Variant;
  szColContentB : Variant;
  szColContentC : Variant;
  szColContentD : Variant;
  szColContentE : Variant;
  szColContentF : Variant;
  szColContentG : Variant;
  szRowIndex:Integer;
  szColName : string;
  szCount : Variant;

begin
  inherited;

  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;

    if szRowIndex >= 0 then
    begin

      szColContentA :=  DataController.Values[szRowIndex,0] ; //名称
      szColContentB :=  DataController.Values[szRowIndex,1] ; //单价
      szColContentC :=  DataController.Values[szRowIndex,2] ; //录入日期
      szColContentD :=  DataController.Values[szRowIndex,3] ; //品牌
      szColContentE :=  DataController.Values[szRowIndex,4] ; //型号
      szColContentF :=  DataController.Values[szRowIndex,5] ; //规格
      szColContentG :=  DataController.Values[szRowIndex,6] ; //单位

      if szColContentA <> null then
      begin

        with Self.Storage do
        begin
          if State <> dsInactive then
          begin
            if (State <> dsInsert) or (State <> dsEdit) then  Edit;

            szColName := Self.tvViewColumn4.DataBinding.FieldName;
            FieldByName(szColName).Value := szColContentA;
            szColName := Self.tvViewColumn5.DataBinding.FieldName; //规格
            FieldByName(szColName).Value := szColContentF;
            szColName := Self.tvViewColumn10.DataBinding.FieldName;//单价
            FieldByName(szColName).Value := szColContentB; //单价
            szColName := Self.tvViewColumn9.DataBinding.FieldName; //单位
            FieldByName(szColName).Value := szColContentG;

            szRowIndex := Self.tvView.Controller.FocusedRowIndex;
            szCount := Self.tvView.DataController.Values[szRowIndex,Self.tvViewColumn8.Index];
            if szCount <> null then
            begin
              szColName := Self.tvViewColumn12.DataBinding.FieldName;
              FieldByName(szColName).Value := szCount * szColContentB;
            end;
          end;

        end;

      end;

    end;

  end;

end;


procedure TfrmBaseStorage.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  szColName : string;
  szValue : Variant;
  szRowIndex : Integer;

begin
  inherited;
  szColName := Self.tvViewColumn28.DataBinding.FieldName;
  if AItem.Index <> Self.tvViewColumn28.Index then
  begin
    szRowIndex := Self.tvView.Controller.FocusedRowIndex;
    szValue := Self.tvView.DataController.Values[szRowIndex,Self.tvViewColumn28.Index];
    if not szValue  then
    begin
      AAllow := False;
    end else
    begin
      AAllow := True;
    end;
    {
    with Self.ADOStorage do
    begin
      if State <> dsInactive then
        begin
          if FieldByName(szColName).Value = False then
          begin
            AAllow := False;
          end else
          begin
            AAllow := True;
          end;

        end;
    end;
    }
  end;

  if AItem.Index = Self.tvViewColumn1.Index then
  begin
    AAllow := False;
  end;
end;

procedure TfrmBaseStorage.tvViewEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = 13) and (AItem.Index = Self.tvViewColumn12.Index) then
  begin
    if (Self.tvView.Controller.FocusedRow.IsLast) and (IsNewRow) then
    begin
      {
      with Self.ADOStorage do
      begin
        if State <> dsInactive then
        begin
          Append;
          Self.tvViewColumn2.FocusWithSelection;
        end;
      end;
      }
    end;
  end;
end;

procedure TfrmBaseStorage.tvViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then Self.RzToolButton85.Click;
end;

procedure TfrmBaseStorage.tvViewStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;

  Value := ARecord.Values[Self.tvViewColumn28.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;

end;


end.
