unit ufrmStorageBase;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, RzPanel, RzButton,
  Vcl.ComCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxStyles, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Vcl.Menus,
  Datasnap.DBClient, cxContainer, cxLabel,UnitADO,UtilsTree,FillThrdTree,
  Vcl.StdCtrls, cxButtons, cxDBEdit, cxMemo, cxDropDownEdit, cxTextEdit,
  cxMaskEdit, cxSpinEdit, cxGroupBox,ufrmBaseController, cxDBLookupComboBox,
  Data.Win.ADODB, cxLookupEdit, cxDBLookupEdit, cxCheckBox, cxCurrencyEdit,
  dxCore, cxDateUtils, cxCalendar, cxRadioGroup, RzTabs, Datasnap.Provider,
  BMDThread, cxButtonEdit;

type
  TfrmStorageBase = class(TfrmBaseController)
    Splitter1: TSplitter;
    RzPanel1: TRzPanel;
    RzPanel3: TRzPanel;
    RzPanel4: TRzPanel;
    RzPanel7: TRzPanel;
    Splitter4: TSplitter;
    TreeView1: TTreeView;
    Grid4: TcxGrid;
    tvSign: TcxGridDBTableView;
    Lv4: TcxGridLevel;
    tvSignColumn1: TcxGridDBColumn;
    tvSignColumn2: TcxGridDBColumn;
    Print: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    pmu: TPopupMenu;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    ADOCompany: TADOQuery;
    DataSign: TDataSource;
    DataMaster: TDataSource;
    DataDetailed: TDataSource;
    RightMaster: TPopupMenu;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    PopupMenu1: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    pmFrozen: TPopupMenu;
    MenuItem16: TMenuItem;
    MenuItem17: TMenuItem;
    pmTv2: TPopupMenu;
    MenuItem18: TMenuItem;
    MenuItem19: TMenuItem;
    N10: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    N17: TMenuItem;
    N18: TMenuItem;
    pmFrozen1: TPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    N11: TMenuItem;
    N19: TMenuItem;
    N20: TMenuItem;
    RzPageControl2: TRzPageControl;
    Tab1: TRzTabSheet;
    Tab2: TRzTabSheet;
    RzPanel10: TRzPanel;
    RzPanel11: TRzPanel;
    RzPanel12: TRzPanel;
    RzPanel13: TRzPanel;
    RzPanel14: TRzPanel;
    RzPanel15: TRzPanel;
    RzPanel16: TRzPanel;
    RzPanel17: TRzPanel;
    RzPanel18: TRzPanel;
    RzPanel19: TRzPanel;
    RzPanel20: TRzPanel;
    RzPanel6: TRzPanel;
    RzToolbar13: TRzToolbar;
    RzSpacer30: TRzSpacer;
    RzToolButton85: TRzToolButton;
    RzSpacer113: TRzSpacer;
    RzToolButton87: TRzToolButton;
    RzToolButton88: TRzToolButton;
    RzSpacer1: TRzSpacer;
    RzSpacer10: TRzSpacer;
    btnGridSet: TRzToolButton;
    RzToolButton3: TRzToolButton;
    RzSpacer5: TRzSpacer;
    RzToolButton6: TRzToolButton;
    RzSpacer6: TRzSpacer;
    RzSpacer11: TRzSpacer;
    RzToolButton8: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzSpacer16: TRzSpacer;
    RzToolButton9: TRzToolButton;
    Grid1: TcxGrid;
    tv1: TcxGridDBTableView;
    tv1Col1: TcxGridDBColumn;
    tv1Column1: TcxGridDBColumn;
    tv1Col2: TcxGridDBColumn;
    tv1Col3: TcxGridDBColumn;
    tv1Col4: TcxGridDBColumn;
    tv1Column3: TcxGridDBColumn;
    tv1Col6: TcxGridDBColumn;
    tv1Col7: TcxGridDBColumn;
    tv1Col8: TcxGridDBColumn;
    tv1Col9: TcxGridDBColumn;
    tv1Col10: TcxGridDBColumn;
    tv1Col11: TcxGridDBColumn;
    tv1Col15: TcxGridDBColumn;
    tv1Col12: TcxGridDBColumn;
    tv1Col13: TcxGridDBColumn;
    tv1Column2: TcxGridDBColumn;
    tv1Column4: TcxGridDBColumn;
    Lv1: TcxGridLevel;
    RzPanel9: TRzPanel;
    cxLabel7: TcxLabel;
    cxLabel9: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    cxDateEdit1: TcxDateEdit;
    cxLabel8: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    cxButton1: TcxButton;
    cxLabel3: TcxLabel;
    cxLabel10: TcxLabel;
    cxDateEdit5: TcxDateEdit;
    cxComboBox2: TcxComboBox;
    RzToolbar3: TRzToolbar;
    RzSpacer17: TRzSpacer;
    RzToolButton14: TRzToolButton;
    RzSpacer18: TRzSpacer;
    RzToolButton15: TRzToolButton;
    RzToolButton16: TRzToolButton;
    RzSpacer19: TRzSpacer;
    RzToolButton18: TRzToolButton;
    RzSpacer21: TRzSpacer;
    RzToolButton19: TRzToolButton;
    RzSpacer22: TRzSpacer;
    RzSpacer24: TRzSpacer;
    RzToolButton21: TRzToolButton;
    RzSpacer25: TRzSpacer;
    RzToolButton22: TRzToolButton;
    cxGrid2: TcxGrid;
    TableView: TcxGridDBTableView;
    Lv2: TcxGridLevel;
    TableView1: TcxGridDBColumn;
    TableView2: TcxGridDBColumn;
    TableView3: TcxGridDBColumn;
    TableView4: TcxGridDBColumn;
    TableView5: TcxGridDBColumn;
    TableView6: TcxGridDBColumn;
    N21: TMenuItem;
    N22: TMenuItem;
    RzPanel5: TRzPanel;
    RzToolbar1: TRzToolbar;
    RzSpacer2: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer8: TRzSpacer;
    RzToolButton7: TRzToolButton;
    RzSpacer9: TRzSpacer;
    RzToolButton5: TRzToolButton;
    cxComboBox1: TcxComboBox;
    cxLabel5: TcxLabel;
    RzPanel8: TRzPanel;
    cxLabel2: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel6: TcxLabel;
    cxDateEdit3: TcxDateEdit;
    cxLabel13: TcxLabel;
    cxDateEdit4: TcxDateEdit;
    cxButton2: TcxButton;
    cxRadioButton4: TcxRadioButton;
    cxRadioButton5: TcxRadioButton;
    cxComboBox4: TcxComboBox;
    cxLabel14: TcxLabel;
    cxLookupComboBox7: TcxLookupComboBox;
    cxLookupComboBox2: TcxLookupComboBox;
    cxLookupComboBox1: TcxLookupComboBox;
    cxLabel1: TcxLabel;
    Grid: TcxGrid;
    tView: TcxGridDBTableView;
    tViewCol1: TcxGridDBColumn;
    tViewCol2: TcxGridDBColumn;
    tViewCol3: TcxGridDBColumn;
    tViewCol4: TcxGridDBColumn;
    tViewCol5: TcxGridDBColumn;
    tViewCol6: TcxGridDBColumn;
    tViewColumn12: TcxGridDBColumn;
    tViewColumn13: TcxGridDBColumn;
    tViewCol7: TcxGridDBColumn;
    tViewColumn10: TcxGridDBColumn;
    tViewCol9: TcxGridDBColumn;
    tViewCol8: TcxGridDBColumn;
    tViewCol15: TcxGridDBColumn;
    tViewCol16: TcxGridDBColumn;
    tViewColumn1: TcxGridDBColumn;
    tViewColumn2: TcxGridDBColumn;
    tViewColumn3: TcxGridDBColumn;
    tViewColumn4: TcxGridDBColumn;
    tViewColumn5: TcxGridDBColumn;
    tViewColumn6: TcxGridDBColumn;
    tViewColumn7: TcxGridDBColumn;
    tViewCol11: TcxGridDBColumn;
    tViewCol12: TcxGridDBColumn;
    tViewCol13: TcxGridDBColumn;
    tViewCol14: TcxGridDBColumn;
    Lv: TcxGridLevel;
    Splitter3: TSplitter;
    Splitter6: TSplitter;
    RzPanel21: TRzPanel;
    RzToolbar4: TRzToolbar;
    RzSpacer26: TRzSpacer;
    RzToolButton23: TRzToolButton;
    RzSpacer31: TRzSpacer;
    RzToolButton24: TRzToolButton;
    RzSpacer32: TRzSpacer;
    ContractSource: TDataSource;
    ContractDetailedSource: TDataSource;
    ContractDetailedDataSet: TClientDataSet;
    pm1: TPopupMenu;
    MenuItem5: TMenuItem;
    MenuItem7: TMenuItem;
    TableView7: TcxGridDBColumn;
    TableView8: TcxGridDBColumn;
    PopupMenu2: TPopupMenu;
    MenuItem6: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    MenuItem14: TMenuItem;
    MenuItem15: TMenuItem;
    MenuItem20: TMenuItem;
    MenuItem21: TMenuItem;
    MenuItem22: TMenuItem;
    MenuItem23: TMenuItem;
    TableView9: TcxGridDBColumn;
    RzSpacer20: TRzSpacer;
    RzToolButton17: TRzToolButton;
    RzPanel22: TRzPanel;
    cxLabel11: TcxLabel;
    cxLabel12: TcxLabel;
    cxDateEdit6: TcxDateEdit;
    cxLabel15: TcxLabel;
    cxDateEdit7: TcxDateEdit;
    cxRadioButton3: TcxRadioButton;
    cxRadioButton6: TcxRadioButton;
    cxButton3: TcxButton;
    cxTextEdit2: TcxTextEdit;
    Master: TClientDataSet;
    Detailed: TClientDataSet;
    Contract: TClientDataSet;
    Timer1: TTimer;
    tv1Column5: TcxGridDBColumn;
    tv1Column6: TcxGridDBColumn;
    tv1Column7: TcxGridDBColumn;
    tViewCol10: TcxGridDBColumn;
    Lv5: TcxGridLevel;
    tGroup: TcxGridDBTableView;
    tGroupColumn1: TcxGridDBColumn;
    tGroupColumn2: TcxGridDBColumn;
    tGroupColumn3: TcxGridDBColumn;
    tGroupColumn4: TcxGridDBColumn;
    dsGroupUnitPrice: TClientDataSet;
    DataGroupUnitPrice: TDataSource;
    tViewColumn14: TcxGridDBColumn;
    cxGrid3: TcxGrid;
    tContractView: TcxGridDBTableView;
    tContractViewColumn1: TcxGridDBColumn;
    tContractViewColumn2: TcxGridDBColumn;
    tContractViewColumn3: TcxGridDBColumn;
    tContractViewColumn4: TcxGridDBColumn;
    tContractViewColumn5: TcxGridDBColumn;
    tContractViewColumn6: TcxGridDBColumn;
    tContractViewColumn7: TcxGridDBColumn;
    tContractViewColumn8: TcxGridDBColumn;
    tContractViewColumn9: TcxGridDBColumn;
    tContractViewColumn10: TcxGridDBColumn;
    tContractViewColumn11: TcxGridDBColumn;
    tContractViewColumn12: TcxGridDBColumn;
    tContractViewColumn13: TcxGridDBColumn;
    tContractViewColumn14: TcxGridDBColumn;
    tContractViewColumn15: TcxGridDBColumn;
    tContractViewColumn16: TcxGridDBColumn;
    cxGridLevel2: TcxGridLevel;
    tViewColumn8: TcxGridDBColumn;
    Bevel1: TBevel;
    dsMaking: TClientDataSet;
    dsMakingSource: TDataSource;
    dsSpec: TClientDataSet;
    dsSpecSource: TDataSource;
    dsSpec1: TClientDataSet;
    dsSpecSource1: TDataSource;
    RzPanel2: TRzPanel;
    Splitter5: TSplitter;
    tv: TTreeView;
    RzToolbar5: TRzToolbar;
    RzSpacer27: TRzSpacer;
    RzBut1: TRzToolButton;
    RzSpacer28: TRzSpacer;
    RzBut2: TRzToolButton;
    RzSpacer29: TRzSpacer;
    RzBut3: TRzToolButton;
    RzSpacer7: TRzSpacer;
    RzBut4: TRzToolButton;
    RzToolbar2: TRzToolbar;
    RzSpacer12: TRzSpacer;
    RzToolButton10: TRzToolButton;
    RzSpacer13: TRzSpacer;
    RzToolButton11: TRzToolButton;
    RzSpacer14: TRzSpacer;
    RzToolButton12: TRzToolButton;
    RzSpacer15: TRzSpacer;
    RzToolButton13: TRzToolButton;
    cxGrid1: TcxGrid;
    tvCompany: TcxGridDBTableView;
    tvCompanyColumn1: TcxGridDBColumn;
    tvCompanyColumn2: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    CompanySource: TDataSource;
    Company: TClientDataSet;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzToolButton88Click(Sender: TObject);
    procedure RzToolButton8Click(Sender: TObject);
    procedure RzToolButton7Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure RzToolButton10Click(Sender: TObject);
    procedure tvCompanyColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton13Click(Sender: TObject);
    procedure tvCompanyDblClick(Sender: TObject);
    procedure CompanyAfterInsert(DataSet: TDataSet);
    procedure tvCompanyEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzToolButton11Click(Sender: TObject);
    procedure RzToolButton12Click(Sender: TObject);
    procedure RzBut1Click(Sender: TObject);
    procedure TreeView1Click(Sender: TObject);
    procedure tvChange(Sender: TObject; Node: TTreeNode);
    procedure MasterAfterOpen(DataSet: TDataSet);
    procedure MasterAfterClose(DataSet: TDataSet);
    procedure tv1Col1GetDisplayText(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AText: string);
    procedure MasterAfterInsert(DataSet: TDataSet);
    procedure RzToolButton85Click(Sender: TObject);
    procedure tvEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure tViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzBut3Click(Sender: TObject);
    procedure btnGridSetClick(Sender: TObject);
    procedure tvSignDblClick(Sender: TObject);
    procedure cxComboBox1PropertiesCloseUp(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure RzBut4Click(Sender: TObject);
    procedure cxComboBox4PropertiesCloseUp(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxLookupComboBox2PropertiesCloseUp(Sender: TObject);
    procedure cxDateEdit5PropertiesCloseUp(Sender: TObject);
    procedure cxComboBox2PropertiesCloseUp(Sender: TObject);
    procedure cxLookupComboBox7PropertiesCloseUp(Sender: TObject);
    procedure DetailedAfterOpen(DataSet: TDataSet);
    procedure DetailedAfterClose(DataSet: TDataSet);
    procedure tv1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzPanel10Resize(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure tv1ColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure tvCompanyColumn2PropertiesCloseUp(Sender: TObject);
    procedure cxLookupComboBox7PropertiesChange(Sender: TObject);
    procedure tvDblClick(Sender: TObject);
    procedure tv1StylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tViewStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure N6Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure tv1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
    procedure cxLookupComboBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure N13Click(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure MenuItem16Click(Sender: TObject);
    procedure MenuItem17Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure MenuItem18Click(Sender: TObject);
    procedure MenuItem19Click(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure tv1Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzToolButton9Click(Sender: TObject);
    procedure N22Click(Sender: TObject);
    procedure ContractAfterOpen(DataSet: TDataSet);
    procedure ContractAfterClose(DataSet: TDataSet);
    procedure MenuItem5Click(Sender: TObject);
    procedure ContractAfterScroll(DataSet: TDataSet);
    procedure TableViewStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure TableViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure MenuItem8Click(Sender: TObject);
    procedure MenuItem9Click(Sender: TObject);
    procedure MenuItem12Click(Sender: TObject);
    procedure MenuItem13Click(Sender: TObject);
    procedure MenuItem23Click(Sender: TObject);
    procedure RzToolButton14Click(Sender: TObject);
    procedure RzToolButton23Click(Sender: TObject);
    procedure MenuItem20Click(Sender: TObject);
    procedure RzToolButton17Click(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure ContractDetailedDataSetAfterOpen(DataSet: TDataSet);
    procedure ContractDetailedDataSetAfterClose(DataSet: TDataSet);
    procedure MenuItem22Click(Sender: TObject);
    procedure RzToolButton22Click(Sender: TObject);
    procedure MasterAfterEdit(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ContractAfterEdit(DataSet: TDataSet);
    procedure GridSetThreadExecute(Sender: TObject; Thread: TBMDExecuteThread;
      var Data: Pointer);
    procedure Timer1Timer(Sender: TObject);
    procedure TableViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tv1DblClick(Sender: TObject);
    procedure DetailedAfterScroll(DataSet: TDataSet);
    procedure tv1MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    dwContractDetailedSQLText : string;
    dwContract : string;
    dwTreeId   : string;
    dwSignName : string;
    dwDetailedTable : string;
    dwDataType : Integer;
    dwNodeId : Integer;
    dwTreeCode   : string;
    dwCompanySqlText : string;
    dwADO : TADO;
    g_NodeText : string;
    g_TreeView : TNewUtilsTree;
    g_TreeCompany : TNewUtilsTree;
    procedure ReadData();
    procedure TreeAddName(Tv : TTreeView); //树增加名称
    function IstreeEdit(S : string ; Node: TTreeNode):Boolean;
    procedure UpDateIndex(lpNode : TTreeNode);
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
    procedure MasterSearch(lpSql : string );
    procedure DetailedSearch( lpSql : string );
    function GetSearchNumber(lpNumber : string):Boolean;
    function SearchDetailed():Boolean;
    function SearchMaster():Boolean;
    function ViewData(Node: TTreeNode):Boolean;
    function SearchContract():Boolean;
    procedure FunFrozen(lpValue : Boolean);
    procedure GetStorageDetailed(lpId : string);
  public
    { Public declarations }
    dwCompanyName  : string;
    dwCompanyParentId : Integer;
    dwIsOpen : Boolean;
    dwSQLtext: string;
    dwDetailedSQLtext : string;
    dwContractListSQLtext : string;
    dwIsSelectCompany : Boolean;
    s : Integer;
  end;

var
  frmStorageBase: TfrmStorageBase;

implementation

uses
   ufastContrctGooutStorage,
   ufastContrctEnterStorage,
   ufrmFastGiveBack,
   ufrmFastReturnGoods,
   ufrmFastGooutStorage,
   uProjectFrame,
   uDataModule,
   ufrmPostStorage,
   global,
   ufunctions,
   ufrmIsViewGrid,
   ufrmContract,
   ufrmFastEnterStorage;

{$R *.dfm}

procedure TfrmStorageBase.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;

procedure TfrmStorageBase.DetailedAfterClose(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton1.Enabled := False;
  Self.RzToolButton7.Enabled := False;
  Self.RzToolButton2.Enabled := False;
  Self.RzToolButton5.Enabled := False;

  Self.MenuItem18.Enabled := False;
  Self.MenuItem19.Enabled := False;
  Self.N20.Enabled := False;
  Self.N11.Enabled := False;
end;

procedure TfrmStorageBase.DetailedAfterOpen(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton1.Enabled := True;
  Self.RzToolButton7.Enabled := True;
  Self.RzToolButton2.Enabled := True;
  Self.RzToolButton5.Enabled := True;
  Self.MenuItem18.Enabled := True;
  Self.MenuItem19.Enabled := True;
  Self.N20.Enabled := True;
  Self.N11.Enabled := True;

end;

procedure TfrmStorageBase.DetailedAfterScroll(DataSet: TDataSet);
var
  s : string;
  szColName : string;
begin
  inherited;
  {
  szColName := 'ParentId';
  with DataSet do
  begin

    s := 'Select * from ' + g_Table_Company_Storage_GroupUnitPrice +
          ' Where ' + szColName + '="' + FieldByName('OddNumbers').AsString + '"';
    if Self.tView.ViewData.RowCount <> 0 then
    begin
      dwADO.OpenSQL(Self.dsGroupUnitPrice,s);
    end;

  end;
  }
end;

procedure TfrmStorageBase.DetailedSearch( lpSql : string );
begin
  dwADO.OpenSQL(Detailed,lpSql);
end;

procedure TfrmStorageBase.MasterSearch(lpSql : string);
begin
  dwADO.OpenSQL(Master,lpSql);
end;

procedure TfrmStorageBase.MenuItem12Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton19.Click;
end;

procedure TfrmStorageBase.MenuItem13Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton14.Click;
end;

procedure TfrmStorageBase.MenuItem16Click(Sender: TObject);
begin
  inherited;
  //显示冻结数据
  case Self.RzPageControl2.ActivePageIndex of
    0:
    begin
      //进销管理
    end;
    1:
    begin
      //合同管理
    end;
  end;
end;

procedure TfrmStorageBase.MenuItem17Click(Sender: TObject);
begin
  inherited;
  //显示未冻结数据
  case Self.RzPageControl2.ActivePageIndex of
    0:
    begin
      //进销管理
    end;
    1:
    begin
      //合同管理
    end;
  end;
end;

procedure TfrmStorageBase.MenuItem18Click(Sender: TObject);
begin
  inherited;
  //明细冻结数据
  SetGridStuat(Self.tView,Self.tViewCol2.DataBinding.FieldName,False);
end;

procedure TfrmStorageBase.MenuItem19Click(Sender: TObject);
begin
  inherited;
  //明细解冻数据
  SetGridStuat(Self.tView,Self.tViewCol2.DataBinding.FieldName,True);
end;

procedure TfrmStorageBase.MenuItem1Click(Sender: TObject);
begin
  inherited;
  case Self.RzPageControl2.ActivePageIndex of
    0: DM.BasePrinterLink1.Component := Self.Grid;
    1: DM.BasePrinterLink1.Component := Self.cxGrid3;
  end;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmStorageBase.MenuItem20Click(Sender: TObject);
begin
  inherited;
//附件
end;

procedure TfrmStorageBase.MenuItem22Click(Sender: TObject);
begin
  SetGridStuat(Self.TableView,TableView7.DataBinding.FieldName,False);
end;

procedure TfrmStorageBase.MenuItem23Click(Sender: TObject);
begin
  inherited;
  //合同解冻
  SetGridStuat(Self.TableView,TableView7.DataBinding.FieldName,True);
end;

procedure TfrmStorageBase.MenuItem2Click(Sender: TObject);
begin
  inherited;
  case Self.RzPageControl2.ActivePageIndex of
   0: CxGridToExcel(Self.Grid,'材料明细表');
   1: CxGridToExcel(Self.cxGrid3,'材料明细表');
  end;

end;

procedure TfrmStorageBase.MenuItem3Click(Sender: TObject);
var
  s : string;

begin
  inherited;
  if Sender = Self.MenuItem3 then
  begin
    s := 'no';
  end else
  if Sender = Self.MenuItem4 then
  begin
    s := 'yes';
  end;
  dwADO.OpenSQL(Self.Detailed,dwDetailedSQLtext + ' AND ' + Self.tViewCol2.DataBinding.FieldName + '=' + s);
end;

procedure TfrmStorageBase.MenuItem5Click(Sender: TObject);
var
  s : string;
  szModule : Integer;
begin
  if dwNodeId = 0 then
  begin

  end else
  if Length(g_NodeText) = 0 then
  begin

  end;

  Application.CreateForm(TfrmContract,frmContract);
  if Sender = Self.MenuItem5 then
  begin
    //入库单
    szModule := 0;
    s := frmContract.Caption + ' - ' + (Sender as TMenuItem).Caption;
  end else
  if Sender = Self.MenuItem7 then
  begin
    //出库单
    szModule := 1;
    s := frmContract.Caption + ' - ' + (Sender as TMenuItem).Caption;
  end else
  if Sender = Self.RzToolButton19 then
  begin
    //编辑
    szModule := 2;
    s := frmContract.Caption + ' - ' + (Sender as TRzToolButton).Caption;
  end;

  frmContract.dwNoId := Self.TableView8.EditValue;
  frmContract.dwModule := szModule;
  frmContract.dwNodeId := dwNodeId;
  frmContract.dwStorageName := g_NodeText;
  frmContract.dwCompanyName := dwCompanyName ;
  frmContract.dwCompanyParentId := dwCompanyParentId;
  frmContract.Caption := s;
  frmContract.ShowModal;
  frmContract.Free;

  dwADO.OpenSQL(Self.Contract,dwContractListSQLtext);

end;

procedure TfrmStorageBase.MenuItem8Click(Sender: TObject);
begin
  inherited;
  Self.MenuItem5.Click;
end;

procedure TfrmStorageBase.MenuItem9Click(Sender: TObject);
begin
  inherited;
  Self.MenuItem7.Click;
end;

function GetMasterSQLText : string;
begin
  Result := 'Select * from ' + g_Table_Company_StorageBillList;
end;

function GetDetailedSQLText : string;
begin
  Result := 'Select * from ' + g_Table_Company_StorageDetailed;
end;

function TfrmStorageBase.SearchDetailed():Boolean;
var
  szColName , sqltext : string;
  szGoodsName , szGoodsNameSQL : string;
  szSpec , szSpecA , szSpecSQL , szSpecASQL : string;
  szEntryType , szEntryTypeSQL : string;
  szSignNameSQL : string;
  szDataTypeSQL : string;
  i : Integer;
  DD1 , DD2 , DD3 , DD4 : Currency;
  s : string;

begin
  if dwNodeId = 0 then
  begin
    Application.MessageBox('请选择仓库后在选择往来单位进行查询','提示',mb_ok + MB_ICONWARNING);
  end else
  begin

    szGoodsName := Self.cxLookupComboBox2.Text; //商品名称
    if Length( szGoodsName ) <> 0 then
       szGoodsNameSQL := ' AND ' +  Self.tViewCol4.DataBinding.FieldName + '="' + szGoodsName + '" ';

    szSpecA := Self.cxLookupComboBox1.Text;   //材料规格
    if Length(szSpecA) <> 0 then
    begin
      Self.cxLookupComboBox7.Text := '';
      szSpecASQL := ' AND ' + Self.tViewCol6.DataBinding.FieldName + '="' + szSpecA + '"';
    end;

    szSpec := Self.cxLookupComboBox7.Text;    //规格
    if Length(szSpec) <> 0 then
    begin
      szSpecSQL := ' AND ' + Self.tViewCol6.DataBinding.FieldName + '="' + szSpec + '"';
      Self.cxLookupComboBox1.Text := '';
      szSpecASQL := '';
    end;
    {
    szEntryType := Self.cxComboBox4.Text;   条目类别
    if Length(szEntryType) <> 0 then
      szEntryTypeSQL := ' AND ' + Self.tViewCol17.DataBinding.FieldName + '="' + szEntryType + '"';
    }

    i := cxComboBox1.ItemIndex;
    if i >= 0 then
    begin
      if i = 3 then i := 4;
      szDataTypeSQL := ' AND  DataType=' + IntToStr(i);
    end;


    if Length(dwSignName) <> 0 then
    begin
      case i of
        0: szSignNameSQL := ' AND ' + Self.tv1Col6.DataBinding.FieldName + '="' + dwSignName + '"' ; //入库
        1: szSignNameSQL := ' AND ' + Self.tv1Col7.DataBinding.FieldName + '="' + dwSignName + '"' ;
        2: szSignNameSQL := ' AND ' + Self.tv1Col6.DataBinding.FieldName + '="' + dwSignName + '"' ;
        4: szSignNameSQL := ' AND ' + Self.tv1Col7.DataBinding.FieldName + '="' + dwSignName + '"' ;
      else
        szSignNameSQL := ' AND (' + Self.tv1Col6.DataBinding.FieldName + '="' + dwSignName + '" or ' + Self.tv1Col7.DataBinding.FieldName + '="' + dwSignName + '")' ;
      end;
    end;
    //入库对应发货、出库对应收货、退还对应发货（退货单位）
    sqltext := ' WHERE ' + dwTreeId + szGoodsNameSQL + szSpecASQL + szSpecSQL + szEntryTypeSQL + szSignNameSQL + szDataTypeSQL;
    Self.DetailedSearch(GetDetailedSQLText +  sqltext);

    //统计
    s := 'Select SUM(CountTotal) as t  from ' + g_Table_Company_StorageDetailed +
         ' WHERE ' + dwTreeId + szGoodsNameSQL + szSpecSQL + szEntryTypeSQL + szSignNameSQL + szDataTypeSQL;

    i := Self.cxComboBox1.ItemIndex;
    DD1 := 0;
    DD2 := 0;
    DD3 := 0;

    case i of
      0:
       begin
          with DM.Qry do
          begin
            Close;
            SQL.Text :=  s + ' AND DataType=0';
            Open;
            if RecordCount <> 0 then DD1 := FieldByName('t').AsCurrency;
          end;
       end;
      1:
       begin
         with DM.Qry do
          begin
            Close;
            SQL.Text :=  s +  ' AND DataType=1';
            Open;
            if RecordCount <> 0 then DD2 := FieldByName('t').AsCurrency;
          end;
       end;
      2:
       begin
         with DM.Qry do
         begin
          Close;
          SQL.Text :=  s + ' AND DataType=2';
          Open;
          if RecordCount <> 0 then DD3 := FieldByName('t').AsCurrency;
         end;
       end;
    else
      //统计
      with DM.Qry do
      begin
        Close;
        SQL.Text :=  s +  ' AND DataType=0';   //入库
        Open;
        if RecordCount <> 0 then DD1 := FieldByName('t').AsCurrency;

        Close;
        SQL.Text :=  s +  ' AND DataType=1';  //出库
        Open;
        if RecordCount <> 0 then DD2 := FieldByName('t').AsCurrency;

        Close;
        SQL.Text :=  s + ' AND DataType=2';   //退还
        Open;
        if RecordCount <> 0 then DD3 := FieldByName('t').AsCurrency;

        Close;
        SQL.Text :=  s + ' AND DataType=4';   //退货
        Open;
        if RecordCount <> 0 then DD4 := FieldByName('t').AsCurrency;

      end;

    end;

    Self.RzPanel11.Caption := CurrToStr(DD1);
    Self.RzPanel16.Caption := CurrToStr(DD2);
    Self.RzPanel18.Caption := CurrToStr(DD3);
    Self.RzPanel14.Caption := CurrToStr( DD1 - DD2 + DD3 ); //剩余库存
    Self.RzPanel20.Caption := CurrToStr( DD2 - DD3 );  //剩余退还
  end;

end;

function TfrmStorageBase.SearchMaster():Boolean;
var
  szSignNameSQL , szDataTypeSQL , sqltext : string;
  i : Integer;
begin
  i := cxComboBox1.ItemIndex;
  if i >= 0 then
  begin
    if i = 3 then i := 4;

    szDataTypeSQL := ' AND  DataType=' + IntToStr(i);
  end;



  if Length(dwSignName) <> 0 then
  begin
    case i of
      0: szSignNameSQL := ' AND ' + Self.tv1Col6.DataBinding.FieldName + '="' + dwSignName + '"' ; //入库
      1: szSignNameSQL := ' AND ' + Self.tv1Col7.DataBinding.FieldName + '="' + dwSignName + '"' ;
      2: szSignNameSQL := ' AND ' + Self.tv1Col6.DataBinding.FieldName + '="' + dwSignName + '"' ;
      4: szSignNameSQL := ' AND ' + Self.tv1Col7.DataBinding.FieldName + '="' + dwSignName + '"' ;
    else
      szSignNameSQL := ' AND (' + Self.tv1Col6.DataBinding.FieldName + '="' + dwSignName + '" or ' + Self.tv1Col7.DataBinding.FieldName + '="' + dwSignName + '")' ;
    end;
  end;

  sqltext := ' WHERE ' + dwTreeId + szSignNameSQL + szDataTypeSQL;
  Self.MasterSearch(GetMasterSQLText + sqltext);
end;

function TfrmStorageBase.GetSearchNumber(lpNumber : string):Boolean;
var
  s : string;
begin
  Result   := False;
  s := GetMasterSQLText + ' WHERE ' + dwTreeId +
                          ' AND '   + Self.tv1Col2.DataBinding.FieldName + ' LIKE "%' + lpNumber +'%"';
  MasterSearch(s);
end;

procedure TfrmStorageBase.tvSignDblClick(Sender: TObject);
var
  szSignName : Variant;

begin
  szSignName := Self.tvSignColumn2.EditValue;
  if szSignName <> null then
  begin
    dwSignName := VarToStr(szSignName);
    case Self.RzPageControl2.ActivePageIndex of
      0:
      begin
        SearchDetailed;
        SearchMaster;
      end;
      1: SearchContract;
    end;


  end;

end;

procedure TfrmStorageBase.UpDateIndex(lpNode : TTreeNode);
var
  s : string;
begin
  if lpNode <> nil then
  begin
    S := 'UPDATE ' + g_Table_Company_StorageTree + ' SET TreeIndex=''' + IntToStr(lpNode.Index) + ''' WHERE SignName="' + lpNode.Text + '"';
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      ExecSQL;
    end;
  end;
end;

procedure TfrmStorageBase.ReadData();
begin
  with DM do
  begin
    dwCompanySqlText := 'Select * from ' + g_Table_Company_StorageTree + ' Where PID=0' ;
    ADOQuery1.Close;
    ADOQuery1.SQL.Text := dwCompanySqlText;
    ADOQuery1.Open;
    Self.Company.Data := DataSetProvider1.Data;
    Self.Company.Open;
  end;
end;

procedure TfrmStorageBase.GridSetThreadExecute(Sender: TObject;
  Thread: TBMDExecuteThread; var Data: Pointer);
var
  i : Integer;
begin
  inherited;
end;

procedure TfrmStorageBase.btnGridSetClick(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;
begin
  inherited;
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_DirBuildStorage;
    Child.ShowModal;
    SetcxGrid(Self.tv1,0, g_DirBuildStorage);
  finally
    Child.Free;
  end;
end;


procedure TfrmStorageBase.CompanyAfterInsert(DataSet: TDataSet);
var
  parent : Integer;
begin
  inherited;
  with DataSet do
  begin
    parent := DM.getLastId( g_Table_Company_StorageTree );
    FieldByName('parent').Value := Parent;
    FieldByName('Code').Value   := GetRowCode( g_Table_Company_StorageTree ,'Code');
  end;
end;

procedure TfrmStorageBase.ContractAfterClose(DataSet: TDataSet);
begin
  inherited;
  RzToolButton17.Enabled := False;
  RzToolButton18.Enabled := False;
  RzToolButton19.Enabled := False;
  RzToolButton14.Enabled := False;
  RzToolButton22.Enabled := False;
  RzToolButton21.Enabled := False;
  RzToolButton15.Enabled := False;
  MenuItem6.Enabled := False;
  MenuItem12.Enabled:= False;
  MenuItem13.Enabled:= False;
  MenuItem20.Enabled:= False;
  MenuItem22.Enabled:= False;
  MenuItem23.Enabled:= False;
end;

procedure TfrmStorageBase.ContractAfterEdit(DataSet: TDataSet);
begin
  inherited;
  RzToolButton22.Enabled := True;
end;

procedure TfrmStorageBase.ContractAfterOpen(DataSet: TDataSet);
begin
  inherited;

  RzToolButton17.Enabled := True;
  RzToolButton18.Enabled := True;
  RzToolButton19.Enabled := True;
  RzToolButton14.Enabled := True;
  RzToolButton21.Enabled := True;
  RzToolButton15.Enabled := True;
  MenuItem6.Enabled := True;
  MenuItem12.Enabled:= True;
  MenuItem13.Enabled:= True;
  MenuItem20.Enabled:= True;
  MenuItem22.Enabled:= True;
  MenuItem23.Enabled:= True;

end;

procedure TfrmStorageBase.ContractAfterScroll(DataSet: TDataSet);
var
  szColName ,  szNoId : string;
begin
  inherited;
  with DataSet do
  begin
    //(Self.TableView.Controller.SelectedRowCount = 1)
    if (not IsEmpty) and
       (Self.TableView.ViewData.RowCount > 0) then
    begin
      szColName := 'NoId';
      szNoId := DataSet.FieldByName(szColName).Value;
      dwContractDetailedSQLText := 'Select * from '+ g_Table_Company_Storage_ContractDetailed +' WHERE ' + szColName + '="' + szNoId +'"';
      dwADO.OpenSQL(ContractDetailedDataSet,dwContractDetailedSQLText);
    end else
    begin
      ContractDetailedDataSet.Close;
    end;

  end;
end;

procedure TfrmStorageBase.ContractDetailedDataSetAfterClose(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton1.Enabled  := False;
  Self.RzToolButton24.Enabled := False;
  Self.RzToolButton23.Enabled := False;
end;

procedure TfrmStorageBase.ContractDetailedDataSetAfterOpen(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton1.Enabled := True;
  Self.RzToolButton24.Enabled:= True;
  Self.RzToolButton23.Enabled := True;
end;

procedure TfrmStorageBase.tv1Col1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmStorageBase.tv1ColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tv1,1, g_DirBuildStorage);
end;

procedure TfrmStorageBase.tv1DblClick(Sender: TObject);
var
  i : Integer;
  s : Variant;
  szIsContract : Boolean;
begin
  inherited;
  with Self.Master do
  begin
    if State <> dsInactive then
    begin
      i := FieldByName('DataType').Value;
      s := FieldByName(tv1Col2.DataBinding.FieldName).Value;
      szIsContract := FieldByName(tv1Column7.DataBinding.FieldName).AsBoolean;
      case i of
        0:
        begin
          //入库
          if not szIsContract then
          begin
            Application.CreateForm(TfrmFastEnterStorage,frmFastEnterStorage);
            try
              frmFastEnterStorage.dwIsContrct  := szIsContract;
              frmFastEnterStorage.dwIsEdit     := True;
              frmFastEnterStorage.dwDataType   := i;
              frmFastEnterStorage.dwBillNumber := s;
              frmFastEnterStorage.ShowModal;
            finally
              frmFastEnterStorage.Free;
            end;
          end else
          begin
            //合同界面
            Application.CreateForm(TfastContrctEnterStorage,fastContrctEnterStorage);
            try
              fastContrctEnterStorage.dwIsContrct  := szIsContract;
              fastContrctEnterStorage.dwIsEdit     := True;
              fastContrctEnterStorage.dwDataType   := i;
              fastContrctEnterStorage.dwBillNumber := s;
              fastContrctEnterStorage.ShowModal;
            finally
              fastContrctEnterStorage.Free;
            end;
          end;

        end;
        1:
        begin
          //出库
          if not szIsContract then
          begin
            Application.CreateForm(TfrmFastGooutStorage,frmFastGooutStorage);
            try
              frmFastGooutStorage.dwIsContrct := szIsContract;
              frmFastGooutStorage.dwIsEdit    := True;
              frmFastGooutStorage.dwIsContrct := False;
              frmFastGooutStorage.dwBillNumber:= s;
              frmFastGooutStorage.dwDataType  := i;
              frmFastGooutStorage.ShowModal;
            finally
              frmFastGooutStorage.Free;
            end;
          end else
          begin
            //合同界面
            Application.CreateForm(TfastContrctGooutStorage,fastContrctGooutStorage);
            try
              fastContrctGooutStorage.dwIsContrct  := szIsContract;
              fastContrctGooutStorage.dwIsEdit     := True;
              fastContrctGooutStorage.dwDataType   := i;
              fastContrctGooutStorage.dwBillNumber := s;
              fastContrctGooutStorage.ShowModal;
            finally
              fastContrctGooutStorage.Free;
            end;
          end;

        end;
        2:
        begin
          //退还
            Application.CreateForm(TfrmFastGiveBack,frmFastGiveBack);
            try
              frmFastGiveBack.dwIsEdit    := true;
              frmFastGiveBack.dwIsContrct := False;
              frmFastGiveBack.dwBillNumber:= s;
              frmFastGiveBack.dwDataType  := i;
              frmFastGiveBack.ShowModal;
            finally
              frmFastGiveBack.Free;
            end;
        end;
        3:
        begin
          //编辑
        end;
        4:
        begin
          //退货
          Application.CreateForm(TfrmFastReturnGoods,frmFastReturnGoods);
          try
            frmFastReturnGoods.dwIsEdit    := true;
            frmFastReturnGoods.dwIsContrct := False;
            frmFastReturnGoods.dwBillNumber:= s;
            frmFastReturnGoods.dwDataType  := i;
            frmFastReturnGoods.ShowModal;
          finally
            frmFastReturnGoods.Free;
          end;
        end;
      end;
    end;
  end;
end;

procedure TfrmStorageBase.tv1Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Master,AAllow);
end;

procedure TfrmStorageBase.GetStorageDetailed(lpId : string);
begin
  inherited;
  dwDetailedSQLtext := 'Select * from ' + g_Table_Company_StorageDetailed +
                     ' WHERE ' + dwTreeId +
                       ' AND ' + Self.tViewCol3.DataBinding.FieldName + ' ="' + lpId +'"';
  dwADO.OpenSQL(Self.Detailed,dwDetailedSQLtext);
end;

procedure TfrmStorageBase.tv1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  szSignName : string;
begin
  inherited;
  if Key = 46 then Self.RzToolButton85.Click;

  if (Key = 40) or (Key = 38) then
  begin
    if Self.tv1.Controller.SelectedRowCount <> 0 then
    begin
      szSignName := VarToStr( Self.tv1Col2.EditValue );
      GetStorageDetailed(szSignName);
    end;
  end;
end;

procedure TfrmStorageBase.tv1MouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  szSignName : string;
begin
  if Self.tv1.Controller.SelectedRowCount <> 0 then
  begin
    szSignName := VarToStr( Self.tv1Col2.EditValue );
    GetStorageDetailed(szSignName);
  end;
end;

procedure TfrmStorageBase.tv1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;

  Value := ARecord.Values[Self.tv1Column1.Index];
  if Value <> null then
  begin
    if Value = False then
      AStyle := DM.cxStyle222;
  end;
  Value := ARecord.Values[Self.tv1Column7.Index];
  if Value <> null then
  begin
    if Value then
      AStyle := DM.cxStyle2;
  end;
end;

procedure TfrmStorageBase.tv1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  t : Currency;
begin
  inherited;
  try
    if AValue <> null then
    begin
      t := StrToCurrDef( VarToStr(AValue) , 0 );
      Self.tv1Column4.Summary.FooterFormat := MoneyConvert(t);
    end;

  except

  end;
end;

procedure TfrmStorageBase.cxButton1Click(Sender: TObject);
var
  szColName , sqltext : string;
begin
  inherited;
  if Self.cxRadioButton1.Checked then
  begin
    GetSearchNumber( Self.cxTextEdit1.Text );
  end else
  if Self.cxRadioButton2.Checked then
  begin
    szColName   := Self.tv1Col3.DataBinding.FieldName;
    sqltext := GetMasterSQLText + ' WHERE '+ dwTreeId + ' AND ' + szColName +' between :t1 and :t2';
    dwADO.ADOSearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text, sqltext ,Master);
  end;

end;

procedure TfrmStorageBase.cxButton2Click(Sender: TObject);
var
  szColName , sqltext : string;
begin
  inherited;
  if Self.cxRadioButton5.Checked then
  begin
    SearchDetailed;
  end else
  if Self.cxRadioButton4.Checked then
  begin
    szColName   := Self.tv1Col3.DataBinding.FieldName;
    sqltext := GetDetailedSQLText + ' WHERE '+ dwTreeId + ' AND ' + szColName +' between :t1 and :t2';
    dwADO.ADOSearchDateRange(Self.cxDateEdit3.Text,Self.cxDateEdit4.Text, sqltext ,Self.Detailed);
  end;

end;

function TfrmStorageBase.SearchContract():Boolean;
var
  szColName , s : string;
  sqltext : string;
  szContractName : string;
  szSignName : string;
begin
  s := Self.cxTextEdit2.Text;
  if Length(s) <> 0 then szContractName :=' AND ' + Self.TableView4.DataBinding.FieldName + ' LIKE "%' + s +'%"';

  s := dwSignName;
  if Length(s) <> 0 then szSignName := ' AND ' + Self.TableView3.DataBinding.FieldName + '="' + s + '"';


  sqltext := 'Select * from ' + g_Table_Company_Storage_ContractList +
                    ' WHERE ' + dwTreeId +
                                szContractName +
                                szSignName;
  dwADO.OpenSQL(Self.Contract,sqltext);
end;

procedure TfrmStorageBase.cxButton3Click(Sender: TObject);
var
  szColName , s : string;
  sqltext : string;
begin
  inherited;
  s := 'Select * from ' + g_Table_Company_Storage_ContractList;
  if Self.cxRadioButton3.Checked then
  begin
    //按合同查询
    SearchContract;
  end else
  if Self.cxRadioButton6.Checked then
  begin
    //按日期查询
    szColName   := Self.TableView2.DataBinding.FieldName;
    sqltext :=  s + ' WHERE '+ dwTreeId + ' AND ' + szColName +' between :t1 and :t2';
    dwADO.ADOSearchDateRange(Self.cxDateEdit6.Text,Self.cxDateEdit7.Text, sqltext ,Self.Contract);
  end;

end;

procedure TfrmStorageBase.cxComboBox1PropertiesCloseUp(Sender: TObject);
var
  i : Integer;
begin
  inherited;
  i := Self.cxComboBox1.ItemIndex;
  case i of
    0:  Self.tViewColumn8.Caption := '发货单位';
    1:  Self.tViewColumn8.Caption := '发货单位';
    2:  Self.tViewColumn8.Caption := '退货单位';
    3:  Self.tViewColumn8.Caption := '退货单位';
  end;
  SearchDetailed;
  SearchMaster;
end;

procedure TfrmStorageBase.cxComboBox2PropertiesCloseUp(Sender: TObject);
var
  s : string;
  szColName , sqltext : string;
begin
  inherited;
  s := Self.cxComboBox2.Text;
  if Length(s) <> 0  then
  begin
    szColName   := Self.tv1Col4.DataBinding.FieldName;
    sqltext := GetMasterSQLText + ' WHERE '+ dwTreeId + ' AND ' + szColName + '="' + s + '"';
    MasterSearch(sqltext);
  end;
end;

procedure TfrmStorageBase.cxComboBox4PropertiesCloseUp(Sender: TObject);
var
  s : string;
begin
  inherited;
  s := Self.cxComboBox4.Text;
  if Length(s) <> 0 then
  begin
     SearchDetailed;
  end;
end;

procedure TfrmStorageBase.cxDateEdit5PropertiesCloseUp(Sender: TObject);
var
  szColName , sqltext : string;
  s : string;

begin
  inherited;
  s := Self.cxDateEdit5.Text;
  szColName   := Self.tv1Col3.DataBinding.FieldName;
  sqltext := GetMasterSQLText + ' WHERE '+ dwTreeId + ' AND ' + szColName + '=#' + s + '#';
  MasterSearch(sqltext);
end;

procedure TfrmStorageBase.cxLookupComboBox1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  s : string;
begin
  inherited;
  if Key = 13 then
  begin
    s := Self.cxLookupComboBox1.Text;
    if Length(s) <> 0  then
    begin
      SearchDetailed;
    end;
  end;
end;

procedure TfrmStorageBase.cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
var
  s : string;
  szColName , sqltext : string;
  szTreeId : string;
begin
  inherited;
  s := Self.cxLookupComboBox1.Text;
  if Length(s) <> 0  then
  begin
    SearchDetailed;
  end;
end;

procedure TfrmStorageBase.cxLookupComboBox2PropertiesCloseUp(Sender: TObject);
var
  s : string;
  szColName , sqltext : string;
  szTreeId : string;
begin
  inherited;
  s := Self.cxLookupComboBox2.Text;
  if Length(s) <> 0  then
  begin
    dwADO.OpenSQL(Self.dsSpec,'Select * from ' + g_Table_MakingsList + ' Where MakingCaption="' + s + '"');
    SearchDetailed;
  end;
end;

procedure TfrmStorageBase.cxLookupComboBox7PropertiesChange(Sender: TObject);
begin
  inherited;
  SearchDetailed;
end;

procedure TfrmStorageBase.cxLookupComboBox7PropertiesCloseUp(Sender: TObject);
begin
  inherited;
  SearchDetailed;
end;

procedure TfrmStorageBase.cxTextEdit1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    GetSearchNumber( Self.cxTextEdit1.Text );
  end;
end;

procedure TfrmStorageBase.FormActivate(Sender: TObject);
begin
  inherited;
  Self.RzPageControl2.ActivePage := Self.Tab1;
  Self.tv.Items.Clear;
  dwIsOpen := False;
end;

procedure TfrmStorageBase.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmStorageBase.FormCreate(Sender: TObject);
begin
  inherited;
  s := GetCurrentTime;
end;

procedure TfrmStorageBase.MasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton3.Enabled  := False;
  Self.RzToolButton6.Enabled  := False;
  Self.RzToolButton85.Enabled := False;
  Self.btnGridSet.Enabled     := False;
  Self.RzToolButton87.Enabled := False;
  Self.RzToolButton8.Enabled  := False;
  Self.cxComboBox1.Enabled    := False;
  Self.RzToolButton4.Enabled  := False;

  Self.N12.Enabled := False;
  Self.N8.Enabled  := False;
  Self.N9.Enabled  := False;
  Self.N7.Enabled  := False;
  Self.N17.Enabled := False;
  Self.N18.Enabled := False;

  Self.Grid4.Enabled := False;
end;

procedure TfrmStorageBase.MasterAfterEdit(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton9.Enabled := True;
end;

procedure TfrmStorageBase.MasterAfterInsert(DataSet: TDataSet);
var
  szConName : string;
  Prefix : string;
  Suffix : string;
  szCode : string;
  szSignName : Variant;

begin
  inherited;
//  Prefix := 'ST-';
  Suffix := '000000';
  szConName := Self.tv1Col2.DataBinding.FieldName;
  szCode := Prefix + GetRowCode(g_Table_Company_StorageBillList,szConName,Suffix,970001);
  Self.tv1Col2.EditValue := szCode;
  Self.tv1Col8.EditValue := Self.tvCompanyColumn2.EditValue;; //名称
  Self.tv1Col3.EditValue := Date;
  Self.tv1Column1.EditValue := True;

  with DataSet do
  begin
    FieldByName('TreeId').Value := dwNodeId;
    FieldByName('DataType').Value := dwDataType;
    FieldByName('Code').Value   := dwTreeCode;
    case dwDataType of
      0:    Self.tv1Col7.EditValue := g_NodeText; //收货
      1..2: Self.tv1Col6.EditValue := g_NodeText; //发货
    end;
  end;

end;

procedure TfrmStorageBase.MasterAfterOpen(DataSet: TDataSet);
begin
  inherited;
  Self.RzToolButton3.Enabled  := True;
  Self.RzToolButton6.Enabled  := True;
  Self.RzToolButton85.Enabled := True;
  Self.btnGridSet.Enabled     := True;
  Self.RzToolButton87.Enabled := True;
  Self.RzToolButton8.Enabled  := True;
  Self.cxComboBox1.Enabled    := True;
  Self.RzToolButton4.Enabled  := True;

  Self.N12.Enabled := True;
  Self.N8.Enabled  := True;
  Self.N9.Enabled  := True;
  Self.N7.Enabled  := True;
  Self.N17.Enabled := True;
  Self.N18.Enabled := True;

  Self.Grid4.Enabled := True;
end;

procedure TfrmStorageBase.N11Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton1.Click;
end;

procedure TfrmStorageBase.N13Click(Sender: TObject);
begin
  inherited;
  Self.N3.Click;
end;

procedure TfrmStorageBase.N14Click(Sender: TObject);
begin
  inherited;
  Self.N4.Click;
end;

procedure TfrmStorageBase.N15Click(Sender: TObject);
begin
  inherited;
  Self.N5.Click;
end;

procedure SaveData(DataClient : TClientDataSet);
var  ErrorCount : Integer;
begin
  with DataClient do
  begin
    if State <> dsInactive then
    begin
      if State in [DsEdit, DSInsert] then Post;

      if ChangeCount > 0 then
      begin
        DM.DataSetProvider1.ApplyUpdates(Delta,0,ErrorCount);
        MergeChangeLog;

      end;

    end;
  end;
end;

procedure TfrmStorageBase.FunFrozen(lpValue : Boolean);
var
  i : Integer;
  szNumber , s , str : string;
begin

    with Self.tv1 do
    begin
      //获取主表ID
      {
      for I := Controller.SelectedRowCount-1 downto 0 do
      begin
        szNumber := VarToStr(Controller.SelectedRows[i].Values[ tv1Col2.Index ] );
        if Length(str) = 0 then
          str := szNumber
        else
          str := str + '","' + szNumber;
      end;
      }
      //查询明细
      {
      Self.Detailed.Close;
      if Length(str) <> 0 then
      begin
        s := 'Select * from ' +  g_Table_Company_StorageDetailed +
             ' where '+ Self.tViewCol3.DataBinding.FieldName +
             ' in("' + str + '")';
        dwADO.OpenSQL(Self.Detailed,s);
      end;
      }

    end;
  //更新主从表状态
//  Self.tView.Controller.SelectAllRecords;
//保存明细
  if Self.tv1.Controller.SelectedRowCount = 1 then
  begin
    SetGridStuat(Self.tv1,Self.tv1Column1.DataBinding.FieldName,lpValue);
    Self.tView.Controller.SelectAll;
    Self.Grid.SetFocus;
    SetGridStuat(Self.tView,Self.tViewCol2.DataBinding.FieldName,lpValue);
    if Application.MessageBox( '主从记录已更新完毕现在是否要保存？', '冻结记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin
      Self.RzToolButton9.Click;
    end;
  end else
  begin
    Application.MessageBox( '一次只能选择一条记录冻结或解冻？', '冻结记录？', MB_OKCANCEL + MB_ICONWARNING)
  end;
end;

procedure TfrmStorageBase.N17Click(Sender: TObject);
var
  I , j : Integer;
  szNumber , str , s : string;

begin
  inherited;
  //冻结
  if Application.MessageBox( '此冻结会连同明细一同冻结，是否要继续？', '冻结记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin
    FunFrozen(False);
  end;
end;

procedure TfrmStorageBase.N18Click(Sender: TObject);
begin
  inherited;
  //解冻
  if Application.MessageBox( '此解冻会连同明细一同解冻，是否要继续？', '解冻记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin
    FunFrozen(True);
  end;
end;

procedure TfrmStorageBase.N1Click(Sender: TObject);
begin
  inherited;
  case Self.RzPageControl2.ActivePageIndex of
    0: DM.BasePrinterLink1.Component := Self.Grid1;
    1: DM.BasePrinterLink1.Component := Self.cxGrid2;
  end;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmStorageBase.N20Click(Sender: TObject);
begin
  inherited;
  dwADO.ADOSaveData(Self.Detailed,dwDetailedSQLtext);
end;

procedure TfrmStorageBase.N22Click(Sender: TObject);
begin
  inherited;
  Self.N21.Click;
end;

procedure TfrmStorageBase.N2Click(Sender: TObject);
begin
  inherited;
  case Self.RzPageControl2.ActivePageIndex of
    0: CxGridToExcel(Self.Grid1,'单据明细表');
    1: CxGridToExcel(Self.cxGrid2,'合同明细表');
  end;
end;

procedure TfrmStorageBase.N5Click(Sender: TObject);
var
  Child : TfrmPostStorage;
begin
  if Sender = Self.RzToolButton6 then
  begin
    if Self.tv1Column1.EditValue = False then
    begin
      ShowMessage('冻结状态禁止编辑!');
      Exit;
    end;

  end;
  dwIsOpen := True;
  Child := TfrmPostStorage.Create(Application);
  try
    Child.BillNumber.DataBinding.DataSource := Self.DataMaster;
    Child.BillNumber.DataBinding.DataField  := Self.tv1Col2.DataBinding.FieldName;

    Child.cxDBComboBox1.DataBinding.DataSource := Self.DataMaster;   //票据类型
    Child.cxDBComboBox1.DataBinding.DataField  := Self.tv1Col4.DataBinding.FieldName;

    Child.cxDBComboBox2.DataBinding.DataSource := Self.DataMaster;   //单据状态
    Child.cxDBComboBox2.DataBinding.DataField  := Self.tv1Col9.DataBinding.FieldName;

    Child.cxDBLookupComboBox1.DataBinding.DataSource := Self.DataMaster;   //发货单位
    Child.cxDBLookupComboBox1.DataBinding.DataField  := Self.tv1Col6.DataBinding.FieldName;

    Child.cxDBLookupComboBox3.DataBinding.DataSource := Self.DataMaster;   //收货单位
    Child.cxDBLookupComboBox3.DataBinding.DataField  := Self.tv1Col7.DataBinding.FieldName;

    Child.cxDBTextEdit1.DataBinding.DataSource := Self.DataMaster;   //名称
    Child.cxDBTextEdit1.DataBinding.DataField  := Self.tv1Col8.DataBinding.FieldName;

    Child.cxDBComboBox2.DataBinding.DataSource := Self.DataMaster;   //单据状态
    Child.cxDBComboBox2.DataBinding.DataField  := Self.tv1Col9.DataBinding.FieldName;

    Child.cxDBTextEdit5.DataBinding.DataSource := Self.DataMaster;   //经手人
    Child.cxDBTextEdit5.DataBinding.DataField  := Self.tv1Col11.DataBinding.FieldName;

    Child.cxDBTextEdit4.DataBinding.DataSource := Self.DataMaster;   //发货人
    Child.cxDBTextEdit4.DataBinding.DataField  := Self.tv1Col15.DataBinding.FieldName;

    Child.cxDBTextEdit6.DataBinding.DataSource := Self.DataMaster;   //审核签字
    Child.cxDBTextEdit6.DataBinding.DataField  := Self.tv1Col12.DataBinding.FieldName;

    Child.cxDBTextEdit3.DataBinding.DataSource := Self.DataMaster;   //签收人
    Child.cxDBTextEdit3.DataBinding.DataField  := Self.tv1Col10.DataBinding.FieldName;

    Child.cxDBDateEdit1.DataBinding.DataSource := Self.DataMaster;   //开票日期
    Child.cxDBDateEdit1.DataBinding.DataField  := Self.tv1Col3.DataBinding.FieldName;

    Child.cxDBDateEdit2.DataBinding.DataSource := Self.DataMaster;   //签收日期
    Child.cxDBDateEdit2.DataBinding.DataField  := Self.tv1Col13.DataBinding.FieldName;

    Child.cxDBComboBox3.DataBinding.DataSource := Self.DataMaster;    //票据状态
    Child.cxDBComboBox3.DataBinding.DataField  := Self.tv1Column3.DataBinding.FieldName;

    Child.cxDBTextEdit7.DataBinding.DataSource := Self.DataMaster;    //大写金额
    Child.cxDBTextEdit7.DataBinding.DataField  := Self.tv1Column4.DataBinding.FieldName;

    Child.cxDBCurrencyEdit1.DataBinding.DataSource := Self.DataMaster;  //小写金额
    Child.cxDBCurrencyEdit1.DataBinding.DataField  := Self.tv1Column2.DataBinding.FieldName;

    Child.dwTableName := g_Table_Company_StorageDetailed;
    Child.dwNodeId    := dwNodeId;  //树目录
    Child.dwTreeCode  := dwTreeCode; //合同代码

    if Sender = N3 then  //入库
    begin
      dwDataType := 0;
      dwADO.ADOAppend(Master);
    end else
    if Sender = N5 then  //退还
    begin
      dwDataType := 2;
      dwADO.ADOAppend(Master);
    end else
    if Sender = Self.RzToolButton6 then
    begin
      Master.Edit;
      dwDataType := 3;
    end else
    if Sender = N4 then  //出库
    begin
      dwDataType := 1;
      dwADO.ADOAppend(Master);
    end else
    if Sender = N21 then
    begin
      dwDataType := 4;  //退货
      dwADO.ADOAppend(Master);
    end;

    Child.Master := Self.Master;
    Child.dwMasterSQL := dwSQLtext;
    Child.dwDataType  := dwDataType;

    //with Self.Master do Child.dwAssortment := FieldByName('DataType').AsInteger;
    Child.ShowModal;
  //  GetDetailed( VarToStr( Self.tv1Col2.EditValue ) );
    dwIsOpen := False;;
  finally
    Child.Free;
  end;
end;

procedure TfrmStorageBase.N6Click(Sender: TObject);
begin
  inherited;
  Self.RzBut4.Click;
end;

procedure TfrmStorageBase.N7Click(Sender: TObject);
var
  szDir : string;
  szNumbers : string;

begin
  inherited;
  szNumbers := VarToStr( Self.tv1Col2.EditValue );
  szDir := Concat(g_Resources, '\'+ g_DirBuildStorage);
  CreateOpenDir(Handle,Concat(szDir,'\' + szNumbers),True);
end;

procedure TfrmStorageBase.N8Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton6.Click;
end;

procedure TfrmStorageBase.N9Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton85.Click;
end;

procedure TfrmStorageBase.TableViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Self.Contract,AAllow);
end;

procedure TfrmStorageBase.TableViewEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    RzToolButton14.Click;
  end;
end;

procedure TfrmStorageBase.TableViewStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  {
  Value :=  ARecord.Values[Self.TableView9.Index];
  if Value <> null then
  begin
    szIndex := StrToIntDef( VarToStr( Value ) , 0);
    case szIndex of
      0: Self.TableView9.EditValue := '入库'; //AStyle := DM.cxStyle104;
      1: Self.TableView9.EditValue := '出库';//AStyle := DM.cxStyle105;
    end;
  end;
  }
  Value := ARecord.Values[Self.TableView7.Index];
  if Value <> null then
  begin
    if Value = False then
    begin
      AStyle := DM.cxStyle222;
    end;
  end;
end;

procedure TfrmStorageBase.Timer1Timer(Sender: TObject);
var
  i : Integer;
begin
  inherited;
  Timer1.Enabled := False;

  g_TreeView := TNewUtilsTree.Create(Self.tv,
                                         DM.ADOconn,
                                     g_Table_Company_StorageTree,
                                     '公司仓库');
  ReadData;
  g_TreeCompany := TNewUtilsTree.Create(Self.TreeView1,DM.ADOconn,g_Table_CompanyTree,'往来单位');
  g_TreeCompany.GetTreeTable(0);

  dwDetailedTable := 'CompanyDetailed';
  dwNodeId := 0;
  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
  Self.cxDateEdit3.Date := Date;
  Self.cxDateEdit4.Date := Date;
  Self.cxDateEdit5.Date := Date;
  Self.cxDateEdit6.Date := Date;
  Self.cxDateEdit7.Date := Date;

  SetcxGrid(Self.tv1,0, g_DirBuildStorage);

  for I := 0 to Self.tView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(Self.tView.Columns[i],0, dwDetailedTable);
  end;

  dwContract := 'Contract';
  for I := 0 to Self.tContractView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(Self.tContractView.Columns[i],0, dwContract);
  end;
//  入库 - 出库 + 退还总数 = 剩余库存
//  出库总数 -    退还总数 = 剩余退还
  //****************************************************************************
  dwADO.OpenSQL(Self.dsMaking,'Select * from ' + g_Table_MakingsList);
  dwADO.OpenSQL(Self.dsSpec1 ,'Select * from unit Where chType=7');
end;

procedure TfrmStorageBase.TreeAddName(Tv : TTreeView); //树增加名称
var
  Node , ChildNode : TTreeNode;
  szCode : string;

begin
  with Tv do
  begin
    Node := Selected;
    if Assigned(Node) then
    begin

      if Node.Level = 0 then
      begin
        MessageBox(handle, PChar('不可以在顶级节点新建?'),'提示',
               MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2)
      end else
      begin
        szCode := 'FS-' + GetRowCode( g_Table_Company_StorageTree ,'Code','100007',170000);
        g_TreeView.AddChildNode(szCode,'',Date,m_OutText);
      end;

    end;

  end;

end;

procedure TfrmStorageBase.TreeView1Click(Sender: TObject);
var
  Node : TTreeNode;
  I: Integer;
  szData : PNodeData;
  szCodeId : Integer;

begin
  inherited;
  if not Master.Active then
  begin
    MessageBox(handle, PChar('请选择仓库?'),'提示',
             MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2)
  end else
  begin
    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin
      if Node.Level > 0 then
      begin
        szData := PNodeData(Node.Data);
        if Assigned(szData) then
        begin
          szCodeId := szData^.Index;

          with Self.ADOCompany do
          begin
            Close;
            SQL.Clear;
            SQL.Text := 'Select * from ' + g_Table_CompanyManage + ' WHERE ' + 'Sign_Id' + '=' + IntToStr(szCodeId) ;
            Open;
          end;

        end;

      end;

    end;
  end;

end;

procedure TfrmStorageBase.RzBut1Click(Sender: TObject);
var
  Node : TTreeNode;
  szCodeList : string;
  szColName  : string;
  szTableList : array[0..1] of TTableList;

begin
  inherited;
  if sender  = Self.RzBut1 then
  begin
    //添加
    TreeAddName(Self.tv);
  end else
  if Sender = Self.RzBut2 then
  begin
    //编辑
    Node := Self.tv.Selected;
    if Node.Level <= 1 then
    begin
      MessageBox(handle, PChar('不可以编辑顶级节点?'),'提示',
             MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2)
    end else
    begin
      Node.EditText;
    end;

  end;

end;

procedure TfrmStorageBase.RzBut3Click(Sender: TObject);
var
  Node : TTreeNode;
  szCodeList : string;
  szColName  : string;
  szTableList : array[0..4] of TTableList;
  szData : PNodeData;
  szParent : Integer;

begin
  inherited;
//删除
  Node := Self.Tv.Selected;
  if Assigned(Node)  then
  begin
    if Node.Level <= 1 then
    begin
      MessageBox(handle, '顶级节点不可以删除？', '提示', MB_ICONQUESTION + MB_YESNO)
    end else
    begin
      if MessageBox(handle, '是否删除节点？', '提示', MB_ICONQUESTION + MB_YESNO) = IDYES then
      begin
      //  Self.cxShellTreeView1.Root.CustomPath := dwCurrentPath;
        if Assigned(Node) then
        begin
          szData := PNodeData(Node.Data);
          szParent := szData.Index;
        end;
      //  GetTreeIndexList(Node,szCodeList);
        DM.getTreeCode(g_Table_Company_StorageTree, szParent,szCodeList); //获取要删除的id表
        szTableList[0].ATableName := g_Table_Company_StorageBillList;
        szTableList[0].ADirectory := '';

        szTableList[1].ATableName := g_Table_Company_StorageDetailed;
        szTableList[1].ADirectory := '';

        szTableList[2].ATableName := g_Table_Company_Storage_ContractList;
        szTableList[2].ADirectory := '';

        szTableList[3].ATableName := g_Table_Company_Storage_ContractDetailed;
        szTableList[3].ADirectory := '';

        szTableList[4].ATableName := g_Table_Company_Storage_GroupUnitPrice;
        szTableList[4].ADirectory := '';

        DeleteTableFile(szTableList,szCodeList);
        DM.InDeleteData(g_Table_Company_StorageTree,szCodeList);
        g_TreeView.DeleteTree(Node);

      end;

    end;

  end;
end;

procedure TfrmStorageBase.RzBut4Click(Sender: TObject);
begin
  inherited;
  GetMaintainInfo;
  ShowMessage('刷新完成!');
end;

procedure TfrmStorageBase.RzPanel10Resize(Sender: TObject);
var
  szWidth : Single;

  DD1 , DD2 , DD3 , DD4: Integer;
begin
  inherited;
  szWidth := (Self.RzPanel10.Width - Self.RzPanel13.Width * 5) / 5 ;

  Self.RzPanel11.Width := Round(szWidth);
  Self.RzPanel16.Width := Round(szWidth);
  Self.RzPanel18.Width := Round(szWidth);
  Self.RzPanel14.Width := Round(szWidth);
  Self.RzPanel20.Width := Round(szWidth);

end;

procedure TfrmStorageBase.RzToolButton10Click(Sender: TObject);
begin
  dwADO.ADOAppend(Self.Company);
  Self.tvCompanyColumn2.FocusWithSelection;
end;

procedure TfrmStorageBase.RzToolButton11Click(Sender: TObject);
begin
  inherited;
  dwADO.ADOIsEdit(Self.Company);
end;

procedure TfrmStorageBase.RzToolButton12Click(Sender: TObject);
var
  Node : TTreeNode;
  szCodeList : string;
  szColName  : string;
  szTableList: array[0..4] of TTableList;
  szCode : string;
  szParent: Integer;
begin
  inherited;
  //删除
  if MessageBox(handle, '是否删除公司节点？', '提示', MB_ICONQUESTION
     + MB_YESNO) = IDYES then
  begin
    with Company do
    begin
      if not IsEmpty then
      begin
        szCode   := FieldByName('Code').AsString;
        szParent := FieldByName('parent').AsInteger;
      end;

    end;
    DM.getTreeCode(g_Table_Company_StorageTree, szParent,szCodeList); //获取要删除的id表

    szTableList[0].ATableName := g_Table_Company_StorageBillList;
    szTableList[0].ADirectory := '';

    szTableList[1].ATableName := g_Table_Company_StorageDetailed;
    szTableList[1].ADirectory := '';

    szTableList[2].ATableName := g_Table_Company_Storage_ContractList;
    szTableList[2].ADirectory := '';

    szTableList[3].ATableName := g_Table_Company_Storage_ContractDetailed;
    szTableList[3].ADirectory := '';

    szTableList[4].ATableName := g_Table_Company_Storage_GroupUnitPrice;
    szTableList[4].ADirectory := '';

    DeleteTableFile(szTableList,szCodeList);

    DM.InDeleteData(g_Table_Company_StorageTree,szCodeList);

    Timer1.Enabled := True;
  end;
end;

procedure TfrmStorageBase.RzToolButton13Click(Sender: TObject);
begin
  inherited;
  dwADO.ADOSaveData(Self.Company,dwCompanySqlText);
end;

procedure TfrmStorageBase.RzToolButton14Click(Sender: TObject);
var
  szRowIndex : Integer;
  I: Integer;
  szCount : Integer;
  szRowsCount : string;
  s , Id , str , err : string;
  szValue : Variant;
  szDir , dir : string;

begin
  inherited;
  szCount := TableView.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;

  if szCount >= 0 then
  begin
    if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin

      with TableView do
      begin
        if DataController.IsEditing then DataController.UpdateData;
        for I := szCount -1 downto 0 do
        begin
          Id := VarToStr(  Controller.SelectedRows[i].Values[ TableView8.Index ] );
          if Length(id) <> 0 then
          begin
            szDir := Concat(g_Resources, '\'+ g_DirCompanyStorageContractList);
            if Length(szDir) <> 0 then
            begin
              dir := ConcatEnclosure(szDir, id);
              if DirectoryExists(dir) then
              begin
                DeleteDirectory(dir);
              end;
            end;
            if Length(str) = 0 then
              str := Id
            else
              str := str + '","' + Id;

          end;

        end;

      end;

      if str <> '' then
      begin
        DM.ADOconn.BeginTrans; //开始事务
        try
          with DM.Qry do
          begin

            Close;
            SQL.Clear;
            SQL.Add( 'delete * from ' +  g_Table_Company_Storage_ContractDetailed +
                        ' where '+ tContractViewColumn3.DataBinding.FieldName +
                        ' in("' + str + '")' );
            ExecSQL;

            Close;
            SQL.Clear;
            SQL.Add('delete * from ' +  g_Table_Company_Storage_ContractList +
                        ' where '+ tContractViewColumn3.DataBinding.FieldName +
                        ' in("' + str + '")');
            ExecSQL;
          end;
          DM.ADOconn.Committrans; //提交事务
        except
          on E:Exception do
          begin
            DM.ADOconn.RollbackTrans;           // 事务回滚
          //  err:=E.Message;
          //  ShowMessage(err);
          end;
        end;

      end;

    //  Self.TableView.Controller.DeleteSelection;  //删除选中的文本
    //  Self.TableView.DataController.UpdateData;
    //  dwADO.DeleteData(Self.ContractDataSet,dwContractListSQLtext);
      ViewData(tv.Selected);
    end;

  end;

end;

procedure TfrmStorageBase.RzToolButton17Click(Sender: TObject);
begin
  inherited;
  with RzPanel22 do
  begin
    if Visible then
      Visible := False
    else
      Visible := True;
  end;
end;

procedure TfrmStorageBase.RzToolButton1Click(Sender: TObject);
var
  s : string;
  szSQLText : string;

begin
  inherited;
  s := GetTableSelectedId(Self.tView,tViewColumn14.Index);
  szSQLText := 'delete * from ' +  g_Table_Company_Storage_GroupUnitPrice  +' where ParentId in("' + s + '")';
//  s := 'delete from b from sk_Company_Storage_GroupUnitPrice AS b inner join sk_Company_StorageDetailed AS u on u.UserId=b.UserId where u.UserId=3';
//  dwADO.ADODeleteSelectionData(tView,Detailed,dwDetailedSQLtext);
  dwADO.ExecSQL(szSQLText);
end;

procedure TfrmStorageBase.RzToolButton22Click(Sender: TObject);
begin
  inherited;
  dwADO.ADOSaveData(Contract,dwContractListSQLtext);
  (Sender as TRzToolButton).Enabled := False;
  dwADO.OpenSQL(Contract,dwContractListSQLtext);
end;

procedure TfrmStorageBase.RzToolButton23Click(Sender: TObject);
begin
  inherited;
  dwADO.ADODeleteSelectionData(Self.tContractView,Detailed,dwContractDetailedSQLText);
end;

procedure TfrmStorageBase.RzToolButton7Click(Sender: TObject);
begin
  with RzPanel8 do
  begin
    if Visible then
      Visible := False
    else
      Visible := True;
  end;
end;

procedure TfrmStorageBase.RzToolButton85Click(Sender: TObject);
var
  szRowIndex : Integer;
  I: Integer;
  szCount : Integer;
  szRowsCount : string;
  s , Id , str , err : string;
  szValue : Variant;
  szDir , dir : string;

begin
  inherited;
  szCount := tv1.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;

  if szCount >= 0 then
  begin
    if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin

      with tv1 do
      begin
        if DataController.IsEditing then DataController.UpdateData;

        for I := szCount -1 downto 0 do
        begin
          Id := VarToStr(  Controller.SelectedRows[i].Values[ tv1Col2.Index ] );
          if Length(id) <> 0 then
          begin
            szDir := Concat(g_Resources, '\'+ g_DirBuildStorage);
            //CreateOpenDir(Handle,Concat(szDir,'\' + szNumbers),True);
            if Length(szDir) <> 0 then
            begin
              dir := ConcatEnclosure(szDir, id);
              if DirectoryExists(dir) then
              begin
                DeleteDirectory(dir);
              end;

            end;

            if Length(str) = 0 then
              str := Id
            else
              str := str + '","' + Id;

          end;

        end;

      end;
    //  g_Table_Company_StorageBillList;
    //  g_Table_Company_StorageDetailed;
      if str <> '' then
      begin
        s := 'delete * from ' +  g_Table_Company_StorageDetailed +
           ' where '+ tv1Col2.DataBinding.FieldName +
           ' in("' + str + '")';
        dwADO.ExecSQL(s);
        s := 'delete * from ' +  g_Table_Company_Storage_GroupUnitPrice +
           ' where '+ tv1Col2.DataBinding.FieldName +
           ' in("' + str + '")';
        dwADO.ExecSQL(s);
      end;

      tv1.Controller.DeleteSelection;  //删除选中的文本
      tv1.DataController.UpdateData;
      dwADO.DeleteData(Master,dwSQLtext);
      Detailed.Close;
      {
      DM.ADOconn.BeginTrans; //开始事务
      try
        DM.ADOconn.Committrans; //提交事务
      except
        on E:Exception do
        begin
          DM.ADOconn.RollbackTrans;           // 事务回滚
          err:=E.Message;
          ShowMessage(err);
        end;
      end;
      }

    end;

  end;

end;

procedure TfrmStorageBase.RzToolButton88Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmStorageBase.RzToolButton8Click(Sender: TObject);
begin
  with RzPanel9 do
  begin
    if Visible then
      Visible := False
    else
      Visible := True;
  end;
end;

procedure TfrmStorageBase.RzToolButton9Click(Sender: TObject);
begin
  inherited;
  SaveData(Detailed);
  dwADO.ADOSaveData(Master,dwSQLtext);
  RzToolButton9.Enabled := False;
  ViewData(tv.Selected);
end;

function TfrmStorageBase.ViewData(Node: TTreeNode):Boolean;
var
  s : string;
  PData: PNodeData;
  szRowCount : Integer;
  t : Integer;

begin
  inherited;
  if Node.Level >= 2 then
  begin
    PData := PNodeData(Node.Data);
    if Assigned(PData) then
    begin
      dwNodeId   := pData^.Index;
      dwTreeCode := pData^.Code;

      g_NodeText := Node.Text;
      dwTreeId   := 'TreeId="' + IntToStr(dwNodeId) +'"';
    //  sk_Company_StorageContract   合同单
    //  sk_Company_StorageContractDetailed  合同单明细
      case RzPageControl2.ActivePageIndex of
        0:
        begin
          t := GetCurrentTime;
          dwSQLtext := 'Select * from ' + g_Table_Company_StorageBillList + ' WHERE ' + dwTreeId;
          dwADO.OpenSQL(Self.Master,dwSQLtext);
          SearchDetailed;
          dwADO.OpenSQL(Self.dsGroupUnitPrice,'Select * from ' + g_Table_Company_Storage_GroupUnitPrice);
        end;
        1:
        begin
          t := GetCurrentTime;
          dwContractListSQLtext := 'Select * from ' + g_Table_Company_Storage_ContractList + ' WHERE ' + dwTreeId;
          dwADO.OpenSQL(Self.Contract,dwContractListSQLtext);
        end;
      end;
      //ShowMessage(IntToStr(GetCurrentTime - t));
      cxComboBox1.ItemIndex := -1;
      dwSignName := '';
      dwDataType := 0;
      cxLookupComboBox1.Text := '';
      cxLookupComboBox2.Text := '';
      cxLookupComboBox7.Text := '';
      cxComboBox4.Text := '';
      cxTextEdit1.Text := '';
      cxTextEdit2.Text := '';

    end;

  end else
  begin
    Master.Close;
    Detailed.Close;
  end;

end;

procedure TfrmStorageBase.tvChange(Sender: TObject; Node: TTreeNode);
begin
//  ViewData(Node);
end;

procedure TfrmStorageBase.tvCompanyColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmStorageBase.tvCompanyColumn2PropertiesCloseUp(Sender: TObject);
var
  szColContentA : Variant;

  szRowIndex:Integer;
  szColName : string;
  DD1 : string;
  i : Integer;
  szIsExist:Boolean;

begin
  inherited;
  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;

    if szRowIndex >= 0 then
    begin
      szColContentA :=  DataController.Values[szRowIndex,0] ; //名称

      szIsExist  := False;
      with tvCompany do
      begin

        for I := 0 to ViewData.RowCount-1 do
        begin
          DD1 := VarToStr( ViewData.Rows[i].Values[tvCompanyColumn2.Index] ); //姓名
          if ( DD1 = VarToStr(szColContentA)) and (i <> Controller.FocusedRowIndex) then
          begin
            szIsExist := True;
            tvCompanyColumn2.EditValue := null;
            tvCompanyColumn2.Editing;
            Application.MessageBox(PWideChar( VarToStr(szColContentA) + ' 已经存在' ), '？', MB_OKCANCEL + MB_ICONWARNING);
            Break;
          end;
        end;

      end;

    end;
  end;

end;

procedure TfrmStorageBase.tvCompanyDblClick(Sender: TObject);
var
  szRowIndex : Integer;
  s : string;

begin
  inherited;

  if (Master.Active) and (dwIsSelectCompany) then
  begin
     if MessageBox(handle, PChar('是否切换公司?'),'提示',
               MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDNO then
    begin
      Exit;
    end;
  end;

  szRowIndex := tvCompany.Controller.FocusedRowIndex;
  if szRowIndex >= 0 then
  begin
    with Company do
    begin
      if State <> dsInactive then
      begin
      //  dwPostCode :=FieldByName('Code').AsString;
        dwCompanyName := FieldByName(tvCompanyColumn2.DataBinding.FieldName).AsString;
        dwCompanyParentId := FieldByName('parent').AsInteger;
        g_TreeView.GetTreeTable( dwCompanyParentId );
        dwIsSelectCompany := True;
        tv.Enabled := True;

        RzBut1.Enabled := True;
        RzBut2.Enabled := True;
        RzBut3.Enabled := True;
        RzBut4.Enabled := True;

      end;
    end;

  end;

end;

function TfrmStorageBase.IstreeEdit(S : string ; Node: TTreeNode):Boolean;
var
  szCodeList : string;
  PrevNode , NextNode : TTreeNode;

begin
  if s = m_OutText then
  begin
    treeCldnode(Node,szCodeList);
    g_TreeView.DeleteTree(Node);
  end else
  begin
    g_TreeView.ModifyNodeSignName(s,Node);
    UpDateIndex(Node);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    if PrevNode.Index <> -1 then UpDateIndex(PrevNode);
    NextNode   := Tv.Selected.getNextSibling;//下
    if NextNode.Index <> -1 then UpDateIndex(NextNode);
  end;
end;

procedure TfrmStorageBase.tvCompanyEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Company,AAllow);
end;

procedure TfrmStorageBase.tvDblClick(Sender: TObject);
begin
  inherited;
  ViewData(tv.Selected);
end;

procedure TfrmStorageBase.tvEdited(Sender: TObject; Node: TTreeNode;
  var S: string);
begin
  inherited;
  IstreeEdit(s,Node);
end;

procedure TfrmStorageBase.tViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Detailed,AAllow);
end;

procedure TfrmStorageBase.tViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then RzToolButton1.Click;
end;

procedure TfrmStorageBase.tViewStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;

  Value := ARecord.Values[tViewCol2.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

end.


