object frmWorkWeight: TfrmWorkWeight
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #24037#31243#37327
  ClientHeight = 284
  ClientWidth = 667
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object cxGroupBox2: TcxGroupBox
    Left = 8
    Top = 7
    Caption = #26032#22686'/'#32534#36753
    TabOrder = 0
    Height = 267
    Width = 649
    object Label8: TLabel
      Left = 15
      Top = 220
      Width = 554
      Height = 33
      AutoSize = False
      Caption = #27880#65306#25968#20540#19981#33021#30452#25509#24405#20837'  '#65292#35201#30452#25509#24405#20837#26102#35201#22312#32534#36753#20462#25913#20013#24405#20837#65292#25968#20540#38656#35201#35745#31639#30340#35201#28857#21152#21495#36827#34892#20844#24335#35745#31639#33258#21160#20986#25968#20540#65292#19981#38656#35201#33258#24049#24405#20837#12290
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      WordWrap = True
    end
    object cxLabel1: TcxLabel
      Left = 30
      Top = 64
      Caption = #21333#12288#12288#20215' '
    end
    object cxCurrencyEdit1: TcxCurrencyEdit
      Left = 91
      Top = 62
      Properties.OnChange = cxCurrencyEdit1PropertiesChange
      TabOrder = 2
      Width = 93
    end
    object cxLabel2: TcxLabel
      Left = 190
      Top = 64
      Caption = #22823#12288#12288#20889
    end
    object cxComboBox1: TcxComboBox
      Left = 91
      Top = 96
      TabOrder = 3
      Width = 93
    end
    object cxLabel3: TcxLabel
      Left = 30
      Top = 97
      Caption = #21333#12288#12288#20301' '
    end
    object cxLabel4: TcxLabel
      Left = 190
      Top = 97
      Caption = #25968#12288#12288#20540
    end
    object cxLabel5: TcxLabel
      Left = 30
      Top = 29
      Caption = #24037#31243#21517#31216
    end
    object cxTextEdit1: TcxTextEdit
      Left = 91
      Top = 28
      TabOrder = 0
      Width = 173
    end
    object cxTextEdit2: TcxTextEdit
      Left = 331
      Top = 29
      TabOrder = 1
      Width = 263
    end
    object cxLabel6: TcxLabel
      Left = 270
      Top = 29
      Caption = #20998#21253#21333#20301' '
    end
    object cxLabel7: TcxLabel
      Left = 308
      Top = 97
      Caption = #26085#12288#12288#26399
    end
    object cxDateEdit1: TcxDateEdit
      Left = 369
      Top = 97
      TabOrder = 5
      Width = 82
    end
    object cxButton1: TcxButton
      Left = 519
      Top = 125
      Width = 75
      Height = 28
      Caption = #38468#20214
      TabOrder = 7
      OnClick = cxButton1Click
    end
    object cxTextEdit3: TcxTextEdit
      Left = 248
      Top = 63
      Enabled = False
      Properties.ReadOnly = True
      TabOrder = 17
      Width = 345
    end
    object cxLabel9: TcxLabel
      Left = 30
      Top = 134
      Caption = #22791#12288#12288#27880' '
    end
    object cxMemo1: TcxMemo
      Left = 91
      Top = 128
      TabOrder = 6
      Height = 86
      Width = 414
    end
    object cxButton2: TcxButton
      Left = 520
      Top = 155
      Width = 74
      Height = 28
      Caption = #30830#23450'(&O)'
      TabOrder = 8
      OnClick = cxButton2Click
    end
    object cxButton3: TcxButton
      Left = 519
      Top = 189
      Width = 74
      Height = 28
      Caption = #36864#20986'(&C)'
      TabOrder = 9
      OnClick = cxButton3Click
    end
    object cxCurrencyEdit2: TcxCurrencyEdit
      Left = 248
      Top = 96
      Properties.DisplayFormat = '0.##;0.##'
      TabOrder = 4
      Width = 54
    end
  end
end
