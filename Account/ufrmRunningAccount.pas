unit ufrmRunningAccount;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, Vcl.ComCtrls, dxCore, cxDateUtils,
  dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, Vcl.StdCtrls, RzPanel,
  RzButton, Vcl.ExtCtrls, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter,
  cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCheckBox,
  cxDBLookupComboBox, cxCurrencyEdit, cxMemo, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, Datasnap.DBClient, Vcl.Menus,ufrmBaseRunningAccount,
  cxGridBandedTableView, dxmdaset, cxLookupEdit, cxDBLookupEdit, cxLabel,UnitADO,
  cxSpinEdit, RzTabs, Data.Win.ADODB;

type
  TfrmRunningAccount = class(TfrmBaseRunningAccount)
    RzToolButton3: TRzToolButton;
    procedure RzToolButton5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RzToolButton8Click(Sender: TObject);
    procedure RzToolButton3Click(Sender: TObject);
    procedure RzToolButton15Click(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure MasterAfterPost(DataSet: TDataSet);
    procedure GridResize(Sender: TObject);
    procedure RzToolButton12Click(Sender: TObject);
    procedure tvCompanyColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
  private
    { Private declarations }
  public
    { Public declarations }

  end;

var
  frmRunningAccount: TfrmRunningAccount;

implementation

uses
   uDataModule,global,ufrmDetailList,DateUtils,Math;

{$R *.dfm}

procedure TfrmRunningAccount.btnDeleteClick(Sender: TObject);
begin
  inherited;
  dwADO.ADODeleteSelectionData(Self.tvDetailed,Self.Detailed,dwDetailedSQLText);
end;

procedure TfrmRunningAccount.FormCreate(Sender: TObject);
var
  s : string;
  Path : string;
  FileName : string;
begin
  inherited;
  //刷新
  Path := ExtractFilePath(Application.Exename);
  s :=  'Select * from ' + g_Table_RuningAccount + ' Where 1=2';
  with DM.ADOQuery1 do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + g_Table_DetailTable + ' Where 1=2';  //明细表

    Open;
  //  with DM.DataSetProvider1.DataSet.FieldByName( 'Id' ) do ProviderFlags := ProviderFlags + [ pfInKey ];
    Self.Detailed.Data := DM.DataSetProvider1.Data;
    Self.Detailed.Open;
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
    with DM.DataSetProvider1.DataSet.FieldByName( 'numbers' ) do ProviderFlags := ProviderFlags + [ pfInKey ];
    Self.Master.Data := DM.DataSetProvider1.Data;
    Self.Master.Open;
    {
    FileName := Path + dwMasterTmpFile;
    if FileExists(FileName) then
    begin
      Self.Master.LoadFromFile( FileName );
    end;
    }
  end;

end;

procedure TfrmRunningAccount.GridResize(Sender: TObject);
begin
  inherited;
  Self.cxGrid2.Height := Self.Grid.Height;
end;

procedure TfrmRunningAccount.MasterAfterPost(DataSet: TDataSet);
var
  Path : string;
begin
  inherited;
//  Path := ExtractFilePath(Application.Exename);
//  Self.Master.SaveToFile( Path + dwMasterTmpFile);
end;

function IsNumbers(ATableName , AFieldName , ANumber : string):Boolean;
var
  s : string;

begin
  Result := False;
  s := 'Select * from '+ ATableName +' where  '+ AFieldName +'="'+ ANumber+'"';
  with dm.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
    Result := 0 <> RecordCount;
  end;

end;

procedure TfrmRunningAccount.RzToolButton12Click(Sender: TObject);
var
  i : Integer;
begin
  with Self.tvMaster do
  begin
    for I := ViewData.RecordCount-1 downto 0 do
    begin
      if ViewData.Rows[i].Values[Self.tvCol6.Index] = null then
      begin
        Controller.DeleteSelection;
      end;

    end;

  end;
  inherited;
end;

procedure TfrmRunningAccount.RzToolButton15Click(Sender: TObject);
var
  szRowIndex : Integer;
  I: Integer;
  szCount : Integer;
  szRowsCount : string;
  szIndex : Integer;
  s , Id , str , err : string;

begin
  inherited;
  szCount := Self.tvMaster.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if szCount >= 0 then
  begin
    if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin
      with Self.tvMaster  do
      begin

        for I := szCount -1 downto 0 do
        begin
          szIndex := GetColumnByFieldName(Self.tvCol3.DataBinding.FieldName).Index;
          s := Controller.SelectedRows[i].Values[szIndex];
          Id := VarToStr(s);

          if Length(id) <> 0 then
          begin
            if Length(str) = 0 then
              str := Id
            else
              str := str + '","' + Id;
          end;

        end;
        s := 'update ' + g_Table_RuningAccount + ' SET IsDelete=1 where ' + Self.tvCol3.DataBinding.FieldName + ' in("' + str + '")';
        DM.ADOconn.BeginTrans; //开始事务
        try

          with DM.Qry do
          begin
            Close;
            SQL.Text := s;
            ExecSQL;
          end;
          Controller.DeleteSelection;

          DM.ADOconn.Committrans; //提交事务
        except
          on E:Exception do
          begin
            DM.ADOconn.RollbackTrans;           // 事务回滚
            err:=E.Message;
            ShowMessage(err);
          end;
        end;
      end;

    end;

  end;

end;

procedure TfrmRunningAccount.RzToolButton3Click(Sender: TObject);
begin
  inherited;
  TfrmDetailList.Create(Self);
end;

procedure TfrmRunningAccount.RzToolButton5Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmRunningAccount.RzToolButton8Click(Sender: TObject);
var
  Child : TfrmDetailList;

begin
  inherited;
  Child := TfrmDetailList.Create(Application);
  try
    Child.Show;
    Close;
  except on E: Exception do
  end;
end;


procedure TfrmRunningAccount.tvCompanyColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

end.
