unit ufrmDetailList;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxMemo, cxCurrencyEdit,
  cxTextEdit, cxDropDownEdit, Vcl.ComCtrls, cxGridLevel, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid,
  RzButton, RzPanel, Vcl.ExtCtrls,ufrmBaseController, Datasnap.DBClient,
  cxContainer, cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxGroupBox, cxLabel, cxDBEdit, Vcl.Menus, Vcl.StdCtrls, cxButtons,ufrmBaseRunningAccount,
  dxCore, cxDateUtils, cxCalendar, cxCheckBox, cxGridBandedTableView, dxmdaset,
  cxSpinEdit, RzTabs, Data.Win.ADODB;

type
  TfrmDetailList = class(TfrmBaseRunningAccount)
    pmFrozen: TPopupMenu;
    RzToolButton3: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzSpacer5: TRzSpacer;
    RzToolButton5: TRzToolButton;
    N15: TMenuItem;
    N16: TMenuItem;
    N19: TMenuItem;
    N13: TMenuItem;
    N14: TMenuItem;
    N17: TMenuItem;
    cxDateEdit1: TcxDateEdit;
    Label10: TLabel;
    cxDateEdit2: TcxDateEdit;
    Label4: TLabel;
    N12: TMenuItem;
    N18: TMenuItem;
    procedure cxButton2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzToolButton5Click(Sender: TObject);
    procedure tvMasterStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvMasterTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure N11Click(Sender: TObject);
    procedure Execl1Click(Sender: TObject);
    procedure DetailAfterInsert(DataSet: TDataSet);
    procedure tvMasterColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure RzToolButton3Click(Sender: TObject);
    procedure tvSignNameDblClick(Sender: TObject);
    procedure RzToolButton9Click(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure tvMasterMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure RzToolButton15Click(Sender: TObject);
    procedure RzToolButton4Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure N16Click(Sender: TObject);
    procedure MasterAfterClose(DataSet: TDataSet);
    procedure GridResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure N14Click(Sender: TObject);
    procedure N17Click(Sender: TObject);
    procedure tvCompanyDblClick(Sender: TObject);
    procedure tvSignNameMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure tvCompanyMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure N12Click(Sender: TObject);
    procedure N18Click(Sender: TObject);
  private
    { Private declarations }
    dwSQLText : string;
    dwCompanyName : string;
    dwSignName : string;
    dwSignIndex: Integer;
    dwCompanyIndex : Integer;
    function FunSearch():Integer;
  public
    { Public declarations }
    g_ParentCode : string;
    procedure SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
                        lpSuffix:string);

  end;

var
  frmDetailList: TfrmDetailList;

implementation

uses
   global,uDataModule,ufrmIsViewGrid;

{$R *.dfm}

procedure TfrmDetailList.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount-1 do
  begin
    Application.ProcessMessages;
  //  SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;

procedure TfrmDetailList.btnDeleteClick(Sender: TObject);
begin
  inherited;
  dwADO.ADODeleteSelectionData(Self.tvDetailed,Self.Detailed,dwDetailedSQLText);
end;

procedure TfrmDetailList.btnEditClick(Sender: TObject);
begin
  inherited;
//
end;

procedure TfrmDetailList.btnNewClick(Sender: TObject);
begin
  inherited;
//
end;

procedure TfrmDetailList.cxButton2Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmDetailList.DetailAfterInsert(DataSet: TDataSet);
begin
  inherited;
//
end;

procedure TfrmDetailList.Execl1Click(Sender: TObject);
begin
  inherited;
//
end;

procedure TfrmDetailList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TfrmDetailList.FormCreate(Sender: TObject);
begin
  inherited;
  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
end;

procedure TfrmDetailList.N11Click(Sender: TObject);
var
  s : string;
begin
  inherited;
  if Length(dwSQLText) <> 0 then
  begin
    s := dwSQLText +  ' AND '+ Self.tvCol2.DataBinding.FieldName +'=true' ;
    dwADO.OpenSQL(Self.Master,s);
  end;
end;

procedure TfrmDetailList.N12Click(Sender: TObject);
var
  s : string;

begin
  inherited;
//  dwSQLText
  if Length(dwSQLText) <> 0 then
  begin
    s := dwSQLText + ' AND ' + Self.tvCol2.DataBinding.FieldName +'=no' ;
    dwADO.OpenSQL(Self.Master,s);
  end;
end;
{
procedure SetGridStuat(lpGrid : TcxGridDBTableView ; lpIndex : Integer ; lpValue : Boolean);
var
  i , szCount : Integer;

begin
  with lpGrid do
  begin
    for I := Controller.SelectedRowCount-1 downto 0 do
    begin
      Controller.SelectedRows[i].Values[ lpIndex ] := lpValue ;
    end;

  end;

end;
}
procedure TfrmDetailList.N14Click(Sender: TObject);
begin
  SetGridStuat(Self.tvMaster,Self.tvCol2.DataBinding.FieldName,False);
end;

procedure TfrmDetailList.N15Click(Sender: TObject);
var
  szCount , I : Integer;
  szIndex : Integer;
  s : string;
  Id: string;
  err:string;
  str:string;
  dir:string;

begin
  inherited;
//恢复记录
  szCount := Self.tvMaster.Controller.SelectedRowCount;
  if szCount >= 0 then
  begin
    if  Application.MessageBox( '是否要恢复所有选中的记录？', '恢复记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin
      with Self.tvMaster  do
      begin

        for I := szCount -1 downto 0 do
        begin
          szIndex := GetColumnByFieldName(Self.tvCol3.DataBinding.FieldName).Index;
          s := Controller.SelectedRows[i].Values[szIndex];
          Id := VarToStr(s);

          if Length(id) <> 0 then
          begin
            if Length(str) = 0 then
              str := Id
            else
              str := str + '","' + Id;
          end;

        end;
        s := 'update ' + g_Table_RuningAccount + ' SET IsDelete=0 where ' + Self.tvCol3.DataBinding.FieldName + ' in("' + str + '")';
        DM.ADOconn.BeginTrans; //开始事务
        try

          with DM.Qry do
          begin
            Close;
            SQL.Text := s;
            ExecSQL;
          end;
          Controller.DeleteSelection;
          Self.Detailed.Close;
          DM.ADOconn.Committrans; //提交事务
        except
          on E:Exception do
          begin
            DM.ADOconn.RollbackTrans;           // 事务回滚
            err:=E.Message;
            ShowMessage(err);
          end;
        end;
      end;

    end;

  end;

end;

procedure TfrmDetailList.N16Click(Sender: TObject);
var
  szCount , I : Integer;
  szIndex : Integer;
  s : string;
  Id: string;
  err:string;
  str:string;
  dir:string;
begin
  inherited;
  //彻底删除记录
  szCount := Self.tvMaster.Controller.SelectedRowCount;
  if szCount >= 0 then
  begin
    if  Application.MessageBox( '是否要彻底删除所有选中的记录？', '彻底删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin

      with Self.tvMaster  do
      begin

        for I := szCount -1 downto 0 do
        begin
          szIndex := GetColumnByFieldName(Self.tvCol3.DataBinding.FieldName).Index;
          s := Controller.SelectedRows[i].Values[szIndex];
          Id := VarToStr(s);

          if Length(id) <> 0 then
          begin
            dir := ConcatEnclosure( Concat(g_Resources ,  '\' + g_Dir_RuningAccount) , id);
            if DirectoryExists(dir) then  DeleteDirectory(dir);

            if Length(str) = 0 then
              str := Id
            else
              str := str + '","' + Id;
          end;

        end;

        DM.ADOconn.BeginTrans; //开始事务
        try

          with DM.Qry do
          begin
            Close;
            SQL.Text := 'delete * from ' +  g_Table_RuningAccount +
                        ' where ' + Self.tvCol3.DataBinding.FieldName +
                        ' in("' + str + '")';
            ExecSQL;

            SQL.Text := 'delete * from ' +  g_Table_DetailTable +
                        ' where ' + Self.tvCol3.DataBinding.FieldName +' in("' + str + '")';
            ExecSQL;

          end;
          Self.RzToolButton4.Click;
          DM.ADOconn.Committrans; //提交事务
        except
          on E:Exception do
          begin
            DM.ADOconn.RollbackTrans;           // 事务回滚
            err:=E.Message;
            ShowMessage('彻底删除 (Error):' + err);
          end;
        end;
      end;

    end;

  end;

end;

procedure TfrmDetailList.N17Click(Sender: TObject);
begin
  inherited;
  SetGridStuat(Self.tvMaster,Self.tvCol2.DataBinding.FieldName,True);
end;

procedure TfrmDetailList.N18Click(Sender: TObject);
var
  s : string;

begin
  inherited;
//  dwSQLText
  if Length(dwSQLText) <> 0 then
  begin
    s := dwSQLText + ' AND ' + Self.tvCol2.DataBinding.FieldName +'=yes' ;
    dwADO.OpenSQL(Self.Master,s);
  end;
end;


procedure TfrmDetailList.RzToolButton15Click(Sender: TObject);
var
  i : Integer;
  szCount : Integer;
  szRowsCount : string;
  szIndex : Integer;
  s , szSQLText , str : string;

begin
  inherited;
  szCount := Self.tvMaster.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if szCount >= 0 then
  begin

    if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin
      Self.Detailed.Close; //关闭明细表
      with Self.tvMaster  do
      begin
        for I := szCount -1 downto 0 do
        begin
          s :=  VarToStr(Controller.SelectedRows[i].Values[ Self.tvCol3.Index ] );
          if Length(s) = 0 then
            str := s
          else
            str := str + '","' + s;
        end;

        szSQLText := 'UPDATE ' + g_Table_RuningAccount +
                     ' SET IsDelete=1 where ' +
                     Self.tvCol3.DataBinding.FieldName + ' in("' + str + '")';
        DM.ADOconn.BeginTrans; //开始事务
        try
          with DM.Qry do
          begin
            Close;
            SQL.Text := szSQLText;
            ExecSQL;
          end;
          DM.ADOconn.Committrans; //提交事务
          Self.tvSignNameDblClick(Sender); //重新读取
        except
          on E:Exception do
          begin
            DM.ADOconn.RollbackTrans;           // 事务回滚
          //  err:=E.Message;
          //  ShowMessage(err);
          end;
        end;

        {
        s := 'update ' + g_Table_RuningAccount + ' SET IsDelete=1 where ' + Self.tvCol3.DataBinding.FieldName + ' in("' + str + '")';
        }
      end;

    end;

  end;

end;

procedure TfrmDetailList.RzToolButton3Click(Sender: TObject);
var
  szColName : string;
  sqltext : string;
begin
  inherited;
  if Length(dwSQLText) <> 0 then
  begin
    szColName := Self.tvCol4.DataBinding.FieldName;
    sqltext := dwSQLText + ' AND ' + szColName  + ' between :t1 and :t2';
    dwADO.ADOSearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,sqltext,Self.Master);
  end;
end;

procedure TfrmDetailList.RzToolButton4Click(Sender: TObject);
begin
  inherited;
//查询记录
  Self.RzToolButton12.Enabled := False;
  Self.RzToolButton13.Enabled := False;
  Self.RzToolButton15.Enabled := False;
  Self.N2.Enabled := False;
  Self.N3.Enabled := False;
  Self.N7.Enabled := False;
  dwADO.OpenSQL(Self.Master,'Select * from ' + g_Table_RuningAccount + ' Where IsDelete=1');
end;

procedure TfrmDetailList.RzToolButton5Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmDetailList.RzToolButton9Click(Sender: TObject);
begin
  inherited;
  dwSignName := Self.Search.Text;
  GridLocateRecord(Self.tvSignName,Self.tvSignNameCol2.DataBinding.FieldName,dwSignName);
  FunSearch();
end;

procedure TfrmDetailList.tvCompanyDblClick(Sender: TObject);
begin
  inherited;

  if dwCompanyName <> '' then
  begin
    if MessageBox(handle, PChar('是否切换往公司?'),'提示',
           MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDNO then
    begin
      Self.tvCompany.Controller.FocusedRowIndex := dwCompanyIndex;
      Exit;
    end;
  end;
  dwCompanyName := VarToStr( Self.tvCompanyColumn2.EditValue );
  FunSearch();
end;

procedure TfrmDetailList.tvCompanyMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  dwCompanyIndex := Self.tvCompany.Controller.FocusedRowIndex;
end;

procedure TfrmDetailList.tvMasterColumnSizeChanged(Sender: TcxGridTableView;
  AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvMaster,1,g_Dir_RuningAccount);
end;

procedure TfrmDetailList.tvMasterMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  szIndex : Integer;
  szCode : Variant;

begin
  inherited;
  if (Button = mbLeft) then
  begin
    szIndex := Self.tvMaster.Controller.FocusedRowIndex;
    if szIndex >= 0 then
    begin
      szCode := Self.tvCol3.EditValue;
      if szCode <> null then
      begin
        dwDetailedSQLText := 'Select * from ' + g_Table_DetailTable +
                                ' Where ' + Self.tvDetailedColumn2.DataBinding.FieldName +
                                '="' + VarToStr( szCode ) + '"';  //明细表
        dwADO.OpenSQL(Self.Detailed,dwDetailedSQLText);
      end;

    end;

  end;

end;

procedure TfrmDetailList.tvMasterStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
  inherited;
//
end;

procedure TfrmDetailList.tvMasterTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  inherited;
//
end;

function TfrmDetailList.FunSearch():Integer;
var
  szCompanyName : string;
  szColName : string;
  szSignName: string;
  t : Integer;

begin
  inherited;

  if (g_RunningIndex = 0) or (g_RunningIndex = 2) then
  begin
    szColName := Self.tvCol8.DataBinding.FieldName;
    if Length(dwCompanyName) <> 0 then
    begin
      szCompanyName := ' AND ' + Self.tvCol6.DataBinding.FieldName + '="' + dwCompanyName + '"';
    end;  
  end else
  begin
    szColName := Self.tvCol6.DataBinding.FieldName;
    if Length(dwCompanyName) <> 0 then
    begin
      szCompanyName := ' AND ' + Self.tvCol8.DataBinding.FieldName + '="' + dwCompanyName + '"';
    end;
      
  end;

  if dwSignName <> '' then
  begin
    szSignName := ' AND ' + szColName + '="' + VarToStr( dwSignName ) + '"';
  end else
  begin
    szSignName := ' AND ' + szColName + '="' + VarToStr( dwSignName ) + '"';
  end;    
    
  dwSQLText := 'Select * from ' + g_Table_RuningAccount +
               ' Where IsDelete=0 AND ' + Self.tvCol5.DataBinding.FieldName + '="' + dwTabTypes + '" ' + szCompanyName + szSignName;
  Self.N2.Enabled := True;
  Self.N3.Enabled := True;
  Self.N7.Enabled := True;
  Self.RzToolButton12.Enabled := True;
  Self.RzToolButton13.Enabled := True;
  Self.RzToolButton15.Enabled := True;
  t := GetTickCount;
  dwADO.OpenSQL(Self.Master,dwSQLText);
//  ShowMessage(IntToStr(GetTickCount - t));

end;


procedure TfrmDetailList.GridResize(Sender: TObject);
begin
  inherited;
  Self.cxGrid2.Height := Self.Grid.Height;
end;

procedure TfrmDetailList.MasterAfterClose(DataSet: TDataSet);
begin
  inherited;
  Self.Detailed.Close;
end;

procedure TfrmDetailList.tvSignNameDblClick(Sender: TObject);
begin
  inherited;
  if dwSignName <> '' then
  begin
    if MessageBox(handle, PChar('是否切换往来单位?'),'提示',
           MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDNO then
    begin
    //  Self.tvSignName.Controller.FocusedRowIndex := dwSignIndex;
    //  Self.tvSignName.Controller.FocusedRow.Index:= dwSignIndex;
      Exit;
    end;
  end;

  dwSignName := VarToStr( Self.tvSignNameCol2.EditValue );
  FunSearch();
end;

procedure TfrmDetailList.tvSignNameMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  inherited;
  dwSignIndex := Self.tvSignName.Controller.FocusedRowIndex;
end;

end.
