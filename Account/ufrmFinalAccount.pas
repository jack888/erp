unit ufrmFinalAccount;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxTextEdit, cxCalendar,
  cxDBLookupComboBox, cxDropDownEdit, cxContainer, Vcl.ComCtrls, cxMaskEdit,
  cxLookupEdit, cxDBLookupEdit, cxLabel, RzButton, RzPanel, Vcl.ExtCtrls,
  cxGridLevel, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxClasses, cxGridCustomView, cxGrid, Data.Win.ADODB,ufrmBaseController,
  Vcl.Menus, Vcl.StdCtrls, Datasnap.DBClient,UnitADO, cxButtonEdit;

type
  TfrmFinalAccount = class(TfrmBaseController)
    Grid: TcxGrid;
    tvView: TcxGridDBTableView;
    tvViewCol1: TcxGridDBColumn;
    tvViewCol3: TcxGridDBColumn;
    tvViewCol2: TcxGridDBColumn;
    tvViewCol4: TcxGridDBColumn;
    tvViewCol5: TcxGridDBColumn;
    Lv: TcxGridLevel;
    RzToolbar10: TRzToolbar;
    RzSpacer77: TRzSpacer;
    RzBut4: TRzToolButton;
    RzSpacer78: TRzSpacer;
    RzBut5: TRzToolButton;
    RzSpacer79: TRzSpacer;
    RzBut6: TRzToolButton;
    RzSpacer80: TRzSpacer;
    RzBut12: TRzToolButton;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzSpacer4: TRzSpacer;
    RzSpacer5: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzSpacer6: TRzSpacer;
    RzSpacer7: TRzSpacer;
    RzSpacer8: TRzSpacer;
    RzToolButton5: TRzToolButton;
    cxLabel1: TcxLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    StatusBar1: TStatusBar;
    ds: TDataSource;
    Old: TPopupMenu;
    MenuItem1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    MenuItem2: TMenuItem;
    Master: TClientDataSet;
    N3: TMenuItem;
    N4: TMenuItem;
    N6: TMenuItem;
    tvViewCol6: TcxGridDBColumn;
    tvViewCol7: TcxGridDBColumn;
    pm: TPopupMenu;
    N7: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N5: TMenuItem;
    N8: TMenuItem;
    procedure RzBut4Click(Sender: TObject);
    procedure RzBut12Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure tvViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton4Click(Sender: TObject);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvViewDblClick(Sender: TObject);
    procedure RzBut5Click(Sender: TObject);
    procedure RzBut6Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure MasterAfterInsert(DataSet: TDataSet);
    procedure N6Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure tvViewCol2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
    dwADO : TADO;
    dwSQLText : string;
  public
    { Public declarations }
    FromTypes : Integer;
    function GetTableData(IsDelete : Integer ):Boolean;
    function IsDelete(IsDele : Integer):Boolean;
  end;

var
  frmFinalAccount: TfrmFinalAccount;

implementation

uses
   ufrmFinalCost,uDataModule,global,ufunctions,Unit2,ufrmCompanyManage;

{$R *.dfm}

function TfrmFinalAccount.GetTableData(IsDelete : Integer ):Boolean;
begin
  dwSQLText := 'Select * from ' + g_Table_Finance_FinalAccount  +
                ' WHERE PID = 0 and IsDelete = '+ IntToStr( IsDelete ) +
                ' AND SignType=' + IntToStr(g_ModuleIndex) ;
  dwADO.OpenSQL(Self.Master, dwSQLText);
  GetMaintainInfo;
end;


procedure TfrmFinalAccount.MasterAfterInsert(DataSet: TDataSet);
var
  parent : Integer;
begin
  inherited;
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      FieldByName(Self.tvViewCol3.DataBinding.FieldName).Value := Date;
      parent := DM.getLastId(g_Table_Finance_FinalAccount);
      FieldByName('Code').Value   := GetRowCode(g_Table_Finance_FinalAccount,'Code');
      FieldByName('parent').Value := parent;
      FieldByName('SignType').Value := g_ModuleIndex;
    end;
  end;
end;

procedure TfrmFinalAccount.MenuItem1Click(Sender: TObject);
begin
  inherited;
  //历史工程
  Self.RzBut4.Enabled := False;
  Self.RzBut5.Enabled := False;
  Self.RzBut6.Enabled := False;
  Self.RzToolButton4.Enabled := False;
  Self.N1.Enabled := True;
  Self.MenuItem2.Enabled := True;

  Self.N9.Enabled := True;
  Self.N11.Enabled:= True;

  GetTableData(1);
end;

procedure TfrmFinalAccount.MenuItem2Click(Sender: TObject);
var
  szTableList : array[0..3] of TTableList;
  szCodeList  : string;
  szParent : Integer;
  szCode : string;

begin
  inherited;
  //彻底删除

  if Application.MessageBox( '是否需要要彻底删除数据，删除后无法恢复？', '提示:',
  MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin
    if Self.tvView.Controller.SelectedRowCount = 1 then
    begin
      with Self.tvView.DataController.DataSet do
      begin
        if not IsEmpty then
        begin
          szCode := FieldByName('Code').AsString;
          szParent := FieldByName('parent').AsInteger;
        end;

      end;

      DM.getTreeCode(g_Table_Finance_FinalAccount, szParent,szCodeList); //获取要删除的id表

      szTableList[0].ATableName := g_Table_Finance_ProjectFinal;  //成本管理
      szTableList[0].ADirectory := g_Dir_ProjectFinal;

      szTableList[1].ATableName := g_Table_BranchMoney; //交易记录
      szTableList[1].ADirectory := g_dir_BranchMoney;

      szTableList[2].ATableName := g_Table_DetailTable; //交易记录明细
      szTableList[2].ADirectory := '';

      DeleteTableFile(szTableList,szCodeList);
      DM.InDeleteData(g_Table_Finance_FinalAccount,szCodeList);

      Self.MenuItem1.Click;
    end else
    begin
      if Self.tvView.Controller.SelectedRowCount > 1 then
      begin
        Application.MessageBox( '禁止同时删除多条工程，只允许一次一条！', '提示:', MB_OKCANCEL + MB_ICONWARNING) ;
      end;
    end;

  end;

end;

procedure TfrmFinalAccount.N11Click(Sender: TObject);
begin
  inherited;
  Self.MenuItem2.Click;
end;

procedure TfrmFinalAccount.N1Click(Sender: TObject);
begin
  //恢复工程
  IsDelete(0);
  Self.MenuItem1.Click;
end;

procedure TfrmFinalAccount.N4Click(Sender: TObject);
begin
  inherited;
  Self.RzBut6.Click;
end;

procedure TfrmFinalAccount.N5Click(Sender: TObject);
begin
  inherited;
  Self.RzBut5.Click;
end;

procedure TfrmFinalAccount.N6Click(Sender: TObject);
begin
  inherited;
  Self.RzBut4.Click;
end;

procedure TfrmFinalAccount.N7Click(Sender: TObject);
begin
  inherited;
  Self.MenuItem1.Click;
end;

procedure TfrmFinalAccount.N8Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton4.Click;
end;

procedure TfrmFinalAccount.N9Click(Sender: TObject);
begin
  inherited;
  Self.N1.Click;
end;

procedure TfrmFinalAccount.FormActivate(Sender: TObject);
begin
  Self.RzToolButton5.Click;
end;

procedure TfrmFinalAccount.RzBut12Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmFinalAccount.RzBut4Click(Sender: TObject);
begin
  Self.Grid.SetFocus;
  dwADO.ADOAppend(Self.Master);
  Self.tvViewCol2.Editing := True;
end;

procedure TfrmFinalAccount.RzBut5Click(Sender: TObject);
begin
  dwADO.ADOSaveData(Self.Master,dwSQLText);
end;

function TfrmFinalAccount.IsDelete(IsDele : Integer):Boolean;
var
  i : Integer;
  szCount : Integer;
  szRowsCount : string;
  szIndex : Integer;
  s , szSQLText , str : string;

begin
  szCount := Self.tvView.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if szCount >= 0 then
  begin

      with Self.tvView  do
      begin

        for I := szCount -1 downto 0 do
        begin
          s :=  VarToStr(Controller.SelectedRows[i].Values[ Self.tvViewCol7.Index ] );
          if Length(s) = 0 then
            str := s
          else
            str := str + '","' + s;
        end;

        szSQLText := 'UPDATE ' + g_Table_Finance_FinalAccount +
                     ' SET IsDelete='+ IntToStr(IsDele) +' where ' +
                     Self.tvViewCol7.DataBinding.FieldName + ' in("' + str + '")';

        DM.ADOconn.BeginTrans; //开始事务
        try
          with DM.Qry do
          begin
            Close;
            SQL.Text := szSQLText;
            ExecSQL;
          end;
          DM.ADOconn.Committrans; //提交事

        except
          on E:Exception do
          begin
            DM.ADOconn.RollbackTrans;           // 事务回滚
          end;
        end;
      end;

  end;

end;

procedure TfrmFinalAccount.RzBut6Click(Sender: TObject);
var
  szRowCount : Integer;

begin

  if Application.MessageBox( '是否需要要删除数据，删除后可在历史数据查看已删除数据？', '提示:',
     MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin
      szRowCount := Self.tvView.Controller.SelectedRowCount;
      if szRowCount = 1 then
      begin
        IsDelete(1);
        RzToolButton5.Click;
      end else
      begin
        if szRowCount > 1 then
        begin
          Application.MessageBox( '禁止删除多条工程，只允许一条一条的删除！', '提示:', MB_OKCANCEL + MB_ICONWARNING) ;
        end;
      end;
  end;

end;

procedure TfrmFinalAccount.RzToolButton1Click(Sender: TObject);
var
  s : string;
begin
  inherited;
  //搜索
  s := Self.cxLookupComboBox1.Text;
//  GridLocateRecord(Self.tvView,Self.FinalAccountSignName.FieldName,s);
  dwSQLText := 'Select * from ' + g_Table_Finance_FinalAccount  +
                  ' WHERE PID = 0 and IsDelete = 0'+
                  ' AND SignType=' + IntToStr(g_ModuleIndex) + ' AND ' + Self.tvViewCol2.DataBinding.FieldName + '="' + s +'"';

  dwADO.OpenSQL(Self.Master, dwSQLText);
end;

procedure TfrmFinalAccount.RzToolButton4Click(Sender: TObject);
begin
  dwADO.ADOIsEdit(Self.Master);
end;

procedure TfrmFinalAccount.RzToolButton5Click(Sender: TObject);
begin
  Self.RzBut4.Enabled := True;
  Self.RzBut5.Enabled := True;
  Self.RzBut6.Enabled := True;
  Self.RzToolButton4.Enabled := True;
  Self.N1.Enabled := False;
  Self.MenuItem2.Enabled := False;

  Self.N9.Enabled := False;
  Self.N11.Enabled:= False;

  GetTableData(0);
end;

procedure TfrmFinalAccount.tvViewCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmFinalAccount.tvViewCol2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  Child : TfrmCompanyManage;
  lvTelephone : Variant;

begin
  inherited;
  Child := TfrmCompanyManage.Create(Application);
  try
    Child.dwIsReadonly := True;
    Child.ShowModal;
    tvViewCol2.EditValue :=  Child.dwSignName;
  finally
    Child.Free;
  end;
end;


procedure TfrmFinalAccount.tvViewDblClick(Sender: TObject);
var
  szRowCount : Integer;
  szParentIndex : Integer;
begin
  szRowCount := Self.tvView.Controller.SelectedRecordCount;
  if szRowCount = 1 then
  begin

    with Self.Master do
    begin
      if State <> dsInactive then
      begin
        g_ParentIndex := FieldByName('parent').AsInteger;
        TfrmFinalCost.Create(Application);
      end;
    end;
    Close;
  end else
  begin
    if szRowCount <> -1 then
    begin
      ShowMessage('选择太多往来单位，无法确定要进入哪个！');
    end;
  end;
end;

procedure TfrmFinalAccount.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  dwADO.ADOIsEditing(Self.Master,AAllow);
end;

end.
