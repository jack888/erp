object frmConstructManage: TfrmConstructManage
  Left = 3
  Top = 131
  Caption = #26045#24037#32508#21512#31649#29702
  ClientHeight = 696
  ClientWidth = 1387
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesigned
  Visible = True
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 269
    Top = 0
    Width = 10
    Height = 696
    Color = 16250613
    ParentColor = False
    ResizeStyle = rsUpdate
    ExplicitLeft = 276
    ExplicitTop = 8
  end
  object Client: TRzPanel
    Left = 279
    Top = 0
    Width = 1108
    Height = 696
    Align = alClient
    BorderOuter = fsNone
    TabOrder = 0
    object RzPageControl2: TRzPageControl
      Left = 0
      Top = 0
      Width = 1108
      Height = 696
      Hint = ''
      ActivePage = TabSheet11
      Align = alClient
      BackgroundColor = clBtnFace
      BoldCurrentTab = True
      ButtonColor = 16250613
      Color = clWhite
      UseColoredTabs = True
      HotTrackStyle = htsText
      Images = il1
      ParentBackgroundColor = False
      ParentColor = False
      ShowShadow = False
      TabOverlap = -4
      TabHeight = 26
      TabIndex = 0
      TabOrder = 0
      TabStyle = tsSquareCorners
      TabWidth = 80
      OnClick = RzPageControl2Click
      FixedDimension = 26
      object TabSheet11: TRzTabSheet
        Color = clWhite
        Caption = #24037#31243#27010#20917
        object cxGroupBox6: TcxGroupBox
          Left = 21
          Top = 35
          Caption = #22522#26412#20449#24687
          TabOrder = 0
          Height = 186
          Width = 652
          object cxLabel1: TcxLabel
            Left = 32
            Top = 32
            AutoSize = False
            Caption = #24037#31243#21517#31216
            Height = 17
            Width = 52
          end
          object cxLabel2: TcxLabel
            Left = 32
            Top = 61
            Caption = #32467#26500#31867#22411
          end
          object cxLabel3: TcxLabel
            Left = 330
            Top = 32
            Caption = #24037#31243#32534#21495
          end
          object cxLabel4: TcxLabel
            Left = 330
            Top = 61
            Caption = #24037#31243#22320#22336
          end
          object cxLabel5: TcxLabel
            Left = 32
            Top = 148
            Caption = #24320#24037#26085#26399
          end
          object cxLabel6: TcxLabel
            Left = 330
            Top = 148
            Caption = #31459#24037#26085#26399
          end
          object cxLabel7: TcxLabel
            Left = 32
            Top = 90
            Caption = #24314#31569#38754#31215
          end
          object cxLabel8: TcxLabel
            Left = 32
            Top = 119
            Caption = #24314#31569#20445#38505
          end
          object cxLabel10: TcxLabel
            Left = 330
            Top = 90
            Caption = #24037#31243#36896#20215
          end
          object cxDBTextEdit1: TcxDBTextEdit
            Left = 92
            Top = 32
            DataBinding.DataField = 'ProjectName'
            DataBinding.DataSource = dsSurvey
            TabOrder = 0
            Width = 225
          end
          object cxDBTextEdit2: TcxDBTextEdit
            Left = 92
            Top = 61
            DataBinding.DataField = 'structureType'
            DataBinding.DataSource = dsSurvey
            TabOrder = 3
            Width = 225
          end
          object cxDBTextEdit3: TcxDBTextEdit
            Left = 92
            Top = 88
            DataBinding.DataField = 'buildacreage'
            DataBinding.DataSource = dsSurvey
            TabOrder = 5
            Width = 225
          end
          object cxDBTextEdit4: TcxDBTextEdit
            Left = 92
            Top = 119
            DataBinding.DataField = 'builsafe'
            DataBinding.DataSource = dsSurvey
            TabOrder = 7
            Width = 225
          end
          object cxDBTextEdit5: TcxDBTextEdit
            Left = 396
            Top = 34
            DataBinding.DataField = 'ProjectNumber'
            DataBinding.DataSource = dsSurvey
            TabOrder = 1
            Width = 230
          end
          object cxDBTextEdit6: TcxDBTextEdit
            Left = 396
            Top = 61
            DataBinding.DataField = 'ProjectAddress'
            DataBinding.DataSource = dsSurvey
            TabOrder = 4
            Width = 230
          end
          object cxDBTextEdit7: TcxDBTextEdit
            Left = 396
            Top = 119
            DataBinding.DataField = 'LargeMoney'
            DataBinding.DataSource = dsSurvey
            TabOrder = 18
            Width = 230
          end
          object cxLabel9: TcxLabel
            Left = 330
            Top = 119
            Caption = #22823#20889#37329#39069
          end
          object cxButton22: TcxButton
            Left = 612
            Top = 227
            Width = 75
            Height = 26
            Caption = #20445#38505#38468#20214
            TabOrder = 20
            Visible = False
          end
          object cxButton20: TcxButton
            Left = 693
            Top = 227
            Width = 67
            Height = 26
            Caption = #22270#32440
            TabOrder = 21
            Visible = False
          end
          object cxButton25: TcxButton
            Left = 766
            Top = 227
            Width = 67
            Height = 26
            Caption = #22806#32463#35777
            TabOrder = 22
            Visible = False
          end
          object cxDBCurrencyEdit1: TcxDBCurrencyEdit
            Left = 396
            Top = 88
            AutoSize = False
            DataBinding.DataField = 'ProjectCost'
            DataBinding.DataSource = dsSurvey
            Properties.Alignment.Horz = taLeftJustify
            Properties.OnEditValueChanged = cxDBCurrencyEdit1PropertiesEditValueChanged
            TabOrder = 6
            Height = 21
            Width = 230
          end
          object cxDBDateEdit1: TcxDBDateEdit
            Left = 92
            Top = 148
            DataBinding.DataField = 'StartDate'
            DataBinding.DataSource = dsSurvey
            TabOrder = 8
            Width = 121
          end
          object cxDBDateEdit2: TcxDBDateEdit
            Left = 396
            Top = 148
            DataBinding.DataField = 'EndDate'
            DataBinding.DataSource = dsSurvey
            TabOrder = 9
            Width = 121
          end
        end
        object cxGroupBox7: TcxGroupBox
          Left = 21
          Top = 227
          Caption = #21442#24314#21333#20301
          TabOrder = 1
          Height = 150
          Width = 652
          object cxLabel12: TcxLabel
            Left = 32
            Top = 74
            Caption = #35774#35745#21333#20301
          end
          object cxLabel13: TcxLabel
            Left = 32
            Top = 45
            Caption = #30417#29702#21333#20301
          end
          object cxLabel14: TcxLabel
            Left = 32
            Top = 16
            Caption = #24314#35774#21333#20301
          end
          object cxLabel11: TcxLabel
            Left = 336
            Top = 16
            Caption = #26045#24037#21333#20301
          end
          object cxLabel15: TcxLabel
            Left = 336
            Top = 45
            Caption = #21208#23519#21333#20301
          end
          object cxLabel16: TcxLabel
            Left = 336
            Top = 74
            Caption = #30417#31649#21333#20301
          end
          object cxButton17: TcxButton
            Left = 632
            Top = 36
            Width = 75
            Height = 26
            Caption = #25307#26631#25991#20214
            TabOrder = 6
            Visible = False
          end
          object cxButton18: TcxButton
            Left = 632
            Top = 68
            Width = 75
            Height = 26
            Caption = #25237#26631#20070
            TabOrder = 7
            Visible = False
          end
          object cxButton19: TcxButton
            Left = 632
            Top = 100
            Width = 75
            Height = 26
            Caption = #39044#31639#20070
            TabOrder = 8
            Visible = False
          end
          object cxDBTextEdit9: TcxDBTextEdit
            Left = 401
            Top = 16
            DataBinding.DataField = 'CreateCompany'
            DataBinding.DataSource = dsSurvey
            TabOrder = 10
            Width = 225
          end
          object cxDBTextEdit10: TcxDBTextEdit
            Left = 92
            Top = 45
            DataBinding.DataField = 'OverseerCompany'
            DataBinding.DataSource = dsSurvey
            TabOrder = 11
            Width = 225
          end
          object cxDBTextEdit8: TcxDBTextEdit
            Left = 92
            Top = 18
            DataBinding.DataField = 'ConstructCompany'
            DataBinding.DataSource = dsSurvey
            TabOrder = 9
            Width = 225
          end
          object cxDBTextEdit11: TcxDBTextEdit
            Left = 92
            Top = 74
            DataBinding.DataField = 'DesignCompany'
            DataBinding.DataSource = dsSurvey
            TabOrder = 13
            Width = 225
          end
          object cxDBTextEdit12: TcxDBTextEdit
            Left = 401
            Top = 45
            DataBinding.DataField = 'surveyCompany'
            DataBinding.DataSource = dsSurvey
            TabOrder = 12
            Width = 225
          end
          object cxDBTextEdit13: TcxDBTextEdit
            Left = 401
            Top = 74
            DataBinding.DataField = 'SuperviseCompany'
            DataBinding.DataSource = dsSurvey
            TabOrder = 14
            Width = 225
          end
          object cxLabel54: TcxLabel
            Left = 32
            Top = 100
            Caption = #36164#36136#21333#20301
          end
          object cxDBTextEdit26: TcxDBTextEdit
            Left = 92
            Top = 101
            DataBinding.DataField = 'AptitudeCompany'
            DataBinding.DataSource = dsSurvey
            TabOrder = 16
            Width = 225
          end
          object cxDBTextEdit29: TcxDBTextEdit
            Left = 401
            Top = 101
            DataBinding.DataField = 'ProjectDepartment'
            DataBinding.DataSource = dsSurvey
            TabOrder = 17
            Width = 225
          end
          object cxLabel17: TcxLabel
            Left = 348
            Top = 100
            Caption = #39033#30446#37096
          end
        end
        object cxGroupBox8: TcxGroupBox
          Left = 21
          Top = 388
          Caption = #39033#30446#37096
          TabOrder = 2
          Height = 250
          Width = 652
          object cxLabel20: TcxLabel
            Left = 16
            Top = 25
            Caption = #26045#24037#36127#36131#20154
          end
          object cxLabel21: TcxLabel
            Left = 38
            Top = 54
            Caption = #30005#12288#35805
          end
          object cxLabel22: TcxLabel
            Left = 40
            Top = 83
            Caption = #36136#26816#21592
          end
          object cxLabel23: TcxLabel
            Left = 348
            Top = 83
            Caption = #36164#26009#21592
          end
          object cxLabel24: TcxLabel
            Left = 348
            Top = 54
            Caption = #30005#12288#35805
          end
          object cxLabel25: TcxLabel
            Left = 324
            Top = 25
            Caption = #30002#26041#36127#36131#20154
          end
          object cxLabel26: TcxLabel
            Left = 348
            Top = 181
            Caption = #23433#20840#21592
          end
          object cxLabel27: TcxLabel
            Left = 40
            Top = 210
            Caption = #30005#12288#35805
          end
          object cxLabel28: TcxLabel
            Left = 16
            Top = 181
            Caption = #30417#29702#36127#36131#20154
          end
          object cxLabel29: TcxLabel
            Left = 40
            Top = 112
            Caption = #39044#31639#21592
          end
          object cxLabel30: TcxLabel
            Left = 348
            Top = 112
            Caption = #26448#26009#21592
          end
          object cxLabel31: TcxLabel
            Left = 348
            Top = 210
            Caption = #26045#24037#21592
          end
          object cxDBTextEdit14: TcxDBTextEdit
            Left = 96
            Top = 24
            DataBinding.DataField = 'Create_DutyPeople'
            DataBinding.DataSource = dsSurvey
            TabOrder = 12
            Width = 225
          end
          object cxDBTextEdit15: TcxDBTextEdit
            Left = 96
            Top = 53
            DataBinding.DataField = 'Create_DutyTel'
            DataBinding.DataSource = dsSurvey
            TabOrder = 13
            Width = 225
          end
          object cxDBTextEdit16: TcxDBTextEdit
            Left = 96
            Top = 82
            DataBinding.DataField = 'Create_Quality'
            DataBinding.DataSource = dsSurvey
            TabOrder = 14
            Width = 225
          end
          object cxDBTextEdit17: TcxDBTextEdit
            Left = 96
            Top = 111
            DataBinding.DataField = 'Create_Budget'
            DataBinding.DataSource = dsSurvey
            TabOrder = 15
            Width = 225
          end
          object cxDBTextEdit18: TcxDBTextEdit
            Left = 395
            Top = 24
            DataBinding.DataField = 'First_DutyPeople'
            DataBinding.DataSource = dsSurvey
            TabOrder = 16
            Width = 225
          end
          object cxDBTextEdit19: TcxDBTextEdit
            Left = 395
            Top = 53
            DataBinding.DataField = 'First_DutyTel'
            DataBinding.DataSource = dsSurvey
            TabOrder = 17
            Width = 225
          end
          object cxDBTextEdit20: TcxDBTextEdit
            Left = 395
            Top = 82
            DataBinding.DataField = 'First_Documenter'
            DataBinding.DataSource = dsSurvey
            TabOrder = 18
            Width = 225
          end
          object cxDBTextEdit21: TcxDBTextEdit
            Left = 395
            Top = 111
            DataBinding.DataField = 'First_Material'
            DataBinding.DataSource = dsSurvey
            TabOrder = 19
            Width = 225
          end
          object cxDBTextEdit22: TcxDBTextEdit
            Left = 97
            Top = 180
            DataBinding.DataField = 'Supervisor_DutyPeople'
            DataBinding.DataSource = dsSurvey
            TabOrder = 20
            Width = 225
          end
          object cxDBTextEdit23: TcxDBTextEdit
            Left = 97
            Top = 209
            DataBinding.DataField = 'Supervisor_DutyTel'
            DataBinding.DataSource = dsSurvey
            TabOrder = 21
            Width = 225
          end
          object cxDBTextEdit24: TcxDBTextEdit
            Left = 395
            Top = 180
            DataBinding.DataField = 'Supervisor_SafetyOfficer'
            DataBinding.DataSource = dsSurvey
            TabOrder = 22
            Width = 225
          end
          object cxDBTextEdit25: TcxDBTextEdit
            Left = 395
            Top = 209
            DataBinding.DataField = 'Supervisor_Builder'
            DataBinding.DataSource = dsSurvey
            TabOrder = 23
            Width = 225
          end
          object cxLabel52: TcxLabel
            Left = 40
            Top = 137
            Caption = #21462#26679#21592
          end
          object cxLabel53: TcxLabel
            Left = 349
            Top = 137
            Caption = #35265#35777#21592
          end
          object cxDBTextEdit27: TcxDBTextEdit
            Left = 97
            Top = 138
            DataBinding.DataField = 'Sampling'
            DataBinding.DataSource = dsSurvey
            TabOrder = 26
            Width = 225
          end
          object cxDBTextEdit28: TcxDBTextEdit
            Left = 395
            Top = 138
            DataBinding.DataField = 'witness'
            DataBinding.DataSource = dsSurvey
            TabOrder = 27
            Width = 225
          end
        end
        object RzToolbar8: TRzToolbar
          Left = 0
          Top = 0
          Width = 1106
          Height = 29
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsGroove
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 3
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer53
            RzToolButton39
            RzSpacer54
            RzToolButton43
            RzSpacer56
            RzToolButton40
            RzSpacer55
            RzToolButton42
            RzSpacer58
            RzToolButton45
            RzSpacer60
            RzToolButton46
            RzSpacer61
            RzToolButton47
            RzSpacer62
            RzToolButton48
            RzSpacer63
            RzToolButton41
            RzSpacer59
            RzToolButton44)
          object RzSpacer53: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton39: TRzToolButton
            Left = 12
            Top = 2
            Width = 63
            SelectionColorStop = 16119543
            ImageIndex = 17
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #20445#23384
            OnClick = RzToolButton39Click
          end
          object RzSpacer54: TRzSpacer
            Left = 75
            Top = 2
          end
          object RzToolButton43: TRzToolButton
            Left = 83
            Top = 2
            Width = 70
            SelectionColorStop = 16119543
            DropDownMenu = pmPrint
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
            Enabled = False
          end
          object RzToolButton44: TRzToolButton
            Left = 689
            Top = 2
            Width = 52
            SelectionColorStop = 16119543
            ImageIndex = 8
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #36864#20986
            OnClick = RzToolButton44Click
          end
          object RzSpacer59: TRzSpacer
            Left = 681
            Top = 2
          end
          object RzSpacer56: TRzSpacer
            Left = 153
            Top = 2
          end
          object RzToolButton40: TRzToolButton
            Left = 161
            Top = 2
            Width = 76
            SelectionColorStop = 16119543
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #20445#38505#38468#20214
            OnClick = RzToolButton40Click
          end
          object RzSpacer55: TRzSpacer
            Left = 237
            Top = 2
          end
          object RzToolButton42: TRzToolButton
            Left = 245
            Top = 2
            Width = 52
            SelectionColorStop = 16119543
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #22270#32440
            OnClick = RzToolButton42Click
          end
          object RzSpacer58: TRzSpacer
            Left = 297
            Top = 2
          end
          object RzToolButton45: TRzToolButton
            Left = 305
            Top = 2
            Width = 64
            DisabledIndex = 21
            SelectionColorStop = 16119543
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #22806#32463#35777
            OnClick = RzToolButton45Click
          end
          object RzSpacer60: TRzSpacer
            Left = 369
            Top = 2
          end
          object RzToolButton46: TRzToolButton
            Left = 377
            Top = 2
            Width = 76
            SelectionColorStop = 16119543
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #25307#26631#25991#20214
            OnClick = RzToolButton46Click
          end
          object RzSpacer61: TRzSpacer
            Left = 453
            Top = 2
          end
          object RzToolButton47: TRzToolButton
            Left = 461
            Top = 2
            Width = 64
            SelectionColorStop = 16119543
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #25237#26631#20070
            OnClick = RzToolButton47Click
          end
          object RzSpacer62: TRzSpacer
            Left = 525
            Top = 2
          end
          object RzToolButton48: TRzToolButton
            Left = 533
            Top = 2
            Width = 64
            SelectionColorStop = 16119543
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #39044#31639#20070
            OnClick = RzToolButton48Click
          end
          object RzSpacer63: TRzSpacer
            Left = 597
            Top = 2
          end
          object RzToolButton41: TRzToolButton
            Left = 605
            Top = 2
            Width = 76
            SelectionColorStop = 16119543
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #32508#21512#25991#26723
            OnClick = RzToolButton41Click
          end
        end
      end
      object TabSheet5: TRzTabSheet
        Color = 16250613
        Caption = #39033#30446#37096#26723#26696
        object Splitter3: TSplitter
          Left = 759
          Top = 0
          Width = 10
          Height = 665
          Align = alRight
          Color = 16250613
          ParentColor = False
          ResizeStyle = rsUpdate
          ExplicitLeft = 505
          ExplicitTop = -68
          ExplicitHeight = 646
        end
        object Panel2: TPanel
          Left = 769
          Top = 0
          Width = 337
          Height = 665
          Align = alRight
          BevelOuter = bvNone
          Caption = '111'
          TabOrder = 0
          object RzPageControl1: TRzPageControl
            Left = 0
            Top = 29
            Width = 337
            Height = 636
            Hint = ''
            ActivePage = TabSheet4
            Align = alClient
            ShowShadow = False
            TabOverlap = -4
            TabHeight = 25
            TabIndex = 3
            TabOrder = 0
            TabStyle = tsSquareCorners
            FixedDimension = 25
            object TabSheet1: TRzTabSheet
              Color = 16250613
              Caption = #24037#31243#26723#26696
              object tv1: TTreeView
                Left = 0
                Top = 0
                Width = 335
                Height = 606
                Align = alClient
                Images = DM.TreeImageList
                Indent = 19
                PopupMenu = PopupMenu1
                TabOrder = 0
                OnDblClick = tv1DblClick
                OnEdited = tv1Edited
                OnEditing = tv1Editing
                OnKeyDown = tv1KeyDown
                Items.NodeData = {
                  0301000000220000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000
                  0004000000010263684868260000000000000000000000FFFFFFFFFFFFFFFFFF
                  FFFFFF00000000000000000104E55D0B7A8269B5512600000000000000000000
                  00FFFFFFFFFFFFFFFFFFFFFFFF00000000000000000104A17B0674ED73C47E28
                  0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000000400000001
                  05A17B06745480FB7C6888220000000000000000000000FFFFFFFFFFFFFFFFFF
                  FFFFFF000000000000000001023275B965220000000000000000000000FFFFFF
                  FFFFFFFFFFFFFFFFFF00000000000000000102D1760674260000000000000000
                  000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000001043F659C5EE890E8
                  95240000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000000000
                  000103E55D3057C47E260000000000000000000000FFFFFFFFFFFFFFFFFFFFFF
                  FF00000000030000000104BD65E55D08540C54220000000000000000000000FF
                  FFFFFFFFFFFFFFFFFFFFFF000000000200000001023275B96524000000000000
                  0000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000000000000103E55D0B7AC4
                  7E240000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000000000
                  0001039B4F278D4655220000000000000000000000FFFFFFFFFFFFFFFFFFFFFF
                  FF00000000020000000102594EB965240000000000000000000000FFFFFFFFFF
                  FFFFFFFFFFFFFF00000000000000000103E55D0B7AC47E240000000000000000
                  000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000001039B4F278D465522
                  0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000000200000001
                  02194EB965240000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000
                  00000000000103E55D0B7AC47E240000000000000000000000FFFFFFFFFFFFFF
                  FFFFFFFFFF000000000000000001039B4F278D4655}
              end
            end
            object TabSheet2: TRzTabSheet
              Color = 16250613
              Caption = #24037#31243#25163#32493
              object tv2: TTreeView
                Left = 0
                Top = 0
                Width = 335
                Height = 606
                Align = alClient
                Images = DM.TreeImageList
                Indent = 19
                PopupMenu = PopupMenu1
                TabOrder = 0
                OnDblClick = tv1DblClick
                OnEdited = tv1Edited
                OnEditing = tv1Editing
                OnKeyDown = tv1KeyDown
              end
            end
            object TabSheet3: TRzTabSheet
              Color = 16250613
              Caption = #24037#31243#31649#29702
              object tv3: TTreeView
                Left = 0
                Top = 0
                Width = 335
                Height = 606
                Align = alClient
                Images = DM.TreeImageList
                Indent = 19
                PopupMenu = PopupMenu1
                TabOrder = 0
                OnDblClick = tv1DblClick
                OnEdited = tv1Edited
                OnEditing = tv1Editing
                OnKeyDown = tv1KeyDown
                Items.NodeData = {
                  0301000000260000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000
                  00040000000104B0733A57A17B0674260000000000000000000000FFFFFFFFFF
                  FFFFFFFFFFFFFF00000000000000000104BD65E55DE565D75F2A000000000000
                  0000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000000000000106BD65E55DC7
                  8F0B7AB08B555F220000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00
                  0000000000000001021A4FAE8B220000000000000000000000FFFFFFFFFFFFFF
                  FFFFFFFFFF0000000000000000010256595A7F}
              end
            end
            object TabSheet4: TRzTabSheet
              Color = 16250613
              Caption = #24037#31243#36164#26009
              object tv4: TTreeView
                Left = 0
                Top = 0
                Width = 335
                Height = 606
                Align = alClient
                Ctl3D = True
                Images = DM.TreeImageList
                Indent = 19
                ParentCtl3D = False
                PopupMenu = PopupMenu1
                TabOrder = 0
                OnDblClick = tv1DblClick
                OnEdited = tv1Edited
                OnEditing = tv1Editing
                OnKeyDown = tv1KeyDown
                Items.NodeData = {
                  03010000002C0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000
                  00070000000107E55D0B7A3652A65ECA53E55D77512200000000000000000000
                  00FFFFFFFFFFFFFFFFFFFFFFFF00000000000000000102FE56C6962200000000
                  00000000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000000000000102C48903
                  83220000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000000000
                  000102895B6851220000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00
                  000000000000000102288DCF91260000000000000000000000FFFFFFFFFFFFFF
                  FFFFFFFFFF0000000000000000010487650E66BD65E55D260000000000000000
                  000000FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000010480622F67A44E41
                  6D260000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000000000
                  000104A17B06743652A65E}
              end
            end
          end
          object RzToolbar1: TRzToolbar
            Left = 0
            Top = 0
            Width = 337
            Height = 29
            AutoStyle = False
            Images = DM.cxImageList1
            BorderInner = fsFlat
            BorderOuter = fsNone
            BorderSides = [sdBottom]
            BorderWidth = 0
            GradientColorStyle = gcsCustom
            TabOrder = 1
            VisualStyle = vsGradient
            ToolbarControls = (
              RzSpacer2
              RzToolButton1
              RzSpacer5
              RzToolButton4
              RzSpacer3
              RzToolButton2
              RzSpacer4
              RzToolButton3)
            object RzSpacer2: TRzSpacer
              Left = 4
              Top = 2
            end
            object RzToolButton1: TRzToolButton
              Left = 12
              Top = 2
              Width = 65
              SelectionColorStop = 16119543
              ImageIndex = 37
              ShowCaption = True
              UseToolbarButtonSize = False
              UseToolbarShowCaption = False
              Caption = #26032#22686
              OnClick = RzToolButton1Click
            end
            object RzSpacer3: TRzSpacer
              Left = 150
              Top = 2
            end
            object RzToolButton2: TRzToolButton
              Left = 158
              Top = 2
              Width = 65
              SelectionColorStop = 16119543
              ImageIndex = 51
              ShowCaption = True
              UseToolbarButtonSize = False
              UseToolbarShowCaption = False
              Caption = #21024#38500
              OnClick = RzToolButton2Click
            end
            object RzSpacer4: TRzSpacer
              Left = 223
              Top = 2
            end
            object RzToolButton3: TRzToolButton
              Left = 231
              Top = 2
              Width = 65
              SelectionColorStop = 16119543
              ImageIndex = 8
              ShowCaption = True
              UseToolbarButtonSize = False
              UseToolbarShowCaption = False
              Caption = #36864#20986
              OnClick = RzToolButton3Click
            end
            object RzSpacer5: TRzSpacer
              Left = 77
              Top = 2
            end
            object RzToolButton4: TRzToolButton
              Left = 85
              Top = 2
              Width = 65
              SelectionColorStop = 16119543
              ImageIndex = 0
              ShowCaption = True
              UseToolbarButtonSize = False
              UseToolbarShowCaption = False
              Caption = #32534#36753
              OnClick = RzToolButton4Click
            end
          end
        end
        object RzPanel1: TRzPanel
          Left = 0
          Top = 0
          Width = 759
          Height = 665
          Align = alClient
          BorderOuter = fsNone
          TabOrder = 1
          object RzToolbar14: TRzToolbar
            Left = 0
            Top = 0
            Width = 759
            Height = 29
            AutoStyle = False
            Images = DM.cxImageList1
            BorderInner = fsFlat
            BorderOuter = fsNone
            BorderSides = [sdBottom]
            BorderWidth = 0
            GradientColorStyle = gcsCustom
            TabOrder = 0
            VisualStyle = vsGradient
            ToolbarControls = (
              RzSpacer112
              RzToolButton84
              RzSpacer121
              RzToolButton96
              RzSpacer122
              RzToolButton6
              RzSpacer7
              RzToolButton91
              RzSpacer125
              cxLabel18
              RzEdit2
              RzSpacer9
              RzToolButton92
              RzSpacer10
              RzToolButton7)
            object RzSpacer112: TRzSpacer
              Left = 4
              Top = 2
            end
            object RzToolButton84: TRzToolButton
              Left = 12
              Top = 2
              Width = 65
              SelectionColorStop = 16119543
              ImageIndex = 37
              ShowCaption = True
              UseToolbarButtonSize = False
              UseToolbarShowCaption = False
              Caption = #28155#21152
              Enabled = False
              OnClick = RzToolButton84Click
            end
            object RzSpacer121: TRzSpacer
              Left = 77
              Top = 2
            end
            object RzToolButton91: TRzToolButton
              Left = 228
              Top = 2
              Width = 65
              SelectionColorStop = 16119543
              ImageIndex = 51
              ShowCaption = True
              UseToolbarButtonSize = False
              UseToolbarShowCaption = False
              Caption = #21024#38500
              Enabled = False
              OnClick = RzToolButton84Click
            end
            object RzSpacer122: TRzSpacer
              Left = 150
              Top = 2
            end
            object RzSpacer125: TRzSpacer
              Left = 293
              Top = 2
            end
            object RzToolButton96: TRzToolButton
              Left = 85
              Top = 2
              Width = 65
              SelectionColorStop = 16119543
              ImageIndex = 0
              ShowCaption = True
              UseToolbarButtonSize = False
              UseToolbarShowCaption = False
              Caption = #32534#36753
              Enabled = False
              OnClick = RzToolButton84Click
            end
            object RzToolButton92: TRzToolButton
              Left = 536
              Top = 2
              Width = 65
              SelectionColorStop = 16119543
              ImageIndex = 10
              ShowCaption = True
              UseToolbarButtonSize = False
              UseToolbarShowCaption = False
              Caption = #26597#35810
              Enabled = False
              OnClick = RzToolButton92Click
            end
            object RzSpacer7: TRzSpacer
              Left = 220
              Top = 2
            end
            object RzToolButton6: TRzToolButton
              Left = 158
              Top = 2
              Width = 62
              ImageIndex = 3
              ShowCaption = True
              UseToolbarButtonSize = False
              UseToolbarShowCaption = False
              Caption = #20445#23384
              Enabled = False
              OnClick = RzToolButton84Click
            end
            object RzSpacer9: TRzSpacer
              Left = 528
              Top = 2
            end
            object RzSpacer10: TRzSpacer
              Left = 601
              Top = 2
            end
            object RzToolButton7: TRzToolButton
              Left = 609
              Top = 2
              Width = 62
              ImageIndex = 8
              ShowCaption = True
              UseToolbarButtonSize = False
              UseToolbarShowCaption = False
              Caption = #36864#20986
              OnClick = RzToolButton3Click
            end
            object RzEdit2: TRzEdit
              Left = 345
              Top = 4
              Width = 183
              Height = 21
              Text = ''
              Enabled = False
              TabOrder = 0
              OnKeyDown = RzEdit2KeyDown
            end
            object cxLabel18: TcxLabel
              Left = 301
              Top = 6
              Caption = #20851#38190#23383':'
              Transparent = True
            end
          end
          object cxGrid1: TcxGrid
            Left = 0
            Top = 29
            Width = 759
            Height = 636
            Align = alClient
            TabOrder = 1
            object tvFiles: TcxGridDBTableView
              PopupMenu = RightKeys
              OnDblClick = tvFilesDblClick
              Navigator.Buttons.CustomButtons = <>
              OnEditing = tvFilesEditing
              OnEditKeyDown = tvFilesEditKeyDown
              DataController.DataSource = DataMaster
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Kind = skCount
                  Column = tvFilesColumn1
                end>
              DataController.Summary.SummaryGroups = <>
              FilterRow.Visible = True
              OptionsBehavior.FocusCellOnTab = True
              OptionsBehavior.GoToNextCellOnEnter = True
              OptionsData.Deleting = False
              OptionsData.DeletingConfirmation = False
              OptionsSelection.InvertSelect = False
              OptionsSelection.MultiSelect = True
              OptionsSelection.CellMultiSelect = True
              OptionsView.DataRowHeight = 23
              OptionsView.Footer = True
              OptionsView.GroupByBox = False
              OptionsView.HeaderHeight = 23
              OptionsView.Indicator = True
              OptionsView.IndicatorWidth = 16
              object tvFilesColumn1: TcxGridDBColumn
                Caption = #24207#21495
                PropertiesClassName = 'TcxTextEditProperties'
                Properties.Alignment.Horz = taCenter
                OnGetDisplayText = tvWorkGridColumn1GetDisplayText
                HeaderAlignmentHorz = taCenter
                Options.Filtering = False
                Width = 41
              end
              object tvFilesColumn2: TcxGridDBColumn
                Caption = #32534#21495
                DataBinding.FieldName = 'Numbers'
                HeaderAlignmentHorz = taCenter
                Width = 168
              end
              object tvFilesColumn4: TcxGridDBColumn
                Caption = #26085#26399
                DataBinding.FieldName = 'InputDate'
                PropertiesClassName = 'TcxDateEditProperties'
                HeaderAlignmentHorz = taCenter
                Width = 105
              end
              object tvFilesColumn3: TcxGridDBColumn
                Caption = #31867#21035
                DataBinding.FieldName = 'fieldType'
                PropertiesClassName = 'TcxLookupComboBoxProperties'
                Properties.DropDownListStyle = lsEditList
                Properties.ImmediateDropDownWhenActivated = True
                Properties.KeyFieldNames = 'TabName'
                Properties.ListColumns = <
                  item
                    Caption = #21517#31216
                    FieldName = 'TabName'
                  end>
                Properties.ListSource = DM.DataTab
                HeaderAlignmentHorz = taCenter
                Width = 87
              end
              object tvFilesColumn5: TcxGridDBColumn
                Caption = #26723#26696#21517#31216
                DataBinding.FieldName = 'fieldsName'
                Width = 212
              end
              object tvFilesColumn6: TcxGridDBColumn
                Caption = #22791#27880
                DataBinding.FieldName = 'Remarks'
                Width = 320
              end
            end
            object Lv: TcxGridLevel
              GridView = tvFiles
            end
          end
        end
      end
      object TabSheet16: TRzTabSheet
        Color = clWhite
        Caption = #21021#35746#21512#21516
        object Grid: TcxGrid
          Left = 0
          Top = 29
          Width = 1106
          Height = 636
          Align = alClient
          TabOrder = 0
          LookAndFeel.SkinName = 'Office2016Colorful'
          object tvPactView: TcxGridDBTableView
            OnDblClick = tvPactViewDblClick
            OnKeyDown = tvPactViewKeyDown
            Navigator.Buttons.CustomButtons = <>
            OnEditing = tvPactViewEditing
            OnEditKeyDown = tvPactViewEditKeyDown
            DataController.DataSource = dsPactInfo
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvPactViewCol14
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvPactViewCol12
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvPactViewCol15
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #21333#20987#31579#36873#26597#35810
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
            OptionsView.DataRowHeight = 32
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 25
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 15
            OnColumnSizeChanged = tvPactViewColumnSizeChanged
            object tvPactViewCol1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvFlowingAccountColumn14GetDisplayText
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 56
            end
            object tvPactViewCol2: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'numbers'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 110
            end
            object tvPactViewCol3: TcxGridDBColumn
              Caption = #26085#26399
              DataBinding.FieldName = 'InputDate'
              PropertiesClassName = 'TcxDateEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol4: TcxGridDBColumn
              Caption = #24037#31243#21517#31216
              DataBinding.FieldName = 'ProjectName'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol5: TcxGridDBColumn
              Caption = #36164#36136#21333#20301
              DataBinding.FieldName = 'AptitudeCompany'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 85
            end
            object tvPactViewCol6: TcxGridDBColumn
              Caption = #39033#30446#37096
              DataBinding.FieldName = 'ProjectDepartment'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 93
            end
            object tvPactViewCol7: TcxGridDBColumn
              Caption = #24448#26469#21333#20301
              DataBinding.FieldName = 'ContactsCompany'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.DropDownWidth = 260
              Properties.ImmediateDropDownWhenActivated = True
              Properties.KeyFieldNames = 'SignName'
              Properties.ListColumns = <
                item
                  Caption = #24448#26469#21333#20301
                  Width = 180
                  FieldName = 'SignName'
                end
                item
                  Caption = #31616#30721
                  Width = 60
                  FieldName = 'PYCode'
                end>
              Properties.ListSource = DataSignName
              HeaderAlignmentHorz = taCenter
              Width = 130
            end
            object tvPactViewColumn1: TcxGridDBColumn
              Caption = #26045#24037#21333#20301
              DataBinding.FieldName = 'CreateCompany'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.DropDownWidth = 200
              Properties.ImmediateDropDownWhenActivated = True
              Properties.KeyFieldNames = 'SignName'
              Properties.ListColumns = <
                item
                  Caption = #21517#31216
                  Width = 200
                  FieldName = 'SignName'
                end>
              Properties.ListSource = DataSource3
              Width = 98
            end
            object tvPactViewCol8: TcxGridDBColumn
              Caption = #21333#20301#31867#21035
              DataBinding.FieldName = 'CompanyType'
              PropertiesClassName = 'TcxComboBoxProperties'
              Properties.ImmediateDropDownWhenActivated = True
              OnGetPropertiesForEdit = tvPactViewCol8GetPropertiesForEdit
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol9: TcxGridDBColumn
              Caption = #24037#31181
              DataBinding.FieldName = 'WorkMaterialBranch'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.ImmediateDropDownWhenActivated = True
              Properties.KeyFieldNames = 'WorkType'
              Properties.ListColumns = <
                item
                  Caption = #21517#31216
                  FieldName = 'WorkType'
                end>
              Properties.ListSource = DataSource1
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol10: TcxGridDBColumn
              Caption = #35268#26684#21517#31216
              DataBinding.FieldName = 'NormsCaption'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.ImmediateDropDownWhenActivated = True
              Properties.KeyFieldNames = 'MakingCaption'
              Properties.ListColumns = <
                item
                  Caption = #21517#31216
                  Width = 100
                  FieldName = 'MakingCaption'
                end>
              Properties.ListSource = DataSource2
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol11: TcxGridDBColumn
              Caption = #25253#20215#21333#20301
              DataBinding.FieldName = 'CompanyOffer'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol12: TcxGridDBColumn
              Caption = #24037#31243#25253#20215
              DataBinding.FieldName = 'ProjectOffer'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.OnValidate = tvPactViewCol12PropertiesValidate
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol13: TcxGridDBColumn
              Caption = #24066#22330#25253#20215#26085#26399
              DataBinding.FieldName = 'MarketOfferDate'
              PropertiesClassName = 'TcxDateEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol14: TcxGridDBColumn
              Caption = #24066#22330#20215
              DataBinding.FieldName = 'MarketOffer'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.OnValidate = tvPactViewCol14PropertiesValidate
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol15: TcxGridDBColumn
              Caption = #24046#39069
              DataBinding.FieldName = 'DisparityMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taCenter
              Styles.Content = DM.SumMoney
              Width = 100
            end
            object tvPactViewCol16: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'Remarks'
              PropertiesClassName = 'TcxMemoProperties'
              HeaderAlignmentHorz = taCenter
              Width = 320
            end
          end
          object LvPact: TcxGridLevel
            GridView = tvPactView
          end
        end
        object RzToolbar6: TRzToolbar
          Left = 0
          Top = 0
          Width = 1106
          Height = 29
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsGroove
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 1
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer24
            RzToolButton13
            RzSpacer34
            RzToolButton26
            RzSpacer35
            RzToolButton27
            RzSpacer36
            RzToolButton28
            RzSpacer47
            RzToolButton29
            RzSpacer37
            cxLabel40
            cxDateEdit1
            RzSpacer38
            cxLabel41
            cxDateEdit2
            RzSpacer39
            RzToolButton31
            RzSpacer48
            RzToolButton30)
          object RzSpacer24: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton13: TRzToolButton
            Left = 12
            Top = 2
            Width = 65
            SelectionColorStop = 16119543
            ImageIndex = 1
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #28155#21152
            OnClick = RzToolButton13Click
          end
          object RzSpacer34: TRzSpacer
            Left = 77
            Top = 2
          end
          object RzToolButton26: TRzToolButton
            Left = 85
            Top = 2
            Width = 65
            SelectionColorStop = 16119543
            ImageIndex = 3
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #20445#23384
            OnClick = RzToolButton26Click
          end
          object RzSpacer35: TRzSpacer
            Left = 150
            Top = 2
          end
          object RzToolButton27: TRzToolButton
            Left = 158
            Top = 2
            Width = 65
            SelectionColorStop = 16119543
            ImageIndex = 2
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #21024#38500
            OnClick = RzToolButton13Click
          end
          object RzSpacer36: TRzSpacer
            Left = 223
            Top = 2
          end
          object RzToolButton28: TRzToolButton
            Left = 231
            Top = 2
            Width = 70
            SelectionColorStop = 16119543
            ImageIndex = 4
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #34920#26684#35774#32622
            OnClick = RzToolButton28Click
          end
          object RzToolButton29: TRzToolButton
            Left = 309
            Top = 2
            Width = 65
            SelectionColorStop = 16119543
            DropDownMenu = pmPrint
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzToolButton30: TRzToolButton
            Left = 723
            Top = 2
            Width = 65
            SelectionColorStop = 16119543
            ImageIndex = 8
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #36864#20986
            OnClick = RzToolButton30Click
          end
          object RzSpacer37: TRzSpacer
            Left = 374
            Top = 2
          end
          object RzSpacer38: TRzSpacer
            Left = 528
            Top = 2
            Width = 5
          end
          object RzToolButton31: TRzToolButton
            Left = 687
            Top = 2
            Width = 28
            SelectionColorStop = 16119543
            ImageIndex = 10
            ShowCaption = True
            UseToolbarShowCaption = False
            OnClick = RzToolButton31Click
          end
          object RzSpacer39: TRzSpacer
            Left = 679
            Top = 2
          end
          object RzSpacer47: TRzSpacer
            Left = 301
            Top = 2
          end
          object RzSpacer48: TRzSpacer
            Left = 715
            Top = 2
          end
          object cxLabel40: TcxLabel
            Left = 382
            Top = 6
            Caption = #36215#22987#26085#26399':'
            Transparent = True
          end
          object cxDateEdit1: TcxDateEdit
            Left = 438
            Top = 4
            Properties.OnCloseUp = cxDateEdit15PropertiesCloseUp
            TabOrder = 1
            Width = 90
          end
          object cxLabel41: TcxLabel
            Left = 533
            Top = 6
            Caption = #32467#26463#26085#26399':'
            Transparent = True
          end
          object cxDateEdit2: TcxDateEdit
            Left = 589
            Top = 4
            TabOrder = 3
            Width = 90
          end
        end
      end
      object TabSheet31: TRzTabSheet
        Color = clWhite
        Caption = #25320#27454#30003#35831
        object AllocateBankrollGrid: TcxGrid
          Left = 0
          Top = 29
          Width = 1106
          Height = 636
          Align = alClient
          TabOrder = 0
          object tvABView: TcxGridDBTableView
            OnDblClick = tvABViewDblClick
            OnKeyDown = tvABViewKeyDown
            Navigator.Buttons.CustomButtons = <>
            OnEditing = tvABViewEditing
            OnEditKeyDown = tvABViewEditKeyDown
            DataController.DataSource = dsAllocateBankroll
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvABViewColumn10
              end
              item
                Kind = skCount
                Column = tvABViewColumn9
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #21333#20987#31579#36873#26597#35810
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
            OptionsView.DataRowHeight = 28
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 30
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 20
            OnColumnSizeChanged = tvABViewColumnSizeChanged
            object tvABViewColumn1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvFlowingAccountColumn14GetDisplayText
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Width = 35
            end
            object tvABViewColumn2: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'numbers'
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 108
            end
            object tvABViewColumn3: TcxGridDBColumn
              Caption = #26085#26399
              DataBinding.FieldName = 'InputDate'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 141
            end
            object tvABViewColumn4: TcxGridDBColumn
              Caption = #24037#31243#21517#31216
              DataBinding.FieldName = 'ProjectName'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 126
            end
            object tvABViewColumn5: TcxGridDBColumn
              Caption = #36164#36136#21333#20301
              DataBinding.FieldName = 'AptitudeCompany'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 145
            end
            object tvABViewColumn6: TcxGridDBColumn
              Caption = #39033#30446#37096
              DataBinding.FieldName = 'ProjectDepartment'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 153
            end
            object tvABViewColumn12: TcxGridDBColumn
              Caption = #24448#26469#21333#20301
              DataBinding.FieldName = 'Remarks2'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.DropDownWidth = 260
              Properties.ImmediateDropDownWhenActivated = True
              Properties.KeyFieldNames = 'SignName'
              Properties.ListColumns = <
                item
                  Caption = #24448#26469#21333#20301
                  Width = 150
                  FieldName = 'SignName'
                end
                item
                  Caption = #31616#30721
                  Width = 60
                  FieldName = 'PYCode'
                end>
              Properties.ListSource = DataSignName
              HeaderAlignmentHorz = taCenter
              Width = 112
            end
            object tvABViewColumn16: TcxGridDBColumn
              Caption = #26045#24037#21333#20301
              DataBinding.FieldName = 'CreateCompany'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.DropDownWidth = 200
              Properties.ImmediateDropDownWhenActivated = True
              Properties.KeyFieldNames = 'SignName'
              Properties.ListColumns = <
                item
                  Caption = #21517#31216
                  Width = 200
                  FieldName = 'SignName'
                end>
              Properties.ListSource = DataSource3
              Width = 121
            end
            object tvABViewColumn7: TcxGridDBColumn
              Caption = #39033#30446#37096#36127#36131#20154
              DataBinding.FieldName = 'ProjectManager'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 127
            end
            object tvABViewColumn8: TcxGridDBColumn
              Caption = #24418#35937#36827#24230
              DataBinding.FieldName = 'ImageSpeed'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 118
            end
            object tvABViewColumn10: TcxGridDBColumn
              Caption = #24212#25320#37329#39069
              DataBinding.FieldName = 'AgreeMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.OnValidate = tvABViewColumn10PropertiesValidate
              HeaderAlignmentHorz = taCenter
              Styles.Content = DM.SumMoney
              Width = 80
            end
            object tvABViewColumn9: TcxGridDBColumn
              Caption = #37329#39069#22823#20889
              DataBinding.FieldName = 'LargeMoney'
              Width = 309
            end
            object tvABViewColumn13: TcxGridDBColumn
              Caption = #25320#27454#29366#24577
              DataBinding.FieldName = 'PayStatus'
              PropertiesClassName = 'TcxComboBoxProperties'
              HeaderAlignmentHorz = taCenter
              Width = 80
            end
            object tvABViewColumn14: TcxGridDBColumn
              Caption = #23457#26680#29366#24577
              DataBinding.FieldName = 'CheckStatus'
              PropertiesClassName = 'TcxComboBoxProperties'
              HeaderAlignmentHorz = taCenter
              Width = 80
            end
            object tvABViewColumn15: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'Remarks'
              PropertiesClassName = 'TcxMemoProperties'
              HeaderAlignmentHorz = taCenter
              Width = 80
            end
            object tvABViewColumn11: TcxGridDBColumn
              Caption = #22791#27880'1'
              DataBinding.FieldName = 'Remarks1'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 80
            end
          end
          object ABLv: TcxGridLevel
            GridView = tvABView
          end
        end
        object RzToolbar7: TRzToolbar
          Left = 0
          Top = 0
          Width = 1106
          Height = 29
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsGroove
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 1
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer40
            RzToolButton32
            RzSpacer41
            RzToolButton33
            RzSpacer42
            RzToolButton34
            RzSpacer43
            RzToolButton35
            RzSpacer44
            RzToolButton36
            RzSpacer46
            cxLabel42
            cxDateEdit3
            RzSpacer45
            cxLabel43
            cxDateEdit4
            RzSpacer52
            RzToolButton38
            RzSpacer50
            RzToolButton37
            RzSpacer51)
          object RzSpacer40: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton32: TRzToolButton
            Left = 12
            Top = 2
            Width = 63
            SelectionColorStop = 16119543
            ImageIndex = 1
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #28155#21152
            OnClick = RzToolButton32Click
          end
          object RzSpacer41: TRzSpacer
            Left = 75
            Top = 2
          end
          object RzToolButton33: TRzToolButton
            Left = 83
            Top = 2
            Width = 70
            SelectionColorStop = 16119543
            ImageIndex = 3
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #20445#23384
            OnClick = RzToolButton32Click
          end
          object RzSpacer42: TRzSpacer
            Left = 153
            Top = 2
          end
          object RzToolButton34: TRzToolButton
            Left = 161
            Top = 2
            Width = 70
            SelectionColorStop = 16119543
            ImageIndex = 2
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #21024#38500
            OnClick = RzToolButton32Click
          end
          object RzSpacer43: TRzSpacer
            Left = 231
            Top = 2
          end
          object RzToolButton35: TRzToolButton
            Left = 239
            Top = 2
            Width = 76
            SelectionColorStop = 16119543
            ImageIndex = 4
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #34920#26684#35774#32622
            OnClick = RzToolButton32Click
          end
          object RzToolButton36: TRzToolButton
            Left = 323
            Top = 2
            Width = 70
            SelectionColorStop = 16119543
            DropDownMenu = pmPrint
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzToolButton37: TRzToolButton
            Left = 742
            Top = 2
            Width = 52
            SelectionColorStop = 16119543
            ImageIndex = 8
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #36864#20986
            OnClick = RzToolButton37Click
          end
          object RzSpacer44: TRzSpacer
            Left = 315
            Top = 2
          end
          object RzSpacer45: TRzSpacer
            Left = 547
            Top = 2
            Width = 5
          end
          object RzToolButton38: TRzToolButton
            Left = 706
            Top = 2
            Width = 28
            SelectionColorStop = 16119543
            ImageIndex = 10
            ShowCaption = True
            UseToolbarShowCaption = False
            OnClick = RzToolButton38Click
          end
          object RzSpacer46: TRzSpacer
            Left = 393
            Top = 2
          end
          object RzSpacer50: TRzSpacer
            Left = 734
            Top = 2
          end
          object RzSpacer51: TRzSpacer
            Left = 794
            Top = 2
          end
          object RzSpacer52: TRzSpacer
            Left = 698
            Top = 2
          end
          object cxLabel42: TcxLabel
            Left = 401
            Top = 6
            Caption = #36215#22987#26085#26399':'
            Transparent = True
          end
          object cxDateEdit3: TcxDateEdit
            Left = 457
            Top = 4
            Properties.OnCloseUp = cxDateEdit15PropertiesCloseUp
            TabOrder = 1
            Width = 90
          end
          object cxLabel43: TcxLabel
            Left = 552
            Top = 6
            Caption = #32467#26463#26085#26399':'
            Transparent = True
          end
          object cxDateEdit4: TcxDateEdit
            Left = 608
            Top = 4
            Properties.OnCloseUp = cxDateEdit15PropertiesCloseUp
            TabOrder = 3
            Width = 90
          end
        end
      end
    end
  end
  object Left: TRzPanel
    Left = 0
    Top = 0
    Width = 269
    Height = 696
    Align = alLeft
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdLeft, sdRight]
    TabOrder = 1
  end
  object PopupMenu1: TPopupMenu
    Images = DM.il1
    Left = 56
    Top = 192
    object N3: TMenuItem
      Caption = #32534#36753
      ImageIndex = 17
    end
    object N1: TMenuItem
      Caption = #21024#38500
      ImageIndex = 16
    end
    object N2: TMenuItem
      Caption = #26032#22686#21152#19968#26465#35760#24405
      ImageIndex = 8
    end
  end
  object il1: TImageList
    Left = 64
    Top = 496
    Bitmap = {
      494C01010A002800E00110001000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      0000000000003600000028000000400000003000000001002000000000000030
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CC996600993300009933000099330000CC996600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CCCCCC00999999009999990099999900CCCCCC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CC9966009933000000000000000000000000000099330000CC9966000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CCCCCC009999990000000000000000000000000099999900CCCCCC000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000993300000000000000000000000000000000000000000000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000999999000000000000000000000000000000000000000000999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009933000000000000CC99660099330000CC99660000000000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      00009999990000000000CCCCCC0099999900CCCCCC0000000000999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000993300000000000099330000000000009933000000000000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000999999000000000099999900000000009999990000000000999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000993300000000000099330000000000009933000000000000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000999999000000000099999900000000009999990000000000999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000993300000000000099330000000000009933000000000000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000999999000000000099999900000000009999990000000000999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000993300000000000099330000000000009933000000000000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000999999000000000099999900000000009999990000000000999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000993300000000000099330000000000009933000000000000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000999999000000000099999900000000009999990000000000999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000993300000000000099330000000000009933000000000000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000999999000000000099999900000000009999990000000000999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000993300000000000099330000000000009933000000000000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000999999000000000099999900000000009999990000000000999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000993300000000000099330000000000009933000000000000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000999999000000000099999900000000009999990000000000999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000000000000000000000000000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099999900000000000000000000000000999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000000000000000000000000000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099999900000000000000000000000000999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000000000000000000000000000993300000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000099999900000000000000000000000000999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000993300009933000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000999999009999990099999900000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000FF990000CC660000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC6600000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E2EFF100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000993300009933
      0000993300009933000099330000993300009933000099330000993300009933
      0000993300000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FF990000CC660000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CC66
      000000000000000000000000000000000000000000000000000000000000E2EF
      F100E5E5E500CCCCCC00E5E5E500E2EFF1000000000000000000000000000000
      00000000000000000000000000000000000000000000CC996600FFCC9900FFCC
      9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900CC99
      6600CC9966009933000000000000000000009933000099330000993300009933
      0000CC9966000000000000000000000000000000000099330000993300009933
      0000993300009933000099330000CCCCCC00FF990000CC660000E5E5E500E5E5
      E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500CC66
      00000000000000000000000000000000000000000000E2EFF100E5E5E500B2B2
      B200CC9999009966660099666600B2B2B200CCCCCC00E5E5E500E2EFF1000000
      000000000000000000000000000000000000CC996600CC996600CC996600CC99
      6600CC996600CC996600CC996600CC996600CC996600CC996600CC996600CC99
      660099330000CC9966009933000000000000CCCCCC00CC996600CC660000CC99
      6600000000000000000000000000000000000000000000000000CC660000CC66
      0000CC660000CC663300CCCCCC0000000000FF990000CC660000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CC66
      000000000000000000000000000000000000E5E5E500CC99990099666600CC99
      9900CC999900FFFFFF00996666009999990099999900B2B2B200E5E5E5000000
      000000000000000000000000000000000000CC996600FFFFFF00FFCC9900FFCC
      9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC
      9900CC99660099330000993300000000000000000000E5E5E500CC660000CC66
      3300000000000000000000000000000000000000000000000000CC660000CC66
      0000CC660000FFCCCC000000000000000000FF990000CC660000E5E5E500E5E5
      E500E5E5E500E5E5E500CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CC66
      00000000000000000000000000000000000099666600CC999900FFCC9900FFCC
      9900FFCCCC00FFFFFF0099666600336699003366990033669900E2EFF1000000
      000000000000000000000000000000000000CC996600FFFFFF00FFCC9900FFCC
      9900FFCC9900FFCC990000CC000000990000FFCC99000000FF000000CC00FFCC
      9900CC996600CC99660099330000000000000000000000000000CC996600CC66
      0000CC663300CC663300CC663300CC663300CC66330099663300CC660000CC66
      0000CC666600000000000000000000000000FF990000CC660000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500CC66
      00000000000000000000000000000000000099666600FFCC9900FFCC9900FFCC
      9900FFCCCC00FFFFFF009966660066CCCC0066CCCC000099CC00FFFFFF00FFCC
      CC0000000000000000000000000000000000CC996600FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CC996600CC996600CC996600993300000000000000000000E5E5E500CC66
      000099330000CC996600CC996600CC996600CC663300CC660000CC660000CC66
      0000FFCCCC00000000000000000000000000FF990000CC660000E5E5E500E5E5
      E500E5E5E500CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00CC66
      00000000000000000000000000000000000099666600FFCC9900FFCC9900FFCC
      9900FFCCCC00FFFFFF009966660066CCCC0066CCFF003399CC00FFCCCC00CC66
      000000000000000000000000000000000000CC996600FFFFFF00FFCC9900FFCC
      9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC9900FFCC
      9900CC996600CC996600CC99660099330000000000000000000000000000CC99
      6600CC660000CCCCCC000000000000000000CC663300CC660000CC660000CC66
      660000000000000000000000000000000000FF990000CC660000FFFFFF00FFFF
      FF00FFFFFF00E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500CC66
      00000000000000000000000000000000000099666600FFCC9900CC999900CC99
      6600FFCCCC00FFFFFF009966660099CCCC0099CCFF00B2B2B200FF660000CC66
      00000000000000000000000000000000000000000000CC996600CC996600CC99
      6600CC996600CC996600CC996600CC996600CC996600CC996600CC996600FFCC
      9900FFCC9900CC996600CC99660099330000000000000000000000000000E5E5
      E500CC660000CC66330000000000CCCCCC00CC660000CC660000CC660000FFCC
      CC0000000000000000000000000000000000FF990000CC660000E5E5E500E5E5
      E500CCCCCC00CCCCCC00CC660000CC660000CC660000CC660000CC660000CC66
      0000CC660000CC660000CC660000CC66000099666600FFCC990099666600FFFF
      FF00FFCCCC00FFFFFF009966660099CCCC00C0C0C000CC660000CC660000CC66
      0000CC660000CC660000CC660000000000000000000000000000CC996600FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00CC99
      6600FFCC9900FFCC9900CC996600993300000000000000000000000000000000
      0000CC996600CC660000CCCCCC00CC663300CC660000CC660000CC6666000000
      000000000000000000000000000000000000FF990000CC660000FFFFFF00FFFF
      FF00E5E5E500CC660000FF990000CC660000CC660000CC660000CC660000CC66
      0000CC660000CC660000CC660000CC66000099666600FFCC9900CC9999009966
      6600FFCCCC00FFFFFF009966660000000000CC660000CC660000CC660000CC66
      0000CC660000CC660000CC66000000000000000000000000000000000000CC99
      6600FFFFFF00E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500FFFFFF00CC99
      6600CC996600CC99660099330000000000000000000000000000000000000000
      0000E5E5E500CC66000099663300CC660000FF990000CC660000FFCCCC000000
      000000000000000000000000000000000000FF990000CC660000E5E5E500CCCC
      CC00FF990000FF990000FF990000FF990000FF990000CC660000CC660000CC66
      0000CC660000CC660000CC6600000000000099666600FFCC9900FFCC9900FFCC
      9900FFCCCC00FFFFFF009966660000000000CC999900CC660000CC660000CC66
      0000CC660000CC660000CC66000000000000000000000000000000000000CC99
      6600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00CC9966000000000000000000000000000000000000000000000000000000
      000000000000CC996600FF990000FF990000FF990000CC996600000000000000
      000000000000000000000000000000000000FF990000CC660000FFFFFF00FF99
      0000FF996600FF996600FF990000FF990000FF990000FF990000FF990000CC66
      0000CC660000CC660000000000000000000099666600FFCC9900FFCC9900FFCC
      9900FFCCCC00FFFFFF0099666600CCCCCC00E2EFF100CC999900FF660000CC66
      0000000000000000000000000000000000000000000000000000000000000000
      0000CC996600FFFFFF00E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500FFFF
      FF00CC9966000000000000000000000000000000000000000000000000000000
      000000000000FFCCCC00CC660000FF993300CC660000FFCCCC00000000000000
      000000000000000000000000000000000000FF990000CC660000FF990000FF99
      6600FF996600FF996600FF996600FF996600FF990000FF990000FF990000FF99
      0000CC66000000000000000000000000000099666600FFCC9900FFCC9900FFCC
      9900FFCCCC00FFFFFF009966660099CCCC000000000099CCCC00FFCC9900CC66
      0000000000000000000000000000000000000000000000000000000000000000
      0000CC996600FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00CC99660000000000000000000000000000000000000000000000
      00000000000000000000CC666600FF993300CC99660000000000000000000000
      000000000000000000000000000000000000FF990000FF990000FF996600FF99
      6600FF996600FF996600FF996600FF996600FF996600FF996600FF990000CC66
      00000000000000000000000000000000000099666600CC999900FFCC9900FFCC
      9900FFCCCC00FFFFFF0099666600CCCCCC00000000003399CC0000000000FFCC
      9900000000000000000000000000000000000000000000000000000000000000
      000000000000CC996600CC996600CC996600CC996600CC996600CC996600CC99
      6600CC996600CC99660000000000000000000000000000000000000000000000
      00000000000000000000FFCCCC00CC996600FFCCCC0000000000000000000000
      00000000000000000000000000000000000000000000FF99000099999900FFFF
      FF0099999900FFFFFF0099999900FFFFFF0099999900FFFFFF0099999900FFFF
      FF000000000000000000000000000000000000000000C0C0C000CC996600CC99
      9900CCCC9900FFFFFF00996666000099CC000099CC000099CC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000099999900CCCC
      CC0099999900CCCCCC0099999900CCCCCC0099999900CCCCCC00999999000000
      000000000000000000000000000000000000000000000000000000000000CCCC
      CC00CC9999009966660099666600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B2B2
      B200CCCCCC00CCCCCC00CCCCCC00CCCCCC00CCCCCC00B2B2B200999999000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      9900000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000FF00000000000000000000000000CC999900CCCC
      CC00FFCCCC00FFFFFF00FFFFFF00FFFFCC00FFFFFF00CCCCFF00FFCCCC0099A8
      AC00808080000000000000000000000000000000000000000000000000000000
      0000000000000000000099330000993300009933000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000003333CC000000
      FF00000099000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000099999900B2B2B200FFCC
      CC00FFFFFF00CCFFFF00FFCC9900FF993300CCCCFF00FF993300CC330000CC33
      0000B2B2B2008080800000000000000000000000000000000000000000000000
      0000000000000000000099330000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000003333CC003399
      FF000000FF000000990000000000000000000000000000000000000000000000
      0000000000000000FF0000000000000000000000000099999900CC999900CCCC
      CC00FFCCCC00FFFFFF00FFCC9900FFCC6600FF993300CC330000FF3333009999
      CC00CC330000B2B2B20080808000000000000000000000000000000000000000
      0000000000000000000099330000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000003333
      CC000066FF000000CC0000000000000000000000000000000000000000000000
      00000000FF000000000000000000000000000000000099999900B2B2B200FFCC
      CC00FFFFFF00CCFFFF00FFCC9900FFFF0000FFCC3300FF663300CCCCFF00FFFF
      FF00CC999900CC33000080808000663333000000000000000000000000000000
      0000000000000000000099330000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000CC000000FF0000009900000000000000000000000000000000000000
      FF00000099000000000000000000000000000000000099999900CC999900CCCC
      CC00FFCCCC00FFFFFF00FFCC9900FFCC3300FF993300FF993300FF663300FFFF
      FF00CCCCFF00CC99990080808000663333000000000000000000993300009933
      0000993300009933000099330000CC660000CC66000099330000993300009933
      0000993300009933000000000000000000000000000000000000993300009933
      0000993300009933000099330000993300009933000099330000993300009933
      0000993300009933000000000000000000000000000000000000000000000000
      0000000000000000CC000000FF000000990000000000000000000000FF000000
      9900000000000000000000000000000000000000000099999900B2B2B200FFCC
      CC00FFFFFF00CCFFFF00FFCC9900FF993300FFFFCC00FFFFFF00FFFFFF00CC00
      0000CC000000CC0000008080800066333300000000000000000099330000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      0000CC660000993300000000000000000000000000000000000099330000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      0000CC6600009933000000000000000000000000000000000000000000000000
      000000000000000000000000CC000000FF00000099000000FF00000099000000
      0000000000000000000000000000000000000000000099999900CC999900CCCC
      CC00FFCCCC00FFFFFF00FFFFFF00CCCCFF00FFCC3300CCCCFF00CCFFFF00CCCC
      FF00FF000000CC0000008080800066333300000000000000000099330000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      0000CC660000993300000000000000000000000000000000000099330000CC66
      0000CC660000CC660000CC660000CC660000CC660000CC660000CC660000CC66
      0000CC6600009933000000000000000000000000000000000000000000000000
      00000000000000000000000000000000CC000000FF0000009900000000000000
      0000000000000000000000000000000000000000000099999900B2B2B200FFCC
      CC00FFFFFF00CCFFFF00FFFFFF00FFFFFF00FFCC3300FF993300FF666600FF33
      3300CC000000CC00000080808000663333000000000000000000993300009933
      0000993300009933000099330000CC660000CC66000099330000993300009933
      0000993300009933000000000000000000000000000000000000993300009933
      0000993300009933000099330000993300009933000099330000993300009933
      0000993300009933000000000000000000000000000000000000000000000000
      000000000000000000000000CC000000FF00000099000000CC00000099000000
      0000000000000000000000000000000000000000000099999900CC999900CCCC
      CC00FFCCCC00E5E5E500E5E5E500E5E5E500E5E5E500FF993300FF333300FF00
      0000FFCC6600CC00000080808000663333000000000000000000000000000000
      0000000000000000000099330000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000CC000000FF000000990000000000000000000000CC000000
      990000000000000000000000000000000000000000009999990099A8AC00CCCC
      FF00CCFFFF00CCFFFF00CCFFFF00CCFFFF00CCFFFF00CCCCFF00CCCCCC00CCCC
      CC00CCCCCC00B2B2B20080808000663333000000000000000000000000000000
      0000000000000000000099330000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      CC000000FF000000FF0000009900000000000000000000000000000000000000
      CC00000099000000000000000000000000000000000099999900CCFFFF00646F
      7100333333006633330066333300646F71009966660099999900CCCCCC00FFFF
      FF00FFCCCC00B2B2B20080808000663333000000000000000000000000000000
      0000000000000000000099330000CC660000CC66000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000CC003399
      FF000000FF000000990000000000000000000000000000000000000000000000
      00000000CC000000990000000000000000000000000099999900CCFFFF000066
      99009999CC00CCCCFF009999CC0066333300FFFFFF00CCCC9900CCCC9900646F
      7100B2B2B200CCCCCC0080808000663333000000000000000000000000000000
      0000000000000000000099330000993300009933000099330000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000666699000000
      CC00666699000000000000000000000000000000000000000000000000000000
      000000000000000000000000CC00000000000000000000000000999999000066
      9900FFFFFF00CCCCFF00CCCCFF000033660066333300FFFFFF00FFFFCC00CCCC
      99006633330099A8AC0080808000663333000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000009999
      990000669900FFFFFF00003366000080800000CC660066333300FFFFFF006633
      3300003366009966660099666600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000006699009966660000CCCC003333330099666600663333009966
      660099666600000000000000000000000000424D3E000000000000003E000000
      2800000040000000300000000100010000000000800100000000000000000000
      000000000000000000000000FFFFFF00F83FF83F00000000F39FF39F00000000
      F7DFF7DF00000000F45FF45F00000000F55FF55F00000000F55FF55F00000000
      F55FF55F00000000F55FF55F00000000F55FF55F00000000F55FF55F00000000
      F55FF55F00000000F55FF55F00000000FDDFFDDF00000000FDDFFDDF00000000
      FDDFFDDF00000000FE3FFE3F00000000FFFFFFFF801FFDFFC007FFFF000FE0FF
      80030780000F801F00010FC1000F001F00018FC3000F001F0001C007000F000F
      0000C007000F000F0000E30F000F000F8000E20F00000001C000F01F00000101
      E001F01F00010101E007F83F0003000FF007F83F0007008FF003FC7F000F00AF
      F803FC7F800F803FFFFFFFFFC01FE1FFFFFFFFFFFFFFE01FFFFFFFFFEFFDC007
      FC3FFFFFC7FF8003FC3FFFFFC3FB8001FC3FFFFFE3F78000FC3FFFFFF1E78000
      C003C003F8CF8000C003C003FC1F8000C003C003FE3F8000C003C003FC1F8000
      FC3FFFFFF8CF8000FC3FFFFFE1E78000FC3FFFFFC3F38000FC3FFFFFC7FDC000
      FFFFFFFFFFFFE001FFFFFFFFFFFFF80700000000000000000000000000000000
      000000000000}
  end
  object dsSurvey: TDataSource
    DataSet = QrySurvey
    Left = 205
    Top = 498
  end
  object QrySurvey: TADOQuery
    CacheSize = 1000
    Connection = DM.ADOconn
    Parameters = <>
    Left = 141
    Top = 498
  end
  object dsPactInfo: TDataSource
    DataSet = QryPactInfo
    Left = 53
    Top = 418
  end
  object QryPactInfo: TADOQuery
    Connection = DM.ADOconn
    AfterOpen = QryPactInfoAfterOpen
    Parameters = <>
    Left = 53
    Top = 362
  end
  object dsAllocateBankroll: TDataSource
    DataSet = QryAllocateBankroll
    Left = 149
    Top = 66
  end
  object QryAllocateBankroll: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 149
    Top = 18
  end
  object pmPrint: TPopupMenu
    Images = DM.cxImageList1
    Left = 376
    Top = 296
    object MenuItem7: TMenuItem
      Caption = #25253#34920#35774#35745
      ImageIndex = 7
    end
    object MenuItem8: TMenuItem
      Caption = #25171#21360#39044#35272
    end
    object MenuItem9: TMenuItem
      Caption = #30452#25509#25171#21360
      ImageIndex = 6
    end
    object MenuItem10: TMenuItem
      Caption = '-'
    end
    object Excel1: TMenuItem
      Caption = #36755#20986'Excel'
      OnClick = Excel1Click
    end
    object N4: TMenuItem
      Caption = #25171#21360#25253#34920
      OnClick = N4Click
    end
  end
  object frxPactInfo: TfrxDBDataset
    UserName = #21512#21516#20449#24687
    CloseDataSource = False
    FieldAliases.Strings = (
      #32534#21495'='#32534#21495
      'code=code'
      'numbers=numbers'
      'cxDate='#26085#26399
      'WorkSiteName=WorkSiteName'
      'LabourFirm=LabourFirm'
      'SignName=SignName'
      'WorkDays=WorkDays'
      'UnitPrice=UnitPrice'
      'Overtimer=Overtimer'
      'WorkUnitPrice=WorkUnitPrice'
      'WorkType=WorkType'
      'WorkTotal=WorkTotal'
      'remarks=remarks'
      'CheckCategory=CheckCategory'
      'Explain=Explain'
      'CheckRange=CheckRange')
    DataSource = dsPactInfo
    BCDToCurrency = False
    Left = 61
    Top = 77
  end
  object frxAllocateBankroll: TfrxDBDataset
    UserName = #25320#27454#30003#35831#21333
    CloseDataSource = False
    FieldAliases.Strings = (
      #32534#21495'='#32534#21495
      'code=code'
      'numbers=numbers'
      'cxDate='#26085#26399
      'WorkSiteName=WorkSiteName'
      'LabourFirm=LabourFirm'
      'SignName=SignName'
      'WorkDays=WorkDays'
      'UnitPrice=UnitPrice'
      'Overtimer=Overtimer'
      'WorkUnitPrice=WorkUnitPrice'
      'WorkType=WorkType'
      'WorkTotal=WorkTotal'
      'remarks=remarks'
      'CheckCategory=CheckCategory'
      'Explain=Explain'
      'CheckRange=CheckRange')
    DataSource = dsAllocateBankroll
    BCDToCurrency = False
    Left = 61
    Top = 29
  end
  object frxSurvey: TfrxDBDataset
    UserName = #24037#31243#27010#20917
    CloseDataSource = False
    FieldAliases.Strings = (
      #32534#21495'='#32534#21495
      'code=code'
      'numbers=numbers'
      'cxDate='#26085#26399
      'WorkSiteName=WorkSiteName'
      'LabourFirm=LabourFirm'
      'SignName=SignName'
      'WorkDays=WorkDays'
      'UnitPrice=UnitPrice'
      'Overtimer=Overtimer'
      'WorkUnitPrice=WorkUnitPrice'
      'WorkType=WorkType'
      'WorkTotal=WorkTotal'
      'remarks=remarks'
      'CheckCategory=CheckCategory'
      'Explain=Explain'
      'CheckRange=CheckRange')
    DataSource = dsSurvey
    BCDToCurrency = False
    Left = 61
    Top = 133
  end
  object ADOSignName: TADOQuery
    Connection = DM.ADOconn
    AfterOpen = QryPactInfoAfterOpen
    Parameters = <>
    Left = 56
    Top = 248
  end
  object DataSignName: TDataSource
    DataSet = ADOSignName
    Left = 56
    Top = 304
  end
  object ADOQuery1: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 152
    Top = 120
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 152
    Top = 176
  end
  object ADOQuery2: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 120
    Top = 368
  end
  object DataSource2: TDataSource
    DataSet = ADOQuery2
    Left = 120
    Top = 424
  end
  object ADOQuery3: TADOQuery
    Connection = DM.ADOconn
    Parameters = <>
    Left = 184
    Top = 360
  end
  object DataSource3: TDataSource
    DataSet = ADOQuery3
    Left = 184
    Top = 416
  end
  object DataMaster: TDataSource
    DataSet = Master
    Left = 680
    Top = 240
  end
  object Master: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterOpen = MasterAfterOpen
    AfterClose = MasterAfterClose
    AfterInsert = MasterAfterInsert
    Left = 680
    Top = 184
  end
  object RightKeys: TPopupMenu
    Images = DM.cxImageList1
    Left = 448
    Top = 174
    object N5: TMenuItem
      Caption = #38468#20214
      ImageIndex = 55
      OnClick = N5Click
    end
  end
end
