unit ufrmfastContrctReturnGoods;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, ufrmInputform, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, cxContainer, cxEdit, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxTextEdit, cxCheckBox, cxDBLookupComboBox, cxButtonEdit, cxSpinEdit,
  cxDropDownEdit, cxCurrencyEdit, Vcl.Menus, Datasnap.DBClient, Vcl.ExtCtrls,
  Vcl.StdCtrls, cxButtons, cxDBEdit, cxMemo, Vcl.DBCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, RzPanel, cxLookupEdit, cxDBLookupEdit, cxCalendar,
  cxMaskEdit, cxLabel, cxGroupBox, cxDBNavigator, RzButton;

type
  TfrmfastContrctReturnGoods = class(TfrmInputformBase)
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmfastContrctReturnGoods: TfrmfastContrctReturnGoods;

implementation

{$R *.dfm}

end.
