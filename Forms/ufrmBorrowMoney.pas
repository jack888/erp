unit ufrmBorrowMoney;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls, DynVarsEh,
  PrnDbgeh, Menus, ImgList, ComCtrls, StdCtrls, RzPanel, RzButton, GridsEh,
  DBAxisGridsEh, DBGridEh, RzTabs, dxExtCtrlsStrs,DB, ADODB,
  Mask, RzEdit, RzDBEdit, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,ShellAPI,
  cxTextEdit, cxDBEdit, RzLabel, FileCtrl, RzFilSys, EhLibVCL, TreeFillThrd,TreeUtils,
  Vcl.ExtCtrls;

type
  TfrmBorrowMoney = class(TForm)
    Panel8: TPanel;
    Panel1: TPanel;
    pcSys: TRzPageControl;
    TabSheet1: TRzTabSheet;
    DBGridEh3: TDBGridEh;
    Panel2: TPanel;
    RzBitBtn3: TRzBitBtn;
    RzBitBtn4: TRzBitBtn;
    RzBitBtn5: TRzBitBtn;
    RzBitBtn6: TRzBitBtn;
    RzBitBtn21: TRzBitBtn;
    RzPanel3: TRzPanel;
    Label14: TLabel;
    Label12: TLabel;
    RzBitBtn7: TRzBitBtn;
    RzPanel4: TRzPanel;
    TabSheet3: TRzTabSheet;
    Panel3: TPanel;
    Panel11: TPanel;
    RzBitBtn10: TRzBitBtn;
    RzBitBtn11: TRzBitBtn;
    RzBitBtn13: TRzBitBtn;
    RzBitBtn2: TRzBitBtn;
    RzPanel1: TRzPanel;
    RzPanel2: TRzPanel;
    RzPanel5: TRzPanel;
    RzPanel6: TRzPanel;
    RzPanel7: TRzPanel;
    DBGridEh1: TDBGridEh;
    Panel5: TPanel;
    TreeView1: TTreeView;
    PrintDBGridEh1: TPrintDBGridEh;
    RzBitBtn1: TRzBitBtn;
    DataSource1: TDataSource;
    ADOQuery1: TADOQuery;
    DataSource2: TDataSource;
    ADOQuery2: TADOQuery;
    PopupMenu2: TPopupMenu;
    N4: TMenuItem;
    RzPanel8: TRzPanel;
    N5: TMenuItem;
    PopupMenu3: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    Splitter1: TSplitter;
    PopupMenu1: TPopupMenu;
    MenuItem4: TMenuItem;
    MenuItem7: TMenuItem;
    Panel6: TPanel;
    RzBitBtn16: TRzBitBtn;
    RzBitBtn18: TRzBitBtn;
    RzBitBtn17: TRzBitBtn;
    procedure RzBitBtn21Click(Sender: TObject);
    procedure RzBitBtn3Click(Sender: TObject);
    procedure RzBitBtn4Click(Sender: TObject);
    procedure RzBitBtn5Click(Sender: TObject);
    procedure RzBitBtn6Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TreeView1Click(Sender: TObject);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure DBGridEh3DblClick(Sender: TObject);
    procedure pcSysTabClick(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure DBGridEh1DblClick(Sender: TObject);
    procedure RzBitBtn11Click(Sender: TObject);
    procedure RzBitBtn13Click(Sender: TObject);
    procedure DBGridEh3KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzBitBtn7Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure N2Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem7Click(Sender: TObject);
    procedure RzBitBtn16Click(Sender: TObject);
  private
    { Private declarations }
    g_TableNameTree : string;
    procedure GetSumMoney(lpNumber : string);
  public
    { Public declarations }
    g_ProjectDir_1 : string;
    g_ProjectDir_2 : string;
    g_Postcode : string;
    g_PatTree : TTreeUtils;
    procedure ShowData(AIndex : Byte; ACode : string);
  end;

var
  frmBorrowMoney     : TfrmBorrowMoney;
  g_TableName        : string = 'sk_BorrowMoney';
  g_GiveBorrowMoney  : string = 'sk_GiveBorrowMoney';

implementation

uses
   ufrmMain,
   ufrmBorrowEdit,
   ufrmBorrowPost,
   global,
   uDataModule,
   ufrmGiveBorrowMoney,
   ufrmInputFiles,
   ufunctions;

{$R *.dfm}

procedure TfrmBorrowMoney.GetSumMoney(lpNumber : string);
var
  s : string;
begin
    s := 'select sum(chPrincipal) as t from '+ g_TableName +' as i where i.code in('''+ g_Postcode  + ''') and i.numbers="' + lpNumber + '"' ;
  OutputLog(s);
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
    if RecordCount <>0 then
    begin
      Self.Label14.Caption := FloatToStr( FieldByName('t').AsCurrency ) + ' 元';  //已付金额统计
      Self.RzPanel6.Caption  := '应还本金:' + self.Label14.caption;
    end;
  end;

end;

procedure TfrmBorrowMoney.MenuItem4Click(Sender: TObject);
begin
  Self.RzBitBtn1.Click;
end;

procedure TfrmBorrowMoney.MenuItem7Click(Sender: TObject);
var
  str:string;
begin
  //打开附件目录
//  str := ConcatEnclosure(g_ProjectDir_2,Self.DBGridEh1.DataSource.DataSet.FieldByName('numbers').AsString) ;
  str := getEnclosurePath(g_ProjectDir_2,Self.DBGridEh1);
  CreateOpenDir(Self.Handle,str,True);
end;

procedure TfrmBorrowMoney.DBGridEh1DblClick(Sender: TObject);
begin
  Self.RzBitBtn10.Click;
end;

procedure TfrmBorrowMoney.DBGridEh3DblClick(Sender: TObject);
var
  szNumbers : string;
  s : string;
  DD1 , DD2 : Currency;

begin
  if not Self.DBGridEh3.DataSource.DataSet.IsEmpty then
  begin
    szNumbers := Self.DBGridEh3.DataSource.DataSet.FieldByName('numbers').AsString;
    GetSumMoney(szNumbers);
    s  := 'Select (e.chGiveMonety + e.chInterest) as t,'+ 'e.numbers,' +
                                                          'e.code,'    +
                                                          'e.Parent,'  +
                                                          'e.chName,'       +
                                                          'e.chContent,'    +
                                                          'e.chGiveMonety,' +
                                                          'e.chInterest,'   +
                                                          'e.chPayDate,'    +
                                                          'e.chRemarks,'    +
                                                          'e.编号'    +
                                                          ' from '+ g_GiveBorrowMoney +' as e where e.Parent=' + szNumbers + ''; // + ' ORDER BY e.pay_time ASC'

    OutputLog(s);
    with Self.ADOQuery2 do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      Open;
      Sort := 'chPayDate ASC';
    end;

    s := 'select sum(chGiveMonety) as t from '+ g_GiveBorrowMoney +' as i where i.Parent in('+ szNumbers  +')';
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      Open;
      if RecordCount <>0 then
      begin
        DD1 := FieldByName('t').AsCurrency;
        Self.RzPanel7.Caption  := '已还本金:' + FloatToStr( DD1 ) + ' 元';
      end;
    end;

    s := 'select sum(chInterest) as t from '+ g_GiveBorrowMoney +' as i where i.Parent in('+ szNumbers  +')';
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      Open;
      if RecordCount <>0 then
      begin
        DD2 := FieldByName('t').AsCurrency;
        
        Self.RzPanel8.Caption  := '已还利息:' + FloatToStr( DD2 ) + ' 元';
      end;
    end;
    Self.RzPanel5.Caption := '本息合计:' + FloatTostr(dd1+dd2) + ' 元';

  end
  else
  begin
    Application.MessageBox('选择一条借款后在查看还款明细!!',m_title,MB_OK + MB_ICONQUESTION )
  end;    
end;

procedure TfrmBorrowMoney.DBGridEh3KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then Self.RzBitBtn5.Click;
end;

procedure TfrmBorrowMoney.FormActivate(Sender: TObject);
begin
  g_ProjectDir_1 := Concat( g_Resources , '\BorrowMoney');
  g_ProjectDir_2 := Concat( g_Resources , '\MakeMoney');
end;

procedure TfrmBorrowMoney.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  //inherited;
//  frmMain.OpenBorrowAccount;
  Action := caFree;
end;

procedure TfrmBorrowMoney.FormCreate(Sender: TObject);
begin
  frmBorrowMoney := Self;
end;

procedure TfrmBorrowMoney.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Close;

end;

procedure TfrmBorrowMoney.FormResize(Sender: TObject);
var
  szWidth : Integer;

begin
  {
  Self.RzBitBtn21.Left := Self.Panel2.Width - 30 - Self.RzBitBtn21.Width;
  Self.RzBitBtn8.Left  := Self.RzBitBtn21.Left - 15 - Self.RzBitBtn8.Width;
  Self.RzBitBtn6.Left  := Self.RzBitBtn8.Left - 15 - Self.RzBitBtn6.Width;

  Self.RzBitBtn2.Left := Self.Panel11.Width - 30 - Self.RzBitBtn2.Width;
  Self.RzBitBtn14.Left:= Self.RzBitBtn2.Left- 15 - Self.RzBitBtn14.Width;
  Self.RzBitBtn13.Left:= Self.RzBitBtn14.Left-15 - Self.RzBitBtn13.Width;
  }
  Self.DBGridEh1.FooterParams.RowHeight := 22;
  Self.DBGridEh3.FooterParams.RowHeight := 22;

  szWidth := Round( Self.RzPanel2.Width / 3 );
  Self.RzPanel5.Width := szWidth;
  Self.RzPanel6.Width := szWidth;
  Self.RzPanel7.Width := szWidth;

end;

procedure TfrmBorrowMoney.FormShow(Sender: TObject);
begin
  case g_pacttype of
    1:  g_TableNameTree := 'sk_BorrowMoney_Tree';
    2:  g_TableNameTree := 'sk_Loan_Tree';
  end;
  
  Self.pcSys.TabIndex := 0;
  //g_PatTree := TNewUtilsTree.Create(Self.TreeView1,DM.ADOconn,g_TableNameTree,'往来单位');
  //g_PatTree.GetTreeTable(0);

  g_PatTree := TTreeUtils.Create(Self.TreeView1,
                                      DM.ADOconn,
                                      g_TableNameTree,
                                      'parent',
                                      'PID',
                                      'Name',
                                      m_Code,
                                      m_money ,
                                      m_dwPhoto ,
                                      m_Remarks,
                                      'Price',
                                      'num',
                                      'Input_time',
                                      'End_time',
                                      'PatType',
                                      '借贷档案管理'
                                      );


  g_PatTree.FillTree(g_Parent,g_pacttype);
end;

procedure TfrmBorrowMoney.N2Click(Sender: TObject);
var
  str:string;
begin
  //打开附件目录
  str := getEnclosurePath(g_ProjectDir_1,Self.DBGridEh3);
  CreateOpenDir(Self.Handle,str,True);
end;

procedure TfrmBorrowMoney.N4Click(Sender: TObject);
begin
  pcSys.TabIndex := 1;
  Self.DBGridEh3DblClick(Sender);
end;

procedure TfrmBorrowMoney.N5Click(Sender: TObject);
begin
  Self.RzBitBtn1.Click;  
end;

procedure TfrmBorrowMoney.pcSysTabClick(Sender: TObject);
begin
  case pcSys.TabIndex of
    1: Self.DBGridEh3DblClick(Sender);
  end;
end;

procedure TfrmBorrowMoney.RzBitBtn11Click(Sender: TObject);
begin
  if m_user.dwType = 0 then
  begin
    DBGridDelete(Self.DBGridEh1,Self.ADOQuery2,g_GiveBorrowMoney,g_ProjectDir_2);
    Self.N4.Click;
  end;
end;

procedure TfrmBorrowMoney.RzBitBtn13Click(Sender: TObject);
begin
  if m_user.dwType = 0 then
  begin
     DBGridExport(DBGridEh1);
  end;
end;

procedure TfrmBorrowMoney.RzBitBtn16Click(Sender: TObject);
var
  Child : TfrmInputFiles;
  Node   : TTreeNode;
  szNode : PNodeData;
  szCodeList : string;
  szTableList : array[0..1] of TTableList;

begin
  if m_user.dwType = 0 then
  begin

      Node := Self.TreeView1.Selected;
      if Assigned(Node) then
      begin
        if Node.Level >= 1 then
        begin
          szNode := PNodeData(Node.Data);
          Child := TfrmInputFiles.Create(Self);
          try
            Child.g_PatTree    := g_PatTree;
            Child.g_PostCode   := szNode.Code;
            Child.g_TableName  := g_TableNameTree;
            Child.g_ProjectDir := ''; //附件目录

            if Sender = RzBitBtn16 then //添加
            begin
            //  Child.RzPageControl1.ActivePage := Child.TabSheet5;
              Child.g_IsModify := False;
              Child.Caption := '借款合同添加';
              Child.ShowModal;
            end else
            if Sender = RzBitBtn18 then //编辑
            begin
              g_tabIndex := 5;
            //  Child.RzPageControl1.ActivePage := Child.TabSheet6;
              Child.g_IsModify := True;
              Child.Caption := '借款合同编辑';
              Child.ShowModal;
            end else
            if Sender = RzBitBtn17 then //删除
            begin
              if Node.Level > 1 then
              begin
                szNode := PNodeData(Node.Data);
                if MessageBox(handle, PChar('你确认要删除"' + szNode.Caption + '"'),
                         '提示', MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
                begin
                  szTableList[0].ATableName := 'sk_BorrowMoney';  //借款条目
                  szTableList[0].ADirectory := 'BorrowMoney';

                  szTableList[1].ATableName := 'sk_GiveBorrowMoney'; //还款明细进度
                  szTableList[1].ADirectory := 'MakeMoney';

                  treeCldnode(Node,szCodeList);
                  if DeleteTableFile(szTableList,szCodeList) then
                  begin
                    DM.InDeleteData(g_TableName,szCodeList);

                    g_PatTree.DeleteTree(Node);
                    Self.TreeView1Click(Sender);
                  end;

                end;
              end else
              begin
                Application.MessageBox('顶级帐目禁止删除!',m_title,MB_OK + MB_ICONQUESTION ) ;
              end;

            end;

          finally
            Child.Free;
          end;

        end else begin
          Application.MessageBox('顶级节点禁止新增/编辑!',m_title,MB_OK + MB_ICONQUESTION ) ;
        end;

      end else begin
        Application.MessageBox('请选择合同在点击新增/编辑/删除!',m_title,MB_OK + MB_ICONQUESTION )
      end;

  end;

end;

procedure TfrmBorrowMoney.RzBitBtn1Click(Sender: TObject);
var
  Child : TfrmGiveBorrowMoney;
  s , Code : string;
  Node: TTreeNode;

begin

  if (m_user.dwType = 0) then
  begin
     Child := TfrmGiveBorrowMoney.Create(Self);
     try
       Child.g_ProjectDir := g_ProjectDir_2;
       if Sender = RzBitBtn1 then
       begin
          if not Self.DBGridEh3.DataSource.DataSet.IsEmpty then
          begin
              Child.g_IsModify := False;
              Child.DateTimePicker1.Date := Date;
              with Self.DBGridEh3.DataSource.DataSet do
              begin
                Child.g_PostCode    := FieldByName('Code').AsString;
                Child.g_PostNumbers := FieldByName('numbers').AsString;
                GetSumMoney(Child.g_PostNumbers);
              end;
              Child.ShowModal;
          end else
          begin
            Application.MessageBox('请选择要还的借款条目,一次只能操作一条!',m_title,MB_OK + MB_ICONQUESTION )
          end;

       end else
       if Sender = RzBitBtn10 then
       begin

          if not Self.DBGridEh1.DataSource.DataSet.IsEmpty then
          begin;
            Child.g_IsModify := True;
            with Self.DBGridEh1.DataSource.DataSet do
            begin
              Child.g_PostCode            := FieldByName('Code').AsString;
              Child.g_PostNumbers         := FieldByName('numbers').AsString;
              Child.cxCurrencyEdit1.Value := FieldByName('chGiveMonety').AsCurrency;
              Child.cxCurrencyEdit2.Value := FieldByName('chInterest').AsCurrency;
              Child.DateTimePicker1.Date  := FieldByName('chPayDate').AsDateTime;
              Child.cxMemo1.Text          := FieldByName('chRemarks').AsString;
            end;
            Child.ShowModal;
            Self.DBGridEh3DblClick(Sender);
          end else
          begin
            Application.MessageBox('请选择还款条目,一次只能操作一条!',m_title,MB_OK + MB_ICONQUESTION )
          end;

       end;
     finally
       Child.Free;
     end;

  end;

end;

procedure TfrmBorrowMoney.RzBitBtn21Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmBorrowMoney.RzBitBtn3Click(Sender: TObject);
var
  szfrmBorrowPost : TfrmBorrowPost;
  s , Code : string;
  Node: TTreeNode;
  szNode : PNodeData;

begin

  if (m_user.dwType = 0) then
  begin
     Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin

      if Node.Count = 0 then
      begin
          szNode := PNodeData(Node.Data);

          szfrmBorrowPost := TfrmBorrowPost.Create(Self);
          try
            g_tabIndex := 0;
            szfrmBorrowPost.g_ProjectDir := g_ProjectDir_1;
            szfrmBorrowPost.g_PostCode   := szNode.Code;

            case g_pacttype of
             1:
             begin
               szfrmBorrowPost.Caption := '添加借款';
             end;
             2:
             begin
               szfrmBorrowPost.Caption := '添加贷款';
             end;     
            end;

            szfrmBorrowPost.ShowModal;

          finally
            szfrmBorrowPost.Free;
          end;

          Self.TreeView1Click(Sender);

      end else
      begin
        Application.MessageBox('只能选择最后一级添加',m_title,MB_OK + MB_ICONQUESTION )
      end;  

    end else
    begin
      Application.MessageBox('请选择要还的借款条目,一次只能操作一条!',m_title,MB_OK + MB_ICONQUESTION )
    end;  

  end;

end;

procedure TfrmBorrowMoney.RzBitBtn4Click(Sender: TObject);
var
  Child : TfrmBorrowEdit;
  
begin
  if not Self.DBGridEh3.DataSource.DataSet.IsEmpty then
  begin
     Child := TfrmBorrowEdit.Create(Self);
     try
       Child.g_TableName := g_TableName;
       Child.g_ProjectDir:= g_ProjectDir_1;
       with DBGridEh3 do
       begin

         Child.g_PostNumbers         := DataSource.DataSet.FieldByName('numbers').AsString;
         Child.cxCurrencyEdit1.Value := DataSource.DataSet.FieldByName('chPrincipal').AsCurrency;  //本金
         Child.cxCurrencyEdit4.Value := Child.cxCurrencyEdit1.Value; //利息本金
         Child.cxCurrencyEdit3.Value := DataSource.DataSet.FieldByName('chInterest').AsCurrency;//利息
         Child.cxCurrencyEdit5.Value := DataSource.DataSet.FieldByName('chRate').AsCurrency;//利率
         Child.cxTextEdit1.Text      := DataSource.DataSet.FieldByName('chName').AsString; //借款单位人
         Child.cxCurrencyEdit2.Value := DataSource.DataSet.FieldByName('chRate').AsCurrency; //约定利率
         Child.cxMemo1.Text          := DataSource.DataSet.FieldByName('chContent').AsString; //借款事由
         Child.cxMemo2.Text          := DataSource.DataSet.FieldByName('chRemarks').AsString; //支付内容
         Child.cxDateEdit2.Date      := DataSource.DataSet.FieldByName('chStartDate').AsDateTime; //开始日期
         Child.cxDateEdit1.Date      := DataSource.DataSet.FieldByName('chEndDate').AsDateTime; //结束日期

       end;
       Child.ShowModal;
     finally
       Child.Free;
     end;

    Self.TreeView1Click(Sender);
  end else
  begin
    Application.MessageBox('似乎需要选中点什么才能继续操作!!',m_title,MB_OK + MB_ICONQUESTION )
  end;

end;

procedure TfrmBorrowMoney.RzBitBtn5Click(Sender: TObject);
var
  szParent : string;
  s     : string;
  szDir : string;
begin
  if m_user.dwType = 0 then
  begin
    if not Self.DBGridEh3.DataSource.DataSet.IsEmpty then
    begin
        szParent := Self.DBGridEh3.DataSource.DataSet.FieldByName('numbers').AsString;
        DBGridDelete(Self.DBGridEh3,Self.ADOQuery1,g_TableName,g_ProjectDir_1);
        //删除已还款表中数据
        with DM.Qry do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'Select * from '+ g_GiveBorrowMoney +' where parent=' + szParent;
          Open;
          while not Eof do
          begin
            szDir := ConcatEnclosure(g_ProjectDir_2,  FieldByName('numbers').AsString ) ;
            DeleteDirectory(szDir );
            Next;
          end;

          s := 'delete * from '+ g_GiveBorrowMoney +' where parent=' + szParent;
          Close;
          SQL.Clear;
          SQL.Text := s;
          ExecSQL;
        end;
        Self.TreeView1Click(Sender);
    end else
    begin
      Application.MessageBox('似乎需要选中点什么才能继续操作!!',m_title,MB_OK + MB_ICONQUESTION )
    end;

  end;
end;

procedure TfrmBorrowMoney.RzBitBtn6Click(Sender: TObject);
begin
  if m_user.dwType = 0 then
  begin
    DBGridExport(DBGridEh3);
  end;
end;


procedure TfrmBorrowMoney.RzBitBtn7Click(Sender: TObject);
var
  Node : TTreeNode;
  szNode : PNodeData;
  s : string;

begin
  Node := Self.TreeView1.Selected;
  if Assigned(Node) then
  begin
    if Node.Level > 0 then
    begin

      szNode := PNodeData(Node.Data);
      s      := GetFilePath + m_Photo + m_Pact + szNode.photo;

      if FileExists(s) then
      //  ShellExecute( 0,'Open',PChar(s),nil,nil,sw_show)
      else
        Application.MessageBox('文件丢失无法打开',m_title,MB_OK + MB_ICONQUESTION )

    end;

  end;

end;


procedure TfrmBorrowMoney.ShowData(AIndex : Byte; ACode : string);
var
  s,s1 : string;

begin
  g_Postcode := ACode;
  s1 := 'code in('''+ g_Postcode +'''';
  s  := 'Select (e.chPrincipal + e.chInterest) as t,'+ 'e.numbers,'  +
                                                        'e.code,'    +
                                                        'e.Parent,'  +
                                                        'e.chRate,'  +
                                                        'e.chName,'       +
                                                        'e.chContent,'    +
                                                        'e.chPrincipal,'  +
                                                        'e.chInterest,'   +
                                                        'e.chStartDate,'  +
                                                        'e.chEndDate,'    +
                                                        'e.chEnclosure,'  +
                                                        'e.chRemarks,'    +
                                                        'e.PatType,'      +
                                                        'e.编号'    +
                                                        ' from '+ g_TableName +' as e where e.' + s1 + ') and e.PatType=' + IntToStr( g_pacttype ) ; // + ' ORDER BY e.pay_time ASC'

  OutputLog(s);
  with Self.ADOQuery1 do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
    Sort := 'chStartDate ASC';
  end;

end;

procedure TfrmBorrowMoney.TreeView1Click(Sender: TObject);
var
  szNode : PNodeData;
  szCodeList : string;
  s : string;
  ImgPath : string;
  parent : string;
  Node : TTreeNode;
  
begin
  if m_user.dwType = 0 then
  begin
    Node := Self.TreeView1.Selected;
    if Assigned(Node) then
    begin
      if Node.Level > 0 then
      begin
        szNode := PNodeData(Node.Data);

        Self.RzPanel4.Caption := szNode.Caption;

        treeCldnode(Node,szCodeList);
        if Node.Level = 1 then
        begin
          ShowData(0,szCodeList);
        end
        else
        begin
          ShowData(1,szCodeList);
        end;

      end;

    end;

  end;

end;


end.
