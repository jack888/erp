object frmMaterialStoragePost: TfrmMaterialStoragePost
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #38050#31563#35745#31639#22120
  ClientHeight = 182
  ClientWidth = 566
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyDown = FormKeyDown
  PixelsPerInch = 96
  TextHeight = 13
  object cxGroupBox1: TcxGroupBox
    Left = 8
    Top = 24
    Caption = #38050#31563#35745#31639#22120
    TabOrder = 0
    Height = 97
    Width = 545
    object Label2: TLabel
      Left = 16
      Top = 25
      Width = 52
      Height = 13
      Caption = #38050#31563#22411#21495':'
    end
    object Label5: TLabel
      Left = 163
      Top = 22
      Width = 59
      Height = 13
      Caption = #29702#35770'(kg/m):'
    end
    object Label4: TLabel
      Left = 328
      Top = 22
      Width = 47
      Height = 13
      Caption = ' '#21333#26681'/'#31859':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label14: TLabel
      Left = 204
      Top = 60
      Width = 28
      Height = 13
      Caption = #20214#25968':'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel
      Left = 20
      Top = 63
      Width = 44
      Height = 13
      Caption = #21333#20214'/'#26681':'
    end
    object Label13: TLabel
      Left = 300
      Top = 63
      Width = 48
      Height = 13
      Caption = #37325#37327'('#21544'):'
    end
    object RzComboBox1: TRzComboBox
      Left = 74
      Top = 19
      Width = 65
      Height = 21
      TabOrder = 0
    end
    object cxCurrencyEdit1: TcxCurrencyEdit
      Left = 228
      Top = 19
      Properties.DecimalPlaces = 8
      Properties.DisplayFormat = '0.00#####;-0.00#####'
      Style.BorderStyle = ebs3D
      TabOrder = 1
      Width = 94
    end
    object cxCurrencyEdit2: TcxCurrencyEdit
      Left = 391
      Top = 19
      EditValue = 9.000000000000000000
      Properties.DecimalPlaces = 0
      Properties.DisplayFormat = '0.;-0.'
      Style.BorderStyle = ebs3D
      TabOrder = 2
      Width = 65
    end
    object cxCurrencyEdit7: TcxCurrencyEdit
      Left = 238
      Top = 57
      EditValue = 1.000000000000000000
      Properties.DecimalPlaces = 0
      Properties.DisplayFormat = '0.;-0.'
      Style.BorderStyle = ebs3D
      TabOrder = 3
      Width = 55
    end
    object cxCurrencyEdit3: TcxCurrencyEdit
      Left = 74
      Top = 57
      EditValue = 1.000000000000000000
      Properties.DecimalPlaces = 0
      Properties.DisplayFormat = '0.;-0.'
      Style.BorderStyle = ebs3D
      TabOrder = 4
      Width = 65
    end
    object cxButton1: TcxButton
      Left = 145
      Top = 57
      Width = 42
      Height = 22
      Caption = #35745#31639
      TabOrder = 5
    end
    object cxCurrencyEdit6: TcxCurrencyEdit
      Left = 354
      Top = 57
      Properties.DecimalPlaces = 3
      Properties.DisplayFormat = '0.00#;-0.00#'
      Style.BorderStyle = ebs3D
      TabOrder = 6
      Width = 113
    end
  end
end
