unit ufrmReconciliationTable;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, cxGridBandedTableView, cxGridDBBandedTableView, Vcl.ExtCtrls, RzPanel,
  Vcl.Menus, Vcl.StdCtrls, cxButtons, RzButton, Data.Win.ADODB,ufrmBaseController,
  cxCurrencyEdit, cxCalendar, cxDBEditRepository, cxDropDownEdit, frxClass,
  frxDBSet, cxTextEdit, cxContainer, cxLabel, dxLayoutContainer,
  cxGridViewLayoutContainer, cxGridLayoutView, cxGridCustomLayoutView,
  Vcl.ComCtrls, cxSplitter,UtilsTree,FillThrdTree, cxMaskEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxDBEdit, cxListBox, cxGridDBLayoutView,
  RzTabs , Generics.Collections, cxProgressBar, cxCheckBox, cxGridChartView,
  cxGridDBChartView, dxBarBuiltInMenu, dxNavBarOfficeNavigationBar, cxVGrid,
  cxInplaceContainer, VclTee.TeeGDIPlus, VCLTee.TeEngine, VCLTee.Series,
  VCLTee.TeeProcs, VCLTee.Chart, VCLTee.DBChart, dxCore, cxDateUtils, cxSpinEdit,
  dxmdaset, dxScreenTip, RzBHints, dxCustomHint, cxHint, cxRadioGroup;

type
  TSum = record
    dwSumMoney : Currency;
    dwPrize    : Currency;
    dwbuckle   : Currency;
    dwPaidMoney: Currency;
    dwDetBranchMoney : Currency;
    dwDetCollectMoney: Currency;
  end;
type
  TSumType = record
     dwPrjectName:string;
     dwIndex : Integer;
     dwSumMoney : Currency;
  end;

type
  TfrmReconciliationTable = class(TfrmBaseController)
    RzPanel34: TRzPanel;
    pmSumReport: TPopupMenu;
    N1: TMenuItem;
    DataSumInfo: TDataSource;
    Excel1: TMenuItem;
    Splitter2: TSplitter;
    RzPanel1: TRzPanel;
    RzPanel2: TRzPanel;
    RzPanel3: TRzPanel;
    RzPanel4: TRzPanel;
    ADODet: TADODataSet;
    DataDet: TDataSource;
    ADOSumInfo: TADODataSet;
    ADOSumInfoSignName: TStringField;
    ADOSumInfoInduceType: TStringField;
    ADOSumInfoClearBalance: TCurrencyField;
    ADOSumInfoClearBalanceCapital: TStringField;
    Splitter4: TSplitter;
    RzPanel5: TRzPanel;
    SignNameGrid: TcxGrid;
    tvSignName: TcxGridDBTableView;
    SignNameLv: TcxGridLevel;
    RzToolbar3: TRzToolbar;
    RzSpacer7: TRzSpacer;
    cxLabel3: TcxLabel;
    RzSpacer13: TRzSpacer;
    RzToolButton4: TRzToolButton;
    tvSignNameColumn1: TcxGridDBColumn;
    tvSignNameColumn2: TcxGridDBColumn;
    cxSplitter1: TcxSplitter;
    Tv: TTreeView;
    ADOSumInfoInputDate: TDateTimeField;
    ADOSumInfoPaidMoney: TCurrencyField;
    ADOSumInfoDeductMoney: TCurrencyField;
    ADOSumInfoRewardMoney: TCurrencyField;
    ADOSumInfoSumMoney: TCurrencyField;
    ADOSumLogInfo: TADOQuery;
    DataSumLogInfo: TDataSource;
    ADOCompany: TADOQuery;
    DataCompany: TDataSource;
    DetGrid: TcxGrid;
    tvDet: TcxGridDBTableView;
    DetCol1: TcxGridDBColumn;
    DetCol2: TcxGridDBColumn;
    DetCol3: TcxGridDBColumn;
    DetLv: TcxGridLevel;
    DetCol4: TcxGridDBColumn;
    ADODetSignName: TStringField;
    ADODetSign_Id: TIntegerField;
    ADODetdetDate: TDateField;
    ADODetdetSumMoney: TCurrencyField;
    ADODetdetType: TStringField;
    LayoutViewGroup_Root: TdxLayoutGroup;
    LayoutView: TcxGridDBLayoutView;
    LayoutViewLayoutItem1: TcxGridLayoutItem;
    LayoutView1: TcxGridDBLayoutViewItem;
    LayoutViewLayoutItem2: TcxGridLayoutItem;
    LayoutView2: TcxGridDBLayoutViewItem;
    LayoutViewLayoutItem3: TcxGridLayoutItem;
    LayoutView3: TcxGridDBLayoutViewItem;
    LayoutViewLayoutItem4: TcxGridLayoutItem;
    LayoutView4: TcxGridDBLayoutViewItem;
    ADOSumInfoSign_Id: TIntegerField;
    Search: TcxLookupComboBox;
    ADOSign: TADOQuery;
    DataSign: TDataSource;
    ADODetModuleIndex: TIntegerField;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzPageControl2: TRzPageControl;
    Tab1: TRzTabSheet;
    Tab2: TRzTabSheet;
    RzPanel13: TRzPanel;
    RzToolbar1: TRzToolbar;
    RzSpacer4: TRzSpacer;
    RzToolButton5: TRzToolButton;
    RzSpacer6: TRzSpacer;
    RzToolButton6: TRzToolButton;
    RzSpacer10: TRzSpacer;
    RzToolButton9: TRzToolButton;
    Grid: TcxGrid;
    tvGrid: TcxGridDBTableView;
    tvGridColumn1: TcxGridDBColumn;
    tvGridColumn2: TcxGridDBColumn;
    tvGridColumn3: TcxGridDBColumn;
    tvGridColumn4: TcxGridDBColumn;
    tvGridColumn5: TcxGridDBColumn;
    tvGridColumn6: TcxGridDBColumn;
    tvGridColumn7: TcxGridDBColumn;
    tvGridColumn8: TcxGridDBColumn;
    tvGridColumn9: TcxGridDBColumn;
    tvGridColumn10: TcxGridDBColumn;
    tvGridColumn11: TcxGridDBColumn;
    tvGridColumn12: TcxGridDBColumn;
    tvGridColumn13: TcxGridDBColumn;
    tvGridColumn14: TcxGridDBColumn;
    Lv: TcxGridLevel;
    RzPanel6: TRzPanel;
    RzPanel7: TRzPanel;
    RzPanel8: TRzPanel;
    RzPanel9: TRzPanel;
    RzPanel10: TRzPanel;
    RzPanel11: TRzPanel;
    RzPanel12: TRzPanel;
    RzPanel14: TRzPanel;
    RzPanel15: TRzPanel;
    RzPanel16: TRzPanel;
    RzPanel17: TRzPanel;
    RzPanel18: TRzPanel;
    RzPanel19: TRzPanel;
    RzPanel20: TRzPanel;
    RzPanel21: TRzPanel;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    DetCol5: TcxGridDBColumn;
    pmRigth: TPopupMenu;
    N5: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    N9: TMenuItem;
    qry1: TADOQuery;
    cxLabel6: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    RzSpacer3: TRzSpacer;
    cxLabel8: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    RzSpacer8: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer9: TRzSpacer;
    tvGridColumn15: TcxGridDBColumn;
    ADOSumInforatio: TFloatField;
    SUMData: TdxMemData;
    SUMDataPrjectName: TStringField;
    SUMDataSumMoney: TCurrencyField;
    TabSheet1: TRzTabSheet;
    DBChart1: TDBChart;
    Button2: TButton;
    Series2: TPieSeries;
    FBalloonHint: TBalloonHint;
    cxProgressBar1: TcxProgressBar;
    PaidData: TdxMemData;
    PaidDataPrjectName: TStringField;
    PaidDataPaidMoney: TCurrencyField;
    N10: TMenuItem;
    ChartData: TdxMemData;
    ChartProjectName: TStringField;
    ChartSumMoney: TCurrencyField;
    RzToolbar4: TRzToolbar;
    RzSpacer17: TRzSpacer;
    cxLabel5: TcxLabel;
    cxComboBox1: TcxComboBox;
    RzSpacer18: TRzSpacer;
    RzToolButton8: TRzToolButton;
    pmDetReport: TPopupMenu;
    Exel1: TMenuItem;
    N11: TMenuItem;
    pmLogReport: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    RzSpacer12: TRzSpacer;
    RzToolButton10: TRzToolButton;
    pmLog: TPopupMenu;
    pmDet: TPopupMenu;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    TabSheet2: TRzTabSheet;
    RzToolbar2: TRzToolbar;
    RzSpacer14: TRzSpacer;
    RzToolButton14: TRzToolButton;
    RzSpacer16: TRzSpacer;
    RzSpacer5: TRzSpacer;
    RzSpacer15: TRzSpacer;
    RzToolButton7: TRzToolButton;
    RzSpacer19: TRzSpacer;
    cxLabel2: TcxLabel;
    cxLabel1: TcxLabel;
    cxDateEdit3: TcxDateEdit;
    cxLabel4: TcxLabel;
    cxDateEdit4: TcxDateEdit;
    cxLookupComboBox1: TcxLookupComboBox;
    LogInfoGrid: TcxGrid;
    tvLogInfoView: TcxGridDBTableView;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGridDBColumn8: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGridDBColumn12: TcxGridDBColumn;
    cxGridDBColumn13: TcxGridDBColumn;
    cxGridDBColumn14: TcxGridDBColumn;
    cxGridDBColumn15: TcxGridDBColumn;
    cxGridDBColumn16: TcxGridDBColumn;
    cxGridDBColumn17: TcxGridDBColumn;
    cxGridDBColumn19: TcxGridDBColumn;
    cxGridDBColumn20: TcxGridDBColumn;
    cxGridDBColumn21: TcxGridDBColumn;
    tvLogInfoViewColumn1: TcxGridDBColumn;
    tvLogInfoViewColumn2: TcxGridDBColumn;
    tvLogInfoViewColumn3: TcxGridDBColumn;
    cxGridDBColumn18: TcxGridDBColumn;
    tvLogInfoViewColumn4: TcxGridDBColumn;
    LogInfoLv: TcxGridLevel;
    RzSpacer11: TRzSpacer;
    RzToolButton11: TRzToolButton;
    RzSpacer20: TRzSpacer;
    RzToolButton12: TRzToolButton;
    tmr: TTimer;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzToolButton6Click(Sender: TObject);
    procedure tvGridColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure N1Click(Sender: TObject);
    procedure tvGridColumn7CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure N2Click(Sender: TObject);
    procedure N3Click(Sender: TObject);
    procedure tvGridKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvGridColumn10CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure tvGroupViewColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton9Click(Sender: TObject);
    procedure RzPanel34Resize(Sender: TObject);
    procedure tvLogInfoViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzToolButton4Click(Sender: TObject);
    procedure SearchKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzPanel5Resize(Sender: TObject);
    procedure TvClick(Sender: TObject);
    procedure tvSignNameDblClick(Sender: TObject);
    procedure tvGridColumn5CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure tvGridEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvGridCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure tvGridColumn13PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvGridColumn14PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure RzPanel6Resize(Sender: TObject);
    procedure tvSignNameEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvSignNameColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure DetCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvDetEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvLogInfoViewCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure tvDetStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure DetCol1CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure LayoutView1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure SearchPropertiesCloseUp(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure ADOSumInfoAfterInsert(DataSet: TDataSet);
    procedure N5Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure Excel1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure RzPanel13Resize(Sender: TObject);
    procedure RzToolButton3Click(Sender: TObject);
    procedure er(Sender: TObject);
    procedure RzPanel15MouseLeave(Sender: TObject);
    procedure RzToolButton7Click(Sender: TObject);
    procedure cxComboBox1PropertiesCloseUp(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure Exel1Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure tvLogInfoViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems5GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure RzToolButton12Click(Sender: TObject);
    procedure tmrTimer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure RzToolButton2Click(Sender: TObject);
  private
    { Private declarations }
    IsDateQuery : Boolean;
    dwIsSumDetailed : Boolean;
    dwSumType : Byte;
    dwSumMoney  : Currency;
    dwbuckle    : Currency;
    dwPrize     : Currency;
    dwPaidMoney : Currency;
    dwDetBranchMoney  : Currency;
    dwDetCollectMoney : Currency;

    g_Prefix : string;
    g_TreeView : TNewUtilsTree;
    g_Total  : Currency;

    function OutSumMoney( lpSignName : string;
                          lpSumMoney ,
                          lpPaidMoney ,
                          lpDeductMoney ,
                          lpRewardMoney ,
                          lpClearMoney  : Currency):Boolean;
    function SearchLog(lpIndex : Byte ; lpSignName : string):Boolean;
    function GetDetInfo(lpIndex : Byte; lpSignName : string):Currency;
    function GetSumMoney(lpIndex : Byte;lpSignName : string):Boolean;
    function PersonalQuery(lpSignName : string):Boolean;
  public
    { Public declarations }
    dwSum : TObjectDictionary<string,TSum>;
    dwProjectSum : TObjectDictionary<string,Currency>;
    ProjectName : array[0..24] of string;
    g_Suffix : string;
  end;

var
  frmReconciliationTable: TfrmReconciliationTable;
  g_TableName : string = 'sk_ReconciliationTable';
  g_ReconciliationTable  : string = 'ReconciliationTable';

implementation

uses
  uDataModule,ufrmIsViewGrid,global,ufrmSpareMoney,ufrmSumDetailed ,TeePrevi,System.Math;

{$R *.dfm}
{
Screen.Cursor := crHourGlass;
 cxGrid2DBTableView1.DataController.GotoFirst;
 for i := 0 to cxgrid2DBTableView1.DataController.FilteredRecordCount - 1 do
 begin
   if cxGrid2DBTableView1.DataController.GetItemByFieldName('IsChecked').EditValue  = True then
   begin
     cxGrid2DBTableView1.DataController.GetItemByFieldName('IsChecked').EditValue := False;
   end
   else if cxGrid2DBTableView1.DataController.GetItemByFieldName('IsChecked').EditValue  = False then
   begin
     cxGrid2DBTableView1.DataController.GetItemByFieldName('IsChecked').EditValue  := True;
   end;

   cxGrid2DBTableView1.DataController.GotoNext;
 end;
 Screen.Cursor := crDefault;
//cxgrid2DBTableView1.DataController.FilteredRecordCount这个是得到筛选后的数据行数
//cxGrid2DBTableView1.DataController.GetItemByFieldName('IsChecked').EditValue这个是得到要修改的字段的值　
}

function TfrmReconciliationTable.PersonalQuery(lpSignName : string):Boolean;
var
  s : string;
begin
  s := lpSignName;
  if (Length( s ) = 0) then
  begin
    Application.MessageBox( '往来单位为空无法查询', '提示:',
             MB_OKCANCEL + MB_ICONWARNING);
    Exit;
  end else
  begin

    GetSumMoney(0,lpSignName);
  end;
end;


function IsAction(DataSet : TADODataSet):Boolean;
begin
  with DataSet do
  begin
    if Active then Active := False;

    CreateDataSet;
  end;
end;

function GetSignNameId(lpSignName : string):Integer;
begin
  Result := 0;
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select Id from ' + g_Table_CompanyManage + ' Where SignName="'  + lpSignName + '"';
    Open;
    if RecordCount <> 0 then
    begin
      Result := FieldByName('Id').AsInteger;
    end;
  end;
end;

procedure TfrmReconciliationTable.SearchKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then Self.RzToolButton4.Click;
end;

function TfrmReconciliationTable.SearchLog(lpIndex : Byte ; lpSignName : string):Boolean;
var
  SqlText , s : string;
begin
  {
  0 : 个人日志
  1 : 全部日志
  2 : 日期范围
  }
  case lpIndex of
    0: s := ' AND A.SignName Like"%' + lpSignName + '%"';
    2:
    begin
      if Length(lpSignName) <> 0 then
        s := ' AND A.SignName Like"%' + lpSignName + '%" and InputDate between :t1 and :t2'
      else
        s := ' AND InputDate between :t1 and :t2';
    end;

  end;
  {
  SqlText :=  'select A.Id,'+
                     'A.InputDate,'+
                     'A.Sign_Id,' +
                     'A.InduceType,'+
                     'A.SumMoney,'+
                     'A.DeductMoney,'+
                     'A.RewardMoney,'+
                     'B.SignName'+
              ' from '+ g_TableName + ' AS A'+
              ' right join ' + g_Table_CompanyManage +' AS B on A.Sign_Id=B.ID'+
              ' WHERE B.SignName Like"%' + lpSignName +'%" OR B.PYCode="' + lpSignName + '"';
  }
  SqlText :=  'select A.Id,'+
                     'A.InputDate,'  +
                     'A.Sign_Id,'    +
                     'A.InduceType,' +
                     'A.SumMoney,'   +
                     'A.DeductMoney,'+
                     'A.RewardMoney,'+
                     'A.SignName,'   +
                     'A.ModuleType,' +
                     'A.ClearBalance,'+
                     'A.ClearBalanceCapital' +
                     //'B.SignName'    +
              ' from '+ g_TableName + ' AS A'+
              //' right join ' + g_Table_CompanyManage +' AS B on A.Sign_Id=B.ID'+
              ' WHERE ModuleType=0 ' + s;

  Self.RzPanel3.Caption := lpSignName;

  with Self.ADOSumLogInfo do
  begin
    Close;
    SQL.Clear;
    SQL.Text := SqlText;

    if lpIndex = 2 then
    begin
      Parameters.ParamByName('t1').Value := Self.cxDateEdit3.Text;  // StrToDate('2016-4-17 00:00:00');
      Parameters.ParamByName('t2').Value := Self.cxDateEdit4.Text;
    end;
    Open;

  end;

end;

procedure TfrmReconciliationTable.SearchPropertiesCloseUp(Sender: TObject);
var
  s : string;
begin
  inherited;
  Self.RzToolButton4.Click;
end;

procedure TfrmReconciliationTable.tmrTimer(Sender: TObject);
var
  I: Integer;
begin
  inherited;
  tmr.Enabled := False;
  RzToolButton2.Enabled := True;
  with Self.ADOSign  do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select *from ' + g_Table_CompanyManage;
    Open;
  end;
  GetSumMoney(2,''); //汇总总额
end;

procedure TfrmReconciliationTable.TvClick(Sender: TObject);
var
  Node : TTreeNode;
  I: Integer;
  szData : PNodeData;
  szCodeId : Integer;

begin
  inherited;
  Node := Self.Tv.Selected;
  if Assigned(Node) then
  begin
    if Node.Level > 0 then
    begin
      szData := PNodeData(Node.Data);
      if Assigned(szData) then
      begin
        szCodeId := szData^.Index;

        with Self.ADOCompany do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'Select * from ' + g_Table_CompanyManage + ' WHERE ' + 'Sign_Id' + '=' + IntToStr(szCodeId) ;
          Open;
        end;

      end;

    end;

  end;

end;

function GetGroupSumMoney(lpSelect , lpTableName , lpWhereName , lpGroupName : string):Currency;
var
  s : string;
begin
  Result := 0;

  s := 'SELECT ' + lpSelect + ' FROM ' + lpTableName + ' WHERE ' + lpWhereName + '  GROUP BY  ' + lpGroupName;

  OutputLog(s);
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;

    if RecordCount <> 0  then
    begin
      Result := FieldByName('t').AsCurrency;
    end;

  end;

end;

procedure TfrmReconciliationTable.tvLogInfoViewCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
//var
//   ATextToDraw:String;
//   ARec: TRect;
begin
  inherited;
  {
  ATextToDraw := AViewInfo.GridRecord.DisplayTexts[AViewInfo.Item.Index];
  ARec := AViewInfo.Bounds;

  if AViewInfo.GridRecord.Values[6]<1000 then
    ACanvas.Canvas.Font.Color := clRed;
  }
  //整行变色：ACanvas.Canvas.brush.color:=clred;
  {列的颜色交替
    if AViewInfo.Item.Index mod 2 = 0 then
      ACanvas.Canvas.brush.color := clInfoBk
    else
    ACanvas.Canvas.brush.color := clInactiveCaptionText;
  }

  //行的颜色交替
//  if AViewInfo.RecordViewInfo.Index mod 2 = 0 then
//    ACanvas.Canvas.brush.color := $02E0F0D7
//  else
//    ACanvas.Canvas.brush.color := $02F0EED7;

//  ACanvas.Canvas.FillRect(ARec);
end;

procedure TfrmReconciliationTable.tvLogInfoViewEditing(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  var AAllow: Boolean);
begin
  inherited;
  AAllow := False;
end;

procedure TfrmReconciliationTable.tvLogInfoViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems5GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  s : string;
begin
  inherited;
  s := '';
  if AValue <> null then
  begin
    s := VarToStr(AValue);
    s := MoneyConvert( AValue );
  end;
  tvLogInfoViewColumn4.Summary.FooterFormat := s;
end;

procedure TfrmReconciliationTable.ADOSumInfoAfterInsert(DataSet: TDataSet);
begin
  inherited;
  Self.tvGridColumn4.EditValue := '成本';
end;

procedure TfrmReconciliationTable.Button2Click(Sender: TObject);
begin
  inherited;
//  ChartPreview(Parent,DBChart1);
  TeePreview(Parent,DBChart1);
end;

procedure TfrmReconciliationTable.cxComboBox1PropertiesCloseUp(Sender: TObject);
var
  s : string;
begin
  inherited;
  s := (Sender as TcxComboBox).Text;
  GetDetInfo(3,s);
end;

procedure TfrmReconciliationTable.cxLookupComboBox1PropertiesCloseUp(
  Sender: TObject);
var
  s : string;

begin
  inherited;
  s := (Sender as TcxLookupComboBox).Text;
  SearchLog(0,s);
end;

procedure TfrmReconciliationTable.DetCol1CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  //  ACanvas.Font.Style := [fsBold];
  //  ACanvas.Brush.Color:= clRed;
  //  ACanvas.Font.Color := clWhite;
end;

procedure TfrmReconciliationTable.DetCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmReconciliationTable.Excel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'汇总明细表');
end;

procedure TfrmReconciliationTable.Exel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.LogInfoGrid,'债务明细表');
end;

procedure TfrmReconciliationTable.FormActivate(Sender: TObject);
begin
  inherited;
  g_TreeView := TNewUtilsTree.Create(Self.tv,DM.ADOconn,g_Table_CompanyTree,'往来单位');
  g_TreeView.GetTreeTable(0);
  ADOSumInfo.CreateDataSet;

  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
  Self.cxDateEdit3.Date := Date;
  Self.cxDateEdit4.Date := Date;

  dwSum := TObjectDictionary<string,TSum>.Create();
  dwProjectSum := TObjectDictionary<string,Currency>.Create();

  ProjectName[0]  := '成本对帐-债务支款';
  ProjectName[1]  := '成本对帐-债务收款';

  ProjectName[2]  := '公司管理-进销管理';
  ProjectName[3]  := '成本管理-合同总价';
  ProjectName[4]  := '施工管理-劳务分包';
  ProjectName[5]  := '施工管理-仓库统计';
  ProjectName[6]  := '施工管理-商混统计';
  ProjectName[7]  := '施工管理-租赁统计';
  ProjectName[8]  := '施工管理-土石方统计';
  ProjectName[9]  := '施工管理-分包统计';
  ProjectName[10] := '施工管理-奖励统计';
  ProjectName[11] := '施工管理-扣罚统计';

  ProjectName[13] := '公司管理-考勤加班';
  ProjectName[14] := '公司管理-奖励';
  ProjectName[15] := '公司管理-罚款';
  ProjectName[16] := '公司管理-员工考勤';

  ProjectName[12] := '成本管理-交易统计';
  ProjectName[17] := '流水帐统计';

  ProjectName[18] := '报价管理-设计师提成';
  ProjectName[19] := '报价管理-设计师分成';
  ProjectName[20] := '报价管理-营销员提成';
  ProjectName[21] := '报价管理-支出项目表';
  ProjectName[22] := '报价管理-成本核算';
  ProjectName[23] := '报价管理-主材核算';
  ProjectName[24] := '报价管理-人工核算';




  tmr.Enabled := True;
end;

procedure TfrmReconciliationTable.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  {
  Self.SUMData.Active   := False;
  Self.ChartData.Active := False;
  Self.ADOSumInfo.Close;
  dwSum.Free;
  }
  dwSum.Free; //泛清空
  dwProjectSum.Free;
  Action := caFree;
end;

procedure TfrmReconciliationTable.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  inherited;
  tmr.Enabled := False;
end;

procedure TfrmReconciliationTable.FormCreate(Sender: TObject);
begin
  inherited;
  RzPageControl2.ActivePage := Self.Tab2;
end;

procedure TfrmReconciliationTable.N11Click(Sender: TObject);
begin
  inherited;
  with DM do
  begin
    BasePrinterLink1.Component := Self.DetGrid;
    BasePrinter.Preview(True, nil);
  end;

end;

procedure TfrmReconciliationTable.N1Click(Sender: TObject);
begin
  inherited;
//  frxReport.DesignReport();
  with DM do
  begin
    BasePrinterLink1.Component := Self.Grid;
  //  BasePrinterLink1.RealPrinterPage.DMPaper := 241 * 280;
  //  DM.BasePrinterLink1.ReportTitleText := '公司名称：' + Self.tvCompanyColumn2.EditValue;
   // DM.BasePrinter.CurrentLink.PrinterPage.PageSize.Create(241,279.4);
    BasePrinter.Preview(True, nil);
  end;

end;

procedure TfrmReconciliationTable.N2Click(Sender: TObject);
begin
  inherited;
  if Self.tvLogInfoView.Controller.FocusedRowIndex >= 0 then
  begin
    IsDeleteData(Self.tvLogInfoView,
                 g_ReconciliationTable,
                 g_TableName,
                 Self.cxGridDBColumn5.DataBinding.FieldName);
    Self.ADOSumLogInfo.Requery(); //刷新
  end;
end;


procedure DeleteData(lpGrid : TcxGridDBTableView ; lpDir  , lpTableName , lpFieldName : string );
var
  Id : string;
  szRecordCount : Integer;
  str , dir :string;
  I: Integer;
  adoTmp : TADOQuery;
  szCount : Integer;
  szRowsCount : string;
  szIndex : Integer;
  s : Variant ;

begin
  szCount := lpGrid.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if szCount > 0 then
  begin
      if  Application.MessageBox(PWideChar( '您共选择: ( '+ szRowsCount +' ) 条数据, 确定要删除所有选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
      begin

        with lpGrid  do
        begin
          if not DataController.DataSource.DataSet.IsEmpty then
          begin
            str := '';
            for I := szCount -1 downto 0 do
            begin
              szIndex := GetColumnByFieldName(lpFieldName).Index;

              s := Controller.SelectedRows[i].Values[szIndex];
              Id := VarToStr(s);

              if Length(id) <> 0 then
              begin

                if Length(lpDir) <> 0 then
                begin
                  dir := ConcatEnclosure(lpDir, id);
                  if DirectoryExists(dir) then
                  begin
                    DeleteDirectory(dir);
                  end;

                end;

                if Length(str) = 0 then
                  str := Id
                else
                  str := str + '","' + Id;

              end else
              begin
                Controller.DeleteSelection;
              end;

            end;

            if (str <> ',') and (Length(str) <> 0) then
            begin

              with DM.Qry do
              begin

                Close;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  lpTableName +' where '+ lpFieldName +' in("' + str + '") AND ModuleIndex=0';

                ExecSQL;

              end;

            end;

          end;

        end;

      end;
  end;

end;

procedure TfrmReconciliationTable.N3Click(Sender: TObject);
begin
  inherited;
  if Self.tvDet.Controller.FocusedRowIndex >= 0 then
  begin
    //债务删除
    //'Select * from '+ g_Table_DetList +' WHERE ModuleIndex=0'
    DeleteData(Self.tvDet,
                 g_Resources + '\det\',
                 g_Table_DetList,
                 Self.DetCol5.DataBinding.FieldName);
    Self.ADODet.Requery();

  end;
end;

procedure TfrmReconciliationTable.N4Click(Sender: TObject);
var
  s : string;
  FileDir : string;

begin
  inherited;
  if Self.tvDet.Controller.FocusedRowIndex >= 0 then
  begin
    s := Self.ADODetSignName.AsString;
    FileDir := Concat(g_Resources,'\det\' + s);
    CreateOpenDir(Handle,FileDir,True);
  end;
end;

procedure TfrmReconciliationTable.N5Click(Sender: TObject);
var
  szName : Variant;

begin
  inherited;
  if Self.tvGrid.Controller.FocusedRowIndex >= 0 then
  begin
    szName := Self.tvGridColumn3.EditValue;
    if szName <> null then
    begin
      dwIsSumDetailed := False;
      dwSumType := 0;
    //  GetSumMoney(0, VarToStr(szName));
      PersonalQuery( VarToStr(szName) );
    end;
  end;
end;

procedure TfrmReconciliationTable.N8Click(Sender: TObject);
var
  szName : Variant;
  Child : TfrmSumDetailed;

begin
  inherited;
  Self.PaidData.Active := False;
  Self.SUMData.Active  := False;
  Self.ChartData.Active:= False;

  if Self.tvGrid.Controller.FocusedRowIndex >= 0 then
  begin
    szName := Self.tvGridColumn3.EditValue;
    if szName <> null then
    begin
      dwIsSumDetailed := True;
      dwSumType := 0;
      //汇总价
      GetSUMMoney(4, VarToStr(szName) );
      dwIsSumDetailed := False;

      Child := TfrmSumDetailed.Create(Nil);
      szName := Child.Caption + ' - ' + szName;
      Child.Caption := szName;
      Child.Lv2.Caption := '已付金额';
      try
        if Sender = Self.N8 then
        begin
          Child.Grid.ActiveLevel := Child.Lv1;
        end else
        if Sender = Self.N9 then
        begin
          Child.Grid.ActiveLevel := Child.Lv2;
        end;
        Child.SumMem.CopyFromDataSet(Self.SUMData);
        Child.PaidMem.CopyFromDataSet(Self.PaidData);
        Child.ChartData.CopyFromDataSet(Self.ChartData);

        Child.ShowModal;
      finally
        Child.Free;
      end;

    end;
  end;
end;

procedure TfrmReconciliationTable.RzPanel13Resize(Sender: TObject);
var
  sumWidth : Integer;
  Width : Integer;
begin
  inherited;
  sumWidth := Self.RzPanel14.Width + Self.RzPanel16.Width + Self.RzPanel18.Width + Self.RzPanel20.Width;
  Width := Trunc( (Self.RzPanel13.Width - sumWidth) / 4 );

  Self.RzPanel15.Width := Width;
  Self.RzPanel17.Width := Width;
  Self.RzPanel19.Width := Width;
  Self.RzPanel21.Width := Width;
end;

procedure TfrmReconciliationTable.er(Sender: TObject);
var
  P1,P2: TPoint;
  Rect: TRect;
  Edit: TControl;
begin
  {
  FBalloonHint.Title := '大写';
  FBalloonHint.Description := (Sender as TRzPanel).Hint;
  Edit := (Sender as TRzPanel); //可以指定任何组件
  P1.X := Edit.Left;
  P1.Y := Edit.Top;

  P2.X := Edit.Left+Edit.Width;
  P2.Y := Edit.Top+Edit.Height;
  P1 := self.ClientToScreen(P1);
  P2 := self.ClientToScreen(P2);
  Rect.TopLeft := P1;
  Rect.BottomRight := P2;
  FBalloonHint.ShowHint(Rect);
  }
end;

procedure TfrmReconciliationTable.RzPanel15MouseLeave(Sender: TObject);
begin
  inherited;
//  FBalloonHint.HideHint;
end;

procedure TfrmReconciliationTable.RzPanel34Resize(Sender: TObject);
begin
  inherited;
  Self.RzPanel4.Width := 200;
end;

procedure TfrmReconciliationTable.RzPanel5Resize(Sender: TObject);
begin
  inherited;
  Self.Search.Width := Self.RzToolbar3.Width - 130;
end;

procedure TfrmReconciliationTable.RzPanel6Resize(Sender: TObject);
var
  szSumWidth : Double;
  d : Double;

begin
  inherited;
   d := Self.RzPanel6.Width  -
        Self.RzPanel11.Width -
        Self.RzPanel8.Width  -
        Self.RzPanel9.Width;

  szSumWidth := d  / 3;

  Self.RzPanel12.Width := Round(szSumwidth);
  Self.RzPanel7.Width  := Round(szSumwidth);
  Self.RzPanel10.Width := Round(szSumwidth);
end;

procedure TfrmReconciliationTable.RzToolButton12Click(Sender: TObject);
begin
  inherited;
  SearchLog(1,'');
end;

procedure TfrmReconciliationTable.RzToolButton1Click(Sender: TObject);
begin
  inherited;
  GetMaintainInfo;
  ShowMessage('信息刷新完成!');
end;

procedure TfrmReconciliationTable.RzToolButton2Click(Sender: TObject);
begin
  inherited;
  Self.RzToolButton2.Enabled := False;
  GetSumMoney(1,'');
  Self.RzToolButton2.Enabled := True;
end;

function TfrmReconciliationTable.GetSumMoney(lpIndex : Byte;lpSignName : string):Boolean;

  function UpdateSumMoney(lpIndex : Byte; lpSignName : string;
                          lpSumMoney,
                          lpPaidMoney,
                          lpDeductMoney,
                          lpRewardMoney,
                          lpClearMoney: Currency;
                          lpRatio : Double
                          ):Boolean;
  var
    a , b , c , d : Currency;

  begin

    with Self.ADOSumInfo do
    begin

      if Self.ADOSumInfo.Locate(Self.tvGridColumn3.DataBinding.FieldName,lpSignName,[loCaseInsensitive, loPartialKey]) then
      begin
        Edit;
          FieldByName(Self.ADOSumInfoSumMoney.FieldName).Value     := lpSumMoney;
          FieldByName(Self.ADOSumInfoPaidMoney.FieldName).Value    := lpPaidMoney;
          FieldByName(Self.ADOSumINfoDeductMoney.FieldName).Value  := lpDeductMoney;
          FieldByName(Self.ADOSumInfoRewardMoney.FieldName).Value  := lpRewardMoney;
          FieldByName(Self.ADOSumInfoClearBalance.FieldName).Value := lpClearMoney; //清算余额
          FieldByName(Self.ADOSumInfoClearBalanceCapital.FieldName).Value:= MoneyConvert( lpClearMoney );
          FieldByName(Self.ADOSumInforatio.FieldName).Value := lpRatio;
        Post;
      end else
      begin
        Append;
          FieldByName(Self.ADOSumInfoSignName.FieldName).Value     := lpSignName;
          FieldByName(Self.ADOSumInfoInputDate.FieldName).Value    := Date; //对帐日期
          FieldByName(Self.ADOSumInfoSumMoney.FieldName).Value     := lpSumMoney;
          FieldByName(Self.ADOSumInfoPaidMoney.FieldName).Value    := lpPaidMoney;
          FieldByName(Self.ADOSumINfoDeductMoney.FieldName).Value  := lpDeductMoney;
          FieldByName(Self.ADOSumInfoRewardMoney.FieldName).Value  := lpRewardMoney;
          FieldByName(Self.ADOSumInfoClearBalance.FieldName).Value := lpClearMoney; //清算余额
          FieldByName(Self.ADOSumInfoClearBalanceCapital.FieldName).Value:= MoneyConvert( lpClearMoney );
          FieldByName(Self.ADOSumInforatio.FieldName).Value := lpRatio;
        Post;
      end;

    end;

  end;

  function GetList(lpSQLtext : string;lpParentIndex, lpIndex : Byte ; lpPrjectName : string ):Boolean;
  var
    szName : string;
    t , money : Currency;
    i : Double;
    szSum : TSum;
    szIsExist : Boolean;
    a , b , c , d : Currency;
    szSumType : TSumType;
  begin
      with DM.Qry do
      begin
        Close;
        if IsDateQuery then
        begin
          SQL.Text := lpSQLtext;
          Parameters.ParamByName('t1').Value := Self.cxDateEdit1.Text;  // StrToDate('2016-4-17 00:00:00');
          Parameters.ParamByName('t2').Value := Self.cxDateEdit2.Text;
        end else
        begin
          SQL.Text := lpSQLtext
        end;

        Open;
        if 0 <> RecordCount then
        begin
          Self.cxProgressBar1.Properties.Max := RecordCount;
          Self.cxProgressBar1.Visible := True;
          ZeroMemory(@szSum,SizeOf(TSum));

          while not Eof do
          begin
            Application.ProcessMessages;
            szName := FieldByName('c').AsString;
            t := FieldByName('t').AsCurrency;
            i := i + 1;
            Self.cxProgressBar1.Position := i;
            szSum.dwSumMoney  := 0;
            szSum.dwPrize     := 0;
            szSum.dwbuckle    := 0;
            szSum.dwPaidMoney := 0;
            szSum.dwDetBranchMoney := 0;
            szSum.dwDetCollectMoney:= 0;
            //汇总明细
            if dwIsSumDetailed then
            begin
              try
                with dwProjectSum do
                begin
                  if ContainsKey(lpPrjectName) then
                  begin
                    Items[lpPrjectName] := t;
                  end;
                end;
              except
                 on e:Exception do
                 begin
                    ShowMessage('AMsg:' + e.Message);
                 end;

              end;
            end else
            begin
              try
                if Assigned(dwSum) then
                begin

                  with dwSum do
                  begin
                    szIsExist := False;

                    if ContainsKey(szName) then
                    begin
                      szIsExist := True;
                      szSum := Items[szName];
                      case lpIndex of
                        0: szSum.dwSumMoney := szSum.dwSumMoney  + t;  //帐目总价
                        1: szSum.dwPaidMoney:= szSum.dwPaidMoney + t;  //支款统计
                        2: szSum.dwPrize    := szSum.dwPrize     + t;  //奖励统计
                        3: szSum.dwbuckle   := szSum.dwbuckle    + t;  //扣罚统计
                        4: szSum.dwDetBranchMoney := szSum.dwDetBranchMoney + t; //债务支款
                        5: szSum.dwDetCollectMoney:= szSum.dwDetCollectMoney+ t; //债务收款
                      end;

                      Items[szName] := szSum;
                    end else
                    begin
                      case lpIndex of
                        0: szSum.dwSumMoney := t;  //帐目总价
                        1: szSum.dwPaidMoney:= t;  //支款统计
                        2: szSum.dwPrize    := t;  //奖励统计
                        3: szSum.dwbuckle   := t;  //扣罚统计
                        4: szSum.dwDetBranchMoney := t; //债务支款
                        5: szSum.dwDetCollectMoney:= t; //债务收款
                      end;

                      Add(szName,szSum);
                    //  ShowMessage('Add:' + CurrToStr(t));
                    end;

                  end;

                end;
              except
                 on e:Exception do
                 begin
                    ShowMessage('BMsg:' + e.Message);
                 end;
              end;

            end;

            Next;
          end;
          Self.cxProgressBar1.Visible := False;

        end;

      end;

  end;

  function GetGroupSQL(lpSelect , lpTableName , lpWhereName , lpGroupName : string):string;
  begin
    Result := 'SELECT ' + lpSelect + ' FROM ' + lpTableName + ' WHERE ' + lpWhereName + '  GROUP BY  ' + lpGroupName;
  end;

var
  s , szSelect , szGroupName , szWhereName , szGSQLText , szNSQLText , szDateRangeSQL : string;
  szCompanyStorage : Currency;
  i : Double;
  t : Integer;
  keys , szColName , szSUMMoney , szStatus , szSignName : string;
  ratio : Double;

begin
  try

      szSignName := lpSignName;
      //清空列表
      t := GetCurrentTime;
      dwSum.Clear; //泛清空

      if (lpIndex = 4) then
      begin
        dwProjectSum.Clear;
        for t := 0 to 24 do dwProjectSum.Add(ProjectName[t],0);
      end else
      begin
        with Self.tvGrid.DataController do
        begin
          SelectAll;
          DeleteSelection;
        end;
      end;

      szStatus  := 'Status=yes';
      szColName := Self.tvGridColumn3.DataBinding.FieldName; //往来单位
      szSUMMoney:= Self.tvGridColumn5.DataBinding.FieldName; //帐目总价
      if ( lpIndex = 1) or (lpIndex = 0) then SearchLog(lpIndex , szSignName);  //查询日志
      //**********************************债务汇总更新开始******************************
      if (lpIndex = 1) or (lpIndex = 0)  then  //个人债务和全部债务
      begin
        GetDetInfo(lpIndex,szSignName); //查债
      end;

      if (lpIndex = 0) or (lpIndex = 2) OR (lpIndex = 3) or (lpIndex = 4) then
      begin

        szGroupName := szColName;

        if lpIndex = 3 then szDateRangeSQL := ' and detDate between :t1 and :t2';
        if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ;  //个人查询
        szSelect    := 'SUM( detSumMoney ) AS t,'+ szGroupName + ' as c';
        szWhereName := 'detType="支" and ModuleIndex=0';
        szGSQLText   :=  GetGroupSQL(szSelect, g_Table_DetList ,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
        GetList(szGSQLText,lpIndex,4,ProjectName[0]); //成本对帐-债务支款

        szWhereName := 'detType="收" and ModuleIndex=0';
        szGSQLText   :=  GetGroupSQL(szSelect, g_Table_DetList ,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
        GetList(szGSQLText,lpIndex,5,ProjectName[1]); //成本对帐-债务收款

      end;

      //**********************************合同总价更新开始******************************
      //进销管理
      szGroupName := 'DeliverCompany';
      szSelect    := 'SUM( SumMonery ) AS t,'+ szGroupName + ' as c';
      szWhereName := 'AccountsStatus="成本挂账" and DataType=0 and ' + szStatus ;
      szNSQLText  := '';
      if lpIndex = 3 then szDateRangeSQL := ' and BillingDate between :t1 and :t2';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ;  //个人查询
      szGSQLText   :=  GetGroupSQL(szSelect,g_Table_Company_StorageDetailed ,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
      GetList(szGSQLText,lpIndex,0, ProjectName[2]); //公司管理-进销管理'

      //成本合同总价
      szGroupName := szColName;
      szSelect    := 'SUM('+ szSUMMoney +') AS t,' + szGroupName + ' as c';
      szNSQLText  := '';
      szDateRangeSQL := '';
      if lpIndex = 3 then szDateRangeSQL := ' and InputDate between :t1 and :t2';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ;//个人查询
      szWhereName :=  szStatus;
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Finance_ProjectFinal,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
      GetList(szGSQLText,lpIndex,0,ProjectName[3]); //成本管理-合同总价

      //施工管理考勤
      if lpIndex = 3 then szDateRangeSQL := ' and cxDate between :t1 and :t2';
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Project_Worksheet,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
      GetList(szGSQLText,lpIndex,0,ProjectName[4]); //施工管理-劳务分包

      //施工仓库
      szGroupName := szColName;
      szNSQLText  := '';
      szSelect    := 'SUM('+ szSUMMoney +') AS t,' + szGroupName + ' AS c' ;
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ;//个人查询
      szWhereName := szStatus  + ' AND TabTypes="入库"';
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Project_BuildStorage,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
      GetList(szGSQLText,lpIndex,0, ProjectName[5]); //施工管理-仓库统计
  
      //施工下商混
      szGroupName := szColName;
      szNSQLText  := '';
      szSelect    := szColName + ',SUM('+ szSUMMoney +') AS t , ' + szGroupName + ' AS c' ;
      szDateRangeSQL := '';
      if lpIndex = 3 then szDateRangeSQL := ' and InputDate between :t1 and :t2';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ;//个人查询
      szWhereName := szStatus ;
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Project_Concrete,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
      GetList(szGSQLText,lpIndex,0,ProjectName[6]); //施工管理-商混统计

      //施工管理下的租赁
      szGroupName := szColName;
      szDateRangeSQL := '';
      if lpIndex = 3 then szDateRangeSQL := ' and InputDate between :t1 and :t2';
      szNSQLText  := '';
      szSelect    := 'SUM('+szSUMMoney+') AS t,' + szGroupName + ' AS c';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ;//个人查询
      szWhereName := szStatus ;
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Project_Lease,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
      GetList(szGSQLText,lpIndex,0,ProjectName[7]); //施工管理-租赁统计

      //施工管理下机械
      szGroupName :=  szColName;
      szDateRangeSQL := '';
      if lpIndex = 3 then szDateRangeSQL := ' and InputDate between :t1 and :t2';
      szNSQLText  := '';
      szSelect    :=  'SUM(WorkTotal) AS t,' + szGroupName + ' AS c';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ; //个人查询
      szWhereName :=  szStatus;
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Project_Mechanics,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
      GetList(szGSQLText,lpIndex,0,ProjectName[8]); //施工管理-机械统计
  
      //施工管理下分包
      szGroupName :=  szColName;
      szDateRangeSQL := '';
      if lpIndex = 3 then szDateRangeSQL := ' and cxDate between :t1 and :t2';
      szNSQLText  := '';
      szSelect    :=  'SUM(' + szSUMMoney + ') AS t,' + szGroupName + ' AS c';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ; //个人查询
      szWhereName :=  szStatus;
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Project_EngineeringQuantity,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
      GetList(szGSQLText ,lpIndex, 0 ,ProjectName[9]); //施工管理-分包统计
      //**********************************合同总价更新结束************************

      //****************************增奖扣罚**************************************
      szNSQLText  := '';
      szSelect    := szColName + ',SUM('+ 'ToatlMoney' +') AS t,' + szGroupName + ' AS c' ;
      szDateRangeSQL := '';
      if lpIndex = 3 then szDateRangeSQL := ' and InputDate between :t1 and :t2';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ; //个人查询
      szWhereName := 'PrizeOrPenalty = "奖励" AND '  + szStatus;  //奖励
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Project_Sanction,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
      GetList(szGSQLText,lpIndex,2,ProjectName[10]); //施工管理-奖励统计

      szWhereName := 'PrizeOrPenalty = "扣罚" AND ' + szStatus;
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Project_Sanction,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
      GetList(szGSQLText,lpIndex,3,ProjectName[11]);  //施工管理-扣罚统计

      //**************************************************************************
      szNSQLText  := '';
      szGroupName := szColName;
      szSelect    := szGroupName + ' as c,SUM(ToatlMoney) AS t';
      szDateRangeSQL := '';
      if lpIndex = 3 then szDateRangeSQL := ' and InputDate between :t1 and :t2';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ; //个人查询
      szWhereName := szStatus; //帐目支款';
      szGSQLText   := GetGroupSQL(szSelect, g_Table_Company_Prize ,szWhereName + szNSQLText + szDateRangeSQL ,szGroupName);
      GetList(szGSQLText,lpIndex,2,ProjectName[14]);  //公司管理-奖励

      szNSQLText  := '';
      szGroupName := szColName;
      szSelect    := szGroupName + ' as c,SUM(ToatlMoney) AS t';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ; //个人查询
      szWhereName := szStatus; //帐目支款';
      szGSQLText   := GetGroupSQL(szSelect, g_Table_Company_buckle ,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
      GetList(szGSQLText,lpIndex,3,ProjectName[15]); //公司管理-罚款

      //****************************增奖扣罚结束*********************************

      //****************************已付金额开始*********************************
      //交易明细
      szNSQLText  := '';
      szGroupName := 'CollectMoneyCompany';
      szSelect    := szGroupName + ' as c,SUM(SettleAccountsMoney) AS t';
      szDateRangeSQL := '';
      if lpIndex = 3 then szDateRangeSQL := ' and InputDate between :t1 and :t2';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ; //个人查询
      szWhereName := szStatus;
      szGSQLText   := GetGroupSQL(szSelect,g_Table_BranchMoney,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
      GetList(szGSQLText,lpIndex,1,ProjectName[12]); //成本管理-交易统计
  
      //支出流水帐 - 支款
      szNSQLText  := '';
      szGroupName := 'Receivables';
      szSelect    := szGroupName + ' as c,SUM( '+ szSUMMoney +') AS t';
      szDateRangeSQL := '';
      if lpIndex = 3 then szDateRangeSQL := ' and cxDate between :t1 and :t2';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ; //个人查询
      szWhereName := 'fromType=1 AND ' + szStatus; //帐目支款';
      szGSQLText   := GetGroupSQL(szSelect,g_Table_RuningAccount,szWhereName + szNSQLText + szDateRangeSQL,szGroupName);
      GetList(szGSQLText,lpIndex,1,ProjectName[17]);  //流水帐统计
      //****************************已付金额结束*********************************

      //****************************公司统计开始*********************************
      if lpIndex <> 3 then
      begin

        szNSQLText  := '';
        szGroupName := szColName;
        szSelect    := szGroupName + ' as c,SUM('+ szSUMMoney +') AS t';
        if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ;  //个人查询
        szWhereName := szStatus;
        szGSQLText   := GetGroupSQL(szSelect,g_Table_Company_Staff_CheckWork,szWhereName + szNSQLText,szGroupName);
        GetList(szGSQLText,lpIndex,0,ProjectName[16]);  //公司管理-考勤

        szNSQLText  := '';
        szGroupName := szColName;
        szSelect    := szGroupName + ' as c,SUM('+ szSUMMoney +') AS t';
        if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ; //个人查询
        szWhereName := szStatus;
        szGSQLText   := GetGroupSQL(szSelect,g_Table_Company_Staff_Over ,szWhereName + szNSQLText,szGroupName);

        GetList(szGSQLText,lpIndex,0, ProjectName[13] ); //公司管理-考勤加班
      end;
   
      //****************************公司统计结束*********************************
      //****************************报价库统计***********************************
      //Designfee   //设计师分成
      //DesignBonus //设计师提成

      szGroupName := 'MainCaseDesign';
      szNSQLText  := ' AND ' +  szGroupName + '<>""';
      szSelect    := szGroupName + ' as c,SUM(DesignBonus) AS t';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ; //个人查询
      szWhereName := 'DesignCheckStatus=yes';
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Decorate_OfferInfo ,szWhereName + szNSQLText,szGroupName);
    //  Self.mmo1.Lines.Add('A:' + szGSQLText);
      GetList(szGSQLText,lpIndex,0, ProjectName[18] ); //设计师提成

      szSelect    := szGroupName + ' as c,SUM(Designfee) AS t';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ; //个人查询
      szWhereName := 'DesignCheckStatus=yes';
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Decorate_OfferInfo ,szWhereName + szNSQLText,szGroupName);
    //  Self.mmo1.Lines.Add('B:' + szGSQLText);
      GetList(szGSQLText,lpIndex,0, ProjectName[19] ); //设计师分成


      szGroupName := 'MainCaseMarkting';
      szNSQLText  := ' AND ' + szGroupName + '<>""';
      szSelect    := szGroupName + ' as c,SUM(MarketingBonus) AS t';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ; //个人查询
      szWhereName := 'MarketingCheckStatus=yes';
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Decorate_OfferInfo ,szWhereName + szNSQLText,szGroupName);
    //  Self.mmo1.Lines.Add('Marketing:' + szGSQLText);
      GetList(szGSQLText,lpIndex,0, ProjectName[20] ); //营销员提成

      szNSQLText  := '';
      szGroupName := 'SignName';
      szSelect    := szGroupName + ' as c,SUM(SumMonery) AS t';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ; //个人查询
      szWhereName := szStatus;
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Decorate_OfferSumCheck ,szWhereName + szNSQLText,szGroupName);
    //  Self.mmo1.Lines.Add('C:' + szGSQLText);
      GetList(szGSQLText,lpIndex,0, ProjectName[21] ); //报价管理-支出项目表

      szGroupName := 'HeadOfWork';
      szNSQLText  := ' AND ' + szGroupName + ' <> "" AND OfferType=1';
      szSelect    := szGroupName + ' as c,SUM(LaborCost) AS t';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '" AND OfferType=1' ; //个人查询
      szWhereName := szStatus;
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Decorate_OfferList ,szWhereName + szNSQLText,szGroupName);
    //  Self.mmo1.Lines.Add('C:' + szGSQLText);
      GetList(szGSQLText,lpIndex,0, ProjectName[22] ); //报价管理-成本核算

    //  szNSQLText  := '';
    //  szGroupName := 'HeadOfWork';
      szNSQLText  := ' AND ' + szGroupName + ' <> "" AND OfferType=3';
      szSelect    := szGroupName + ' as c,SUM(CostSummary) AS t';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '" AND OfferType=3' ; //个人查询
      szWhereName := szStatus;
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Decorate_OfferList ,szWhereName + szNSQLText,szGroupName);
    //  Self.mmo1.Lines.Add('C:' + szGSQLText);
      GetList(szGSQLText,lpIndex,0, ProjectName[24] ); //报价管理-人工核算

      szGroupName := 'Supplier';
      szNSQLText  := ' AND ' + szGroupName + ' <> ""';
      szSelect    := szGroupName + ' as c,SUM(CostSummary) AS t';
      if (lpIndex = 0) or (lpIndex = 4) then szNSQLText := ' AND ' + szGroupName + '="' + szSignName + '"' ; //个人查询
      szWhereName := szStatus;
      szGSQLText   := GetGroupSQL(szSelect,g_Table_Decorate_OfferList ,szWhereName + szNSQLText,szGroupName);
    //  Self.mmo1.Lines.Add('C:' + szGSQLText);
      GetList(szGSQLText,lpIndex,0, ProjectName[23] ); //报价管理-主材核算'


      //****************************报价库统计结束*******************************
      if (lpIndex <> 4) then
      begin
        t := GetCurrentTime;
        Self.cxProgressBar1.Visible := True;
        i := 0;

        with dwSum do
        begin
          //***********************全部汇总***************************************
          if Count <= 0 then
          begin
            //一个也没找到
          //  ShowMessage('没有找到符合条件的对帐信息！');
          end else
          begin
            dwSumMoney  := 0;
            dwbuckle    := 0;
            dwPrize     := 0;
            dwPaidMoney := 0;
            dwDetBranchMoney  := 0;
            dwDetCollectMoney := 0;

            for s in Keys do
            begin
              if ContainsKey(s) then
              begin
                Application.ProcessMessages;
                i := i + 1;
                Self.cxProgressBar1.Position := i;
                {
                if ContainsValue(TSum( Items[s] ) ) then
                begin
                  dwSumMoney  := dwSumMoney  + Items[s].dwSumMoney;
                  dwbuckle    := dwbuckle    + Items[s].dwbuckle;
                  dwPrize     := dwPrize     + Items[s].dwPrize;
                  dwPaidMoney := dwPaidMoney + Items[s].dwPaidMoney;
                  dwDetBranchMoney  := dwDetBranchMoney + Items[s].dwDetBranchMoney;
                  dwDetCollectMoney := dwDetCollectMoney+ Items[s].dwDetCollectMoney;
                end;
                }

                  dwSumMoney  := dwSumMoney  + Items[s].dwSumMoney;
                  dwbuckle    := dwbuckle    + Items[s].dwbuckle;
                  dwPrize     := dwPrize     + Items[s].dwPrize;
                  dwPaidMoney := dwPaidMoney + Items[s].dwPaidMoney;
                  dwDetBranchMoney  := dwDetBranchMoney + Items[s].dwDetBranchMoney;
                  dwDetCollectMoney := dwDetCollectMoney+ Items[s].dwDetCollectMoney;

              end;

            end;

            //***********************全部汇总结束*********************************
            Self.cxProgressBar1.Properties.Max := Count;
            if (lpIndex = 0) or (lpIndex = 1) or (lpIndex = 3) then
            begin
              //对帐汇总
        
              for s in Keys do
              begin

                if ContainsKey(s) then
                begin

                  Application.ProcessMessages;
                  i := i + 1;
                  Self.cxProgressBar1.Position := i;

                  if (lpIndex = 0) or (Items[s].dwSumMoney <> 0) or (Items[s].dwPaidMoney <> 0) then
                  begin

                    if lpIndex = 0 then
                    begin
                      Self.tvGridColumn15.Visible := False;
                      g_Total := Items[s].dwSumMoney;

                      ratio   := 0;
                      UpdateSumMoney(0,s,
                                     Items[s].dwSumMoney + Items[s].dwDetBranchMoney,
                                     Items[s].dwPaidMoney+ Items[s].dwDetCollectMoney,
                                     Items[s].dwbuckle,
                                     Items[s].dwPrize,
                                     (Items[s].dwSumMoney  + Items[s].dwPrize + Items[s].dwDetBranchMoney) - Items[s].dwPaidMoney - Items[s].dwbuckle - Items[s].dwDetCollectMoney ,
                                     ratio
                                     );

                    end else
                    begin

                      Self.tvGridColumn15.Visible := True;
                      try
                        if dwSumMoney = 0 then
                          ratio := 0
                        else
                          ratio := RoundTo( (Items[s].dwSumMoney / dwSumMoney) * 100 , -2 );
                      except
                        ratio := 0;
                      end;
                    //  Self.Memo1.Lines.Add('Ratio:' + FloatToStr(ratio) + ' SumMoney:' + CurrToStr(dwSumMoney));
                      UpdateSumMoney(0,s,
                                     Items[s].dwSumMoney,
                                     Items[s].dwPaidMoney,
                                     Items[s].dwbuckle,
                                     Items[s].dwPrize,
                                     (Items[s].dwSumMoney  + Items[s].dwPrize) - Items[s].dwPaidMoney - Items[s].dwbuckle,
                                     ratio
                                     );
                    end;

                  end;

                end;

              end;
              try
                if lpIndex = 0 then
                begin
                  Self.RzToolButton9.Enabled := True;
                  Self.RzPanel12.Caption:= MoneyConvert( Items[s].dwSumMoney );
                  Self.RzPanel10.Caption:= MoneyConvert( Items[s].dwPaidMoney );
                  Self.RzPanel7.Caption := MoneyConvert( (Items[s].dwSumMoney  + Items[s].dwPrize) - Items[s].dwPaidMoney - Items[s].dwbuckle );
                end else
                begin
                  Self.RzToolButton9.Enabled := False;
                end;
              except
              end;
            end;

            if lpIndex = 2 then
            begin
              //黑条汇总
              try
                Self.FBalloonHint.Title := '大写';
                Self.RzPanel15.Caption := Format('%2.2m',[dwSumMoney]);
                Self.RzPanel15.Hint := MoneyConvert(dwSumMoney);
                Self.RzPanel17.Caption := Format('%2.2m',[dwPaidMoney]);
                Self.RzPanel17.Hint := MoneyConvert(dwPaidMoney);
                Self.RzPanel19.Caption := Format('%2.2m',[dwDetCollectMoney]);
                Self.RzPanel19.Hint := MoneyConvert(dwDetCollectMoney);
                Self.RzPanel21.Caption := Format('%2.2m',[dwDetBranchMoney]);
                Self.RzPanel21.Hint := MoneyConvert(dwDetBranchMoney);
              except
              end;
            end;

          end;
          Self.cxProgressBar1.Visible := False;
          IsDateQuery := False;
        end;

      end else
      begin

        dwSumMoney  := 0;
        dwPaidMoney := 0;
        dwbuckle    := 0;
        dwPrize     := 0;
        dwDetBranchMoney  := 0;
        dwDetCollectMoney := 0;

        with dwProjectSum do
        begin
          for s in Keys do
          begin

            if ContainsKey(s) then
            begin

              if items[s]<>0 then
              begin
                if (s = ProjectName[0] )  or
                   (s = ProjectName[2] )  or
                   (s = ProjectName[3] )  or
                   (s = ProjectName[4] )  or
                   (s = ProjectName[5] )  or
                   (s = ProjectName[6] )  or
                   (s = ProjectName[7] )  or
                   (s = ProjectName[8] )  or
                   (s = ProjectName[9] )  or
                   (s = ProjectName[16])  or
                   (s = ProjectName[13])  or
                   (s = ProjectName[10])  or
                   (s = ProjectName[14])  or
                   (s = ProjectName[18])  or
                   (s = ProjectName[19])  or
                   (s = ProjectName[20])  or
                   (s = ProjectName[21])  or
                   (s = ProjectName[22])  or
                   (s = ProjectName[23])  or
                   (s = ProjectName[24])
                    then
                begin
                  with Self.SUMData do
                  begin
                    if not Active then Active := True;
                    Append;
                    FieldByName(Self.SUMDataPrjectName.FieldName).Value := s;
                    FieldByName(Self.SUMDataSumMoney.FieldName).Value   := Items[s];
                    //合同总价
                    dwSumMoney := dwSumMoney + Items[s];
                  end;
                end;

                if (s = ProjectName[12]) or
                   (s = ProjectName[17]) or
                   (s = ProjectName[1])  or
                   (s = ProjectName[11]) or
                   (s = ProjectName[15])
                   then
                begin
                  with Self.PaidData do
                  begin
                    if not Active then Active := True;
                    Append;
                    FieldByName(Self.PaidDataPrjectName.FieldName).Value := s;
                    FieldByName(Self.PaidDataPaidMoney.FieldName).Value  := Items[s];
                    //已付
                    dwPaidMoney := dwPaidMoney + Items[s];
                  end;
                end;

                if (s = ProjectName[10]) or
                   (s = ProjectName[11]) or
                   (s = ProjectName[14]) or
                   (s = ProjectName[15]) then
                begin

                  if  (s = ProjectName[10]) or
                      (s = ProjectName[14]) then
                  begin
                    //奖励
                    dwSumMoney := dwSumMoney + Items[s];
                  end else
                  if (s = ProjectName[11]) or
                     (s = ProjectName[15]) then
                  begin
                    //扣罚
                    dwPaidMoney := dwPaidMoney + Items[s];
                  end;

                end;

                //一个合同总价丶奖的在合同总价
                //一个己付金额丶罚款的在己付
                //两个
              end;
            end;

          end;

          with Self.ChartData do
          begin
            if not Active then Active := True;
            Append;
            FieldByName(Self.ChartProjectName.FieldName).Value := '未付金额';
            FieldByName(Self.ChartSumMoney.FieldName).Value    := dwSumMoney;
            Append;
            FieldByName(Self.ChartProjectName.FieldName).Value := '已付金额';
            FieldByName(Self.ChartSumMoney.FieldName).Value    := dwPaidMoney;
          end;
        end;

      end;

  except
    on e:Exception do
    begin
      ShowMessage('GetSummonaryMsg:' + e.Message);
    end;
  end;

end;

procedure TfrmReconciliationTable.RzToolButton3Click(Sender: TObject);
begin
  inherited;
  IsDateQuery := true;
  GetSumMoney(3,'');
end;

function TfrmReconciliationTable.GetDetInfo(lpIndex : Byte; lpSignName : string):Currency;
var
  s : string;

begin
  //债务表
  //0=个人对帐 1=对帐汇总
  if lpIndex <> 2 then
  begin
    with Self.ADODet do
    begin
      Close;

      case lpIndex of
        0:
        begin
          s := 'Select * from '+ g_Table_DetList +' WHERE '+ Self.ADODetSignName.FieldName +'="' + lpSignName +'" AND ModuleIndex=0' ;
          Self.ADODet.CommandText := s;
          Open;
        end;
        1:
        begin
          s := 'Select * from '+ g_Table_DetList +' WHERE ModuleIndex=0' ;
          Self.ADODet.CommandText := s;
          Open;
        end;
        3:
        begin
          s := 'Select * from ' + g_Table_DetList +' WHERE ModuleIndex=0 and ' + Self.DetCol4.DataBinding.FieldName + '="' + lpSignName  +'"' ;
          Self.ADODet.CommandText := s;
          Open;
        end;
      end;

    end;

  end;

end;

{
function TfrmReconciliationTable.SUMMoney(lpSignName : string ; lpIndex : Byte):Boolean;
var
  szSignName : string;
  szSelect : string;
  szWhereName : string;
  szGroupName : string;
  szColName   : string;
  szSUMMoney  : string;
  szProjectWorksheet , SumMoney : Currency; //施工考勤
  szBranchWork : Currency; //支款考勤
  szPaidMoney  : Currency; //已付金额
  szProjectBuildStorage : Currency;
  szProjectConcrete: Currency;
  szProjectPenalty : Currency;
  szProjectPrize   : Currency;
  szBranch_Money : Currency;
  szProjectEngineeringQuantity : Currency;
  szProjectMechanics  : Currency;
  szProjectLease      : Currency ;
  szSignId : Integer;
  szStatus : string;
  szDetMoney : Currency;
  szRuningMoney : Currency;
  szIncomeMoney : Currency;  //收款
  szBranchMoney : Currency;  //支款
  szCompanyWork : Currency;  //公司考勤
  szCompanyOver : Currency;  //公司考勤加班
  szCompanyPrize: Currency;  //公司奖励
  szCompanyBuckle:Currency;  //公司罚款
  szCompanyStorage : Currency; //公司仓库

  szParent : string;
  t: Integer;
begin
  inherited;
  t := GetCurrentTime;
  //施工管理，考勤，仓库，分包，租赁，增奖，扣罚，机械
  //支款管理，考勤，材料，混凝土，机械，分包，租赁(两项)，钢筋

  //select min（字段2），max（字段2） from tablename where 字段1=‘员工a’；
  szProjectWorksheet := 0;
  SumMoney  := 0 ; //施工考勤
  szBranchWork := 0 ; //支款考勤
  szPaidMoney  := 0 ; //已付金额
  szProjectBuildStorage := 0 ;
  szProjectPenalty := 0 ;
  szProjectPrize   := 0 ;
  szBranch_Money := 0 ;
  szProjectEngineeringQuantity := 0 ;
  szProjectMechanics  := 0 ;
  szProjectLease      := 0 ;
  szSignId := 0 ;
  szIncomeMoney := 0;
  szBranchMoney := 0;
  szCompanyWork := 0;
  szCompanyOver := 0;


  szSignName := trim( lpSignName );
  if Length(szSignName) <> 0 then
  begin

    szStatus := ' AND status=yes';  //1为正常 0为冻结
    if lpIndex = 0 then
    begin
      with Self.tvGrid.DataController do
      begin
        SelectAll;
        DeleteSelection;
      end;
    //  SearchLog(szSignName);  //查询日志
      GetDetInfo(0,szSignName); //查债
    end;

    szColName := Self.tvGridColumn3.DataBinding.FieldName; //往来单位名
    szSUMMoney:= Self.tvGridColumn5.DataBinding.FieldName;

    szSelect    := szColName + ',SUM('+szSUMMoney+') AS t';
    szWhereName := szColName + '="' + szSignName + '"' + szStatus ;
    szGroupName := szColName;
    //施工考勤
    szProjectWorksheet   := GetGroupSumMoney(szSelect,g_Table_Project_Worksheet,szWhereName,szGroupName);
    szBranchWork  := GetGroupSumMoney(szSelect,g_Table_Finance_ProjectFinal,szWhereName,szGroupName);

    //对帐债务
    szSelect      := 'SUM(detSumMoney) AS t';
    szWhereName   := szColName + '="' + szSignName + '" and detType="收" and ModuleIndex=0';
    szIncomeMoney := GetGroupSumMoney(szSelect,g_Table_DetList,szWhereName,szGroupName);  //收款

    szWhereName   := szColName + '="' + szSignName + '" and detType="支" and ModuleIndex=0';
    szBranchMoney := GetGroupSumMoney(szSelect,g_Table_DetList,szWhereName,szGroupName);  //支款

    //施工仓库
    szSelect    := szColName + ',SUM('+ 'SumMoney' +') AS t' ;
    szWhereName := szColName + '="' + szSignName + '"' + szStatus + ' AND TabTypes="入库"' ;
    szGroupName := szColName;
    szProjectBuildStorage:= GetGroupSumMoney(szSelect,g_Table_Project_BuildStorage,szWhereName,szGroupName);

    //施工下商混
    szSelect    := szColName + ',SUM('+ 'SumMoney' +') AS t' ;
    szWhereName := szColName + '="' + szSignName + '"' + szStatus ;
    szGroupName := szColName;
    szProjectConcrete := GetGroupSumMoney(szSelect,g_Table_Project_Concrete,szWhereName,szGroupName);

    //施工管理下的租赁
    szGroupName := szColName;
    szSelect    := szColName + ',SUM('+ szSUMMoney +') AS t';
    szWhereName := szColName + '="' + szSignName + '" and ' + szStatus ;
    szProjectLease := GetGroupSumMoney(szSelect,g_Table_Project_Lease,szWhereName,szGroupName);

    //施工管理下机械
    szSelect    :=  szColName + ',SUM(WorkTotal) AS t';
    szGroupName :=  szColName;
    szWhereName :=  szColName + '="' + szSignName + '"' + szStatus;
    szProjectMechanics  := GetGroupSumMoney(szSelect,g_Table_Project_Mechanics,szWhereName,szGroupName);

    //增奖扣罚
    szSelect    := szColName + ',SUM('+ 'ToatlMoney' +') AS t';
    szWhereName := szColName + '="' + szSignName + '" AND PrizeOrPenalty = "奖励"'  + szStatus;  //奖励
    szProjectPrize    := GetGroupSumMoney(szSelect,g_Table_Project_Sanction,szWhereName,szGroupName);

    szWhereName := szColName + '="' + szSignName + '" AND PrizeOrPenalty = "扣罚"' + szStatus;
    szProjectPenalty  := GetGroupSumMoney(szSelect,g_Table_Project_Sanction,szWhereName,szGroupName);

    //施工分包
    szSelect    := szColName + ',SUM( '+'SumMoney' +') AS t';
    szGroupName := szColName;
    szWhereName := szColName + '="' + szSignName + '"' + szStatus;
    szProjectEngineeringQuantity := GetGroupSumMoney(szSelect,g_Table_Project_EngineeringQuantity,szWhereName,szGroupName);

    //交易明细
    szSelect    := 'CollectMoneyCompany,SUM(SettleAccountsMoney) AS t';
    szGroupName := 'CollectMoneyCompany';
    szWhereName := 'CollectMoneyCompany="' + szSignName + '"' + szStatus;
    szPaidMoney := GetGroupSumMoney(szSelect,g_Table_BranchMoney,szWhereName,szGroupName);

    //支出流水帐 - 支款
    szSelect    := 'Receivables,SUM(SumMoney) AS t';
    szGroupName := 'Receivables';
    szWhereName := 'Receivables="' + szSignName + '" and fromType=1 ' + szStatus; //帐目支款';
    szRuningMoney := GetGroupSumMoney(szSelect,g_Table_RuningAccount,szWhereName,szGroupName);

    //公司下考勤 加班
    with DM.Qry do
    begin
      Close;
      SQL.Text := 'Select Id from ' + g_Table_Maintain_CompanyInfoList + ' Where Contacts="' + lpSignName + '"';
      Open;
      szParent := FieldByName('Id').AsString;
      Close;
      if szParent <> '' then
      begin
        SQL.Text :='Select SUM(SumMoney) AS t from ' + g_Table_Company_Staff_CheckWork +
                   ' WHERE Parent = ' + szParent + szStatus;
        Open;
        if RecordCount <> 0 then
        begin
          szCompanyWork := FieldByName('t').AsCurrency;
        end;
        Close;
        SQL.Text := 'Select SUM(SumMoney) AS t from '+ g_Table_Company_Staff_Over +
                    ' WHERE Parent = ' + szParent + szStatus;
        Open;
        if RecordCount <> 0 then
        begin
          szCompanyOver := FieldByName('t').AsCurrency;
        end;
        //公司奖励
        Close;
        SQL.Text := 'Select SUM(ToatlMoney) AS t from '+ g_Table_Company_Prize +
                    ' WHERE SignName = "' + szSignName + '" '+ szStatus;
        Open;
        if RecordCount <> 0 then szCompanyPrize := FieldByName('t').AsCurrency; //公司奖励
        Close;
        SQL.Text := 'Select SUM(ToatlMoney) AS t from '+ g_Table_Company_buckle +
                    ' WHERE SignName = "' + szSignName + '" '+ szStatus;
        Open;
        if RecordCount <> 0 then szCompanyBuckle := FieldByName('t').AsCurrency; //公司罚款
      end;
    end;

    //土石方
    //g_Table_Project_Mechanics

    //公司仓库
    szSelect    := 'SUM(SumMonery) AS t';
    szGroupName := 'DeliverCompany';
    szWhereName := 'DeliverCompany="' + szSignName + '" and AccountsStatus="成本挂账" and DataType=0 ' + szStatus; //帐目支款';
    szCompanyStorage := GetGroupSumMoney(szSelect,g_Table_Company_StorageDetailed ,szWhereName,szGroupName);
    ShowMessage('time:' + IntToStr(GetCurrentTime - t));

    SumMoney := szBranchWork            +     //支款管理下考勤
                szProjectWorksheet      +     //施工管理下考勤
                szProjectBuildStorage   +     //施工管理下仓库
                szProjectEngineeringQuantity+ //施工管理下分包
                szProjectMechanics          + //施工管理下机械
                szProjectLease          +     //施工管理下租赁
                szProjectConcrete       +     //施工管理下商混
                szCompanyWork           +     //公司下考勤
                szCompanyOver                 //公司下考勤加班
                ;


    SumMoney := SumMoney + szBranchMoney + szCompanyStorage;                //债务金额
    g_Total  := SumMoney;

    szPaidMoney := szPaidMoney +   //交易记录
                   szIncomeMoney + //债务收入
                   szRuningMoney   //流水帐
                   ;
    OutSumMoney(szSignName,
                SumMoney,
                szPaidMoney,  //已付金额
                szProjectPenalty + szCompanyBuckle,//扣罚
                szProjectPrize   + szCompanyPrize ,  //奖励
                SumMoney  + (szProjectPrize + szCompanyPrize) - szPaidMoney - (szProjectPenalty + szCompanyBuckle)
                );

    if (SumMoney <> 0) or (szPaidMoney <> 0) then
    begin
      Self.RzToolButton9.Enabled := True;
    end else
    begin
      Self.RzToolButton9.Enabled := False;
    end;

    Self.RzPanel3.Caption := szSignName;


  end else
  begin
    Self.Search.SetFocus;
  end;

end;
}
procedure TfrmReconciliationTable.LayoutView1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1 );
end;

procedure TfrmReconciliationTable.MenuItem1Click(Sender: TObject);
begin
  inherited;
  with DM do
  begin
    BasePrinterLink1.Component := Self.LogInfoGrid;
    BasePrinter.Preview(True, nil);
  end;

end;

procedure TfrmReconciliationTable.MenuItem2Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.LogInfoGrid,'结算日志明细表');
end;

procedure TfrmReconciliationTable.RzToolButton4Click(Sender: TObject);
var
  s : string;
begin
  s := Self.Search.Text;

  PersonalQuery(s);
end;

procedure TfrmReconciliationTable.RzToolButton6Click(Sender: TObject);
begin
  inherited;
  Close;
end;

procedure TfrmReconciliationTable.RzToolButton7Click(Sender: TObject);
var
  s : string;

begin
  inherited;
  s := Self.cxLookupComboBox1.Text;
  SearchLog(2,s);
end;

function UPDATEData(lpTableName , lpFieldName , lpSignName , lpWhereName : string):Boolean;
begin

  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' + lpTableName +
                ' WHERE '+ lpFieldName +'="' + lpSignName + '" AND status=yes ' + lpWhereName;
    Open;
    if RecordCount <> 0 then
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'UPDATE '+ lpTableName +' AS BW SET BW.status=no WHERE BW.'+ lpFieldName +'="'+ lpSignName +'" AND BW.status=yes ' + lpWhereName;
      ExecSQL;

    end;

  end;

end;

function TfrmReconciliationTable.OutSumMoney(lpSignName : string;
    lpSumMoney ,
    lpPaidMoney ,
    lpDeductMoney ,
    lpRewardMoney ,
    lpClearMoney : Currency
    ):Boolean;
begin
  inherited;
  {
  with Self.ADOSumInfo do
  begin
    if Self.ADOSumInfo.Locate(Self.tvGridColumn3.DataBinding.FieldName,lpSignName,[loCaseInsensitive, loPartialKey]) then
    begin
    end else
    begin
      Append;
      FieldByName(Self.ADOSumInfoSignName.FieldName).Value     := lpSignName;
      FieldByName(Self.ADOSumInfoInputDate.FieldName).Value    := Date; //对帐日期
      FieldByName(Self.ADOSumInfoSumMoney.FieldName).Value     := lpSumMoney;
      Post;
    end;
  end;
  }

  with Self.ADOSumInfo do
  begin
    if State <> dsInactive then
    begin
    {
    if Self.ADOSumInfo.Locate(Self.tvGridColumn3.DataBinding.FieldName,lpSignName,[loCaseInsensitive, loPartialKey]) then
    begin
    end else
    begin

    end;
    }
      Append;
      //  Edit;
      //FieldByName(Self.ADOSumInfoInduceType.FieldName).Value   := '职工';
      FieldByName(Self.ADOSumInfoSignName.FieldName).Value     := lpSignName;
      FieldByName(Self.ADOSumInfoSumMoney.FieldName).Value     := lpSumMoney;
      FieldByName(Self.ADOSumInfoPaidMoney.FieldName).Value    := lpPaidMoney;
      FieldByName(Self.ADOSumINfoDeductMoney.FieldName).Value  := lpDeductMoney;
      FieldByName(Self.ADOSumInfoRewardMoney.FieldName).Value  := lpRewardMoney;
      FieldByName(Self.ADOSumInfoClearBalance.FieldName).Value := lpClearMoney; //清算余额
      FieldByName(Self.ADOSumInfoClearBalanceCapital.FieldName).Value:= MoneyConvert( lpClearMoney );
      FieldByName(Self.ADOSumInfoInputDate.FieldName).Value    := Date; //对帐日期

      Self.RzPanel12.Caption:= MoneyConvert( lpSumMoney );
      Self.RzPanel10.Caption:= MoneyConvert( lpPaidMoney );
      Self.RzPanel7.Caption := MoneyConvert( lpClearMoney );

      Post;
    end;

  end;

end;

procedure TfrmReconciliationTable.RzToolButton9Click(Sender: TObject);
  function ADOUPDATE(lpTableName , lpFieldName , lpSignName , lpWhereName : string ; lpStatus : string = 'Status'):Boolean;
  begin

    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'Select * from ' + lpTableName +
                  ' WHERE '+ lpFieldName +'="' + lpSignName + '" AND '+ lpStatus +'=yes ' + lpWhereName;
      Open;
      if RecordCount <> 0 then
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'UPDATE '+ lpTableName +
        ' AS BW SET BW.'+ lpStatus +'=no WHERE BW.'+ lpFieldName +
        '="'+ lpSignName +'" AND BW.'+ lpStatus +'=yes ' + lpWhereName;
        ExecSQL;

      end;

    end;

  end;
var
  szSumMoney  : Currency;
  szInduceType: string;
  szSignId : Integer;
  szInputDate : TDate;
  szPaidMoney : Currency;
  SpareMoney  : TfrmSpareMoney;
  IsSpareStatus : Boolean;
  szSpareMoney , szClearBalance: Currency;
  szDetType : Integer;
  err : string;
  szSignName : string;
  szFieldName: string;
  szFileDir: string;
begin
  inherited;

  if MessageBox(handle, PChar('是否清算?'),'提示',
       MB_ICONQUESTION + MB_YESNO) = IDYES then
  begin

    szClearBalance :=Self.ADOSumInfoClearBalance.AsCurrency;
    if szClearBalance < 0 then
    begin
      szSpareMoney := 0- szClearBalance;
      szDetType := 0;//收
    end else
    begin
      szSpareMoney := szClearBalance;
      szDetType := 1;//支
    end;

    IsSpareStatus := False;
    DM.ADOconn.BeginTrans; //开始事务
    try
      szSignName := Self.ADOSumInfoSignName.AsString;
      if Length(szSignName) = 0 then
      begin
        MessageBox(handle, PChar('无法获取往来单位！'),'提示',
         MB_ICONQUESTION + MB_YESNO) ;
        Exit;
      end;
      szFileDir  := Concat(g_Resources,'\det\' + szSignName);
      //ADOQuery 代码
        if szSpareMoney <> 0 then
        begin
          //弹框写入附件
          //检测欠据
          SpareMoney := TfrmSpareMoney.Create(Application);
          try

            SpareMoney.FieldSignName    := Self.ADODetSignName.FieldName;
            SpareMoney.FieldSign_Id     := Self.ADODetSign_Id.FieldName;
            SpareMoney.FielddetDate     := Self.ADODetdetDate.FieldName;
            SpareMoney.FielddetSumMoney := Self.ADODetdetSumMoney.FieldName;
            SpareMoney.FielddetType     := Self.ADODetdetType.FieldName;
            SpareMoney.FildModuleIndex  := Self.ADODetModuleIndex.FieldName;

            SpareMoney.FileDir     := szFileDir;
            SpareMoney.ModuleIndex := 0;
            SpareMoney.SignName    := Self.ADOSumInfoSignName.AsString;
            SpareMoney.TableName   := g_Table_DetList;
            SpareMoney.DetType:=szDetType;
            SpareMoney.cxCurrencyEdit1.Value := szSpareMoney;
            SpareMoney.ShowModal;

            IsSpareStatus := SpareMoney.IsSpareStatus;
            Self.ADODet.Requery();

          finally
            SpareMoney.Free;
          end;

        end else
        begin
          //删除债务
          with DM.Qry do
          begin
            Close;
            SQL.Clear;
            SQL.Text := 'Select * from '+ g_Table_DetList +
                        ' WHERE '+ Self.ADODetSignName.FieldName +'="' + szSignName + '" AND ModuleIndex=0' ;
            Open;
            if RecordCount <> 0 then
            begin
              //检测欠据
              Delete;
              IsSpareStatus := True;

            end else
            begin
              IsSpareStatus := True;
            end;
          end;

        end;

        if (IsSpareStatus) then
        begin
            //******************更新状态**********************
            //支款管理
            szFieldName := Self.ADOSumInfoSignName.FieldName;
            UPDATEData(g_Table_BranchMoney,'CollectMoneyCompany',szSignName,''); //支款下交易记录

            UPDATEData(g_Table_Finance_ProjectFinal,szFieldName,szSignName,''); //支款下考勤

            //流水帐
            UPDATEData(g_Table_RuningAccount,'Receivables',szSignName,'AND fromType=1'); //支款下流水帐记录
            //************************************************
            //施工管理
            UPDATEData(g_Table_Project_Worksheet,szFieldName,szSignName,'');     //施工下考勤
            UPDATEData(g_Table_Project_BuildStorage,szFieldName,szSignName,'');  //施工下仓库
            UPDATEData(g_Table_Project_Sanction,szFieldName,szSignName,'');      //施工下奖励扣罚
            UPDATEData(g_Table_Project_EngineeringQuantity,szFieldName,szSignName,'');//施工下分包

            UPDATEData(g_Table_Project_Mechanics,szFieldName,szSignName,''); //施工下机械土石方

            UPDATEData(g_Table_Project_Lease,szFieldName,szSignName,'');     //施工下租赁
            UPDATEData(g_Table_Project_Concrete,szFieldName,szSignName,'');  //施工下商混
            //******************公司管理下********************
            UPDATEData(g_Table_Company_Prize  , szFieldName , szSignName , '');  //公司管理奖励
            UPDATEData(g_Table_Company_buckle , szFieldName , szSignName , '');  //公司罚款

            UPDATEData(g_Table_Company_Staff_CheckWork  , szFieldName , szSignName , '');  //公司考勤
            UPDATEData(g_Table_Company_Staff_Over , szFieldName , szSignName , '');  //公司考勤加班
            //******************公司仓库**********************
            {
            szSelect    := 'SUM(SumMonery) AS t';
            szGroupName := 'DeliverCompany';
            szWhereName := 'DeliverCompany="' + szSignName + '" and DataType=0 ' + szStatus; //帐目支款';

            szCompanyStorage := GetGroupSumMoney(szSelect,g_Table_Company_StorageDetailed ,szWhereName,szGroupName);
            }
            UPDATEData( g_Table_Company_StorageBillList , 'DeliverCompany' , szSignName , ' and AccountsStatus="成本挂账" and DataType=0 ');
            UPDATEData( g_Table_Company_StorageDetailed , 'DeliverCompany' , szSignName , ' and AccountsStatus="成本挂账" and DataType=0 ');
            //*****************报价****************************
            ADOUPDATE(g_Table_Decorate_OfferInfo,'MainCaseMarkting',szSignName,'','MarketingCheckStatus');
            ADOUPDATE(g_Table_Decorate_OfferInfo,'MainCaseDesign',szSignName,'','DesignCheckStatus');
            ADOUPDATE(g_Table_Decorate_OfferSumCheck,'SignName',szSignName,'');
            ADOUPDATE(g_Table_Decorate_OfferList,'HeadOfWork',szSignName,'');
            ADOUPDATE(g_Table_Decorate_OfferList,'Supplier',szSignName,'');

        //    g_Table_Decorate_OfferInfo  = MainCaseMarkting= MarketingCheckStatus=yes
        //    g_Table_Decorate_OfferInfo  = MainCaseDesign  = DesignCheckStatus=yes

        //    g_Table_Decorate_OfferSumCheck = SignName
        //    g_Table_Decorate_OfferList  = HeadOfWork
        //    g_Table_Decorate_OfferList  = Supplier



            //*****************报价结束************************


            //******************更新状态完毕******************
            with Self.ADOSumLogInfo do
            begin

              if State <> dsInactive then
              begin
                Append;
                  FieldByName(Self.ADOSumInfoInputDate.FieldName).Value  := Self.ADOSumInfoInputDate.AsDateTime;
                  FieldByName(Self.ADOSumInfoSign_Id.FieldName).Value    := GetSignNameId(Self.RzPanel3.Caption);  //往来单位ID
                  FieldByName(Self.ADOSumInfoSignName.FieldName).Value   := Self.ADOSumInfoSignName.AsString;
                  FieldByName(Self.ADOSumInfoInduceType.FieldName).Value := Self.ADOSumInfoInduceType.AsString;    //归纳分类
                  FieldByName(Self.ADOSumInfoSumMoney.FieldName).Value   := g_Total;// Self.ADOSumInfoSumMoney.AsCurrency;    //帐目总价
                  FieldByName(Self.ADOSumInfoDeductMoney.FieldName).Value:= Self.ADOSumInfoDeductMoney.AsCurrency; //扣除
                  FieldByName(Self.ADOSumInfoRewardMoney.FieldName).Value:= Self.ADOSumInfoRewardMoney.AsCurrency; //增奖
                  FieldByName(Self.ADOSumInfoClearBalance.FieldName).Value:=Self.ADOSumInfoClearBalance.AsCurrency;//清算余款
                  FieldByName(Self.ADOSumInfoClearBalanceCapital.FieldName).Value := Self.ADOSumInfoClearBalanceCapital.Value; //余款大写
                  FieldByName('ModuleType').Value := 0;
                Post;
                Requery();

              end;

            end;

        end;

       DM.ADOconn.Committrans; //提交事务
    except
      on E:Exception do
      begin
        DM.ADOconn.RollbackTrans;           // 事务回滚
        err:=E.Message;
        ShowMessage(err);
      end;
    end;

    GetSumMoney(2,''); //汇总总额
    GetSUMMoney(0,szSignName);


  end;

end;

procedure TfrmReconciliationTable.tvDetEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  AAllow := False;
end;

procedure TfrmReconciliationTable.tvDetStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
begin
  inherited;
  if ARecord.Values[Self.DetCol4.Index] = '收' then
    AStyle := DM.cxStyle222
  else
    AStyle := DM.cxStyle326;
end;

procedure TfrmReconciliationTable.tvGridColumn10CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
//  ACanvas.Font.Color := clRed;
  ACanvas.Font.Style := [fsBold];
end;

procedure TfrmReconciliationTable.tvGridColumn13PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var
  szMoney : Currency;
  szRecordIndex : Integer;
  szColIndex : Integer;
  szColName : string;

begin
  inherited;
  szMoney := StrToCurrDef(VarToStr(DisplayValue),0);
  with Self.ADOSumInfo do
  begin
    if State <> dsInactive  then
    begin
      Edit;
      FieldByName(Self.ADOSumInfoClearBalance.FieldName).Value := szMoney;
      FieldByName(Self.ADOSumInfoClearBalanceCapital.FieldName).Value := MoneyConvert(szMoney);
      Post;
    end;
  end;
end;

procedure TfrmReconciliationTable.tvGridColumn14PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
begin
  inherited;
  ShowMessage(DisplayValue);
end;

procedure TfrmReconciliationTable.tvGridColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(  ARecord.Index + 1 );
end;

procedure TfrmReconciliationTable.tvGridColumn5CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  ACanvas.Font.Style := [fsBold];
  ACanvas.Brush.Color:= clYellow;
  ACanvas.Font.Color := clBlue;
end;

procedure TfrmReconciliationTable.tvGridColumn7CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  ACanvas.Font.Style := [fsBold];
  ACanvas.Brush.Color:= clRed;
  ACanvas.Font.Color := clWhite;
end;

procedure TfrmReconciliationTable.tvGridCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
  inherited;
  ACanvas.Font.Style := [fsBold];
end;

procedure TfrmReconciliationTable.tvGridEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  if (AItem.Index = Self.tvGridColumn5.Index) or
     (AItem.Index = Self.tvGridColumn6.Index) or
     (AItem.Index = Self.tvGridColumn7.Index) or
     (AItem.Index = Self.tvGridColumn8.Index) or
     (AItem.Index = Self.tvGridColumn13.Index) then
  begin
    AAllow := False;
  end;
end;

procedure TfrmReconciliationTable.tvGridKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
    9:
    begin

    end;
    46:
    begin
      Self.N2.Click;
    end;
  end;

  if (Key = 9) then
  begin
  //  cxGrid1DBTableView1.Controller.FocusedRowIndex 当前行号
  //  cxGrid1DBTableView1.Controller.FocusedRow 当前行
  //  cxGrid1DBTableView1.Controller.FocusedColumn 当前列
  //  cxGrid1DBTableView1.Controller.FocusedColumnIndex 当前列号
  //  cxGrid1DBTableView1.Controller.EditingItem 当前编辑中的单元框
  //  cxGrid1DBTableView1.Controller.EditingController.Edit 当前的编辑框
  end;
end;

procedure TfrmReconciliationTable.tvGroupViewColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmReconciliationTable.tvSignNameColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1 );
end;

procedure TfrmReconciliationTable.tvSignNameDblClick(Sender: TObject);
var
  szSignName : string;
begin
  inherited;
  //取出往来单
  if Self.tvSignName.Controller.FocusedRowIndex >= 0 then
  begin
    szSignName := VarToStr(Self.tvSignNameColumn2.EditValue);
    PersonalQuery( szSignName );
  end;
end;

procedure TfrmReconciliationTable.tvSignNameEditing(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  var AAllow: Boolean);
begin
  inherited;
  AAllow := False;
end;

end.
