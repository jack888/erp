unit ufrmReceivablesAccount;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, RzPanel, cxGraphics,
  cxControls, cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit,
  dxSkinsCore, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, cxLabel, cxTextEdit, RzButton, Vcl.ComCtrls, Data.DB,
  Data.Win.ADODB, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxCalendar, Datasnap.DBClient,UnitADO, Vcl.StdCtrls,ufrmBaseController,
  Vcl.Menus, cxButtonEdit;

type
  TfrmReceivablesAccount = class(TfrmBaseController)
    RzToolbar10: TRzToolbar;
    RzSpacer77: TRzSpacer;
    RzBut2: TRzToolButton;
    RzSpacer78: TRzSpacer;
    RzBut4: TRzToolButton;
    RzSpacer79: TRzSpacer;
    RzBut5: TRzToolButton;
    RzSpacer80: TRzSpacer;
    RzBut8: TRzToolButton;
    RzSpacer1: TRzSpacer;
    RzBut1: TRzToolButton;
    RzBut7: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzSpacer4: TRzSpacer;
    RzSpacer5: TRzSpacer;
    RzBut3: TRzToolButton;
    RzSpacer6: TRzSpacer;
    RzSpacer7: TRzSpacer;
    RzSpacer8: TRzSpacer;
    RzBut6: TRzToolButton;
    cxLabel2: TcxLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    Grid: TcxGrid;
    tvView: TcxGridDBTableView;
    tvViewCol1: TcxGridDBColumn;
    tvViewCol3: TcxGridDBColumn;
    tvViewCol2: TcxGridDBColumn;
    tvViewCol4: TcxGridDBColumn;
    tvViewCol5: TcxGridDBColumn;
    Lv: TcxGridLevel;
    Master: TClientDataSet;
    DataMaster: TDataSource;
    Old: TPopupMenu;
    MenuItem1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    MenuItem2: TMenuItem;
    tvViewCol6: TcxGridDBColumn;
    tvViewCol7: TcxGridDBColumn;
    StatusBar1: TStatusBar;
    pm: TPopupMenu;
    N7: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    N8: TMenuItem;
    procedure ListView1Data(Sender: TObject; Item: TListItem);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzBut2Click(Sender: TObject);
    procedure RzBut8Click(Sender: TObject);
    procedure tvViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure MasterAfterInsert(DataSet: TDataSet);
    procedure RzBut4Click(Sender: TObject);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzBut3Click(Sender: TObject);
    procedure RzBut5Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure RzBut6Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tvViewDblClick(Sender: TObject);
    procedure RzBut1Click(Sender: TObject);
    procedure cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
    procedure cxLookupComboBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure N7Click(Sender: TObject);
    procedure N9Click(Sender: TObject);
    procedure N11Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure tvViewCol2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
  private
    { Private declarations }
    function UpdateIsDelete(DataSet : TClientDataSet;AFields : string; IsDele : Integer):Boolean;
    function IsDelete(lpValue : Integer):Boolean;
  public
    { Public declarations }
    dwADO : TADO;
    dwSQLText : string;
  end;

var
  frmReceivablesAccount: TfrmReceivablesAccount;
  g_TableName : string = 'contract_tree';
  g_Table_RevenueTree : string = 'sk_RevenueTree';

implementation

uses
  ufrmCompanyManage,
  TreeFillThrd,
  uDataModule,
  global,
  ufunctions,
  ufrmManage,
  ufrmPostAccounts;

{$R *.dfm}

function TfrmReceivablesAccount.IsDelete(lpValue : Integer):Boolean;
var
  i : Integer;
  szCount : Integer;
  szRowsCount : string;
  szIndex : Integer;
  s , szSQLText , str : string;

begin
  szCount := Self.tvView.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if szCount >= 0 then
  begin

      with Self.tvView  do
      begin

        for I := szCount -1 downto 0 do
        begin
          s :=  VarToStr(Controller.SelectedRows[i].Values[ Self.tvViewCol7.Index ] );
          if Length(s) = 0 then
            str := s
          else
            str := str + '","' + s;
        end;

        szSQLText := 'UPDATE ' + g_Table_RevenueTree +
                     ' SET IsDelete='+ IntToStr(lpValue) +' where ' +
                     Self.tvViewCol7.DataBinding.FieldName + ' in("' + str + '")';

        DM.ADOconn.BeginTrans; //开始事务
        try
          with DM.Qry do
          begin
            Close;
            SQL.Text := szSQLText;
            ExecSQL;
          end;
          DM.ADOconn.Committrans; //提交事

        except
          on E:Exception do
          begin
            DM.ADOconn.RollbackTrans;           // 事务回滚
          end;
        end;
      end;

  end;

end;

function TfrmReceivablesAccount.UpdateIsDelete(DataSet : TClientDataSet ;AFields : string; IsDele : Integer):Boolean;
begin
  {
  with AQry do
  begin
    if State <> dsInactive then
    begin
      Edit;
      FieldByName(AFields).AsInteger := IsDele ;
      UpdateBatch();
      Requery();
    end;
  end;
  }
end;

procedure TfrmReceivablesAccount.tvViewCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmReceivablesAccount.tvViewCol2PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  Child : TfrmCompanyManage;
  lvTelephone : Variant;

begin
  inherited;
  Child := TfrmCompanyManage.Create(Application);
  try
    Child.dwIsReadonly := True;
    Child.ShowModal;
    tvViewCol2.EditValue :=  Child.dwSignName;
  finally
    Child.Free;
  end;
end;

procedure TfrmReceivablesAccount.tvViewDblClick(Sender: TObject);
var
  szRowCount : Integer;
  szParentIndex : Integer;
begin
  inherited;
  szRowCount := Self.tvView.Controller.SelectedRecordCount;
  if szRowCount = 1 then
  begin
    with Self.Master do
    begin
      if State <> dsInactive then
      begin
        g_ParentIndex := FieldByName('parent').AsInteger;

        g_ModuleIndex := 1; //收款项目下的
        TfrmManage.Create(Application);
      end;
    end;
    Close;
  end else
  begin
    if szRowCount > 0 then
    begin
      ShowMessage('选择太多往来单位，无法确定要进入哪个！');
    end;
  end;
end;

procedure TfrmReceivablesAccount.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  dwADO.ADOIsEditing(Self.Master,AAllow);

end;

procedure TfrmReceivablesAccount.cxLookupComboBox1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    Self.RzBut1.Click;
  end;
end;

procedure TfrmReceivablesAccount.cxLookupComboBox1PropertiesCloseUp(
  Sender: TObject);
begin
  inherited;
  Self.RzBut1.Click;
end;

procedure TfrmReceivablesAccount.FormCreate(Sender: TObject);
begin
  inherited;
  Self.RzBut6.Click;
end;

procedure TfrmReceivablesAccount.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then  Close;
end;

procedure TfrmReceivablesAccount.ListView1Data(Sender: TObject;
  Item: TListItem);
begin
  if Assigned(Item.Data) then
  begin
    Dispose(Item.Data);
  end;
end;

procedure TfrmReceivablesAccount.MasterAfterInsert(DataSet: TDataSet);
var
  szparent : Integer;

begin
  with DataSet do
  begin
    FieldByName('parent').Value := DM.getLastId(g_Table_RevenueTree) + Self.Master.ChangeCount;
    FieldByName('Code').Value   := GetRowCode(g_Table_RevenueTree,'Code');
    Self.tvViewCol3.EditValue := Date;
  end;
end;

procedure TfrmReceivablesAccount.MenuItem1Click(Sender: TObject);
begin
  inherited;
  //历史工程
  Self.RzBut1.Enabled := False;
  Self.RzBut2.Enabled := False;
  Self.RzBut3.Enabled := False;
  Self.RzBut4.Enabled := False;
  Self.RzBut5.Enabled := False;

  Self.MenuItem2.Enabled := True;
  Self.N1.Enabled := True;

  Self.N9.Enabled := True;
  Self.N11.Enabled:= True;

  dwSQLText := 'Select * from ' + g_Table_RevenueTree + ' where ' + 'PID=0 AND IsDelete=1' ;
  dwADO.OpenSQL(Self.Master, dwSQLText);
end;

procedure TfrmReceivablesAccount.MenuItem2Click(Sender: TObject);
var
  szTableList : array[0..3] of TTableList;
  szCodeList  : string;
  szParent : Integer;
  szCode : string;

begin
  inherited;
  //彻底删除
  if Application.MessageBox( '是否需要要彻底删除数据，删除后无法恢复？', '提示:',
  MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin
    if Self.tvView.Controller.SelectedRowCount = 1 then
    begin
      with Self.tvView.DataController.DataSet do
      begin
        if not IsEmpty then
        begin
          szCode := FieldByName('Code').AsString;
          szParent := FieldByName('parent').AsInteger;
        end;

      end;

      DM.getTreeCode(g_Table_RevenueTree, szParent,szCodeList); //获取要删除的id表

      szTableList[0].ATableName := g_Table_Collect_Receivables;  //合同总价
      szTableList[0].ADirectory := g_CollectReceivables;

      szTableList[1].ATableName := g_Table_Income; //交易记录
      szTableList[1].ADirectory := g_dir_BranchMoney;

      szTableList[2].ATableName := g_Table_pactDebtmoney; //债务组合
      szTableList[2].ADirectory := '';

      szTableList[3].ATableName := g_Table_DetailTable; //交易记录明细
      szTableList[3].ADirectory := '';


      DeleteTableFile(szTableList,szCodeList);
      DM.InDeleteData(g_Table_RevenueTree,szCodeList);
      Self.MenuItem1.Click;
    end else
    begin
      if Self.tvView.Controller.SelectedRowCount > 1 then
      begin
        Application.MessageBox( '禁止同时删除多条工程，只允许一次一条！', '提示:', MB_OKCANCEL + MB_ICONWARNING) ;
      end;
    end;

  end;

end;

procedure TfrmReceivablesAccount.N11Click(Sender: TObject);
begin
  inherited;
  Self.MenuItem2.Click;
end;

procedure TfrmReceivablesAccount.N1Click(Sender: TObject);
begin
  if Application.MessageBox( '是否要恢复所有选中的数据？', '恢复数据？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin
    IsDelete(0);
    Self.MenuItem1.Click;
  end;
end;

procedure TfrmReceivablesAccount.N4Click(Sender: TObject);
begin
  inherited;
  RzBut5.Click;
end;

procedure TfrmReceivablesAccount.N7Click(Sender: TObject);
begin
  inherited;
  Self.MenuItem1.Click;
end;

procedure TfrmReceivablesAccount.N8Click(Sender: TObject);
begin
  inherited;
  Self.RzBut4.Click;
end;

procedure TfrmReceivablesAccount.N9Click(Sender: TObject);
begin
  inherited;
  Self.N1.Click;
end;

procedure TfrmReceivablesAccount.RzBut8Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmReceivablesAccount.RzBut1Click(Sender: TObject);
var
  s : string;
begin
  inherited;
  //搜索
  s := Self.cxLookupComboBox1.Text;
//  GridLocateRecord(Self.tvView,Self.tvViewCol2.DataBinding.FieldName,s);

  dwSQLText := 'Select * from ' + g_Table_RevenueTree +
  ' where ' + 'PID=0 AND IsDelete=0 AND ' + Self.tvViewCol2.DataBinding.FieldName + '="' + s + '"' ;
  dwADO.OpenSQL(Self.Master, dwSQLText);
end;

procedure TfrmReceivablesAccount.RzBut2Click(Sender: TObject);
begin
  dwADO.ADOAppend(Self.Master);
  Self.Grid.SetFocus;
  Self.tvViewCol2.FocusWithSelection;
end;

procedure TfrmReceivablesAccount.RzBut4Click(Sender: TObject);
begin
  dwADO.ADOSaveData(Self.Master,dwSQLText);
end;

procedure TfrmReceivablesAccount.RzBut5Click(Sender: TObject);
begin
  inherited;
  if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin
    IsDelete(1);
    Self.RzBut6.Click();
  end;
end;

procedure TfrmReceivablesAccount.RzBut3Click(Sender: TObject);
begin
  inherited;
  dwADO.ADOIsEdit(Self.Master);
end;

procedure TfrmReceivablesAccount.RzBut6Click(Sender: TObject);
begin
  inherited;
  //刷新
  Self.RzBut1.Enabled := True;
  Self.RzBut2.Enabled := True;
  Self.RzBut3.Enabled := True;
  Self.RzBut4.Enabled := True;
  Self.RzBut5.Enabled := True;
  Self.RzBut6.Enabled := True;

  Self.MenuItem2.Enabled := False;
  Self.N1.Enabled := False;

  Self.N9.Enabled := False;
  Self.N11.Enabled:= False;

  dwSQLText := 'Select * from ' + g_Table_RevenueTree + ' where ' + 'PID=0 AND IsDelete=0' ;
  dwADO.OpenSQL(Self.Master, dwSQLText);

  GetMaintainInfo;
end;

end.


