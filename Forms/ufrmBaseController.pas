unit ufrmBaseController;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,cxGridCustomView,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,global,
  cxDBData, cxGridLevel, Data.Win.ADODB, RzTray, dxPSGlbl, dxPSUtl, dxPSEngn,
  dxPrnPg, dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinscxPCPainter, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxSkinsdxBarPainter,
  dxSkinsdxRibbonPainter, dxPSCore, dxPScxCommon, cxClasses,cxGridDBTableView,cxGrid,
  frxClass, frxDesgn, frxDBSet,cxGridDBBandedTableView, cxGridCustomTableView,cxGridBandedTableView,
  Vcl.ComCtrls,FillThrdTree ,UtilsTree, frxCross, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, cxGridTableView,
  Datasnap.Provider,cxDropDownEdit, Vcl.Menus, dxBar, BMDThread ;

type
  TfrmBaseController = class(TForm)
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }

    procedure PopupWindowKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

  public
    { Public declarations }
    dwProjectName : Variant;
    dwSignName : Variant;
    dwBuildComPany : Variant;

    g_SettleStatus : array[0..1] of string;
    g_DetailArray  : array[0..10] of string;
    g_ConcreteArray: array[0..4] of string;
    g_AutoCalcParameter : array[0..2] of string;
    g_ReportPath   : string;
    function GetTableSelectedId(lpGrid : TcxGridDBTableView; lpIndex : Integer):string;
    function GetMaintainInfo():Boolean;
    function GetProjectSurvey(lpCode , lpFieldNmae : string ):string;
    function GetCompanyInfo(var AProperties : TcxCustomEditProperties ):Boolean;
    function FunExpCalc(str : string ; lpDecimal : Integer  ):string;
    function GetCompany(var Box : TStringList ; Pos : Integer):Boolean;
    function GetPayType(var Box : TStringList ; Pos : Integer):Boolean;
    function IsNewRow():BOOL;
    function GetTreeLevelName(Node : TTreeNode ):string;
    function IsSaveData(tvGrid : TcxGridDBTableView ; PostIndex : Integer ; lpQry : TADOQuery):Boolean;
    function ClearNullData(lpTableName , lpFildName : string):Integer;
    function IsDelete(lpView : TcxGridDBTableView) : Boolean;
    function IsDeleteEmptyData(lpADO : TADOQuery ; lpTableName , lpSignName : string):Boolean;
    function GridLocateRecord(View : TcxGridDBTableView; FieldName, LocateValue : String) : Boolean;
    function IsRport(Qry : TADOQuery):BOOL;
    function CreateEnclosure(Dataset : TDataSet ;h : THandle; lpDir : string):Boolean;//创建附件
    function GetTreeIndexList(Anode: TTreeNode;var s : string):string;stdcall;
    function ADOIsEdit(IsStatus : Variant ; ADOQry : TADOQuery):Boolean;
    function ADOBanEditing(ADO : TADOQuery):Boolean;
    procedure SetNewGridFocus(lpGridView : TcxGridDBTableView; lpColName : string);
    procedure DeleteSelection(lpGrid : TcxGridDBTableView ; lpDir  , lpTableName , lpFieldName : string );
    procedure IsDeleteData(lpGrid : TcxGridDBTableView ; lpDir  , lpTableName , lpFieldName : string );
    procedure GetFrxDataSet(var frxData : TfrxDBDataset; Grid : TcxGridDBTableView);
    procedure CxGridToExcel(AcxGrid: TcxGrid ; TitleName: string);
    procedure SetGridFocus(lpGridView : TcxGridDBTableView);
    procedure SaveGrid(lpQry : TADOQuery);
    procedure SetGrid(lpGridColumn : TcxGridDBColumn; lpIndex : Byte;lpFromtName : string);
    procedure cxGridDeleteData(lpGrid : TcxGridDBTableView ; lpDir ,  lpTableName : string);
    procedure GridDBBandedTable(lpGrid : TcxGridDBBandedTableView ; lpDir  , lpTableName: string );
    procedure getSontreeIndex(Anode: TTreeNode;var s : string);
    function GetIndexList(Anode: TTreeNode;var s : string):string;stdcall;
    procedure GetfrxDBDataset(var Dataset : TfrxDBDataset; tvDealGrid:TcxGridDBTableView);
    function GetTableMaintain(var AProperties : TcxCustomEditProperties ; ATableName :string ):Boolean;
    procedure PropertiesInitPopup(Sender: TObject);
    procedure SetGridStuat(lpGrid : TcxGridDBTableView ; lpFileName : string ; lpValue : Variant);
    
  end;

var
  frmBaseController: TfrmBaseController;

implementation

uses

   uDataModule,UnitExpCalc,System.StrUtils,System.Math,cxGridExportLink,ShellAPI,ActiveX;

type
  TCusDropDownEdit = class(TcxCustomDropDownEdit);

{$R *.dfm}

procedure TfrmBaseController.PopupWindowKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = 27 then //如果按下“ESC”键
    TcxCustomEditPopupWindow(sender).ClosePopup;//关闭弹出窗
end;

procedure TfrmBaseController.PropertiesInitPopup(
  Sender: TObject);
begin
  inherited;
  TCusDropDownEdit(sender).PopupWindow.KeyPreview := true;//令弹出窗按键事件有效
  TCusDropDownEdit(sender).PopupWindow.OnKeyDown  := PopupWindowKeyDown;//关联按键事件给弹出窗
end;

procedure TfrmBaseController.GetfrxDBDataset(var Dataset : TfrxDBDataset; tvDealGrid:TcxGridDBTableView);
var
  i : Integer;
  szFieldName : string;
  szCaption   : string;
  szName      : string;
begin
  Dataset.FieldAliases.Clear;
  with tvDealGrid do
  begin
    for I := 1 to ColumnCount-1 do
    begin
      Application.ProcessMessages;
      szFieldName := Columns[i].DataBinding.FieldName ;
      szCaption   := Columns[i].Caption;
      szName := szFieldName + '=' + szCaption  ;
      Dataset.FieldAliases.Add(szName);
    end;
  end;
end;

function TfrmBaseController.CreateEnclosure(Dataset : TDataSet ;h : THandle; lpDir : string):Boolean;
var
  szNumbers : string;
begin
  inherited;
  with DataSet do
  begin
    if not IsEmpty then
    begin
      szNumbers := FieldByName('Numbers').AsString;
      CreateOpenDir(H,Concat(lpDir,'\' + szNumbers),True);
    end;
  end;
end;

function TfrmBaseController.IsRport(Qry : TADOQuery):BOOL;
begin
  Result := False;
  with Qry do
  begin
    if State = dsInactive then
    begin
      Application.MessageBox( '当前查询状态无效，无法进行报表操作？', '提示:', MB_OKCANCEL + MB_ICONWARNING)
    end else
    begin
      Result := True;
    end;
  end;
end;

procedure TfrmBaseController.GetFrxDataSet(var frxData : TfrxDBDataset; Grid : TcxGridDBTableView);
var
  i : Integer;
  szFieldName : string;
  szCaption   : string;
  szName      : string;

begin
  frxData.FieldAliases.Clear;
  with Grid do
  begin
    for I := 1 to ColumnCount-1 do
    begin
      Application.ProcessMessages;
      szFieldName := Columns[i].DataBinding.FieldName ;
      szCaption   := Columns[i].Caption;
      szName := szFieldName + '=' + szCaption  ;
      frxData.FieldAliases.Add(szName);
    end;
  end;

end;

function TfrmBaseController.IsSaveData(tvGrid : TcxGridDBTableView ; PostIndex : Integer ; lpQry : TADOQuery):Boolean;
begin
  Result := False;
  if tvGrid.Controller.FocusedColumnIndex = PostIndex then
  begin
    with lpQry do
    begin

      if State <> dsInactive then
      begin
        if (State = dsEdit) or (State = dsInsert) then
        begin
          Post;
          Refresh;
        end;

      end;

    end;

    if tvGrid.Controller.FocusedRow.IsLast then
    begin
      Result := True;
    end;

  end;
end;

function TfrmBaseController.GetTreeLevelName(Node : TTreeNode ):string;
begin
  while Node <> nil do
  begin
    if Node.Level = 1 then
    begin
      Result := Node.Text;
      Break;
    end;
    Node := Node.Parent;
  end;
end;

function TfrmBaseController.IsNewRow():BOOL;
begin
  if MessageBox(handle, PChar('是否要新增一条?'),'提示',
     MB_ICONQUESTION + MB_YESNO) = IDYES then // + MB_DEFBUTTON2
  begin
    Result := True;
  end else
  begin
    Result := False;
  end;
end;

procedure TfrmBaseController.SaveGrid(lpQry : TADOQuery);
begin
  with lpQry do
  begin
    if (State = dsEdit) or (State = dsInsert) then
    begin
      Post;
      refresh;
      Application.MessageBox( '记录保存成功。', '保存记录', MB_OK + MB_ICONQUESTION)
    end else
    begin
      Application.MessageBox( '当前操作处于非编辑或插入状态保存！！', '保存记录', MB_OK + MB_ICONEXCLAMATION)
    end;

  end;

end;

procedure TfrmBaseController.SetGridFocus(lpGridView : TcxGridDBTableView);
var
  i : Integer;
  szFieldName : string;

begin
  for I := 0 to lpGridView.ColumnCount-1 do
  begin
    szFieldName := lpGridView.Columns[i].DataBinding.FieldName;
    if (lpGridView.Columns[i].Visible) and
       (szFieldName <> '') and
       (LowerCase( szFieldName ) <> 'numbers') then
    begin
      lpGridView.Columns[i].FocusWithSelection;
      Break;
    end;
  end;
end;

procedure TfrmBaseController.SetNewGridFocus(lpGridView : TcxGridDBTableView; lpColName : string);
var
  i : Integer;
  szFieldName : string;

begin
  for I := 0 to lpGridView.ColumnCount-1 do
  begin
    szFieldName := lpGridView.Columns[i].DataBinding.FieldName;
    if (lpGridView.Columns[i].Visible) and (szFieldName = lpColName) then
    begin
      lpGridView.Columns[i].Focused := True;
      Break;
    end;
  end;
end;

procedure TfrmBaseController.CxGridToExcel(AcxGrid: TcxGrid ; TitleName : string);
var
  SaveDialog: TSaveDialog;
begin
  SaveDialog := TSaveDialog.Create(nil);
  with SaveDialog do
  begin
    FileName := TitleName + '_' + FormatDateTime('YYMMDDHHmmSS', now);
    Options := [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing];
    Filter := '*.xls|*.xls';
    if Execute then
    begin

      ExportGridToExcel(FileName, AcxGrid,True,True);

      ShellExecute(0,'open',PWideChar(FileName  + '.xls'),nil,nil,SW_SHOW);
    end;
  end;
  SaveDialog.Free;
end;

function TfrmBaseController.GetCompany(var Box : TStringList ; Pos : Integer):Boolean;
begin
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from unit where chType=' + IntToStr(pos);
    Open;

    if 0 <> RecordCount then
    begin

      while not Eof do
      begin
        Box.Add(FieldByName('name').AsString );
        Next;
      end;

    end;

  end;
end;

function TfrmBaseController.GetCompanyInfo(var AProperties : TcxCustomEditProperties ):Boolean;
begin

  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    DM.Qry.Close;
    DM.Qry.SQL.Clear;
    DM.Qry.SQL.Text := 'Select * from ' + g_Table_Maintain_CompanyInfoTree + ' WHere PID=0';
    DM.Qry.Open;

    if 0 <> DM.Qry.RecordCount then
    begin
      while not DM.Qry.Eof do
      begin
        Items.Add(DM.Qry.FieldByName('SignName').AsString);
        DM.Qry.Next;
      end;

    end;

  end;

end;

function TfrmBaseController.GetTableMaintain(var AProperties : TcxCustomEditProperties ; ATableName :string ):Boolean;
begin
  DM.Qry.Close;
  DM.Qry.SQL.Clear;
  DM.Qry.SQL.Text := 'Select * from ' + ATableName;
  DM.Qry.Open;
  if DM.Qry.RecordCount <> 0 then
  begin
    with (AProperties AS TcxComboBoxProperties) do
    begin
      Items.Clear;
      while Not DM.Qry.Eof do
      begin
        Items.Add(DM.Qry.Fields[1].AsString);
        DM.Qry.Next;

      end;

    end;

  end;

end;

function TfrmBaseController.GetPayType(var Box : TStringList ; Pos : Integer):Boolean;
begin
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from pay_type';
    Open;
    if 0 <> RecordCount then
    begin
      while not Eof do
      begin
        Box.Add( FieldByName('pay_name').AsString );
        Next;
      end;

    end;

  end;
end;

procedure SQLDelete( lpTableName , str : string);
begin
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'delete * from ' +  lpTableName +' where '+ 'numbers' +' in("' + str + '")';
    ExecSQL;
  end;
end;

procedure TfrmBaseController.cxGridDeleteData(lpGrid : TcxGridDBTableView ; lpDir  , lpTableName: string );
var
  Id : string;
  szRecordCount : Integer;
  str , dir :string;
  I: Integer;
  adoTmp : TADOQuery;
  szCount : Integer;
  szRowsCount : string;
  szIndex : Integer;

begin
  szCount := lpGrid.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if  Application.MessageBox(PWideChar( '您共选择: ( '+ szRowsCount +' ) 条数据, 确定要删除所有选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin

    with lpGrid.DataController.DataSource.DataSet do
    begin
      if not IsEmpty then
      begin

        for I := 0 to szCount -1 do
        begin
          szIndex := lpGrid.GetColumnByFieldName('numbers').Index;
          Id := lpGrid.Controller.SelectedRows[i].Values[szIndex];

          if Length(lpDir) <> 0 then
          begin
            dir := ConcatEnclosure(lpDir, id);
            if DirectoryExists(dir) then
            begin
              DeleteDirectory(dir);
            end;

          end;

          if Length(str) = 0 then
            str := Id
          else
            str := str + '","' + Id;

        end;

        SQLDelete(lpTableName,str);

      end;

    end;

  end;

end;

procedure TfrmBaseController.GridDBBandedTable(lpGrid : TcxGridDBBandedTableView ; lpDir  , lpTableName: string );
var
  Id : string;
  szRecordCount : Integer;
  str , dir :string;
  I: Integer;
  adoTmp : TADOQuery;
  szCount : Integer;
  szRowsCount : string;
  szIndex : Integer;

begin
  szCount := lpGrid.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if  Application.MessageBox(PWideChar( '您共选择: ( '+ szRowsCount +' ) 条数据, 确定要删除所有选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin

    with lpGrid.DataController.DataSource.DataSet do
    begin
      if not IsEmpty then
      begin

        for I := 0 to szCount -1 do
        begin
          szIndex := lpGrid.GetColumnByFieldName('numbers').Index;
          Id := lpGrid.Controller.SelectedRows[i].Values[szIndex];

          if Length(lpDir) <> 0 then
          begin
            dir := ConcatEnclosure(lpDir, id);
            if DirectoryExists(dir) then
            begin
              DeleteDirectory(dir);
            end;

          end;

          if Length(str) = 0 then
            str := Id
          else
            str := str + '","' + Id;

        end;

        SQLDelete(lpTableName,str);

      end;

    end;

  end;
end;

procedure TfrmBaseController.IsDeleteData(lpGrid : TcxGridDBTableView ; lpDir  , lpTableName , lpFieldName : string );
var
  Id : string;
  szRecordCount : Integer;
  str , dir :string;
  I: Integer;
  adoTmp : TADOQuery;
  szCount : Integer;
  szRowsCount : string;
  szIndex : Integer;
  s : Variant ;

begin
  szCount := lpGrid.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if szCount > 0 then
  begin
      if  Application.MessageBox(PWideChar( '您共选择: ( '+ szRowsCount +' ) 条数据, 确定要删除所有选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
      begin

        with lpGrid  do
        begin
          if not DataController.DataSource.DataSet.IsEmpty then
          begin
            str := '';
            for I := szCount -1 downto 0 do
            begin
              szIndex := GetColumnByFieldName(lpFieldName).Index;

              s := Controller.SelectedRows[i].Values[szIndex];
              Id := VarToStr(s);

              if Length(id) <> 0 then
              begin

                if Length(lpDir) <> 0 then
                begin
                  dir := ConcatEnclosure(lpDir, id);
                  if DirectoryExists(dir) then
                  begin
                    DeleteDirectory(dir);
                  end;

                end;

                if Length(str) = 0 then
                  str := Id
                else
                  str := str + ',' + Id;

              end else
              begin
                Controller.DeleteSelection;
              end;

            end;

            if (str <> ',') and (Length(str) <> 0) then
            begin

              with DM.Qry do
              begin

                Close;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  lpTableName +' where '+ 'Id' +' in(' + str + ')';
                ExecSQL;

              end;

            end;

          end;

        end;

      end;
  end;

end;

function TfrmBaseController.IsDelete(lpView : TcxGridDBTableView) : Boolean;
begin
  Result := False;
  with lpView do
  begin
    if DataController.RowCount <> 0 then
    begin
      if Application.MessageBox('确认删除当前记录?','确认删除',MB_YesNo+MB_IconQuestion) = IDYES then
      begin
        Controller.DeleteSelection;
        Result := True;
      end;
    end;
  end;

end;

procedure TfrmBaseController.getSontreeIndex(Anode: TTreeNode;var s : string);
var
  i: integer;
  node: TTreeNode;
  szData : PNodeData;
  szIndex: string;

begin
    for i := 0 to Anode.Count - 1 do
    begin
      Node := ANode.Item[i];
      if Assigned(node) then
      begin
        szData := PNodeData(Node.Data);
        szIndex:= IntToStr(szData.Index);
        if Length(s) = 0 then
          s := szIndex + ''',''' + IntToStr( PNodeData(ANode.Data).Index )
        else
          s := s + ''','''+ szIndex;
      end;

      if (node.Count > 0)   then
      begin
        getSontreeIndex(Node , s);
      end;

    end;
end;

function TfrmBaseController.GetTreeIndexList(Anode: TTreeNode;var s : string):string;stdcall;
var
  i: integer;
  node: TTreeNode;
  szData : PNodeData;
  szCodeList : string;

begin
  if Assigned(Anode) then
  begin
    szData := PNodeData(ANode.Data);
    s := IntToStr( szData.Index );
    if Anode.Count <> 0 then
    begin
      getSontreeIndex(Anode,szCodeList);
      Result := szCodeList;
    end
    else
    begin
      Result := s;
    end;

    s := Result;
  end;

end;


procedure getSonIndex(Anode: TTreeNode;var s : string);
var
  i: integer;
  node: TTreeNode;
  szData : PNodeData;
  szIndex: string;

begin
  for i := 0 to Anode.Count - 1 do
  begin
    Node := ANode.Item[i];
    if Assigned(node) then
    begin
      szData := PNodeData(Node.Data);
      szIndex:= IntToStr(szData.Index);
      if Length(s) = 0 then
        s := szIndex + ',' + IntToStr( PNodeData(ANode.Data).Index )
      else
        s := s + ','+ szIndex;
    end;

    if (node.Count > 0)   then
    begin
      getSonIndex(Node , s);
    end;

  end;
end;

function TfrmBaseController.GetIndexList(Anode: TTreeNode;var s : string):string;stdcall;
var
  i: integer;
  node: TTreeNode;
  szData : PNodeData;
  szCodeList : string;

begin
  if Assigned(Anode) then
  begin
    szData := PNodeData(ANode.Data);
    s := IntToStr( szData.Index );
    if Anode.Count <> 0 then
    begin
      getSonIndex(Anode,szCodeList);
      Result := szCodeList;
    end
    else
    begin
      Result := s;
    end;

    s := Result;
  end;

end;


function TfrmBaseController.ClearNullData(lpTableName , lpFildName : string):Integer;
begin
  DM.Qry.SQL.Clear;
  DM.Qry.SQL.Text:='delete from '+ lpTableName +' where '+ lpFildName + ' is NULL'; // AND ID IS NULL
  Result := DM.Qry.ExecSQL;
end;


function HaveSigned(MaxWaitTime: Cardinal): Boolean;
var
  I:Integer;
  WaitedTime:Cardinal;
begin
  WaitedTime:=0;
  while (WaitedTime<MaxWaitTime)   do
  begin
    SleepEx(100,False);
    Inc(WaitedTime,100);
    Application.ProcessMessages ;
  end
end;

function QueryAppend(lpQry : TADOQuery) : Boolean;
begin
  Result := False;
  with lpQry do
  begin
    if State <> dsInactive then
    begin
      Append;
      Result := True;
    end;
  end;
end;

function TfrmBaseController.IsDeleteEmptyData(lpADO : TADOQuery ; lpTableName , lpSignName : string):Boolean;
var
  I : Integer;
  s : string;
  Id: Integer;

begin
  Result := False;

  with lpADO do
  begin

    if (not IsEmpty) and (State <> dsInactive) then
    begin
      case State of
        dsInactive: ;
        dsBrowse: ;
        dsEdit: UpdateBatch(arAll);
        dsInsert: Post;
        dsSetKey: ;
        dsCalcFields: ;
        dsFilter: ;
        dsNewValue: ;
        dsOldValue: ;
        dsCurValue: ;
        dsBlockRead: ;
        dsInternalCalc: ;
        dsOpening: ;
      end;
      HaveSigned(300);
      {
      if State = dsInsert then
      begin
        Post;
        HaveSigned(30);
      end;
      }
      {
      for I := RecordCount downto 0 do
      begin
        RecNo := i + 1;  //指定序号

        s := FieldByName(lpSignName).AsString;
        Id:= FieldByName('Id').AsInteger;
        OutputLog('RecNo:' + IntToStr(RecNo) + ' s:' +  s); // and (Id = 0)
        if (s = '') then
        begin
        //  HaveSigned(30);
          Delete;
        end;

      end;
      }
      if (lpTableName <> '') and (lpSignName <> '') then
      begin
         ClearNullData(lpTableName,lpSignName);
      end;

      if not IsEmpty then
      begin
        UpdateBatch(arAll);
        Requery();
        ShowMessage('保存完成!');
      end;

    end;

  end;

  {
  with Self.ADOWork do
  begin
    if (Not IsEmpty) and ( State <> dsInactive ) then
    begin
      if (State = dsEdit) or (State = dsInsert)  then Post;

      for i := RecordCount downto 0 do
      begin
        RecNo := i + 1;  //指定序号
        s := FieldByName( lpFieldName ).AsString;
        Id:= FieldByName( 'Id' ).AsInteger;
        if (s = '') and (Id = 0) then
        begin
          Delete;
          HaveSigned(30);
        end;
      end;
    //  if Not IsEmpty then RecNo := RecordCount + 1;
    end;
  end;
  }
  {
  with Self.ADOSubContract do
  begin

    if (not IsEmpty) and (State <> dsInactive) then
    begin
      if (State = dsEdit) or (State = dsInsert) then Post;

      for I := RecordCount downto 0 do
      begin
        RecNo := i+1;  //指定序号
        s := FieldByName(Self.tvSubContractCol2.DataBinding.FieldName).AsString;
        Id:= FieldByName(Self.tvSubContractColumn1.DataBinding.FieldName).AsString;
        if (s = '') and (Id = '') then
        begin
          Delete;
        end;

      end;

      if not IsEmpty then
      begin
        UpdateBatch(arAll);
        ShowMessage('保存完成!:' + IntToStr(RecordCount));
      end;
    end;
  end;
  }
end;

procedure TfrmBaseController.DeleteSelection(lpGrid : TcxGridDBTableView ; lpDir  , lpTableName , lpFieldName : string );
var
  Id : string;
  szRecordCount : Integer;
  str , dir :string;
  I: Integer;
  adoTmp : TADOQuery;
  szCount : Integer;
  szRowsCount : string;
  szIndex : Integer;
  s : Variant ;

begin
  szCount := lpGrid.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if szCount > 0 then
  begin
      if Application.MessageBox(PWideChar( '您共选择: ( '+ szRowsCount +' ) 条数据, 确定要删除所有选中的记录吗？' ), '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
      begin

        with lpGrid  do
        begin
          if not DataController.DataSource.DataSet.IsEmpty then
          begin
            str := '';
            for I := szCount -1 downto 0 do
            begin

              szIndex := GetColumnByFieldName(lpFieldName).Index;
              s := Controller.SelectedRows[i].Values[szIndex];
              Id := VarToStr(s);

              if Length(id) <> 0 then
              begin

                if Length(lpDir) <> 0 then
                begin
                  dir := ConcatEnclosure(lpDir, id);
                  if DirectoryExists(dir) then
                  begin
                    DeleteDirectory(dir);
                  end;

                end;

                if Length(str) = 0 then
                  str := Id
                else
                  str := str + '","' + Id;
              end;

            end;
            Controller.DeleteSelection;

            if (str <> ',') and (Length(str) <> 0) and ( Length( lpFieldName ) <> 0) then
            begin

              with DM.Qry do
              begin

                Close;
                SQL.Clear;
                SQL.Text := 'delete * from ' +  lpTableName +' where '+ lpFieldName +' in("' + str + '")';

                ExecSQL;

              end;

            end;

          end;

        end;

      end;
  end;

end;

function TfrmBaseController.GridLocateRecord(View : TcxGridDBTableView; FieldName, LocateValue : String) : Boolean;
begin
  {表格数据查找定位}
  Result := False;
  if (View.GetColumnByFieldName(FieldName) <> nil) then
  begin
    Result := View.DataController.Search.Locate(View.GetColumnByFieldName(FieldName).Index, LocateValue);
  end;
end;

function TfrmBaseController.GetProjectSurvey(lpCode , lpFieldNmae : string ):string;
begin
  Result := '';
  with DM.Qry do
  begin
    Close;
    SQL.Text := 'Select * from ' + g_Table_Project_Survey + ' Where code = "' + lpCode + '"';
    Open;
    if RecordCount <> 0 then
    begin
      Result := FieldByName(lpFieldNmae).AsString;
    end;
  end;

end;

function TfrmBaseController.ADOIsEdit(IsStatus : Variant ; ADOQry : TADOQuery):Boolean;
begin
  if IsStatus then
  begin
    with ADOQry do
    begin
      if State <> dsInactive then
        begin
          if (State <> dsEdit) and ( State <> dsInsert ) then
             Edit;
        end;
    end;
  end else
  begin
    Application.MessageBox( '冻结状态下不可以编辑！', '提示:', MB_OK + MB_ICONWARNING)
  end;

end;

function TfrmBaseController.ADOBanEditing(ADO : TADOQuery):Boolean;
begin
  Result := False;
  with ADO do
  begin
    if State <> dsInactive then
    begin
      if (State <> dsEdit) AND (State <> dsInsert) then
        Result := False
      else
        Result := true;
    end;
  end;
end;

function TfrmBaseController.GetMaintainInfo():Boolean;
begin
  DM.DataInitialization;
end;

procedure TfrmBaseController.FormCreate(Sender: TObject);
begin
  g_SettleStatus[0] := '已结算';
  g_SettleStatus[1] := '未结算';

  g_DetailArray[0] := '项目扣罚';
  g_DetailArray[1] := '甲方供材';
//  g_DetailArray[2] := '税金';
  g_DetailArray[3] := '水电';
  g_DetailArray[4] := '发票';
  g_DetailArray[5] := '增项';
  g_DetailArray[6] := '它项';
  g_DetailArray[7] := '奖励';

  g_ConcreteArray[0] := '砼强度';
  g_ConcreteArray[1] := '传送方式';
  g_ConcreteArray[2] := '外加剂';
  g_ConcreteArray[3] := '抗渗等级';
  g_ConcreteArray[4] := '其他技术要求';

  g_AutoCalcParameter[0] := '税金';
  g_AutoCalcParameter[1] := '工程直接费的总额';
  g_AutoCalcParameter[2] := '装修面积';

  g_ReportPath := ExtractFilePath(ParamStr(0)) + 'Report';
  GetMaintainInfo;
end;

procedure TfrmBaseController.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Close;
end;

procedure TfrmBaseController.SetGrid(lpGridColumn : TcxGridDBColumn; lpIndex : Byte;lpFromtName : string);
var
  szFromName : string;
  szClassName: string;
  szTableName: string;
  szCaption : string;

begin
  CoInitialize(nil);  //必须调用（需Uses ActiveX）
  try
    szTableName := 'sk_Config';
    szClassName := lpGridColumn.Name;
    with DM.Qry do
    begin
    // Application.ProcessMessages;
      Close();
      SQL.Clear;
      SQL.Text := 'Select * from ' + szTableName +
               ' where fromName="' + lpFromtName +
               '" and ClassName="' + szClassName + '"' ;
      case lpIndex of
        0:
        begin
          Open;
          if RecordCount <> 0 then
          begin
            lpGridColumn.Caption := FieldByName('Caption').AsString;
            lpGridColumn.Width   := FieldByName('Width').AsInteger;
            lpGridColumn.Visible := FieldByName('IsView').AsBoolean;
          end else
          begin
            Insert;
            FieldByName('fromName').Value  := lpFromtName;
            FieldByName('Caption').Value   := lpGridColumn.Caption;
            FieldByName('ClassName').Value := lpGridColumn.Name;
            FieldByName('IsView').Value    := lpGridColumn.Visible;
            FieldByName('Width').Value     := lpGridColumn.Width;
            Post;
          end;
        end;
        1:
        begin
          Open;
          if RecordCount <> 0 then
          begin
            Edit;
            FieldByName('Width').Value   := lpGridColumn.Width;
            Post;
          end;

        end;

      end;

    end;
  finally
    CoUninitialize;
  end;
end;

function TfrmBaseController.FunExpCalc(str : string ; lpDecimal : Integer  ):string;
var
  infoStr, signParamStr, showItemStr: string;
  f1: double;
  i, len, decN, signP: integer;
  r : string;
  s : string;

begin
  if str <> '' then
  begin
    len := Length(str);
    for i := 1 to len do
    begin
      if ord(str[i]) in [10, 13] then str[i] := ' ';    // 先去掉回车符

      if (Ord(str[i]) = 65288) then str[i] := '(';
      if (Ord(str[i]) = 65289) then str[i] := ')';
      if (Ord(str[i]) = 65292) then str[i] := ',';
      if (Ord(str[i]) = 12304) or (Ord(str[i]) = 12310) then str[i] := '[';
      if (Ord(str[i]) = 12305) or (Ord(str[i]) = 12311) then str[i] := ']';

      if (ord(str[i]) = 163) and ((i + 1) <= len) then  // 替换全角的括号
      begin
        if ord(str[i + 1]) = 168 then
        begin
          str[i] := '(';
          str[i + 1] := ' ';
        end;

        if ord(str[i + 1]) = 169 then
        begin
          str[i] := ')';
          str[i + 1] := ' ';
        end;
      end;  // if
      if (ord(str[i]) = 163) and ((i + 1) <= len) then  // 替换全角的逗号
        if ord(str[i + 1]) = 172 then
        begin
          str[i] := ',';
          str[i + 1] := ' ';
        end;
    end;   // for
    str :=  StringReplace(str, ' ', '', [rfReplaceAll]);
    // ********************************
    InitSignParam();      // 清除前面自定义的参数，恢复常数 pi, e
    // 表达式中以 @@之后的 表示符号参数标记内容 格式为 'a=1,b=2.1,c=3'
    signP := pos('@@', str);
    if signP > 0 then
    begin
      signParamStr := midStr(str, signP + 2, Length(str) - signP - 1);
      signParamStr := StringReplace(signParamStr, ' ', '', [rfReplaceAll]);
      AddSignParam(signParamStr);
      str := leftStr(str, signP - 1);
    end;

     // ********************************
    if CalcExp(str, f1, infoStr) then
    begin

      case lpDecimal of  //保留小数
        0: decN := 0;
        1: decN := -1;
        2: decN := -2;
        3: decN := -3;
        4: decN := -4;
        5: decN := -5;
        6: decN := -6;
        7: decN := -7;
        8: decN := -8;
        9: decN := 100;
      else
        decN := 100;
      end;

      if decN <= 0 then
      begin
        r := floatToStr(RoundTo(f1, decN))  //结果
      end
      else
      begin
        r := floatToStr(f1);  //结果
      end;
      {
      if signP > 0 then
         showItemStr := str + '@@' + signParamStr + '  =  ' + r //结果
      else
         showItemStr := str + ' = ' + r;
      }
      Result := r;

    end
    else
    begin

      if signP > 0 then
         showItemStr := str + '@@' + signParamStr + '  =  出错' + infoStr
      else
        showItemStr := str + ' = 出错: ' + infoStr;

      Result := showItemStr;
    //  Application.MessageBox(PWideChar( showItemStr ), '提示:', MB_OKCANCEL + MB_ICONWARNING)

    end;
  end else
  begin
    Result := '';
  end;
end;

//修改表现值
procedure TfrmBaseController.SetGridStuat(lpGrid : TcxGridDBTableView ; lpFileName : string ; lpValue : Variant);
var
  i:Integer;
begin
  with lpGrid do
  begin
    BeginUpdate();
    for i := 0 to Controller.SelectedRowCount-1 do
    begin
      Application.ProcessMessages;
      Controller.FocusedRow := Controller.SelectedRows[i];
      DataController.DataSet.Edit;
      DataController.DataSet.FieldByName(lpFileName).Value := lpValue;
      DataController.DataSet.Post;
    end;
    EndUpdate;
  end;

end;

function TfrmBaseController.GetTableSelectedId(lpGrid : TcxGridDBTableView; lpIndex : Integer):string;
var
  s : string;
  szCount : Integer;
  szRowsCount  : Integer;
  i : Integer;
  Id : string;
  szDir : string;
begin
  Result := '';
  szCount := lpGrid.Controller.SelectedRowCount;
  if szCount >= 0 then
  begin
    with lpGrid do
    begin
      if DataController.IsEditing then DataController.UpdateData;
      for I := szCount -1 downto 0 do
      begin
        Id := VarToStr(  Controller.SelectedRows[i].Values[ lpIndex ] );
        if Length(id) <> 0 then
        begin
          if Length(Result) = 0 then
            Result := Id
          else
            Result := Result + '","' + Id;

        end;

      end;

    end;

  end;
end;

{
//修改表现值
procedure etGridALLStuat(lpGrid : TcxGridDBTableView ; lpFileName : string ; lpValue : Variant);
var
  i:Integer;
begin
  with lpGrid do
  begin
    BeginUpdate();
    for i := 0 to Controller.SelectAll-1 do
    begin
      Controller.FocusedRow := Controller.SelectedRows[i];
      DataController.DataSet.Edit;
      DataController.DataSet.FieldByName(lpFileName).Value := lpValue;
      DataController.DataSet.Post;
    end;
    EndUpdate;
  end;

end;
}
end.
