object frmBuild: TfrmBuild
  Left = 0
  Top = 0
  Caption = 'frmBuild'
  ClientHeight = 521
  ClientWidth = 1234
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Client: TRzPanel
    Left = 269
    Top = 0
    Width = 965
    Height = 521
    Align = alClient
    BorderOuter = fsNone
    TabOrder = 0
    object RzPageControl2: TRzPageControl
      Left = 0
      Top = 0
      Width = 965
      Height = 521
      Hint = ''
      ActivePage = TabSheet12
      Align = alClient
      BackgroundColor = clBtnFace
      BoldCurrentTab = True
      ButtonColor = 16250613
      Color = clWhite
      UseColoredTabs = True
      HotTrackStyle = htsText
      ParentBackgroundColor = False
      ParentColor = False
      ShowShadow = False
      TabOverlap = -4
      TabHeight = 30
      TabIndex = 4
      TabOrder = 0
      TabStyle = tsSquareCorners
      TabWidth = 80
      FixedDimension = 30
      object TabSheet5: TRzTabSheet
        Color = 16250613
        Caption = #39033#30446#37096#26723#26696
        object RzPanel1: TRzPanel
          Left = 0
          Top = 0
          Width = 963
          Height = 486
          Align = alClient
          BorderOuter = fsNone
          Color = 16250613
          Ctl3D = False
          ParentCtl3D = False
          TabOrder = 0
          VisualStyle = vsClassic
        end
      end
      object TabSheet6: TRzTabSheet
        Color = clWhite
        TabVisible = False
        Caption = #39033#30446#37096#32771#21220
        object Panel1: TPanel
          Left = -5
          Top = 239
          Width = 1091
          Height = 40
          BevelOuter = bvNone
          Color = 16250613
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBackground = False
          ParentFont = False
          TabOrder = 0
          Visible = False
          object RzBitBtn26: TRzBitBtn
            Left = 640
            Top = 6
            Width = 65
            Height = 30
            FrameColor = 7617536
            Caption = #20851#38381
            Color = 15791348
            HotTrack = True
            TabOrder = 2
            Glyph.Data = {
              36060000424D3606000000000000360400002800000020000000100000000100
              08000000000000020000420B0000420B00000001000000000000000000003300
              00006600000099000000CC000000FF0000000033000033330000663300009933
              0000CC330000FF33000000660000336600006666000099660000CC660000FF66
              000000990000339900006699000099990000CC990000FF99000000CC000033CC
              000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
              0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
              330000333300333333006633330099333300CC333300FF333300006633003366
              33006666330099663300CC663300FF6633000099330033993300669933009999
              3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
              330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
              66006600660099006600CC006600FF0066000033660033336600663366009933
              6600CC336600FF33660000666600336666006666660099666600CC666600FF66
              660000996600339966006699660099996600CC996600FF99660000CC660033CC
              660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
              6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
              990000339900333399006633990099339900CC339900FF339900006699003366
              99006666990099669900CC669900FF6699000099990033999900669999009999
              9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
              990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
              CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
              CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
              CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
              CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
              CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
              FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
              FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
              FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
              FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
              000000808000800000008000800080800000C0C0C00080808000191919004C4C
              4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
              6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A5D8
              D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8E8E8E8D890B4
              B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8D8B4B4B4
              B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8D8B4B4B4B4
              B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8A590B4B4D7D7
              B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8D8B4B4B4D7D7
              D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8D8B4B4B4B4D7
              D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4B4B4
              D7D7D7B4B4B4B4B4D8E881ACACACACACD7D7D7ACACACACAC81E8D8B4B4B4B4D7
              D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4D7D7
              D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8A590B4B4D7D7
              B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8E8D8B4B4B4B4
              B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8E8E8D8B4B4B4
              B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E8E8D890B4
              B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8E8E8A5D8
              D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8}
            NumGlyphs = 2
          end
          object RzBitBtn58: TRzBitBtn
            Left = 400
            Top = 6
            Width = 65
            Height = 30
            FrameColor = 7617536
            Caption = #26597#35810
            Color = 15791348
            HotTrack = True
            TabOrder = 0
            Glyph.Data = {
              36060000424D3606000000000000360400002800000020000000100000000100
              08000000000000020000830E0000830E00000001000000000000000000003300
              00006600000099000000CC000000FF0000000033000033330000663300009933
              0000CC330000FF33000000660000336600006666000099660000CC660000FF66
              000000990000339900006699000099990000CC990000FF99000000CC000033CC
              000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
              0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
              330000333300333333006633330099333300CC333300FF333300006633003366
              33006666330099663300CC663300FF6633000099330033993300669933009999
              3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
              330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
              66006600660099006600CC006600FF0066000033660033336600663366009933
              6600CC336600FF33660000666600336666006666660099666600CC666600FF66
              660000996600339966006699660099996600CC996600FF99660000CC660033CC
              660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
              6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
              990000339900333399006633990099339900CC339900FF339900006699003366
              99006666990099669900CC669900FF6699000099990033999900669999009999
              9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
              990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
              CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
              CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
              CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
              CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
              CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
              FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
              FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
              FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
              FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
              000000808000800000008000800080800000C0C0C00080808000191919004C4C
              4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
              6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
              E8E8E8E8E8E8E8820982E8E8E8E8E8E8E8E8E8E8E8E8E8AC81ACE85E5E5E5E5E
              5E5E5E5E5E5E82090909E88181818181818181818181AC818181E85ED7D7D7D7
              D7D7D7D7D7DF09090982E881E8E8E8E8E8E8E8E8E8DF818181ACE85ED7E3E3E3
              E3E3DFDFDF09090982E8E881E8ACACACACACDFDFDF818181ACE8E85ED7D7D7D7
              E35DB3B3D7880982E8E8E881E8E8E8E8AC81E3E3D7E381ACE8E8E85ED7E3E3E3
              81E6B3B3B3D756E8E8E8E881E8ACACAC81E3E3E3E3D756E8E8E8E85ED7D7D7D7
              81E6D7E6B3B356E8E8E8E881E8E8E8E881E3D7E3E3E356E8E8E8E85ED7E3E3E3
              81E6D7D7E6B356E8E8E8E881E8ACACAC81E3D7D7E3E356E8E8E8E85ED7D7D7D7
              D781E6E6E65DDFE8E8E8E881E8E8E8E8E881E3E3E381DFE8E8E8E85ED7E3E3E3
              E3E3818181E35EE8E8E8E881E8ACACACACAC818181E381E8E8E8E85ED7D7D7D7
              D7D7D7D7D7D75EE8E8E8E881E8E8E8E8E8E8E8E8E8E881E8E8E8E85ED7E3E3E3
              E3E3D75E5E5E5EE8E8E8E881E8ACACACACACE881818181E8E8E8E85ED7D7D7D7
              D7D7D75EE35EE8E8E8E8E881E8E8E8E8E8E8E881E381E8E8E8E8E85ED7D7D7D7
              D7D7D75E5EE8E8E8E8E8E881E8E8E8E8E8E8E88181E8E8E8E8E8E85E5E5E5E5E
              5E5E5E5EE8E8E8E8E8E8E8818181818181818181E8E8E8E8E8E8E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
            NumGlyphs = 2
          end
          object cxButton10: TcxButton
            Left = 561
            Top = 6
            Width = 73
            Height = 30
            Caption = #25253#34920
            Kind = cxbkDropDownButton
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000080000
              000C0000000D0000000D0000000D0000000D0000000D0000000D0000000D0000
              000E0000000E0000000E0000000E0000000E0000000D000000097B5A50BFAB7D
              6FFFAA7D6FFFAA7C6FFFAA7C6EFFAA7C6DFFA97B6DFFA97B6DFFA87B6DFFA97A
              6CFFA97A6CFFA8796CFFA87A6BFFA7796BFFA8786AFF78564CC0AC8072FFF9F2
              EEFFF9F3EEFFF9F1EEFFF9F1EEFFCFB2A8FFF9F1EEFFF8F1EEFFF8F2EEFFF8F1
              EDFFCEB0A6FFF9F1EDFFF9F2EDFFF9F1EDFFF9F1EEFFA87A6CFFAE8274FFF9F3
              F0FFF7EFEAFFF7EFEAFFF8F2EEFFD2B7AFFFF9F2EFFFF9F2EFFFF9F3EFFFF9F3
              EFFFD1B7AEFFF8F3EFFFF8F3EFFFF9F3EEFFFAF5F2FFBE9B91FFAF8375FFD3B8
              B0FFC5A399FFC5A298FFD3B9B1FF2B8758FF2B8758FF2A8658FF2A8657FF2A84
              58FF2A8457FF298456FF298357FF298254FF298256FF298254FFB18578FFFAF4
              F0FFF8F0EBFFF7F0EBFFF9F3F0FF2C895BFF74DEC1FF74DEC1FF74DEC1FF74DE
              C0FF35976AFF74DEC0FF74DEC0FF74DEC0FF74DEC0FF298455FFB28779FFF9F5
              F1FFF8F0ECFFF8F1EBFFFAF4F1FF2E8B5DFF76DFC1FF5FD6AFFF5DD6AEFF5DD5
              ADFF369A6CFF5DD5ACFF5DD5ACFF5CD5ADFF74DEC1FF2B8758FFB4887CFFD8C0
              B8FFC8A79DFFC8A79DFFD5BCB4FF2E8E5EFF389D70FF389D70FF389C6FFF389C
              6FFF389B6EFF379B6EFF389B6DFF379A6DFF379A6DFF2C885AFFB48A7EFFFAF5
              F3FFF8F2EDFFF8F1EDFFFAF4F1FF2F9062FF77E0C3FF63D8B1FF62D7B1FF62D7
              B2FF399D70FF62D7B1FF62D7B0FF61D7B1FF76DFC2FF2E8A5CFFB68D7FFFFAF6
              F3FFF9F2EDFFF9F2EDFFFAF4F1FF309264FF78E0C4FF78E0C4FF78E0C4FF78E0
              C4FF3A9F73FF78DFC4FF78DFC3FF77E0C3FF78DFC3FF2E8D5EFFB78E81FFDDC6
              BFFFCBABA2FFCBACA2FFD7C0B8FF329566FF339366FF329265FF309265FF3292
              64FF329164FF309163FF2F9062FF2F9062FF2F8F60FF2E8F60FFB88F82FFFAF7
              F4FFF8F3EFFFF8F2EEFFF9F4F1FFD8C0B9FFFAF6F2FFFAF5F3FFFAF5F2FFFAF5
              F2FFD7C0B8FFFAF6F2FFFAF5F2FFFAF5F2FFFCF8F6FFC7A79DFFB99085FFFBF8
              F5FFFAF8F5FFFBF8F4FFFBF6F4FFDFCAC3FFFBF6F5FFFBF7F4FFFBF7F4FFFBF6
              F4FFDDC8C1FFFAF6F4FFFBF7F5FFFBF6F5FFFBF7F5FFB68C80FF8A6C63C0BA91
              85FFBA9185FFBA9185FFBA9084FFB99185FFB99084FFB99084FFB99084FFB990
              83FFB89083FFB98F83FFB78F83FFB88E82FFB78E82FF876960C1000000030000
              0004000000050000000500000005000000050000000500000005000000050000
              0005000000050000000500000006000000060000000500000004}
            TabOrder = 1
          end
        end
        object cxGroupBox2: TcxGroupBox
          Left = 0
          Top = 74
          Align = alTop
          Caption = #26597#35810
          TabOrder = 1
          Visible = False
          Height = 55
          Width = 963
          object Label7: TLabel
            Left = 429
            Top = 24
            Width = 40
            Height = 13
            Caption = #31614#21040#20154':'
          end
          object Label8: TLabel
            Left = 24
            Top = 24
            Width = 52
            Height = 13
            Caption = #24037#22320#21517#31216':'
          end
          object Label9: TLabel
            Left = 820
            Top = 25
            Width = 52
            Height = 13
            Caption = #36215#22987#26085#26399':'
          end
          object Label11: TLabel
            Left = 969
            Top = 25
            Width = 52
            Height = 13
            Caption = #32467#26463#26085#26399':'
          end
          object Label12: TLabel
            Left = 540
            Top = 24
            Width = 56
            Height = 13
            Caption = #37096#38376'/'#24037#31181':'
          end
          object Label20: TLabel
            Left = 215
            Top = 24
            Width = 52
            Height = 13
            Caption = #25152#23646#21333#20301':'
          end
          object Label54: TLabel
            Left = 677
            Top = 24
            Width = 52
            Height = 13
            Caption = #32771#21220#31867#21035':'
          end
          object cxTextEdit6: TcxTextEdit
            Left = 475
            Top = 22
            TabOrder = 2
            Width = 59
          end
          object cxTextEdit7: TcxTextEdit
            Left = 82
            Top = 28
            TabOrder = 0
            TextHint = #22238#36710#21363#21487#26597#35810
            Width = 129
          end
          object cxDateEdit3: TcxDateEdit
            Left = 878
            Top = 23
            TabOrder = 4
            Width = 85
          end
          object cxDateEdit4: TcxDateEdit
            Left = 1027
            Top = 23
            TabOrder = 5
            Width = 96
          end
          object cxTextEdit8: TcxTextEdit
            Left = 598
            Top = 22
            Style.BorderStyle = ebs3D
            TabOrder = 3
            Width = 73
          end
          object cxTextEdit11: TcxTextEdit
            Left = 273
            Top = 22
            Style.BorderStyle = ebs3D
            TabOrder = 1
            Width = 142
          end
          object cxButton5: TcxButton
            Left = 1129
            Top = 19
            Width = 64
            Height = 30
            Caption = #26597#35810
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000020000000B000000120000000C00000003000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              00000000000200000010071334970F276AFF0A193B970000000B000000000000
              00007B5043B8AB705CFFAB6F5AFFAB705CFFAA6F5BFFAA6E59FFA96F5AFFBE91
              82FFC9ACA3FF5F617FFF417CB9FF70C7FFFF265198FF00000010000000000000
              0000AD735FFFFDFBF9FFFBF5F2FFF7F2EEFFF3EDE9FFEFE9E5FFECE5E1FFE6DE
              DAFF707E9FFF4C83BCFF83CFFFFF5694CEFF142B4D930000000A000000000000
              0000B07762FFFDFBFAFFF7F3F0FFE2D8D2FFA5816CFF8E5E42FF8C5D41FF7A5E
              54FF577EA6FF92D4FAFF619CD0FF727F9BFF0000000E00000002000000000000
              0000B07966FFFBF9F9FFE1D5CEFF936346FFC8A37FFFEFD7B2FFF0DAB8FFC7A6
              88FF895D43FF6891B2FF849DB9FFCCB0A7FF0000000200000000000000000000
              0000B37C69FFFAF8F7FFAD8975FFC7A07BFFF7D39CFFF5CD93FFF7D39BFFF9DD
              B2FFC7A688FF84695DFFE8E2DEFFC29888FF0000000000000000000000000000
              0000B67F6CFFF9F8F7FF98694CFFF1D4A7FFFAE5C0FFFBEACAFFF7D6A0FFF6D3
              9BFFF2DBBBFF8F5D42FFF0E9E7FFB27A66FF0000000000000000000000000000
              0000B98371FFFAF9F8FF9D6E51FFF2D4A5FFFDF6E2FFFDF3DCFFFBEACAFFF5CE
              92FFF1DAB5FF936245FFF2EDE9FFB47D6AFF0000000000000000000000000000
              0000BC8877FFFCFCFBFFB99783FFCDA77EFFF9E0B5FFFEF7E5FFFBE5C1FFF6D4
              9DFFCAA681FFAF8C77FFF5F1EEFFB6806DFF0000000000000000000000000000
              0000BF8C7AFFFDFDFCFFEDE4DFFFA87A5DFFCEA77FFFEFD2A3FFEFD2A5FFCCA7
              80FFA17356FFE4DAD4FFFAF6F3FFB98371FF0000000000000000000000000000
              0000C18F7FFFFEFEFEFFFDFCFBFFEDE4DFFFBE9C87FFAA7E62FFA97D60FFBB98
              82FFEADFD8FFFBF8F6FFFDF9F8FFBD8774FF0000000000000000000000000000
              0000C49382FFFFFEFEFFFEFEFDFFFBF6F4FFFAF5F3FFF9F3F0FFF9F3F0FFFAF2
              F0FFFAF4F0FFFDFBF9FFFDFBF9FFBF8C7BFF0000000000000000000000000000
              0000C79985FFFFFEFEFFFFFEFEFFFEFEFDFFFEFDFDFFFEFDFDFFFEFDFCFFFEFC
              FCFFFEFCFBFFFEFCFAFFFDFCFAFFC28F7FFF0000000000000000000000000000
              0000C99A89FFFFFFFEFFFFFFFEFFFFFEFEFFFFFEFEFFFEFEFEFFFEFEFEFFFEFE
              FDFFFEFEFDFFFEFDFDFFFEFDFDFFC49382FF0000000000000000000000000000
              0000967467BDCA9C8BFFCA9C8BFFC99C8AFFC99B89FFC99B8AFFCA9A88FFC89A
              88FFC99987FFC79887FFC89886FF927163BD0000000000000000}
            TabOrder = 6
          end
          object cxTextEdit18: TcxTextEdit
            Left = 735
            Top = 22
            TabOrder = 7
            Width = 78
          end
        end
        object WorkGrid: TcxGrid
          Left = 0
          Top = 129
          Width = 963
          Height = 357
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          LookAndFeel.Kind = lfOffice11
          LookAndFeel.SkinName = 'Office2016Colorful'
          object tvWorkGrid: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataModeController.GridModeBufferCount = 10
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #24635#39069' '#165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvWorkGridColumn11
              end
              item
                Format = #22823#20889
                Kind = skCount
                Column = tvWorkGridColumn12
              end
              item
                Kind = skSum
                Column = tvWorkGridColumn9
              end
              item
                Kind = skSum
                Column = tvWorkGridColumn7
              end
              item
                Format = #21512#35745':'
                Kind = skCount
                Column = tvWorkGridColumn15
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #21333#20987#31579#36873#26597#35810
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.DataRowSizing = True
            OptionsData.CancelOnExit = False
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.CellEndEllipsis = True
            OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
            OptionsView.CellAutoHeight = True
            OptionsView.DataRowHeight = 32
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.GroupRowHeight = 25
            OptionsView.HeaderFilterButtonShowMode = fbmButton
            OptionsView.HeaderHeight = 25
            OptionsView.Indicator = True
            object tvWorkGridColumn16: TcxGridDBColumn
              Caption = #24207#21495
              MinWidth = 40
              Options.Editing = False
              Options.Filtering = False
              Options.FilteringAddValueItems = False
              Options.FilteringFilteredItemsList = False
              Options.FilteringMRUItemsList = False
              Options.FilteringPopup = False
              Options.FilteringPopupMultiSelect = False
              Options.FilteringWithFindPanel = False
              Options.Focusing = False
              Options.IgnoreTimeForFiltering = False
              Options.IncSearch = False
              Options.GroupFooters = False
              Options.Grouping = False
              Options.HorzSizing = False
              Options.Moving = False
              Options.Sorting = False
              Width = 40
            end
            object tvWorkGridColumn19: TcxGridDBColumn
              Caption = #29366#24577
              DataBinding.FieldName = 'status'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.Alignment = taCenter
              HeaderAlignmentHorz = taCenter
              Width = 40
            end
            object tvWorkGridColumn1: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'numbers'
              Options.Editing = False
              Options.Filtering = False
              Options.Sorting = False
              Width = 93
            end
            object tvWorkGridColumn2: TcxGridDBColumn
              Caption = #26085#26399
              DataBinding.FieldName = 'cxDate'
              Width = 91
            end
            object tvWorkGridColumn3: TcxGridDBColumn
              Caption = #24037#22320#21517#31216
              DataBinding.FieldName = 'ProjectName'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 130
            end
            object tvWorkGridColumn17: TcxGridDBColumn
              Caption = #26045#24037#21333#20301
              DataBinding.FieldName = 'CreateCompany'
              Width = 98
            end
            object tvWorkGridColumn4: TcxGridDBColumn
              Caption = #36164#36136#21333#20301
              DataBinding.FieldName = 'LabourFirm'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 121
            end
            object tvWorkGridColumn5: TcxGridDBColumn
              Caption = #31614#21040#21517#31216
              DataBinding.FieldName = 'SignName'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.DropDownListStyle = lsEditList
              Properties.KeyFieldNames = 'SignName'
              Properties.ListColumns = <
                item
                  Caption = #24448#26469#21333#20301
                  FieldName = 'SignName'
                end>
              Width = 114
            end
            object tvWorkGridColumn18: TcxGridDBColumn
              Caption = #39033#30446#37096
              DataBinding.FieldName = 'ProjectDepartment'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 114
            end
            object tvWorkGridColumn6: TcxGridDBColumn
              Caption = #37096#38376'/'#24037#31181
              DataBinding.FieldName = 'WorkType'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 70
            end
            object tvWorkGridColumn13: TcxGridDBColumn
              Caption = #32771#21220#31867#21035
              DataBinding.FieldName = 'CheckCategory'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 70
            end
            object tvWorkGridColumn14: TcxGridDBColumn
              Caption = #35828#26126
              DataBinding.FieldName = 'Explain'
              Width = 80
            end
            object tvWorkGridColumn15: TcxGridDBColumn
              Caption = #32771#21220#33539#22260
              DataBinding.FieldName = 'CheckRange'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 70
            end
            object tvWorkGridColumn7: TcxGridDBColumn
              Caption = #24037#26085'/'#22825
              DataBinding.FieldName = 'WorkDays'
              PropertiesClassName = 'TcxSpinEditProperties'
              Properties.ValueType = vtFloat
              HeaderAlignmentHorz = taRightJustify
              Width = 70
            end
            object tvWorkGridColumn8: TcxGridDBColumn
              Caption = #21333#20215
              DataBinding.FieldName = 'UnitPrice'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.EditFormat = '0'
              HeaderAlignmentHorz = taRightJustify
              Width = 70
            end
            object tvWorkGridColumn9: TcxGridDBColumn
              Caption = #21152#29677'/'#23567#26102
              DataBinding.FieldName = 'SumOvertime'
              PropertiesClassName = 'TcxSpinEditProperties'
              Properties.ValueType = vtFloat
              HeaderAlignmentHorz = taRightJustify
              Width = 70
            end
            object tvWorkGridColumn10: TcxGridDBColumn
              Caption = #21152#29677#21333#20215
              DataBinding.FieldName = 'WorkUnitPrice'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.EditFormat = '0'
              HeaderAlignmentHorz = taRightJustify
              Width = 70
            end
            object tvWorkGridColumn11: TcxGridDBColumn
              Caption = #37329#39069
              DataBinding.FieldName = 'SumMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taRightJustify
              Styles.Content = DM.SumMoney
              Width = 70
            end
            object tvWorkGridColumn12: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'remarks'
              PropertiesClassName = 'TcxMemoProperties'
              Properties.ScrollBars = ssVertical
              Width = 230
            end
          end
          object lv2: TcxGridLevel
            GridView = tvWorkGrid
          end
        end
        object RzPanel25: TRzPanel
          Left = 0
          Top = 29
          Width = 963
          Height = 45
          Align = alTop
          BorderOuter = fsGroove
          TabOrder = 3
          Visible = False
          object Label6: TLabel
            Left = 16
            Top = 16
            Width = 28
            Height = 13
            Caption = #24320#22987':'
          end
          object Label15: TLabel
            Left = 183
            Top = 16
            Width = 28
            Height = 13
            Caption = #32467#26463':'
          end
          object Label19: TLabel
            Left = 497
            Top = 16
            Width = 28
            Height = 13
            Caption = #26102#38388':'
          end
          object Label39: TLabel
            Left = 531
            Top = 16
            Width = 33
            Height = 13
            Caption = '0 '#23567#26102
          end
          object Label40: TLabel
            Left = 621
            Top = 16
            Width = 44
            Height = 13
            Caption = #24037#26102'/'#22825':'
          end
          object Label41: TLabel
            Left = 671
            Top = 16
            Width = 36
            Height = 13
            AutoSize = False
            Caption = '0'
          end
          object Label42: TLabel
            Left = 346
            Top = 16
            Width = 28
            Height = 13
            Caption = #21333#20215':'
          end
          object Label43: TLabel
            Left = 713
            Top = 16
            Width = 28
            Height = 13
            Caption = #24635#39069':'
          end
          object Label44: TLabel
            Left = 747
            Top = 16
            Width = 143
            Height = 13
            AutoSize = False
            Caption = '0 '#20803
          end
          object cxDateEdit11: TcxDateEdit
            Left = 50
            Top = 13
            Properties.AssignedValues.DisplayFormat = True
            Properties.Kind = ckDateTime
            Properties.View = cavClassic
            TabOrder = 0
            Width = 127
          end
          object cxDateEdit12: TcxDateEdit
            Left = 212
            Top = 13
            Properties.Kind = ckDateTime
            TabOrder = 1
            Width = 128
          end
          object cxButton11: TcxButton
            Left = 444
            Top = 10
            Width = 39
            Height = 25
            Caption = #35745#31639
            TabOrder = 3
          end
          object cxCurrencyEdit1: TcxCurrencyEdit
            Left = 380
            Top = 13
            TabOrder = 2
            Width = 58
          end
        end
        object RzToolbar9: TRzToolbar
          Left = 0
          Top = 0
          Width = 963
          Height = 29
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsGroove
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 4
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer64
            RzToolButton49
            RzSpacer65
            RzToolButton50
            RzSpacer66
            RzToolButton51
            RzSpacer67
            RzToolButton52
            RzSpacer76
            RzToolButton56
            RzSpacer72
            RzToolButton57
            RzSpacer68
            RzToolButton53
            RzSpacer73
            cxLabel44
            RzSpacer69
            cxDateEdit31
            RzSpacer71
            cxLabel45
            RzSpacer70
            cxDateEdit32
            RzToolButton55
            RzSpacer74
            RzToolButton54
            RzSpacer75)
          object RzSpacer64: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton49: TRzToolButton
            Left = 12
            Top = 2
            Width = 70
            ImageIndex = 1
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #28155#21152
          end
          object RzSpacer65: TRzSpacer
            Left = 82
            Top = 2
          end
          object RzToolButton50: TRzToolButton
            Left = 90
            Top = 2
            Width = 70
            ImageIndex = 3
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #20445#23384
          end
          object RzSpacer66: TRzSpacer
            Left = 160
            Top = 2
          end
          object RzToolButton51: TRzToolButton
            Left = 168
            Top = 2
            Width = 70
            ImageIndex = 2
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #21024#38500
          end
          object RzSpacer67: TRzSpacer
            Left = 238
            Top = 2
          end
          object RzToolButton52: TRzToolButton
            Left = 246
            Top = 2
            Width = 70
            ImageIndex = 4
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #34920#26684#35774#32622
          end
          object RzToolButton53: TRzToolButton
            Left = 460
            Top = 2
            Width = 65
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzSpacer68: TRzSpacer
            Left = 452
            Top = 2
          end
          object RzToolButton54: TRzToolButton
            Left = 879
            Top = 2
            Width = 70
            ImageIndex = 8
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #36864#20986
          end
          object RzSpacer69: TRzSpacer
            Left = 589
            Top = 2
          end
          object RzSpacer70: TRzSpacer
            Left = 748
            Top = 2
          end
          object RzSpacer71: TRzSpacer
            Left = 687
            Top = 2
            Width = 5
          end
          object RzSpacer72: TRzSpacer
            Left = 384
            Top = 2
          end
          object RzToolButton55: TRzToolButton
            Left = 846
            Top = 2
            ImageIndex = 10
            ShowCaption = True
            UseToolbarShowCaption = False
          end
          object RzSpacer73: TRzSpacer
            Left = 525
            Top = 2
          end
          object RzToolButton56: TRzToolButton
            Left = 324
            Top = 2
            Width = 60
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #32508#21512#25991#26723
          end
          object RzSpacer74: TRzSpacer
            Left = 871
            Top = 2
          end
          object RzToolButton57: TRzToolButton
            Left = 392
            Top = 2
            Width = 60
            ImageIndex = 23
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #21152#29677#35745#31639
          end
          object RzSpacer75: TRzSpacer
            Left = 949
            Top = 2
          end
          object RzSpacer76: TRzSpacer
            Left = 316
            Top = 2
          end
          object cxLabel44: TcxLabel
            Left = 533
            Top = 6
            Caption = #36215#22987#26085#26399':'
            Transparent = True
          end
          object cxDateEdit31: TcxDateEdit
            Left = 597
            Top = 4
            TabOrder = 1
            Width = 90
          end
          object cxLabel45: TcxLabel
            Left = 692
            Top = 6
            Caption = #32467#26463#26085#26399':'
            Transparent = True
          end
          object cxDateEdit32: TcxDateEdit
            Left = 756
            Top = 4
            TabOrder = 3
            Width = 90
          end
        end
      end
      object TabSheet7: TRzTabSheet
        Color = clWhite
        TabVisible = False
        Caption = #39033#30446#37096#20179#24211
        object cxGroupBox1: TcxGroupBox
          Left = 12
          Top = 41
          Caption = #26597#35810
          TabOrder = 0
          Visible = False
          Height = 85
          Width = 1091
          object Label2: TLabel
            Left = 24
            Top = 25
            Width = 52
            Height = 13
            Caption = #20379#24212#21333#20301':'
          end
          object Label3: TLabel
            Left = 224
            Top = 25
            Width = 52
            Height = 13
            Caption = #26448#26009#21517#31216':'
          end
          object Label10: TLabel
            Left = 695
            Top = 25
            Width = 52
            Height = 13
            Caption = #36215#22987#26085#26399':'
          end
          object Label4: TLabel
            Left = 849
            Top = 25
            Width = 52
            Height = 13
            Caption = #32467#26463#26085#26399':'
          end
          object Label33: TLabel
            Left = 422
            Top = 25
            Width = 52
            Height = 13
            Caption = #35268#12288#12288#26684':'
          end
          object Label5: TLabel
            Left = 552
            Top = 25
            Width = 52
            Height = 13
            Caption = #24037#22320#31614#25910':'
          end
          object Label21: TLabel
            Left = 24
            Top = 55
            Width = 52
            Height = 13
            Caption = #36755#36865#26041#24335':'
          end
          object Label22: TLabel
            Left = 224
            Top = 55
            Width = 52
            Height = 13
            Caption = #25239#12288#12288#28183':'
          end
          object Label23: TLabel
            Left = 422
            Top = 55
            Width = 52
            Height = 13
            Caption = #38468#12288#12288#21152':'
          end
          object Label24: TLabel
            Left = 564
            Top = 55
            Width = 40
            Height = 13
            Caption = #22806#21152#21058':'
          end
          object Label25: TLabel
            Left = 695
            Top = 55
            Width = 52
            Height = 13
            Caption = #31199#36161#35268#26684':'
          end
          object Label26: TLabel
            Left = 1009
            Top = 25
            Width = 52
            Height = 13
            Caption = #21046#34920#31867#21035':'
          end
          object Label59: TLabel
            Left = 861
            Top = 54
            Width = 40
            Height = 13
            Caption = #30780#24378#24230':'
          end
          object cxTextEdit2: TcxTextEdit
            Left = 82
            Top = 22
            TabOrder = 0
            TextHint = #22238#36710#21363#21487#26597#35810
            Width = 136
          end
          object cxTextEdit3: TcxTextEdit
            Left = 282
            Top = 22
            TabOrder = 1
            TextHint = #22238#36710#21363#21487#26597#35810
            Width = 129
          end
          object cxButton4: TcxButton
            Left = 1175
            Top = 19
            Width = 75
            Height = 52
            Caption = #26597#35810
            TabOrder = 2
          end
          object cxDateEdit1: TcxDateEdit
            Left = 753
            Top = 25
            TabOrder = 3
            Width = 88
          end
          object cxDateEdit2: TcxDateEdit
            Left = 907
            Top = 25
            TabOrder = 4
            Width = 94
          end
          object cxTextEdit5: TcxTextEdit
            Left = 610
            Top = 22
            Style.BorderStyle = ebs3D
            TabOrder = 5
            Width = 79
          end
          object cxComboBox1: TcxComboBox
            Left = 82
            Top = 53
            TabOrder = 6
            Width = 136
          end
          object cxComboBox3: TcxComboBox
            Left = 282
            Top = 53
            TabOrder = 7
            Width = 129
          end
          object cxComboBox4: TcxComboBox
            Left = 480
            Top = 53
            TabOrder = 8
            Width = 66
          end
          object cxComboBox5: TcxComboBox
            Left = 610
            Top = 53
            TabOrder = 9
            Width = 79
          end
          object cxComboBox6: TcxComboBox
            Left = 753
            Top = 53
            TabOrder = 10
            Width = 90
          end
          object cxComboBox7: TcxComboBox
            Left = 1067
            Top = 25
            TabOrder = 11
            Width = 94
          end
          object cxComboBox9: TcxComboBox
            Left = 480
            Top = 21
            TabOrder = 12
            Width = 66
          end
          object cxComboBox10: TcxComboBox
            Left = 907
            Top = 52
            TabOrder = 13
            Width = 94
          end
        end
        object cxGrid1: TcxGrid
          Left = 0
          Top = 29
          Width = 963
          Height = 457
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          LookAndFeel.SkinName = 'Office2016Colorful'
          object cxGridView: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = #21512#35745
                Position = spFooter
                Column = cxGridViewColumn1
              end
              item
                Format = #25968#20540':0.########;-0.########'
                Kind = skSum
                Column = cxGridViewColumn7
              end
              item
                Format = #25968#20540':0.########;-0.########'
                Kind = skSum
                Position = spFooter
                Column = cxGridViewColumn7
              end
              item
                Format = #25968#37327':0.###;-0.###'
                Kind = skSum
                Column = cxGridViewColumn8
              end
              item
                Format = #25968#37327':0.###;-0.###'
                Kind = skSum
                Position = spFooter
                Column = cxGridViewColumn8
              end
              item
                Format = #37329#39069':'#165',0.00;'#165'-,0.00'
                Kind = skSum
                Position = spFooter
                Column = cxGridViewColumn12
              end
              item
                Format = #37329#39069':'#165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = cxGridViewColumn12
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #37329#39069':'#165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = cxGridViewColumn12
              end
              item
                Format = #21512#35745#65306
                Kind = skCount
                Column = cxGridViewColumn1
                Sorted = True
              end
              item
                Format = #25968#20540':0.########;-0.########'
                Kind = skSum
                Column = cxGridViewColumn7
              end
              item
                Format = #25968#37327':0.###;-0.###'
                Kind = skSum
                Column = cxGridViewColumn8
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = cxGridViewColumn12
              end
              item
                Format = #22823#20889':'
                Kind = skCount
                Column = cxGridViewColumn13
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #21333#20987#31579#36873#26597#35810
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsCustomize.DataRowSizing = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.CellEndEllipsis = True
            OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
            OptionsView.CellAutoHeight = True
            OptionsView.DataRowHeight = 32
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.GroupRowHeight = 25
            OptionsView.HeaderFilterButtonShowMode = fbmButton
            OptionsView.HeaderHeight = 25
            OptionsView.Indicator = True
            object cxGridViewColumn24: TcxGridDBColumn
              Caption = #24207#21495
              HeaderAlignmentHorz = taCenter
              MinWidth = 40
              Options.Focusing = False
              Width = 40
            end
            object cxGridViewColumn28: TcxGridDBColumn
              Caption = #29366#24577
              DataBinding.FieldName = 'status'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.Alignment = taCenter
              HeaderAlignmentHorz = taCenter
              Width = 40
            end
            object cxGridViewColumn1: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'Numbers'
              Width = 100
            end
            object cxGridViewColumn2: TcxGridDBColumn
              Caption = #26085#26399
              DataBinding.FieldName = 'cxDate'
              Width = 80
            end
            object cxGridViewColumn3: TcxGridDBColumn
              Caption = #24037#31243#21517#31216
              DataBinding.FieldName = 'ProjectName'
              Width = 109
            end
            object cxGridViewColumn15: TcxGridDBColumn
              Caption = #20379#24212#21333#20301
              DataBinding.FieldName = 'SignName'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 108
            end
            object cxGridViewColumn20: TcxGridDBColumn
              Caption = #21046#34920#31867#21035
              DataBinding.FieldName = 'TabTypes'
              PropertiesClassName = 'TcxComboBoxProperties'
              Width = 95
            end
            object cxGridViewColumn4: TcxGridDBColumn
              Caption = #26448#26009'\'#21517#31216
              DataBinding.FieldName = 'StuffName'
              Width = 124
            end
            object cxGridViewColumn23: TcxGridDBColumn
              Caption = #30780#24378#24230
              DataBinding.FieldName = 'Concrete'
              PropertiesClassName = 'TcxComboBoxProperties'
              Width = 70
            end
            object cxGridViewColumn5: TcxGridDBColumn
              Caption = #35268#26684
              DataBinding.FieldName = 'cxSpec'
              PropertiesClassName = 'TcxComboBoxProperties'
              Width = 72
            end
            object cxGridViewColumn21: TcxGridDBColumn
              Caption = #31199#36161#35268#26684
              DataBinding.FieldName = 'UnitTypes'
              PropertiesClassName = 'TcxComboBoxProperties'
              Width = 96
            end
            object cxGridViewColumn11: TcxGridDBColumn
              Caption = #35745#31639#20844#24335
              DataBinding.FieldName = 'CalculatingFormula'
              PropertiesClassName = 'TcxMemoProperties'
              Properties.ScrollBars = ssVertical
              Width = 183
            end
            object cxGridViewColumn7: TcxGridDBColumn
              Caption = #35745#31639#25968#20540
              DataBinding.FieldName = 'NumericalValue'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.DecimalPlaces = 8
              Properties.DisplayFormat = '0.########;-0.########'
              Width = 112
            end
            object cxGridViewColumn6: TcxGridDBColumn
              Caption = #21464#37327#25968
              DataBinding.FieldName = 'VariableCount'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.DecimalPlaces = 7
              Properties.DisplayFormat = '0.########;-0.########'
              Width = 108
            end
            object cxGridViewColumn8: TcxGridDBColumn
              Caption = #25968#37327
              DataBinding.FieldName = 'Total'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Properties.DecimalPlaces = 3
              Properties.DisplayFormat = '0.########;-0.########'
              Width = 110
            end
            object cxGridViewColumn9: TcxGridDBColumn
              Caption = #21333#20301
              DataBinding.FieldName = 'Company'
              PropertiesClassName = 'TcxComboBoxProperties'
              Properties.Alignment.Horz = taLeftJustify
              Width = 64
            end
            object cxGridViewColumn10: TcxGridDBColumn
              Caption = #21333#20215
              DataBinding.FieldName = 'UnitPrice'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              Width = 73
            end
            object cxGridViewColumn12: TcxGridDBColumn
              Caption = #24635#20215
              DataBinding.FieldName = 't'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.Alignment.Horz = taLeftJustify
              HeaderGlyphAlignmentHorz = taRightJustify
              Styles.Content = DM.SumMoney
              Width = 144
            end
            object cxGridViewColumn13: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'Remarks'
              PropertiesClassName = 'TcxMemoProperties'
              Properties.Alignment = taLeftJustify
              Properties.ScrollBars = ssVertical
              Width = 230
            end
            object cxGridViewColumn14: TcxGridDBColumn
              Caption = #24037#22320#31614#25910
              DataBinding.FieldName = 'Supplier'
              Width = 77
            end
            object cxGridViewColumn16: TcxGridDBColumn
              Caption = #36755#36865#26041#24335
              DataBinding.FieldName = 'Deliver'
              PropertiesClassName = 'TcxComboBoxProperties'
              Width = 80
            end
            object cxGridViewColumn17: TcxGridDBColumn
              Caption = #25239#28183
              DataBinding.FieldName = 'Permeability'
              PropertiesClassName = 'TcxComboBoxProperties'
              Width = 78
            end
            object cxGridViewColumn18: TcxGridDBColumn
              Caption = #22806#21152#21058
              DataBinding.FieldName = 'chAdditive'
              PropertiesClassName = 'TcxComboBoxProperties'
              Width = 78
            end
            object cxGridViewColumn19: TcxGridDBColumn
              Caption = #38468#21152
              DataBinding.FieldName = 'chAttach'
              PropertiesClassName = 'TcxComboBoxProperties'
              Width = 80
            end
            object cxGridViewColumn22: TcxGridDBColumn
              Caption = #27975#31569#37096#20301
              DataBinding.FieldName = 'chPosition'
              Width = 89
            end
            object cxGridViewColumn25: TcxGridDBColumn
              Caption = #36164#36136#21333#20301
              DataBinding.FieldName = 'AptitudeCompany'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 95
            end
            object cxGridViewColumn26: TcxGridDBColumn
              Caption = #39033#30446#37096
              DataBinding.FieldName = 'ProjectDepartment'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 118
            end
            object cxGridViewColumn27: TcxGridDBColumn
              Caption = #26045#24037#21333#20301
              DataBinding.FieldName = 'CreateCompany'
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGridView
          end
        end
        object RzToolbar13: TRzToolbar
          Left = 0
          Top = 0
          Width = 963
          Height = 29
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsGroove
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 2
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer30
            RzToolButton23
            RzSpacer57
            RzToolButton90
            RzSpacer120
            RzToolButton85
            RzSpacer113
            RzToolButton86
            RzSpacer116
            RzToolButton87
            RzSpacer114
            cxLabel55
            RzSpacer115
            cxDateEdit39
            RzSpacer117
            cxLabel56
            cxDateEdit40
            RzSpacer119
            RzToolButton89
            RzSpacer118
            RzToolButton88)
          object RzSpacer30: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton23: TRzToolButton
            Left = 12
            Top = 2
            Width = 65
            ImageIndex = 1
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #28155#21152
          end
          object RzSpacer57: TRzSpacer
            Left = 77
            Top = 2
          end
          object RzToolButton85: TRzToolButton
            Left = 129
            Top = 2
            Width = 65
            ImageIndex = 2
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #21024#38500
          end
          object RzSpacer113: TRzSpacer
            Left = 194
            Top = 2
          end
          object RzToolButton86: TRzToolButton
            Left = 202
            Top = 2
            Width = 70
            ImageIndex = 4
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #34920#26684#35774#32622
          end
          object RzToolButton87: TRzToolButton
            Left = 280
            Top = 2
            Width = 65
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzSpacer114: TRzSpacer
            Left = 345
            Top = 2
          end
          object RzToolButton88: TRzToolButton
            Left = 699
            Top = 2
            Width = 65
            ImageIndex = 8
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #36864#20986
          end
          object RzSpacer115: TRzSpacer
            Left = 409
            Top = 2
          end
          object RzSpacer116: TRzSpacer
            Left = 272
            Top = 2
          end
          object RzSpacer117: TRzSpacer
            Left = 507
            Top = 2
            Width = 5
          end
          object RzSpacer118: TRzSpacer
            Left = 691
            Top = 2
          end
          object RzToolButton89: TRzToolButton
            Left = 666
            Top = 2
            ImageIndex = 10
            ShowCaption = True
            UseToolbarShowCaption = False
          end
          object RzSpacer119: TRzSpacer
            Left = 658
            Top = 2
          end
          object RzToolButton90: TRzToolButton
            Left = 85
            Top = 2
            Width = 36
            ImageIndex = 0
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #32534#36753
          end
          object RzSpacer120: TRzSpacer
            Left = 121
            Top = 2
          end
          object cxLabel55: TcxLabel
            Left = 353
            Top = 6
            Caption = #36215#22987#26085#26399':'
            Transparent = True
          end
          object cxDateEdit39: TcxDateEdit
            Left = 417
            Top = 4
            TabOrder = 1
            Width = 90
          end
          object cxLabel56: TcxLabel
            Left = 512
            Top = 6
            Caption = #32467#26463#26085#26399':'
            Transparent = True
          end
          object cxDateEdit40: TcxDateEdit
            Left = 568
            Top = 4
            TabOrder = 3
            Width = 90
          end
        end
      end
      object TabSheet8: TRzTabSheet
        Color = clWhite
        TabVisible = False
        Caption = #39033#30446#20998#21253#21333#20301
        object Panel13: TPanel
          Left = 5
          Top = 71
          Width = 1091
          Height = 40
          BevelOuter = bvNone
          Color = 16250613
          ParentBackground = False
          TabOrder = 0
          Visible = False
          object RzBitBtn36: TRzBitBtn
            Left = 81
            Top = 6
            Width = 65
            Height = 30
            FrameColor = 7617536
            Caption = #28155#21152
            Color = 15791348
            HotTrack = True
            TabOrder = 0
            Glyph.Data = {
              36060000424D3606000000000000360400002800000020000000100000000100
              08000000000000020000330B0000330B00000001000000000000000000003300
              00006600000099000000CC000000FF0000000033000033330000663300009933
              0000CC330000FF33000000660000336600006666000099660000CC660000FF66
              000000990000339900006699000099990000CC990000FF99000000CC000033CC
              000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
              0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
              330000333300333333006633330099333300CC333300FF333300006633003366
              33006666330099663300CC663300FF6633000099330033993300669933009999
              3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
              330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
              66006600660099006600CC006600FF0066000033660033336600663366009933
              6600CC336600FF33660000666600336666006666660099666600CC666600FF66
              660000996600339966006699660099996600CC996600FF99660000CC660033CC
              660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
              6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
              990000339900333399006633990099339900CC339900FF339900006699003366
              99006666990099669900CC669900FF6699000099990033999900669999009999
              9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
              990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
              CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
              CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
              CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
              CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
              CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
              FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
              FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
              FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
              FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
              000000808000800000008000800080800000C0C0C00080808000191919004C4C
              4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
              6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E80909
              E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8E8E8E8E8E8091010
              09E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8E8E8E809101010
              1009E8E8E8E8E8E8E8E8E8E881ACACACAC81E8E8E8E8E8E8E8E8E80910101010
              101009E8E8E8E8E8E8E8E881ACACACACACAC81E8E8E8E8E8E8E8E80910100909
              10101009E8E8E8E8E8E8E881ACAC8181ACACAC81E8E8E8E8E8E8E8091009E8E8
              0910101009E8E8E8E8E8E881AC81E8E881ACACAC81E8E8E8E8E8E80909E8E8E8
              E80910101009E8E8E8E8E88181E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E8
              E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8
              E8E8E80910101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8
              E8E8E8E809101009E8E8E8E8E8E8E8E8E8E8E8E881ACAC81E8E8E8E8E8E8E8E8
              E8E8E8E8E8091009E8E8E8E8E8E8E8E8E8E8E8E8E881AC81E8E8E8E8E8E8E8E8
              E8E8E8E8E8E80909E8E8E8E8E8E8E8E8E8E8E8E8E8E88181E8E8E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
            NumGlyphs = 2
          end
          object RzBitBtn37: TRzBitBtn
            Left = 524
            Top = 6
            Width = 65
            Height = 30
            FrameColor = 7617536
            Caption = #26597#35810
            Color = 15791348
            HotTrack = True
            TabOrder = 1
            Glyph.Data = {
              36060000424D3606000000000000360400002800000020000000100000000100
              08000000000000020000330B0000330B00000001000000000000000000003300
              00006600000099000000CC000000FF0000000033000033330000663300009933
              0000CC330000FF33000000660000336600006666000099660000CC660000FF66
              000000990000339900006699000099990000CC990000FF99000000CC000033CC
              000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
              0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
              330000333300333333006633330099333300CC333300FF333300006633003366
              33006666330099663300CC663300FF6633000099330033993300669933009999
              3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
              330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
              66006600660099006600CC006600FF0066000033660033336600663366009933
              6600CC336600FF33660000666600336666006666660099666600CC666600FF66
              660000996600339966006699660099996600CC996600FF99660000CC660033CC
              660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
              6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
              990000339900333399006633990099339900CC339900FF339900006699003366
              99006666990099669900CC669900FF6699000099990033999900669999009999
              9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
              990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
              CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
              CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
              CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
              CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
              CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
              FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
              FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
              FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
              FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
              000000808000800000008000800080800000C0C0C00080808000191919004C4C
              4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
              6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000E8ACDEE3E8E8
              E8E8E8E8E8E8E8E8E8E8E8ACDEE3E8E8E8E8E8E8E8E8E8E8E8E8AC807A81E3E8
              E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8E8CEA37A81E3
              E8E8E8E8E8E8E8E8E8E8E8ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A81
              E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA37A
              81E3E8E8E8E8E8E8E8E8E8E8E3ACE28181E3E8E8E8E8E8E8E8E8E8E8E8D0CEA3
              7AACAD82828288E3E8E8E8E8E8E3ACE281ACE3818181E2E3E8E8E8E8E8E8D0CE
              E28288E6B3E6E682EBE8E8E8E8E8E3ACE281E2ACACACAC81E3E8E8E8E8E8E8E3
              8289B3B3B3D7D7D782E3E8E8E8E8E8E381E3ACACACE3E3E381E3E8E8E8E8E8AD
              88B3E6B3B3D7D7D7E688E8E8E8E8E8E3E2ACACACACE3E3E3ACE2E8E8E8E8E888
              89E6E6B3B3B3D7D7E682E8E8E8E8E8E2E3ACACACACACE3E3AC81E8E8E8E8E882
              E6E6E6E6B3B3B3B3B382E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E888
              E6B3E6E6E6B3B3B3E682E8E8E8E8E8E2ACACACACACACACACAC81E8E8E8E8E8AD
              88D7D7E6E6E6E6B38888E8E8E8E8E8E3E2E3E3ACACACACACE2E2E8E8E8E8E8E3
              82EBD7B3E6E6E68982E3E8E8E8E8E8E381E3E3ACACACACE381E3E8E8E8E8E8E8
              AD82ADE6E6E68882ADE8E8E8E8E8E8E8E381E3ACACACE281E3E8E8E8E8E8E8E8
              E8E38882828282E3E8E8E8E8E8E8E8E8E8E3E281818181E3E8E8}
            NumGlyphs = 2
          end
          object RzBitBtn38: TRzBitBtn
            Left = 152
            Top = 6
            Width = 65
            Height = 30
            FrameColor = 7617536
            Caption = #21024#38500
            Color = 15791348
            HotTrack = True
            TabOrder = 2
            Glyph.Data = {
              36060000424D3606000000000000360400002800000020000000100000000100
              08000000000000020000330B0000330B00000001000000000000000000003300
              00006600000099000000CC000000FF0000000033000033330000663300009933
              0000CC330000FF33000000660000336600006666000099660000CC660000FF66
              000000990000339900006699000099990000CC990000FF99000000CC000033CC
              000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
              0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
              330000333300333333006633330099333300CC333300FF333300006633003366
              33006666330099663300CC663300FF6633000099330033993300669933009999
              3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
              330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
              66006600660099006600CC006600FF0066000033660033336600663366009933
              6600CC336600FF33660000666600336666006666660099666600CC666600FF66
              660000996600339966006699660099996600CC996600FF99660000CC660033CC
              660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
              6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
              990000339900333399006633990099339900CC339900FF339900006699003366
              99006666990099669900CC669900FF6699000099990033999900669999009999
              9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
              990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
              CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
              CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
              CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
              CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
              CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
              FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
              FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
              FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
              FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
              000000808000800000008000800080800000C0C0C00080808000191919004C4C
              4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
              6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909E8
              E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E80910101009
              E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E8E809101010
              09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E8E8E8091010
              100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E8E8E80910
              1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8E8E809
              10101009E8E8E8E8E8E8E8E8E8E8E881ACACAC81E8E8E8E8E8E8E8E8E8E80910
              1010101009E8E8E8E8E8E8E8E8E881ACACACACAC81E8E8E8E8E8E8E8E8091010
              100910101009E8E8E8E8E8E8E881ACACAC81ACACAC81E8E8E8E8E8E809101010
              09E80910101009E8E8E8E8E881ACACAC81E881ACACAC81E8E8E8E80910101009
              E8E8E80910101009E8E8E881ACACAC81E8E8E881ACACAC81E8E8E809090909E8
              E8E8E8E809090909E8E8E881818181E8E8E8E8E881818181E8E8E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
            NumGlyphs = 2
          end
          object RzBitBtn41: TRzBitBtn
            Left = 674
            Top = 6
            Width = 65
            Height = 30
            FrameColor = 7617536
            Caption = #20851#38381
            Color = 15791348
            HotTrack = True
            TabOrder = 3
            Glyph.Data = {
              36060000424D3606000000000000360400002800000020000000100000000100
              08000000000000020000420B0000420B00000001000000000000000000003300
              00006600000099000000CC000000FF0000000033000033330000663300009933
              0000CC330000FF33000000660000336600006666000099660000CC660000FF66
              000000990000339900006699000099990000CC990000FF99000000CC000033CC
              000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
              0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
              330000333300333333006633330099333300CC333300FF333300006633003366
              33006666330099663300CC663300FF6633000099330033993300669933009999
              3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
              330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
              66006600660099006600CC006600FF0066000033660033336600663366009933
              6600CC336600FF33660000666600336666006666660099666600CC666600FF66
              660000996600339966006699660099996600CC996600FF99660000CC660033CC
              660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
              6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
              990000339900333399006633990099339900CC339900FF339900006699003366
              99006666990099669900CC669900FF6699000099990033999900669999009999
              9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
              990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
              CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
              CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
              CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
              CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
              CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
              FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
              FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
              FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
              FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
              000000808000800000008000800080800000C0C0C00080808000191919004C4C
              4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
              6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A5D8
              D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8E8E8E8D890B4
              B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8D8B4B4B4
              B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8D8B4B4B4B4
              B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8A590B4B4D7D7
              B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8D8B4B4B4D7D7
              D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8D8B4B4B4B4D7
              D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4B4B4
              D7D7D7B4B4B4B4B4D8E881ACACACACACD7D7D7ACACACACAC81E8D8B4B4B4B4D7
              D7D7D7D7B4B4B4B4D8E881ACACACACD7D7D7D7D7ACACACAC81E8D8B4B4B4D7D7
              D7B4D7D7D7B4B4B4D8E881ACACACD7D7D7ACD7D7D7ACACAC81E8A590B4B4D7D7
              B4B4B4D7D7B4B490A5E8ACE2ACACD7D7ACACACD7D7ACACE2ACE8E8D8B4B4B4B4
              B4B4B4B4B4B4B4D8E8E8E881ACACACACACACACACACACAC81E8E8E8E8D8B4B4B4
              B4B4B4B4B4B4D8E8E8E8E8E881ACACACACACACACACAC81E8E8E8E8E8E8D890B4
              B4B4B4B490D8E8E8E8E8E8E8E881E2ACACACACACE281E8E8E8E8E8E8E8E8A5D8
              D8D8D8D8A5E8E8E8E8E8E8E8E8E8AC8181818181ACE8E8E8E8E8}
            NumGlyphs = 2
          end
          object RzBitBtn28: TRzBitBtn
            Left = 223
            Top = 6
            Width = 88
            Height = 30
            FrameColor = 7617536
            Caption = #34920#26684#35774#32622
            Color = 15791348
            HotTrack = True
            TabOrder = 4
            Glyph.Data = {
              36060000424D3606000000000000360400002800000020000000100000000100
              08000000000000020000830E0000830E00000001000000000000000000003300
              00006600000099000000CC000000FF0000000033000033330000663300009933
              0000CC330000FF33000000660000336600006666000099660000CC660000FF66
              000000990000339900006699000099990000CC990000FF99000000CC000033CC
              000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
              0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
              330000333300333333006633330099333300CC333300FF333300006633003366
              33006666330099663300CC663300FF6633000099330033993300669933009999
              3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
              330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
              66006600660099006600CC006600FF0066000033660033336600663366009933
              6600CC336600FF33660000666600336666006666660099666600CC666600FF66
              660000996600339966006699660099996600CC996600FF99660000CC660033CC
              660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
              6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
              990000339900333399006633990099339900CC339900FF339900006699003366
              99006666990099669900CC669900FF6699000099990033999900669999009999
              9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
              990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
              CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
              CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
              CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
              CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
              CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
              FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
              FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
              FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
              FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
              000000808000800000008000800080800000C0C0C00080808000191919004C4C
              4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
              6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E85E5E5E5E5E
              5E5E5E5E5E5E5E5E5E5EE8818181818181818181818181818181E85ED7D7D7D7
              D7D7D7D7D7D7D7D7D75EE881D7D7D7D7D7D7D7D7D7D7D7D7D781E85ED7090909
              0909090909090909D75EE881D78181818181818181818181D781E85ED7095E5E
              5E5E5E5E5E5E5E09D75EE881D781ACACACACACACACACAC81D781E85ED7090909
              0909090909090909D75EE881D78181818181818181818181D781E85ED7095E5E
              5E5E5E5E5E5E5E09D75EE881D781ACACACACACACACACAC81D781E85ED7090909
              0909090909090909D75EE881D78181818181818181818181D781E85ED7095E5E
              5E5E5E5E5E5E5E09D75EE881D781ACACACACACACACACAC81D781E85ED7090909
              0909090909090909D75EE881D78181818181818181818181D781E85ED7095E5E
              5E5E5E5E5E5E5E09D75EE881D781ACACACACACACACACAC81D781E85ED7090909
              0909090909090909D75EE881D78181818181818181818181D781E85ED7095E5E
              5E5E5E5E5E5E5E09D75EE881D781ACACACACACACACACAC81D781E85ED7090909
              0909090909090909D75EE881D78181818181818181818181D781E85ED7D7D7D7
              D7D7D7D7D7D7D7D7D75EE881D7D7D7D7D7D7D7D7D7D7D7D7D781E85E5E5E5E5E
              5E5E5E5E5E5E5E5E5E5EE8818181818181818181818181818181}
            NumGlyphs = 2
          end
          object RzBitBtn29: TRzBitBtn
            Left = 430
            Top = 6
            Width = 88
            Height = 30
            FrameColor = 7617536
            Caption = #32508#21512#25991#26723
            Color = 15791348
            HotTrack = True
            TabOrder = 5
            Glyph.Data = {
              36060000424D3606000000000000360400002800000020000000100000000100
              08000000000000020000330B0000330B00000001000000000000000000003300
              00006600000099000000CC000000FF0000000033000033330000663300009933
              0000CC330000FF33000000660000336600006666000099660000CC660000FF66
              000000990000339900006699000099990000CC990000FF99000000CC000033CC
              000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
              0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
              330000333300333333006633330099333300CC333300FF333300006633003366
              33006666330099663300CC663300FF6633000099330033993300669933009999
              3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
              330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
              66006600660099006600CC006600FF0066000033660033336600663366009933
              6600CC336600FF33660000666600336666006666660099666600CC666600FF66
              660000996600339966006699660099996600CC996600FF99660000CC660033CC
              660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
              6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
              990000339900333399006633990099339900CC339900FF339900006699003366
              99006666990099669900CC669900FF6699000099990033999900669999009999
              9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
              990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
              CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
              CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
              CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
              CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
              CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
              FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
              FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
              FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
              FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
              000000808000800000008000800080800000C0C0C00080808000191919004C4C
              4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
              6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8A378787878
              787878787878AAE8E8E8E88181818181818181818181ACE8E8E8A3A3D5CECECE
              CECECECECEA378E8E8E88181E3ACACACACACACACAC8181E8E8E8A3A3CED5D5D5
              D5D5D5D5D5CE78A3E8E88181ACE3E3E3E3E3E3E3E3AC8181E8E8A3A3CED5D5D5
              D5D5D5D5D5CEAA78E8E88181ACE3E3E3E3E3E3E3E3ACAC81E8E8A3CEA3D5D5D5
              D5D5D5D5D5CED578A3E881AC81E3E3E3E3E3E3E3E3ACE38181E8A3CEAAAAD5D5
              D5D5D5D5D5CED5AA78E881ACACACE3E3E3E3E3E3E3ACE3AC81E8A3D5CEA3D6D6
              D6D6D6D6D6D5D6D678E881E3AC81E3E3E3E3E3E3E3E3E3E381E8A3D5D5CEA3A3
              A3A3A3A3A3A3A3A3CEE881E3E3AC81818181818181818181ACE8A3D6D5D5D5D5
              D6D6D6D6D678E8E8E8E881E3E3E3E3E3E3E3E3E3E381E8E8E8E8E8A3D6D6D6D6
              A3A3A3A3A3E8E8E8E8E8E881E3E3E3E38181818181E8E8E8E8E8E8E8A3A3A3A3
              E8E8E8E8E8E8E8E8E8E8E8E881818181E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
            NumGlyphs = 2
          end
          object RzBitBtn32: TRzBitBtn
            Left = 317
            Top = 6
            Width = 107
            Height = 30
            FrameColor = 7617536
            Caption = #24037#31243#37327#26126#32454#21333
            Color = 15791348
            HotTrack = True
            TabOrder = 6
            Glyph.Data = {
              36060000424D3606000000000000360400002800000020000000100000000100
              08000000000000020000330B0000330B00000001000000000000000000003300
              00006600000099000000CC000000FF0000000033000033330000663300009933
              0000CC330000FF33000000660000336600006666000099660000CC660000FF66
              000000990000339900006699000099990000CC990000FF99000000CC000033CC
              000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
              0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
              330000333300333333006633330099333300CC333300FF333300006633003366
              33006666330099663300CC663300FF6633000099330033993300669933009999
              3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
              330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
              66006600660099006600CC006600FF0066000033660033336600663366009933
              6600CC336600FF33660000666600336666006666660099666600CC666600FF66
              660000996600339966006699660099996600CC996600FF99660000CC660033CC
              660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
              6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
              990000339900333399006633990099339900CC339900FF339900006699003366
              99006666990099669900CC669900FF6699000099990033999900669999009999
              9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
              990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
              CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
              CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
              CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
              CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
              CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
              FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
              FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
              FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
              FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
              000000808000800000008000800080800000C0C0C00080808000191919004C4C
              4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
              6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E80B0B0B0B0B
              0B0B0B0B0B0B0B0B0BE8E88181818181818181818181818181E8E80BD7D7D7D7
              D7D7D7D7D7D7D7D70BE8E881E8E8E8E8E8E8E8E8E8E8E8E881E8E80BD7E2E2D7
              D7E2E2D7D7E2E2D70BE8E881E8E2E2E8E8E2E2E8E8E2E2E881E8E80BD7C1C1D7
              D70909D7D7E4E4D70BE8E881E88181E8E88181E8E88181E881E8E80BD7C1C1D7
              D70909D7D7E4E4D70BE8E881E88181E8E88181E8E88181E881E8E80BD7D7D7D7
              D7D7D7D7D7D7D7D70BE8E881E8E8E8E8E8E8E8E8E8E8E8E881E8E80BD7D7D7D7
              D7D7D7D7D7D7D7D70BE8E881E8E8E8E8E8E8E8E8E8E8E8E881E8E80BD7E2E2D7
              D7E2E2D7D7E2E2D70BE8E881E8E2E2E8E8E2E2E8E8E2E2E881E8E80BD75151D7
              D77373D7D71212D70BE8E881E88181E8E88181E8E88181E881E8E80BD75151D7
              D77373D7D71212D70BE8E881E88181E8E88181E8E88181E881E8E80BD7D7D7D7
              D7D7D7D7D7D7D7D70BE8E881E8E8E8E8E8E8E8E8E8E8E8E881E8E80B0B0B0B0B
              0B0B0B0B0B0B0B0B0BE8E88181818181818181818181818181E8E80B0B0B0B0B
              0B0B0B0B0B0B0B0B0BE8E88181818181818181818181818181E8E8890B0B0B0B
              0B0B0B0B0B0B0B0B89E8E8AC818181818181818181818181ACE8E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
            NumGlyphs = 2
          end
          object RzBitBtn56: TRzBitBtn
            Left = 10
            Top = 6
            Width = 65
            Height = 30
            FrameColor = 7617536
            Caption = #20445#23384
            Color = 15791348
            HotTrack = True
            TabOrder = 7
            Glyph.Data = {
              36060000424D3606000000000000360400002800000020000000100000000100
              08000000000000020000730E0000730E00000001000000000000000000003300
              00006600000099000000CC000000FF0000000033000033330000663300009933
              0000CC330000FF33000000660000336600006666000099660000CC660000FF66
              000000990000339900006699000099990000CC990000FF99000000CC000033CC
              000066CC000099CC0000CCCC0000FFCC000000FF000033FF000066FF000099FF
              0000CCFF0000FFFF000000003300330033006600330099003300CC003300FF00
              330000333300333333006633330099333300CC333300FF333300006633003366
              33006666330099663300CC663300FF6633000099330033993300669933009999
              3300CC993300FF99330000CC330033CC330066CC330099CC3300CCCC3300FFCC
              330000FF330033FF330066FF330099FF3300CCFF3300FFFF3300000066003300
              66006600660099006600CC006600FF0066000033660033336600663366009933
              6600CC336600FF33660000666600336666006666660099666600CC666600FF66
              660000996600339966006699660099996600CC996600FF99660000CC660033CC
              660066CC660099CC6600CCCC6600FFCC660000FF660033FF660066FF660099FF
              6600CCFF6600FFFF660000009900330099006600990099009900CC009900FF00
              990000339900333399006633990099339900CC339900FF339900006699003366
              99006666990099669900CC669900FF6699000099990033999900669999009999
              9900CC999900FF99990000CC990033CC990066CC990099CC9900CCCC9900FFCC
              990000FF990033FF990066FF990099FF9900CCFF9900FFFF99000000CC003300
              CC006600CC009900CC00CC00CC00FF00CC000033CC003333CC006633CC009933
              CC00CC33CC00FF33CC000066CC003366CC006666CC009966CC00CC66CC00FF66
              CC000099CC003399CC006699CC009999CC00CC99CC00FF99CC0000CCCC0033CC
              CC0066CCCC0099CCCC00CCCCCC00FFCCCC0000FFCC0033FFCC0066FFCC0099FF
              CC00CCFFCC00FFFFCC000000FF003300FF006600FF009900FF00CC00FF00FF00
              FF000033FF003333FF006633FF009933FF00CC33FF00FF33FF000066FF003366
              FF006666FF009966FF00CC66FF00FF66FF000099FF003399FF006699FF009999
              FF00CC99FF00FF99FF0000CCFF0033CCFF0066CCFF0099CCFF00CCCCFF00FFCC
              FF0000FFFF0033FFFF0066FFFF0099FFFF00CCFFFF00FFFFFF00000080000080
              000000808000800000008000800080800000C0C0C00080808000191919004C4C
              4C00B2B2B200E5E5E500C8AC2800E0CC6600F2EABF00B59B2400D8E9EC009933
              6600D075A300ECC6D900646F710099A8AC00E2EFF10000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E809090909
              090909090909090909E8E8E881818181818181818181818181E8E809101009E3
              1009E3E3E309101009E8E881ACAC81E3AC81E3E3E381ACAC81E8E809101009E3
              1009E3E3E309101009E8E881ACAC81E3AC81E3E3E381ACAC81E8E809101009E3
              1009E3E3E309101009E8E881ACAC81E3AC81E3E3E381ACAC81E8E809101009E3
              E3E3E3E3E309101009E8E881ACAC81E3E3E3E3E3E381ACAC81E8E80910101009
              090909090910101009E8E881ACACAC818181818181ACACAC81E8E80910101010
              101010101010101009E8E881ACACACACACACACACACACACAC81E8E80910100909
              090909090909101009E8E881ACAC8181818181818181ACAC81E8E8091009D7D7
              D7D7D7D7D7D7091009E8E881AC81D7D7D7D7D7D7D7D781AC81E8E8091009D709
              0909090909D7091009E8E881AC81D7818181818181D781AC81E8E8091009D7D7
              D7D7D7D7D7D7091009E8E881AC81D7D7D7D7D7D7D7D781AC81E8E809E309D709
              0909090909D7090909E8E881E381D7818181818181D7818181E8E8091009D7D7
              D7D7D7D7D7D7091009E8E881AC81D7D7D7D7D7D7D7D781AC81E8E80909090909
              090909090909090909E8E88181818181818181818181818181E8E8E8E8E8E8E8
              E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8E8}
            NumGlyphs = 2
          end
          object cxButton13: TcxButton
            Left = 595
            Top = 6
            Width = 73
            Height = 30
            Caption = #25253#34920
            Kind = cxbkDropDownButton
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000080000
              000C0000000D0000000D0000000D0000000D0000000D0000000D0000000D0000
              000E0000000E0000000E0000000E0000000E0000000D000000097B5A50BFAB7D
              6FFFAA7D6FFFAA7C6FFFAA7C6EFFAA7C6DFFA97B6DFFA97B6DFFA87B6DFFA97A
              6CFFA97A6CFFA8796CFFA87A6BFFA7796BFFA8786AFF78564CC0AC8072FFF9F2
              EEFFF9F3EEFFF9F1EEFFF9F1EEFFCFB2A8FFF9F1EEFFF8F1EEFFF8F2EEFFF8F1
              EDFFCEB0A6FFF9F1EDFFF9F2EDFFF9F1EDFFF9F1EEFFA87A6CFFAE8274FFF9F3
              F0FFF7EFEAFFF7EFEAFFF8F2EEFFD2B7AFFFF9F2EFFFF9F2EFFFF9F3EFFFF9F3
              EFFFD1B7AEFFF8F3EFFFF8F3EFFFF9F3EEFFFAF5F2FFBE9B91FFAF8375FFD3B8
              B0FFC5A399FFC5A298FFD3B9B1FF2B8758FF2B8758FF2A8658FF2A8657FF2A84
              58FF2A8457FF298456FF298357FF298254FF298256FF298254FFB18578FFFAF4
              F0FFF8F0EBFFF7F0EBFFF9F3F0FF2C895BFF74DEC1FF74DEC1FF74DEC1FF74DE
              C0FF35976AFF74DEC0FF74DEC0FF74DEC0FF74DEC0FF298455FFB28779FFF9F5
              F1FFF8F0ECFFF8F1EBFFFAF4F1FF2E8B5DFF76DFC1FF5FD6AFFF5DD6AEFF5DD5
              ADFF369A6CFF5DD5ACFF5DD5ACFF5CD5ADFF74DEC1FF2B8758FFB4887CFFD8C0
              B8FFC8A79DFFC8A79DFFD5BCB4FF2E8E5EFF389D70FF389D70FF389C6FFF389C
              6FFF389B6EFF379B6EFF389B6DFF379A6DFF379A6DFF2C885AFFB48A7EFFFAF5
              F3FFF8F2EDFFF8F1EDFFFAF4F1FF2F9062FF77E0C3FF63D8B1FF62D7B1FF62D7
              B2FF399D70FF62D7B1FF62D7B0FF61D7B1FF76DFC2FF2E8A5CFFB68D7FFFFAF6
              F3FFF9F2EDFFF9F2EDFFFAF4F1FF309264FF78E0C4FF78E0C4FF78E0C4FF78E0
              C4FF3A9F73FF78DFC4FF78DFC3FF77E0C3FF78DFC3FF2E8D5EFFB78E81FFDDC6
              BFFFCBABA2FFCBACA2FFD7C0B8FF329566FF339366FF329265FF309265FF3292
              64FF329164FF309163FF2F9062FF2F9062FF2F8F60FF2E8F60FFB88F82FFFAF7
              F4FFF8F3EFFFF8F2EEFFF9F4F1FFD8C0B9FFFAF6F2FFFAF5F3FFFAF5F2FFFAF5
              F2FFD7C0B8FFFAF6F2FFFAF5F2FFFAF5F2FFFCF8F6FFC7A79DFFB99085FFFBF8
              F5FFFAF8F5FFFBF8F4FFFBF6F4FFDFCAC3FFFBF6F5FFFBF7F4FFFBF7F4FFFBF6
              F4FFDDC8C1FFFAF6F4FFFBF7F5FFFBF6F5FFFBF7F5FFB68C80FF8A6C63C0BA91
              85FFBA9185FFBA9185FFBA9084FFB99185FFB99084FFB99084FFB99084FFB990
              83FFB89083FFB98F83FFB78F83FFB88E82FFB78E82FF876960C1000000030000
              0004000000050000000500000005000000050000000500000005000000050000
              0005000000050000000500000006000000060000000500000004}
            TabOrder = 8
          end
        end
        object cxGrid4: TcxGrid
          Left = 0
          Top = 86
          Width = 963
          Height = 400
          Align = alClient
          TabOrder = 1
          LookAndFeel.SkinName = 'Office2016Colorful'
          object tvEngineeringQuantity: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.KeyFieldNames = 'numbers'
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '0.##'
                Kind = skSum
                Column = tvEngineeringQuantityColumn8
              end
              item
                Format = #24635#39069':'#165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvEngineeringQuantityColumn11
              end
              item
                Kind = skCount
                Column = tvEngineeringQuantityColumn12
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #21333#20987#31579#36873#26597#35810
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsBehavior.FocusCellOnCycle = True
            OptionsCustomize.DataRowSizing = True
            OptionsCustomize.GroupRowSizing = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.CellEndEllipsis = True
            OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
            OptionsView.CellAutoHeight = True
            OptionsView.DataRowHeight = 32
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 25
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 15
            object tvEngineeringQuantityColumn13: TcxGridDBColumn
              Caption = #24207#21495
              MinWidth = 30
              Options.Editing = False
              Options.Filtering = False
              Options.FilteringAddValueItems = False
              Options.FilteringFilteredItemsList = False
              Options.FilteringMRUItemsList = False
              Options.FilteringPopup = False
              Options.FilteringPopupMultiSelect = False
              Options.FilteringWithFindPanel = False
              Options.Focusing = False
              Options.IgnoreTimeForFiltering = False
              Options.IncSearch = False
              Options.GroupFooters = False
              Options.Grouping = False
              Options.HorzSizing = False
              Options.Moving = False
              Options.Sorting = False
              Width = 30
            end
            object tvEngineeringQuantityColumn17: TcxGridDBColumn
              Caption = #29366#24577
              DataBinding.FieldName = 'status'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.Alignment = taCenter
              HeaderAlignmentHorz = taCenter
              Width = 40
            end
            object tvEngineeringQuantityColumn1: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'numbers'
              Width = 71
            end
            object tvEngineeringQuantityColumn2: TcxGridDBColumn
              Caption = #26085#26399
              DataBinding.FieldName = 'cxDate'
              PropertiesClassName = 'TcxDateEditProperties'
              Width = 92
            end
            object tvEngineeringQuantityColumn3: TcxGridDBColumn
              Caption = #24037#31243#21517#31216
              DataBinding.FieldName = 'ProjectName'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 120
            end
            object tvEngineeringQuantityColumn4: TcxGridDBColumn
              Caption = #24037#31181
              DataBinding.FieldName = 'WorkType'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 120
            end
            object tvEngineeringQuantityColumn5: TcxGridDBColumn
              Caption = #35745#37327#37096#20301#21517#31216
              DataBinding.FieldName = 'MeteringPosName'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 120
            end
            object tvEngineeringQuantityColumn6: TcxGridDBColumn
              Caption = #25215#21253#21333#20301
              DataBinding.FieldName = 'ContractCompany'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 120
            end
            object tvEngineeringQuantityColumn7: TcxGridDBColumn
              Caption = #35745#37327#31867#21035
              DataBinding.FieldName = 'MeteringType'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 120
            end
            object tvEngineeringQuantityColumn9: TcxGridDBColumn
              Caption = #21333#20215
              DataBinding.FieldName = 'UnitPrice'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taRightJustify
              Width = 120
            end
            object tvEngineeringQuantityColumn10: TcxGridDBColumn
              Caption = #21333#20301
              DataBinding.FieldName = 'Company'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 87
            end
            object tvEngineeringQuantityColumn8: TcxGridDBColumn
              Caption = #25968#37327
              DataBinding.FieldName = 'MeteringCount'
              PropertiesClassName = 'TcxSpinEditProperties'
              Properties.ValueType = vtFloat
              HeaderAlignmentHorz = taRightJustify
              Width = 72
            end
            object tvEngineeringQuantityColumn11: TcxGridDBColumn
              Caption = #24635#20215
              DataBinding.FieldName = 'Total'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taRightJustify
              Styles.Content = DM.SumMoney
              Width = 120
            end
            object tvEngineeringQuantityColumn12: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'remarks'
              Width = 230
            end
            object tvEngineeringQuantityColumn14: TcxGridDBColumn
              Caption = #36164#36136#21333#20301
              Width = 70
            end
            object tvEngineeringQuantityColumn15: TcxGridDBColumn
              Caption = #39033#30446#37096
              DataBinding.FieldName = 'ProjectDepartment'
              Width = 70
            end
            object tvEngineeringQuantityColumn16: TcxGridDBColumn
              Caption = #26045#24037#21333#20301
              DataBinding.FieldName = 'CreateCompany'
              Width = 70
            end
          end
          object tvDetailedGrid: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DetailKeyFieldNames = 'parent'
            DataController.MasterKeyFieldNames = 'numbers'
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #24635#25968':0.##'
                Kind = skSum
                Column = tvDetailedGridColumn5
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.DataRowSizing = True
            OptionsView.CellEndEllipsis = True
            OptionsView.CellAutoHeight = True
            OptionsView.DataRowHeight = 32
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 25
            OptionsView.Indicator = True
            object tvDetailedGridColumn1: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'numbers'
              Options.Filtering = False
              Options.Sorting = False
              Width = 103
            end
            object tvDetailedGridColumn2: TcxGridDBColumn
              Caption = #35745#31639#37096#20301
              DataBinding.FieldName = 'ItemPosition'
              Options.Filtering = False
              Options.Sorting = False
              Width = 129
            end
            object tvDetailedGridColumn3: TcxGridDBColumn
              Caption = #35268#26684#31867#21035
              DataBinding.FieldName = 'ItemType'
              Options.Filtering = False
              Options.Sorting = False
              Width = 136
            end
            object tvDetailedGridColumn4: TcxGridDBColumn
              Caption = #35745#31639#20844#24335
              DataBinding.FieldName = 'ItemSpec'
              Options.Filtering = False
              Options.Sorting = False
              Width = 119
            end
            object tvDetailedGridColumn5: TcxGridDBColumn
              Caption = #25968#37327
              DataBinding.FieldName = 'ItemValue'
              HeaderAlignmentHorz = taRightJustify
              Options.Filtering = False
              Options.Sorting = False
              Width = 80
            end
            object tvDetailedGridColumn6: TcxGridDBColumn
              Caption = #21333#20301
              DataBinding.FieldName = 'ItemUnit'
              Options.Filtering = False
              Options.Sorting = False
              Width = 121
            end
            object tvDetailedGridColumn7: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'remarks'
              PropertiesClassName = 'TcxMemoProperties'
              Width = 230
            end
            object tvDetailedGridColumn8: TcxGridDBColumn
              Caption = #22791#27880'1'
              DataBinding.FieldName = 'remarks1'
              PropertiesClassName = 'TcxMemoProperties'
              Options.Filtering = False
              Options.Sorting = False
              Width = 230
            end
          end
          object LvQuantity: TcxGridLevel
            GridView = tvEngineeringQuantity
            object LvDetailed: TcxGridLevel
              GridView = tvDetailedGrid
            end
          end
        end
        object cxGroupBox5: TcxGroupBox
          Left = 0
          Top = 29
          Align = alTop
          Caption = #26597#35810
          TabOrder = 2
          Visible = False
          Height = 57
          Width = 963
          object Label32: TLabel
            Left = 429
            Top = 24
            Width = 52
            Height = 13
            Caption = #25215#21253#21333#20301':'
          end
          object Label34: TLabel
            Left = 24
            Top = 24
            Width = 52
            Height = 13
            Caption = #24037#22320#21517#31216':'
          end
          object Label35: TLabel
            Left = 916
            Top = 25
            Width = 52
            Height = 13
            Caption = #36215#22987#26085#26399':'
          end
          object Label36: TLabel
            Left = 1075
            Top = 25
            Width = 52
            Height = 13
            Caption = #32467#26463#26085#26399':'
          end
          object Label37: TLabel
            Left = 596
            Top = 25
            Width = 28
            Height = 13
            Caption = #24037#31181':'
          end
          object Label38: TLabel
            Left = 215
            Top = 24
            Width = 76
            Height = 13
            Caption = #35745#37327#37096#20301#21517#31216':'
          end
          object Label55: TLabel
            Left = 716
            Top = 25
            Width = 52
            Height = 13
            Caption = #35745#37327#31867#21035':'
          end
          object cxTextEdit14: TcxTextEdit
            Left = 487
            Top = 22
            TabOrder = 2
            TextHint = #22238#36710#21363#21487#26597#35810
            Width = 103
          end
          object cxTextEdit15: TcxTextEdit
            Left = 82
            Top = 22
            TabOrder = 0
            TextHint = #22238#36710#21363#21487#26597#35810
            Width = 129
          end
          object cxButton9: TcxButton
            Left = 1235
            Top = 21
            Width = 60
            Height = 25
            Caption = #26597#35810
            TabOrder = 6
          end
          object cxDateEdit9: TcxDateEdit
            Left = 974
            Top = 30
            TabOrder = 4
            Width = 95
          end
          object cxDateEdit10: TcxDateEdit
            Left = 1133
            Top = 30
            TabOrder = 5
            Width = 96
          end
          object cxTextEdit16: TcxTextEdit
            Left = 630
            Top = 22
            Style.BorderStyle = ebs3D
            TabOrder = 3
            Width = 73
          end
          object cxTextEdit17: TcxTextEdit
            Left = 297
            Top = 22
            Style.BorderStyle = ebs3D
            TabOrder = 1
            Width = 126
          end
          object cxTextEdit4: TcxTextEdit
            Left = 774
            Top = 30
            Style.BorderStyle = ebs3D
            TabOrder = 7
            Width = 136
          end
        end
        object RzToolbar12: TRzToolbar
          Left = 0
          Top = 0
          Width = 963
          Height = 29
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsGroove
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 3
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer100
            RzToolButton75
            RzSpacer101
            RzToolButton76
            RzSpacer102
            RzToolButton77
            RzSpacer103
            RzToolButton78
            RzSpacer104
            RzToolButton83
            RzSpacer111
            RzToolButton79
            RzSpacer108
            RzToolButton82
            RzSpacer105
            cxLabel50
            cxDateEdit37
            RzSpacer107
            cxLabel51
            cxDateEdit38
            RzSpacer110
            RzToolButton81
            RzSpacer106
            RzToolButton80
            RzSpacer109)
          object RzSpacer100: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton75: TRzToolButton
            Left = 12
            Top = 2
            Width = 65
            ImageIndex = 1
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #28155#21152
          end
          object RzSpacer101: TRzSpacer
            Left = 77
            Top = 2
          end
          object RzToolButton76: TRzToolButton
            Left = 85
            Top = 2
            Width = 67
            ImageIndex = 3
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #20445#23384
          end
          object RzSpacer102: TRzSpacer
            Left = 152
            Top = 2
          end
          object RzToolButton77: TRzToolButton
            Left = 160
            Top = 2
            Width = 65
            ImageIndex = 2
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #21024#38500
          end
          object RzSpacer103: TRzSpacer
            Left = 225
            Top = 2
          end
          object RzToolButton78: TRzToolButton
            Left = 233
            Top = 2
            Width = 70
            ImageIndex = 4
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #34920#26684#35774#32622
          end
          object RzToolButton79: TRzToolButton
            Left = 391
            Top = 2
            Width = 65
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzSpacer104: TRzSpacer
            Left = 303
            Top = 2
          end
          object RzToolButton80: TRzToolButton
            Left = 870
            Top = 2
            Width = 65
            ImageIndex = 8
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #36864#20986
          end
          object RzSpacer105: TRzSpacer
            Left = 524
            Top = 2
          end
          object RzSpacer106: TRzSpacer
            Left = 862
            Top = 2
          end
          object RzSpacer107: TRzSpacer
            Left = 678
            Top = 2
            Width = 5
          end
          object RzSpacer108: TRzSpacer
            Left = 456
            Top = 2
          end
          object RzToolButton81: TRzToolButton
            Left = 837
            Top = 2
            ImageIndex = 10
            ShowCaption = True
            UseToolbarShowCaption = False
          end
          object RzSpacer109: TRzSpacer
            Left = 935
            Top = 2
          end
          object RzToolButton82: TRzToolButton
            Left = 464
            Top = 2
            Width = 60
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #32508#21512#25991#26723
          end
          object RzSpacer110: TRzSpacer
            Left = 829
            Top = 2
          end
          object RzSpacer111: TRzSpacer
            Left = 383
            Top = 2
          end
          object RzToolButton83: TRzToolButton
            Left = 311
            Top = 2
            Width = 72
            ImageIndex = 16
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #24037#31243#21517#32454#34920
          end
          object cxLabel50: TcxLabel
            Left = 532
            Top = 6
            Caption = #36215#22987#26085#26399':'
            Transparent = True
          end
          object cxDateEdit37: TcxDateEdit
            Left = 588
            Top = 4
            TabOrder = 1
            Width = 90
          end
          object cxLabel51: TcxLabel
            Left = 683
            Top = 6
            Caption = #32467#26463#26085#26399':'
            Transparent = True
          end
          object cxDateEdit38: TcxDateEdit
            Left = 739
            Top = 4
            TabOrder = 3
            Width = 90
          end
        end
      end
      object TabSheet9: TRzTabSheet
        Color = clWhite
        TabVisible = False
        Caption = #26426#26800#22303#30707#26041
        object cxGroupBox3: TcxGroupBox
          Left = 0
          Top = 29
          Align = alTop
          Caption = #26597#35810
          TabOrder = 0
          Visible = False
          Height = 57
          Width = 963
          object Label14: TLabel
            Left = 16
            Top = 27
            Width = 52
            Height = 13
            Caption = #24037#22320#21517#31216':'
          end
          object Label16: TLabel
            Left = 780
            Top = 27
            Width = 52
            Height = 13
            Caption = #36215#22987#26085#26399':'
          end
          object Label17: TLabel
            Left = 939
            Top = 27
            Width = 52
            Height = 13
            Caption = #32467#26463#26085#26399':'
          end
          object Label27: TLabel
            Left = 215
            Top = 27
            Width = 52
            Height = 13
            Caption = #25215#21253#21333#20301':'
          end
          object Label28: TLabel
            Left = 421
            Top = 27
            Width = 52
            Height = 13
            Caption = #26426#26800#31867#21035':'
          end
          object Label56: TLabel
            Left = 613
            Top = 27
            Width = 52
            Height = 13
            Caption = #36153#29992#31867#21035':'
          end
          object cxTextEdit10: TcxTextEdit
            Left = 74
            Top = 23
            TabOrder = 0
            TextHint = #22238#36710#21363#21487#26597#35810
            Width = 129
          end
          object cxButton6: TcxButton
            Left = 1099
            Top = 26
            Width = 60
            Height = 25
            Caption = #26597#35810
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000020000000E0B14308329448DFB1D2F58A5000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000020000000E0D1937883C6DB2FF5BB1F9FF325196F4000000000000
              00000000000100000004000000090000000D0000000F0000000F0000000C0000
              00070000000E0F1D3C864A7CBCFF73C4FFFF467CC3FF17254485000000000000
              0002000000081C130F465A3B31BC7C5043F87F5244FF7B4E42FA57382FC11E14
              1059112142875686C2FF88D0FFFF5186C7FF142343880000000F000000010302
              02104A332C91946B5DFDC6ACA1FFE4D1C6FFEDDDD2FFE2D0C5FFC0A599FF855C
              50FF6E6B7EFF98D4F8FF5B8ECBFF152545840000000D00000002000000076046
              3DA6B39288FFE9DAD0FFDAC0A1FFCBA87AFFC49B66FFCCAA7EFFDCC2A5FFE5D2
              C6FF9A766AFF736A77FF162747850000000E00000002000000002A201D4AAE88
              7CFFEFE6DFFFCDA67CFFCDA26BFFE3C28CFFEDD5A2FFE7CD9EFFD3B182FFD0AE
              88FFE7D5CAFF885F53FF25181464000000070000000000000000755B53ACDFCE
              C9FFDDC1A8FFC99865FFE8BE83FFE9C388FFEDCA97FFEFD3A7FFF2D9B0FFD5B1
              87FFDBBEA6FFC5ACA2FF5A3D33C10000000C0000000000000000A9877CE9F8F4
              F2FFC79873FFDEAB77FFEFCDABFFF0D0B1FFEDC9A1FFECC69AFFEFCFA9FFE9C9
              A4FFC89B77FFE6D6CEFF7C5448F10000000F0000000000000000C09C90FFFDFD
              FCFFBE875FFFEDCFB9FFF5DFD2FFF2D6C1FFF1CFB4FFEDC6A4FFECC19BFFEFC8
              A6FFC08B67FFF1E6DFFF8B6154FF0000000F0000000000000000AF9186E6F9F5
              F4FFC69474FFE8CDC3FFF9E8E4FFF6DED2FFF3D4C2FFF0CBB2FFEBB78EFFE5B7
              92FFC59172FFEBDFD9FF866055EE0000000D0000000000000000876F68B0E7D9
              D4FFE2C6B7FFC89072FFFAEFF2FFF9E7E4FFF6DDD3FFF1C8B2FFEBAF88FFC98E
              6CFFDCBBAAFFD3C0B7FF6B4F46BC00000009000000000000000026201E36CCAF
              A7FAFBF8F7FFCF9F88FFC78E72FFE9CDC6FFEDC7B5FFDD9F79FFC88865FFCE9D
              84FFF5EFEBFFB39387FF2A201D52000000040000000000000000000000036454
              4F84D9C2BAFFFDFBFAFFE2C6B8FFCB977EFFC08163FFCB977DFFE0C4B4FFFAF6
              F5FFC9B0A7FF6B564EA700000009000000010000000000000000000000000202
              020762534D81CEB2A9FAEADDD8FFF9F5F4FFFFFFFFFFF9F5F4FFE9DCD7FFC8AC
              A2FC62504B900404031000000002000000000000000000000000000000000000
              000000000003241F1D3486726BADB69B91E6CCADA1FFB99C92E988736CB22822
              1F3E000000060000000100000000000000000000000000000000}
            TabOrder = 5
          end
          object cxDateEdit5: TcxDateEdit
            Left = 838
            Top = 23
            TabOrder = 3
            Width = 95
          end
          object cxDateEdit6: TcxDateEdit
            Left = 997
            Top = 23
            TabOrder = 4
            Width = 96
          end
          object cxTextEdit13: TcxTextEdit
            Left = 273
            Top = 23
            Style.BorderStyle = ebs3D
            TabOrder = 1
            Width = 142
          end
          object cxComboBox2: TcxComboBox
            Left = 479
            Top = 23
            TabOrder = 2
            Width = 113
          end
          object cxTextEdit19: TcxTextEdit
            Left = 671
            Top = 23
            Style.BorderStyle = ebs3D
            TabOrder = 6
            Width = 103
          end
        end
        object cxGrid2: TcxGrid
          Left = 0
          Top = 131
          Width = 963
          Height = 355
          Align = alClient
          TabOrder = 1
          LookAndFeel.SkinName = 'Office2016Colorful'
          object tvMechanicsGrid: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = ColMechanicsGrid10
              end
              item
                Kind = skSum
                Column = ColMechanicsGrid8
              end
              item
                Kind = skSum
                Column = ColMechanicsGrid6
              end
              item
                Kind = skSum
                Column = ColMechanicsGrid14
              end
              item
                Kind = skCount
                Column = ColMechanicsGrid11
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #21333#20987#31579#36873#26597#35810
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
            OptionsView.DataRowHeight = 32
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 25
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 15
            object tvMechanicsGridColumn1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Focusing = False
              Options.Sorting = False
              Width = 40
            end
            object tvMechanicsGridColumn5: TcxGridDBColumn
              Caption = #29366#24577
              DataBinding.FieldName = 'status'
              PropertiesClassName = 'TcxCheckBoxProperties'
              Properties.Alignment = taCenter
              Width = 40
            end
            object ColMechanicsGrid1: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'Numbers'
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 87
            end
            object ColMechanicsGrid2: TcxGridDBColumn
              Caption = #26085#26399
              DataBinding.FieldName = 'cxDate'
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 97
            end
            object ColMechanicsGrid3: TcxGridDBColumn
              Caption = #24037#22320#21517#31216
              DataBinding.FieldName = 'WorkSiteName'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 128
            end
            object ColMechanicsGrid4: TcxGridDBColumn
              Caption = #25215#21253#21333#20301
              DataBinding.FieldName = 'SignName'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 101
            end
            object ColMechanicsGrid5: TcxGridDBColumn
              Caption = #26426#26800#31867#21035
              DataBinding.FieldName = 'MechanicsType'
              PropertiesClassName = 'TcxComboBoxProperties'
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
            end
            object ColMechanicsGrid12: TcxGridDBColumn
              Caption = #36153#29992#31867#21035
              DataBinding.FieldName = 'CostTyps'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 80
            end
            object ColMechanicsGrid13: TcxGridDBColumn
              Caption = #21333#36710#26041#37327
              DataBinding.FieldName = 'BicycleVolume'
              PropertiesClassName = 'TcxSpinEditProperties'
              Properties.ValueType = vtFloat
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 91
            end
            object ColMechanicsGrid6: TcxGridDBColumn
              Caption = #25968#37327'/'#36710'--'#24037#26085'/'#26102
              DataBinding.FieldName = 'WorkDays'
              PropertiesClassName = 'TcxSpinEditProperties'
              Properties.Alignment.Horz = taRightJustify
              Properties.ValueType = vtFloat
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 100
            end
            object ColMechanicsGrid14: TcxGridDBColumn
              Caption = #21512#35745#26041#37327
              DataBinding.FieldName = 'TotalVolume'
              PropertiesClassName = 'TcxSpinEditProperties'
              Properties.DisplayFormat = '0.00'
              Properties.EditFormat = '0.00'
              Properties.ValueType = vtFloat
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 58
            end
            object ColMechanicsGrid15: TcxGridDBColumn
              Caption = #21333#20301
              DataBinding.FieldName = 'Company'
              PropertiesClassName = 'TcxComboBoxProperties'
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 66
            end
            object ColMechanicsGrid7: TcxGridDBColumn
              Caption = #21333#20215
              DataBinding.FieldName = 'UnitPrice'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.Alignment.Horz = taRightJustify
              Properties.EditFormat = '0'
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 35
            end
            object ColMechanicsGrid8: TcxGridDBColumn
              Caption = #35013#36710'/'#23567#26102'--'#21152#29677'/'#23567#26102
              DataBinding.FieldName = 'Overtimer'
              PropertiesClassName = 'TcxSpinEditProperties'
              Properties.Alignment.Horz = taRightJustify
              Properties.ValueType = vtFloat
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 127
            end
            object ColMechanicsGrid9: TcxGridDBColumn
              Caption = #21333#20215
              DataBinding.FieldName = 'WorkUnitPrice'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.Alignment.Horz = taRightJustify
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 78
            end
            object ColMechanicsGrid10: TcxGridDBColumn
              Caption = #37329#39069
              DataBinding.FieldName = 'WorkTotal'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.Alignment.Horz = taRightJustify
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Styles.Content = DM.SumMoney
              Width = 115
            end
            object ColMechanicsGrid11: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'remarks'
              PropertiesClassName = 'TcxMemoProperties'
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 230
            end
            object tvMechanicsGridColumn2: TcxGridDBColumn
              Caption = #36164#36136#21333#20301
              DataBinding.FieldName = 'AptitudeCompany'
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 70
            end
            object tvMechanicsGridColumn3: TcxGridDBColumn
              Caption = #39033#30446#37096
              DataBinding.FieldName = 'ProjectDepartment'
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 70
            end
            object tvMechanicsGridColumn4: TcxGridDBColumn
              Caption = #26045#24037#21333#20301
              DataBinding.FieldName = 'CreateCompany'
              HeaderAlignmentHorz = taCenter
              Options.Sorting = False
              Width = 70
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = tvMechanicsGrid
          end
        end
        object RzPanel27: TRzPanel
          Left = 0
          Top = 86
          Width = 963
          Height = 45
          Align = alTop
          BorderOuter = fsGroove
          TabOrder = 2
          Visible = False
          object Label45: TLabel
            Left = 16
            Top = 16
            Width = 28
            Height = 13
            Caption = #24320#22987':'
          end
          object Label46: TLabel
            Left = 183
            Top = 16
            Width = 28
            Height = 13
            Caption = #32467#26463':'
          end
          object Label47: TLabel
            Left = 497
            Top = 16
            Width = 28
            Height = 13
            Caption = #26102#38388':'
          end
          object Label48: TLabel
            Left = 531
            Top = 16
            Width = 33
            Height = 13
            Caption = '0 '#23567#26102
          end
          object Label49: TLabel
            Left = 621
            Top = 16
            Width = 44
            Height = 13
            Caption = #24037#26102'/'#22825':'
          end
          object Label50: TLabel
            Left = 671
            Top = 16
            Width = 36
            Height = 13
            AutoSize = False
            Caption = '0'
          end
          object Label51: TLabel
            Left = 346
            Top = 16
            Width = 28
            Height = 13
            Caption = #21333#20215':'
          end
          object Label52: TLabel
            Left = 713
            Top = 16
            Width = 28
            Height = 13
            Caption = #24635#39069':'
          end
          object Label53: TLabel
            Left = 747
            Top = 16
            Width = 143
            Height = 13
            AutoSize = False
            Caption = '0 '#20803
          end
          object cxDateEdit13: TcxDateEdit
            Left = 50
            Top = 13
            Properties.AssignedValues.DisplayFormat = True
            Properties.Kind = ckDateTime
            Properties.View = cavClassic
            TabOrder = 0
            Width = 127
          end
          object cxDateEdit14: TcxDateEdit
            Left = 212
            Top = 13
            Properties.Kind = ckDateTime
            TabOrder = 1
            Width = 128
          end
          object cxButton16: TcxButton
            Left = 444
            Top = 10
            Width = 39
            Height = 25
            Caption = #35745#31639
            TabOrder = 3
          end
          object cxCurrencyEdit2: TcxCurrencyEdit
            Left = 380
            Top = 13
            TabOrder = 2
            Width = 58
          end
        end
        object RzToolbar11: TRzToolbar
          Left = 0
          Top = 0
          Width = 963
          Height = 29
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsGroove
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 3
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer88
            RzToolButton66
            RzSpacer89
            RzToolButton67
            RzSpacer90
            RzToolButton68
            RzSpacer91
            RzToolButton69
            RzSpacer97
            RzToolButton70
            RzSpacer92
            RzToolButton73
            RzSpacer99
            RzToolButton74
            RzSpacer94
            cxLabel48
            cxDateEdit35
            RzSpacer95
            cxLabel49
            cxDateEdit36
            RzSpacer93
            RzToolButton72
            RzSpacer96
            RzToolButton71
            RzSpacer98)
          object RzSpacer88: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton66: TRzToolButton
            Left = 12
            Top = 2
            Width = 65
            ImageIndex = 1
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #28155#21152
          end
          object RzSpacer89: TRzSpacer
            Left = 77
            Top = 2
          end
          object RzToolButton67: TRzToolButton
            Left = 85
            Top = 2
            Width = 67
            ImageIndex = 3
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #20445#23384
          end
          object RzSpacer90: TRzSpacer
            Left = 152
            Top = 2
          end
          object RzToolButton68: TRzToolButton
            Left = 160
            Top = 2
            Width = 65
            ImageIndex = 2
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #21024#38500
          end
          object RzSpacer91: TRzSpacer
            Left = 225
            Top = 2
          end
          object RzToolButton69: TRzToolButton
            Left = 233
            Top = 2
            Width = 70
            ImageIndex = 4
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #34920#26684#35774#32622
          end
          object RzToolButton70: TRzToolButton
            Left = 311
            Top = 2
            Width = 65
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzSpacer92: TRzSpacer
            Left = 376
            Top = 2
          end
          object RzToolButton71: TRzToolButton
            Left = 870
            Top = 2
            Width = 65
            ImageIndex = 8
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #36864#20986
          end
          object RzSpacer93: TRzSpacer
            Left = 829
            Top = 2
          end
          object RzSpacer94: TRzSpacer
            Left = 524
            Top = 2
          end
          object RzSpacer95: TRzSpacer
            Left = 678
            Top = 2
            Width = 5
          end
          object RzSpacer96: TRzSpacer
            Left = 862
            Top = 2
          end
          object RzToolButton72: TRzToolButton
            Left = 837
            Top = 2
            ImageIndex = 10
            ShowCaption = True
            UseToolbarShowCaption = False
          end
          object RzSpacer97: TRzSpacer
            Left = 303
            Top = 2
          end
          object RzToolButton73: TRzToolButton
            Left = 384
            Top = 2
            Width = 60
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #32508#21512#25991#26723
          end
          object RzSpacer98: TRzSpacer
            Left = 935
            Top = 2
          end
          object RzSpacer99: TRzSpacer
            Left = 444
            Top = 2
          end
          object RzToolButton74: TRzToolButton
            Left = 452
            Top = 2
            Width = 72
            ImageIndex = 23
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #26102#38388#35745#31639#22120
          end
          object cxLabel48: TcxLabel
            Left = 532
            Top = 6
            Caption = #36215#22987#26085#26399':'
            Transparent = True
          end
          object cxDateEdit35: TcxDateEdit
            Left = 588
            Top = 4
            TabOrder = 1
            Width = 90
          end
          object cxLabel49: TcxLabel
            Left = 683
            Top = 6
            Caption = #32467#26463#26085#26399':'
            Transparent = True
          end
          object cxDateEdit36: TcxDateEdit
            Left = 739
            Top = 4
            TabOrder = 3
            Width = 90
          end
        end
      end
      object TabSheet10: TRzTabSheet
        Color = clWhite
        TabVisible = False
        Caption = #39033#30446#37096#36130#21153
        object FlowingAccountGrid: TcxGrid
          Left = 0
          Top = 29
          Width = 963
          Height = 457
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          LookAndFeel.SkinName = 'Office2016Colorful'
          object tvFlowingAccount: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.Append.Enabled = False
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = #21512#35745
                Position = spFooter
              end
              item
                Format = #25968#20540':0.########;-0.########'
                Kind = skSum
              end
              item
                Format = #25968#20540':0.########;-0.########'
                Kind = skSum
                Position = spFooter
              end
              item
                Format = #25968#37327':0.###;-0.###'
                Kind = skSum
              end
              item
                Format = #25968#37327':0.###;-0.###'
                Kind = skSum
                Position = spFooter
              end
              item
                Format = #37329#39069':'#165',0.00;'#165'-,0.00'
                Kind = skSum
                Position = spFooter
                Column = tvFlowingAccountColumn11
              end
              item
                Format = #37329#39069':'#165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvFlowingAccountColumn11
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #24635#39069':'#165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvFlowingAccountColumn11
              end
              item
                Format = #22823#20889
                Kind = skCount
                Column = tvFlowingAccountColumn13
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #21333#20987#31579#36873#26597#35810
            FilterRow.Visible = True
            OptionsBehavior.AlwaysShowEditor = True
            OptionsBehavior.DragFocusing = dfDragDrop
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.FocusFirstCellOnNewRecord = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsCustomize.DataRowSizing = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.CellEndEllipsis = True
            OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
            OptionsView.CellAutoHeight = True
            OptionsView.DataRowHeight = 32
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.GroupRowHeight = 30
            OptionsView.HeaderFilterButtonShowMode = fbmButton
            OptionsView.HeaderHeight = 25
            OptionsView.Indicator = True
            object tvFlowingAccountColumn14: TcxGridDBColumn
              Caption = #24207#21495
              HeaderAlignmentHorz = taCenter
              MinWidth = 40
              Options.Editing = False
              Options.Filtering = False
              Options.FilteringAddValueItems = False
              Options.FilteringFilteredItemsList = False
              Options.FilteringMRUItemsList = False
              Options.FilteringPopup = False
              Options.FilteringPopupMultiSelect = False
              Options.FilteringWithFindPanel = False
              Options.Focusing = False
              Options.IgnoreTimeForFiltering = False
              Options.IncSearch = False
              Options.GroupFooters = False
              Options.Grouping = False
              Options.HorzSizing = False
              Options.Moving = False
              Options.Sorting = False
              Width = 40
            end
            object tvFlowingAccountColumn1: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'numbers'
              HeaderAlignmentHorz = taCenter
              Width = 82
            end
            object tvFlowingAccountColumn2: TcxGridDBColumn
              Caption = #26085#26399
              DataBinding.FieldName = 'cxDate'
              PropertiesClassName = 'TcxDateEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 80
            end
            object tvFlowingAccountColumn3: TcxGridDBColumn
              Caption = #21046#34920#31867#21035
              DataBinding.FieldName = 'TabType'
              PropertiesClassName = 'TcxComboBoxProperties'
              Properties.DropDownListStyle = lsEditFixedList
              HeaderAlignmentHorz = taCenter
              Width = 60
            end
            object tvFlowingAccountColumn4: TcxGridDBColumn
              Caption = #25910#27454#21333#20301
              DataBinding.FieldName = 'Receivables'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 109
            end
            object tvFlowingAccountColumn5: TcxGridDBColumn
              Caption = #20132#26131#26041#24335
              DataBinding.FieldName = 'TradeType'
              PropertiesClassName = 'TcxComboBoxProperties'
              HeaderAlignmentHorz = taCenter
              Width = 108
            end
            object tvFlowingAccountColumn6: TcxGridDBColumn
              Caption = #20184#27454#21333#20301
              DataBinding.FieldName = 'PaymentType'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 124
            end
            object tvFlowingAccountColumn7: TcxGridDBColumn
              Caption = #24037#31243'/'#21517#31216
              DataBinding.FieldName = 'ProjectName'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 72
            end
            object tvFlowingAccountColumn8: TcxGridDBColumn
              Caption = #31867#21035#21517#31216
              DataBinding.FieldName = 'CategoryName'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 123
            end
            object tvFlowingAccountColumn9: TcxGridDBColumn
              Caption = #25910#27454#20154#21517#31216
              DataBinding.FieldName = 'Payee'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 77
            end
            object tvFlowingAccountColumn10: TcxGridDBColumn
              Caption = #20184#27454#20154#21517#31216
              DataBinding.FieldName = 'Drawee'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 102
            end
            object tvFlowingAccountColumn11: TcxGridDBColumn
              Caption = #37329#39069
              DataBinding.FieldName = 'Total'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Properties.Alignment.Horz = taRightJustify
              HeaderAlignmentHorz = taCenter
              Styles.Content = DM.SumMoney
              Width = 102
            end
            object tvFlowingAccountColumn13: TcxGridDBColumn
              Caption = #22823#20889#37329#39069
              DataBinding.FieldName = 'CapitalMoney'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 230
            end
            object tvFlowingAccountColumn12: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'Remarks'
              PropertiesClassName = 'TcxMemoProperties'
              Properties.Alignment = taLeftJustify
              Properties.ScrollBars = ssVertical
              HeaderAlignmentHorz = taCenter
              Width = 230
            end
            object tvFlowingAccountColumn15: TcxGridDBColumn
              Caption = #36164#36136#21333#20301
              DataBinding.FieldName = 'AptitudeCompany'
              HeaderAlignmentHorz = taCenter
            end
            object tvFlowingAccountColumn16: TcxGridDBColumn
              Caption = #39033#30446#37096
              DataBinding.FieldName = 'ProjectDepartment'
              HeaderAlignmentHorz = taCenter
            end
            object tvFlowingAccountColumn17: TcxGridDBColumn
              Caption = #26045#24037#21333#20301
              DataBinding.FieldName = 'CreateCompany'
              HeaderAlignmentHorz = taCenter
              Width = 70
            end
          end
          object lvFlowing1: TcxGridLevel
            GridView = tvFlowingAccount
          end
        end
        object cxGroupBox4: TcxGroupBox
          Left = 5
          Top = 131
          Caption = #26597#35810
          TabOrder = 1
          Visible = False
          Height = 56
          Width = 1091
          object Label13: TLabel
            Left = 24
            Top = 24
            Width = 52
            Height = 13
            Caption = #25910#27454#21333#20301':'
          end
          object Label18: TLabel
            Left = 224
            Top = 24
            Width = 52
            Height = 13
            Caption = #20184#27454#21333#20301':'
          end
          object Label29: TLabel
            Left = 961
            Top = 24
            Width = 52
            Height = 13
            Caption = #36215#22987#26085#26399':'
          end
          object Label30: TLabel
            Left = 1126
            Top = 24
            Width = 52
            Height = 13
            Caption = #32467#26463#26085#26399':'
          end
          object Label31: TLabel
            Left = 416
            Top = 24
            Width = 52
            Height = 13
            Caption = #21046#34920#31867#21035':'
          end
          object Label57: TLabel
            Left = 762
            Top = 23
            Width = 52
            Height = 13
            Caption = #31867#21035#21517#31216':'
          end
          object Label58: TLabel
            Left = 561
            Top = 23
            Width = 56
            Height = 13
            Caption = #24037#31243'/'#21517#31216':'
          end
          object cxTextEdit9: TcxTextEdit
            Left = 82
            Top = 22
            TabOrder = 0
            TextHint = #22238#36710#21363#21487#26597#35810
            Width = 136
          end
          object cxTextEdit12: TcxTextEdit
            Left = 282
            Top = 22
            TabOrder = 1
            TextHint = #22238#36710#21363#21487#26597#35810
            Width = 129
          end
          object cxButton7: TcxButton
            Left = 1287
            Top = 17
            Width = 66
            Height = 25
            Caption = #26597#35810
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000020000000E0B14308329448DFB1D2F58A5000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000020000000E0D1937883C6DB2FF5BB1F9FF325196F4000000000000
              00000000000100000004000000090000000D0000000F0000000F0000000C0000
              00070000000E0F1D3C864A7CBCFF73C4FFFF467CC3FF17254485000000000000
              0002000000081C130F465A3B31BC7C5043F87F5244FF7B4E42FA57382FC11E14
              1059112142875686C2FF88D0FFFF5186C7FF142343880000000F000000010302
              02104A332C91946B5DFDC6ACA1FFE4D1C6FFEDDDD2FFE2D0C5FFC0A599FF855C
              50FF6E6B7EFF98D4F8FF5B8ECBFF152545840000000D00000002000000076046
              3DA6B39288FFE9DAD0FFDAC0A1FFCBA87AFFC49B66FFCCAA7EFFDCC2A5FFE5D2
              C6FF9A766AFF736A77FF162747850000000E00000002000000002A201D4AAE88
              7CFFEFE6DFFFCDA67CFFCDA26BFFE3C28CFFEDD5A2FFE7CD9EFFD3B182FFD0AE
              88FFE7D5CAFF885F53FF25181464000000070000000000000000755B53ACDFCE
              C9FFDDC1A8FFC99865FFE8BE83FFE9C388FFEDCA97FFEFD3A7FFF2D9B0FFD5B1
              87FFDBBEA6FFC5ACA2FF5A3D33C10000000C0000000000000000A9877CE9F8F4
              F2FFC79873FFDEAB77FFEFCDABFFF0D0B1FFEDC9A1FFECC69AFFEFCFA9FFE9C9
              A4FFC89B77FFE6D6CEFF7C5448F10000000F0000000000000000C09C90FFFDFD
              FCFFBE875FFFEDCFB9FFF5DFD2FFF2D6C1FFF1CFB4FFEDC6A4FFECC19BFFEFC8
              A6FFC08B67FFF1E6DFFF8B6154FF0000000F0000000000000000AF9186E6F9F5
              F4FFC69474FFE8CDC3FFF9E8E4FFF6DED2FFF3D4C2FFF0CBB2FFEBB78EFFE5B7
              92FFC59172FFEBDFD9FF866055EE0000000D0000000000000000876F68B0E7D9
              D4FFE2C6B7FFC89072FFFAEFF2FFF9E7E4FFF6DDD3FFF1C8B2FFEBAF88FFC98E
              6CFFDCBBAAFFD3C0B7FF6B4F46BC00000009000000000000000026201E36CCAF
              A7FAFBF8F7FFCF9F88FFC78E72FFE9CDC6FFEDC7B5FFDD9F79FFC88865FFCE9D
              84FFF5EFEBFFB39387FF2A201D52000000040000000000000000000000036454
              4F84D9C2BAFFFDFBFAFFE2C6B8FFCB977EFFC08163FFCB977DFFE0C4B4FFFAF6
              F5FFC9B0A7FF6B564EA700000009000000010000000000000000000000000202
              020762534D81CEB2A9FAEADDD8FFF9F5F4FFFFFFFFFFF9F5F4FFE9DCD7FFC8AC
              A2FC62504B900404031000000002000000000000000000000000000000000000
              000000000003241F1D3486726BADB69B91E6CCADA1FFB99C92E988736CB22822
              1F3E000000060000000100000000000000000000000000000000}
            TabOrder = 5
          end
          object cxDateEdit7: TcxDateEdit
            Left = 1019
            Top = 21
            TabOrder = 3
            Width = 94
          end
          object cxDateEdit8: TcxDateEdit
            Left = 1184
            Top = 22
            TabOrder = 4
            Width = 97
          end
          object cxComboBox8: TcxComboBox
            Left = 474
            Top = 22
            TabOrder = 2
            Width = 79
          end
          object cxTextEdit20: TcxTextEdit
            Left = 820
            Top = 21
            TabOrder = 6
            TextHint = #22238#36710#21363#21487#26597#35810
            Width = 129
          end
          object cxTextEdit21: TcxTextEdit
            Left = 619
            Top = 21
            TabOrder = 7
            TextHint = #22238#36710#21363#21487#26597#35810
            Width = 129
          end
        end
        object RzToolbar10: TRzToolbar
          Left = 0
          Top = 0
          Width = 963
          Height = 29
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsGroove
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 2
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer77
            RzToolButton58
            RzSpacer78
            RzToolButton59
            RzSpacer79
            RzToolButton60
            RzSpacer80
            RzToolButton61
            RzSpacer87
            RzToolButton62
            RzSpacer86
            RzToolButton65
            RzSpacer81
            cxLabel46
            RzSpacer82
            cxDateEdit33
            RzSpacer84
            cxLabel47
            cxDateEdit34
            RzSpacer85
            RzToolButton64
            RzSpacer83
            RzToolButton63)
          object RzSpacer77: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton58: TRzToolButton
            Left = 12
            Top = 2
            Width = 65
            ImageIndex = 1
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #28155#21152
          end
          object RzSpacer78: TRzSpacer
            Left = 77
            Top = 2
          end
          object RzToolButton59: TRzToolButton
            Left = 85
            Top = 2
            Width = 67
            ImageIndex = 3
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #20445#23384
          end
          object RzSpacer79: TRzSpacer
            Left = 152
            Top = 2
          end
          object RzToolButton60: TRzToolButton
            Left = 160
            Top = 2
            Width = 65
            ImageIndex = 2
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #21024#38500
          end
          object RzSpacer80: TRzSpacer
            Left = 225
            Top = 2
          end
          object RzToolButton61: TRzToolButton
            Left = 233
            Top = 2
            Width = 70
            ImageIndex = 4
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #34920#26684#35774#32622
          end
          object RzToolButton62: TRzToolButton
            Left = 311
            Top = 2
            Width = 65
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzSpacer81: TRzSpacer
            Left = 444
            Top = 2
          end
          object RzToolButton63: TRzToolButton
            Left = 798
            Top = 2
            Width = 65
            ImageIndex = 8
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #36864#20986
          end
          object RzSpacer82: TRzSpacer
            Left = 508
            Top = 2
          end
          object RzSpacer83: TRzSpacer
            Left = 790
            Top = 2
          end
          object RzSpacer84: TRzSpacer
            Left = 606
            Top = 2
            Width = 5
          end
          object RzSpacer85: TRzSpacer
            Left = 757
            Top = 2
          end
          object RzToolButton64: TRzToolButton
            Left = 765
            Top = 2
            ImageIndex = 10
            ShowCaption = True
            UseToolbarShowCaption = False
          end
          object RzSpacer86: TRzSpacer
            Left = 376
            Top = 2
          end
          object RzToolButton65: TRzToolButton
            Left = 384
            Top = 2
            Width = 60
            ImageIndex = 19
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #32508#21512#25991#26723
          end
          object RzSpacer87: TRzSpacer
            Left = 303
            Top = 2
          end
          object cxLabel46: TcxLabel
            Left = 452
            Top = 6
            Caption = #36215#22987#26085#26399':'
            Transparent = True
          end
          object cxDateEdit33: TcxDateEdit
            Left = 516
            Top = 4
            TabOrder = 1
            Width = 90
          end
          object cxLabel47: TcxLabel
            Left = 611
            Top = 6
            Caption = #32467#26463#26085#26399':'
            Transparent = True
          end
          object cxDateEdit34: TcxDateEdit
            Left = 667
            Top = 4
            TabOrder = 3
            Width = 90
          end
        end
      end
      object TabSheet13: TRzTabSheet
        Color = clWhite
        TabVisible = False
        Caption = #22686#22870#25187#32602
        object cxGroupBox10: TcxGroupBox
          Left = 5
          Top = 159
          Caption = #26597#35810
          TabOrder = 0
          Visible = False
          Height = 57
          Width = 806
          object Label66: TLabel
            Left = 16
            Top = 22
            Width = 52
            Height = 13
            Caption = #24448#26469#21333#20301':'
          end
          object Label67: TLabel
            Left = 972
            Top = 22
            Width = 52
            Height = 13
            Caption = #36215#22987#26085#26399':'
          end
          object Label68: TLabel
            Left = 1131
            Top = 22
            Width = 52
            Height = 13
            Caption = #32467#26463#26085#26399':'
          end
          object Label69: TLabel
            Left = 215
            Top = 22
            Width = 52
            Height = 13
            Caption = #21333#20301#31867#21035':'
          end
          object Label70: TLabel
            Left = 421
            Top = 22
            Width = 64
            Height = 13
            Caption = #37096#38376'/'#24037#31181#65306
          end
          object Label71: TLabel
            Left = 655
            Top = 22
            Width = 52
            Height = 13
            Caption = #22870#21169#31867#22411':'
          end
          object Label78: TLabel
            Left = 815
            Top = 22
            Width = 52
            Height = 13
            Caption = #25187#32602#31867#22411':'
          end
          object cxTextEdit26: TcxTextEdit
            Left = 74
            Top = 20
            TabOrder = 0
            TextHint = #22238#36710#21363#21487#26597#35810
            Width = 129
          end
          object cxButton29: TcxButton
            Left = 1291
            Top = 20
            Width = 60
            Height = 25
            Caption = #26597#35810
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000020000000E0B14308329448DFB1D2F58A5000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000020000000E0D1937883C6DB2FF5BB1F9FF325196F4000000000000
              00000000000100000004000000090000000D0000000F0000000F0000000C0000
              00070000000E0F1D3C864A7CBCFF73C4FFFF467CC3FF17254485000000000000
              0002000000081C130F465A3B31BC7C5043F87F5244FF7B4E42FA57382FC11E14
              1059112142875686C2FF88D0FFFF5186C7FF142343880000000F000000010302
              02104A332C91946B5DFDC6ACA1FFE4D1C6FFEDDDD2FFE2D0C5FFC0A599FF855C
              50FF6E6B7EFF98D4F8FF5B8ECBFF152545840000000D00000002000000076046
              3DA6B39288FFE9DAD0FFDAC0A1FFCBA87AFFC49B66FFCCAA7EFFDCC2A5FFE5D2
              C6FF9A766AFF736A77FF162747850000000E00000002000000002A201D4AAE88
              7CFFEFE6DFFFCDA67CFFCDA26BFFE3C28CFFEDD5A2FFE7CD9EFFD3B182FFD0AE
              88FFE7D5CAFF885F53FF25181464000000070000000000000000755B53ACDFCE
              C9FFDDC1A8FFC99865FFE8BE83FFE9C388FFEDCA97FFEFD3A7FFF2D9B0FFD5B1
              87FFDBBEA6FFC5ACA2FF5A3D33C10000000C0000000000000000A9877CE9F8F4
              F2FFC79873FFDEAB77FFEFCDABFFF0D0B1FFEDC9A1FFECC69AFFEFCFA9FFE9C9
              A4FFC89B77FFE6D6CEFF7C5448F10000000F0000000000000000C09C90FFFDFD
              FCFFBE875FFFEDCFB9FFF5DFD2FFF2D6C1FFF1CFB4FFEDC6A4FFECC19BFFEFC8
              A6FFC08B67FFF1E6DFFF8B6154FF0000000F0000000000000000AF9186E6F9F5
              F4FFC69474FFE8CDC3FFF9E8E4FFF6DED2FFF3D4C2FFF0CBB2FFEBB78EFFE5B7
              92FFC59172FFEBDFD9FF866055EE0000000D0000000000000000876F68B0E7D9
              D4FFE2C6B7FFC89072FFFAEFF2FFF9E7E4FFF6DDD3FFF1C8B2FFEBAF88FFC98E
              6CFFDCBBAAFFD3C0B7FF6B4F46BC00000009000000000000000026201E36CCAF
              A7FAFBF8F7FFCF9F88FFC78E72FFE9CDC6FFEDC7B5FFDD9F79FFC88865FFCE9D
              84FFF5EFEBFFB39387FF2A201D52000000040000000000000000000000036454
              4F84D9C2BAFFFDFBFAFFE2C6B8FFCB977EFFC08163FFCB977DFFE0C4B4FFFAF6
              F5FFC9B0A7FF6B564EA700000009000000010000000000000000000000000202
              020762534D81CEB2A9FAEADDD8FFF9F5F4FFFFFFFFFFF9F5F4FFE9DCD7FFC8AC
              A2FC62504B900404031000000002000000000000000000000000000000000000
              000000000003241F1D3486726BADB69B91E6CCADA1FFB99C92E988736CB22822
              1F3E000000060000000100000000000000000000000000000000}
            TabOrder = 4
          end
          object cxDateEdit19: TcxDateEdit
            Left = 1030
            Top = 20
            TabOrder = 2
            Width = 95
          end
          object cxDateEdit20: TcxDateEdit
            Left = 1189
            Top = 20
            TabOrder = 3
            Width = 96
          end
          object cxTextEdit27: TcxTextEdit
            Left = 273
            Top = 20
            Style.BorderStyle = ebs3D
            TabOrder = 1
            Width = 142
          end
          object cxTextEdit28: TcxTextEdit
            Left = 491
            Top = 20
            Style.BorderStyle = ebs3D
            TabOrder = 5
            Width = 158
          end
          object cxTextEdit29: TcxTextEdit
            Left = 713
            Top = 20
            Style.BorderStyle = ebs3D
            TabOrder = 6
            Width = 96
          end
          object cxTextEdit35: TcxTextEdit
            Left = 873
            Top = 20
            Style.BorderStyle = ebs3D
            TabOrder = 7
            Width = 93
          end
        end
        object SanctionGrid: TcxGrid
          Left = 0
          Top = 29
          Width = 963
          Height = 457
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          LookAndFeel.SkinName = 'Office2016Colorful'
          object tvSanctionView: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvSanctionView12
              end
              item
                Kind = skCount
                Column = tvSanctionView13
              end
              item
                Kind = skCount
                Column = tvSanctionView1
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #21333#20987#31579#36873#26597#35810
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
            OptionsView.DataRowHeight = 32
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 25
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 15
            object tvSanctionView1: TcxGridDBColumn
              Caption = #24207#21495
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Focusing = False
              Options.Sorting = False
              Width = 40
            end
            object tvSanctionViewColumn3: TcxGridDBColumn
              Caption = #29366#24577
              DataBinding.FieldName = 'status'
              PropertiesClassName = 'TcxCheckBoxProperties'
              HeaderAlignmentHorz = taCenter
            end
            object tvSanctionView2: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'Numbers'
              HeaderAlignmentHorz = taCenter
              Width = 130
            end
            object tvSanctionView3: TcxGridDBColumn
              Caption = #26085#26399
              DataBinding.FieldName = 'InputDate'
              PropertiesClassName = 'TcxDateEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 120
            end
            object tvSanctionView4: TcxGridDBColumn
              Caption = #24037#31243#21517#31216
              DataBinding.FieldName = 'ProjectName'
              HeaderAlignmentHorz = taCenter
              Width = 163
            end
            object tvSanctionView5: TcxGridDBColumn
              Caption = #36164#36136#21333#20301
              DataBinding.FieldName = 'AptitudeCompany'
              Width = 160
            end
            object tvSanctionView6: TcxGridDBColumn
              Caption = #39033#30446#37096
              DataBinding.FieldName = 'ProjectDepartment'
              Width = 160
            end
            object tvSanctionView7: TcxGridDBColumn
              Caption = #24448#26469#21333#20301
              DataBinding.FieldName = 'SignName'
              HeaderAlignmentHorz = taCenter
              Width = 160
            end
            object tvSanctionView8: TcxGridDBColumn
              Caption = #37096#38376'/'#24037#31181
              DataBinding.FieldName = 'WorkType'
              HeaderAlignmentHorz = taCenter
              Width = 79
            end
            object tvSanctionView9: TcxGridDBColumn
              Caption = #22870#21169#65295#25187#32602
              DataBinding.FieldName = 'PrizeOrPenalty'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 70
            end
            object tvSanctionViewColumn1: TcxGridDBColumn
              Caption = #22791#27880'1'
              DataBinding.FieldName = 'Prize'
              Width = 70
            end
            object tvSanctionView10: TcxGridDBColumn
              Caption = #22791#27880'2'
              DataBinding.FieldName = 'Penalty'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 70
            end
            object tvSanctionView11: TcxGridDBColumn
              Caption = #20107#20214#20869#23481
              DataBinding.FieldName = 'EventConent'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 79
            end
            object tvSanctionView12: TcxGridDBColumn
              Caption = #37329#39069
              DataBinding.FieldName = 'ToatlMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taCenter
              Styles.Content = DM.SumMoney
              Width = 141
            end
            object tvSanctionView13: TcxGridDBColumn
              Caption = #22823#20889#37329#39069
              DataBinding.FieldName = 'LargeMoney'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 313
            end
            object tvSanctionView14: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'Remarks'
              PropertiesClassName = 'TcxMemoProperties'
              HeaderAlignmentHorz = taCenter
              Width = 320
            end
            object tvSanctionViewColumn2: TcxGridDBColumn
              Caption = #26045#24037#21333#20301
              DataBinding.FieldName = 'CreateCompany'
              Width = 70
            end
          end
          object Lv1: TcxGridLevel
            GridView = tvSanctionView
          end
        end
        object RzToolbar3: TRzToolbar
          Left = 0
          Top = 0
          Width = 963
          Height = 29
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsGroove
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 2
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer15
            RzToolButton8
            RzSpacer14
            RzToolButton9
            RzSpacer16
            RzToolButton10
            RzSpacer17
            RzToolButton11
            RzSpacer18
            RzToolButton12
            RzSpacer13
            cxLabel17
            cxDateEdit15
            RzSpacer31
            cxLabel37
            cxDateEdit16
            RzSpacer32
            RzToolButton24
            RzSpacer19
            btnClose)
          object RzSpacer13: TRzSpacer
            Left = 376
            Top = 2
          end
          object RzToolButton8: TRzToolButton
            Left = 12
            Top = 2
            Width = 65
            ImageIndex = 1
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #28155#21152
          end
          object RzSpacer14: TRzSpacer
            Left = 77
            Top = 2
          end
          object RzToolButton9: TRzToolButton
            Left = 85
            Top = 2
            Width = 67
            ImageIndex = 3
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #20445#23384
          end
          object RzSpacer15: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton10: TRzToolButton
            Left = 160
            Top = 2
            Width = 65
            ImageIndex = 2
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #21024#38500
          end
          object RzSpacer16: TRzSpacer
            Left = 152
            Top = 2
          end
          object RzToolButton11: TRzToolButton
            Left = 233
            Top = 2
            Width = 70
            ImageIndex = 4
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #34920#26684#35774#32622
          end
          object RzToolButton12: TRzToolButton
            Left = 311
            Top = 2
            Width = 65
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzSpacer17: TRzSpacer
            Left = 225
            Top = 2
          end
          object btnClose: TRzToolButton
            Left = 722
            Top = 2
            Width = 65
            ImageIndex = 8
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #36864#20986
          end
          object RzSpacer18: TRzSpacer
            Left = 303
            Top = 2
          end
          object RzSpacer19: TRzSpacer
            Left = 714
            Top = 2
          end
          object RzSpacer31: TRzSpacer
            Left = 530
            Top = 2
            Width = 5
          end
          object RzSpacer32: TRzSpacer
            Left = 681
            Top = 2
          end
          object RzToolButton24: TRzToolButton
            Left = 689
            Top = 2
            ImageIndex = 10
            ShowCaption = True
            UseToolbarShowCaption = False
          end
          object cxLabel17: TcxLabel
            Left = 384
            Top = 6
            Caption = #36215#22987#26085#26399':'
            Transparent = True
          end
          object cxDateEdit15: TcxDateEdit
            Left = 440
            Top = 4
            TabOrder = 1
            Width = 90
          end
          object cxLabel37: TcxLabel
            Left = 535
            Top = 6
            Caption = #32467#26463#26085#26399':'
            Transparent = True
          end
          object cxDateEdit16: TcxDateEdit
            Left = 591
            Top = 4
            TabOrder = 3
            Width = 90
          end
        end
      end
      object TabSheet14: TRzTabSheet
        Color = clWhite
        TabVisible = False
        Caption = #31199#36161#31649#29702
        object cxGroupBox11: TcxGroupBox
          Left = -5
          Top = 245
          Caption = #26597#35810
          TabOrder = 0
          Visible = False
          Height = 57
          Width = 1351
          object Label72: TLabel
            Left = 16
            Top = 22
            Width = 52
            Height = 13
            Caption = #24448#26469#21333#20301':'
          end
          object Label73: TLabel
            Left = 793
            Top = 22
            Width = 52
            Height = 13
            Caption = #36215#22987#26085#26399':'
          end
          object Label74: TLabel
            Left = 952
            Top = 22
            Width = 52
            Height = 13
            Caption = #32467#26463#26085#26399':'
          end
          object Label75: TLabel
            Left = 215
            Top = 22
            Width = 52
            Height = 13
            Caption = #21333#20301#31867#21035':'
          end
          object Label76: TLabel
            Left = 421
            Top = 22
            Width = 60
            Height = 13
            Caption = #31199#36161#21517#31216#65306
          end
          object Label77: TLabel
            Left = 609
            Top = 22
            Width = 52
            Height = 13
            Caption = #31199#36161#35268#26684':'
          end
          object cxTextEdit30: TcxTextEdit
            Left = 74
            Top = 20
            TabOrder = 0
            TextHint = #22238#36710#21363#21487#26597#35810
            Width = 129
          end
          object cxButton31: TcxButton
            Left = 1112
            Top = 20
            Width = 60
            Height = 25
            Caption = #26597#35810
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000020000000E0B14308329448DFB1D2F58A5000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000020000000E0D1937883C6DB2FF5BB1F9FF325196F4000000000000
              00000000000100000004000000090000000D0000000F0000000F0000000C0000
              00070000000E0F1D3C864A7CBCFF73C4FFFF467CC3FF17254485000000000000
              0002000000081C130F465A3B31BC7C5043F87F5244FF7B4E42FA57382FC11E14
              1059112142875686C2FF88D0FFFF5186C7FF142343880000000F000000010302
              02104A332C91946B5DFDC6ACA1FFE4D1C6FFEDDDD2FFE2D0C5FFC0A599FF855C
              50FF6E6B7EFF98D4F8FF5B8ECBFF152545840000000D00000002000000076046
              3DA6B39288FFE9DAD0FFDAC0A1FFCBA87AFFC49B66FFCCAA7EFFDCC2A5FFE5D2
              C6FF9A766AFF736A77FF162747850000000E00000002000000002A201D4AAE88
              7CFFEFE6DFFFCDA67CFFCDA26BFFE3C28CFFEDD5A2FFE7CD9EFFD3B182FFD0AE
              88FFE7D5CAFF885F53FF25181464000000070000000000000000755B53ACDFCE
              C9FFDDC1A8FFC99865FFE8BE83FFE9C388FFEDCA97FFEFD3A7FFF2D9B0FFD5B1
              87FFDBBEA6FFC5ACA2FF5A3D33C10000000C0000000000000000A9877CE9F8F4
              F2FFC79873FFDEAB77FFEFCDABFFF0D0B1FFEDC9A1FFECC69AFFEFCFA9FFE9C9
              A4FFC89B77FFE6D6CEFF7C5448F10000000F0000000000000000C09C90FFFDFD
              FCFFBE875FFFEDCFB9FFF5DFD2FFF2D6C1FFF1CFB4FFEDC6A4FFECC19BFFEFC8
              A6FFC08B67FFF1E6DFFF8B6154FF0000000F0000000000000000AF9186E6F9F5
              F4FFC69474FFE8CDC3FFF9E8E4FFF6DED2FFF3D4C2FFF0CBB2FFEBB78EFFE5B7
              92FFC59172FFEBDFD9FF866055EE0000000D0000000000000000876F68B0E7D9
              D4FFE2C6B7FFC89072FFFAEFF2FFF9E7E4FFF6DDD3FFF1C8B2FFEBAF88FFC98E
              6CFFDCBBAAFFD3C0B7FF6B4F46BC00000009000000000000000026201E36CCAF
              A7FAFBF8F7FFCF9F88FFC78E72FFE9CDC6FFEDC7B5FFDD9F79FFC88865FFCE9D
              84FFF5EFEBFFB39387FF2A201D52000000040000000000000000000000036454
              4F84D9C2BAFFFDFBFAFFE2C6B8FFCB977EFFC08163FFCB977DFFE0C4B4FFFAF6
              F5FFC9B0A7FF6B564EA700000009000000010000000000000000000000000202
              020762534D81CEB2A9FAEADDD8FFF9F5F4FFFFFFFFFFF9F5F4FFE9DCD7FFC8AC
              A2FC62504B900404031000000002000000000000000000000000000000000000
              000000000003241F1D3486726BADB69B91E6CCADA1FFB99C92E988736CB22822
              1F3E000000060000000100000000000000000000000000000000}
            TabOrder = 4
          end
          object cxDateEdit21: TcxDateEdit
            Left = 851
            Top = 20
            TabOrder = 2
            Width = 95
          end
          object cxDateEdit22: TcxDateEdit
            Left = 1010
            Top = 20
            TabOrder = 3
            Width = 96
          end
          object cxTextEdit32: TcxTextEdit
            Left = 273
            Top = 20
            Style.BorderStyle = ebs3D
            TabOrder = 1
            Width = 142
          end
          object cxTextEdit33: TcxTextEdit
            Left = 480
            Top = 20
            Style.BorderStyle = ebs3D
            TabOrder = 5
            Width = 123
          end
          object cxTextEdit34: TcxTextEdit
            Left = 667
            Top = 20
            Style.BorderStyle = ebs3D
            TabOrder = 6
            Width = 120
          end
        end
        object LeaseGrid: TcxGrid
          Left = 0
          Top = 29
          Width = 963
          Height = 457
          Align = alClient
          TabOrder = 1
          LookAndFeel.SkinName = 'Office2016Colorful'
          object tvLeaseView: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvLeaseCol15
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvLeaseCol18
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvLeaseCol19
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #21333#20987#31579#36873#26597#35810
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
            OptionsView.DataRowHeight = 32
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 25
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 15
            object tvLeaseCol1: TcxGridDBColumn
              Caption = #24207#21495
              HeaderAlignmentHorz = taCenter
              Options.Focusing = False
              Width = 40
            end
            object tvLeaseViewColumn2: TcxGridDBColumn
              Caption = #29366#24577
              DataBinding.FieldName = 'status'
              PropertiesClassName = 'TcxCheckBoxProperties'
              HeaderAlignmentHorz = taCenter
              Width = 40
            end
            object tvLeaseCol2: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'numbers'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 130
            end
            object tvLeaseCol3: TcxGridDBColumn
              Caption = #26085#26399
              DataBinding.FieldName = 'InputDate'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 120
            end
            object tvLeaseCol4: TcxGridDBColumn
              Caption = #24037#31243#21517#31216
              DataBinding.FieldName = 'ProjectName'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 160
            end
            object tvLeaseCol5: TcxGridDBColumn
              Caption = #36164#36136#21333#20301
              DataBinding.FieldName = 'AptitudeCompany'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 160
            end
            object tvLeaseCol6: TcxGridDBColumn
              Caption = #39033#30446#37096
              DataBinding.FieldName = 'ProjectDepartment'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 160
            end
            object tvLeaseCol7: TcxGridDBColumn
              Caption = #24448#26469#21333#20301
              DataBinding.FieldName = 'SignName'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 160
            end
            object tvLeaseCol8: TcxGridDBColumn
              Caption = #21046#34920#31867#22411
              DataBinding.FieldName = 'TabType'
              PropertiesClassName = 'TcxComboBoxProperties'
              HeaderAlignmentHorz = taCenter
              Width = 66
            end
            object tvLeaseCol9: TcxGridDBColumn
              Caption = #31199#36161#21517#31216
              DataBinding.FieldName = 'LeaseCaption'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 150
            end
            object tvLeaseCol10: TcxGridDBColumn
              Caption = #31199#36161#35268#26684
              DataBinding.FieldName = 'LeaseNorms'
              PropertiesClassName = 'TcxComboBoxProperties'
              Width = 91
            end
            object tvLeaseCol11: TcxGridDBColumn
              Caption = #31199#36161#25968#37327
              DataBinding.FieldName = 'LeaseNumber'
              PropertiesClassName = 'TcxSpinEditProperties'
              Properties.ValueType = vtFloat
              Width = 150
            end
            object tvLeaseCol12: TcxGridDBColumn
              Caption = #31199#36161#22825#25968
              DataBinding.FieldName = 'LeaseDayNum'
              PropertiesClassName = 'TcxSpinEditProperties'
              Properties.ValueType = vtFloat
              Width = 88
            end
            object tvLeaseCol13: TcxGridDBColumn
              Caption = #21333#20301
              DataBinding.FieldName = 'Company'
              PropertiesClassName = 'TcxComboBoxProperties'
              Width = 150
            end
            object tvLeaseCol14: TcxGridDBColumn
              Caption = #31199#36161#21333#20215
              DataBinding.FieldName = 'LeasePrice'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Width = 100
            end
            object tvLeaseCol15: TcxGridDBColumn
              Caption = #31199#36161#37329#39069
              DataBinding.FieldName = 'LeaseSumMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Width = 180
            end
            object tvLeaseCol16: TcxGridDBColumn
              Caption = #36816#36755#27425#25968
              DataBinding.FieldName = 'ConveyanceNumber'
              PropertiesClassName = 'TcxSpinEditProperties'
              Width = 100
            end
            object tvLeaseCol17: TcxGridDBColumn
              Caption = #36816#36755#21333#20215
              DataBinding.FieldName = 'ConveyancePrice'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Width = 100
            end
            object tvLeaseCol18: TcxGridDBColumn
              Caption = #36816#36755#37329#39069
              DataBinding.FieldName = 'ConveyanceSumMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Width = 180
            end
            object tvLeaseCol19: TcxGridDBColumn
              Caption = #24635#20215
              DataBinding.FieldName = 'SumMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              Styles.Content = DM.SumMoney
              Width = 150
            end
            object tvLeaseCol20: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'Remarks'
              PropertiesClassName = 'TcxMemoProperties'
              Width = 320
            end
            object tvLeaseCol21: TcxGridDBColumn
              Caption = #35828#26126
              DataBinding.FieldName = 'Explain'
            end
            object tvLeaseViewColumn1: TcxGridDBColumn
              Caption = #26045#24037#21333#20301
              DataBinding.FieldName = 'CreateCompany'
              Width = 70
            end
          end
          object LvLease: TcxGridLevel
            GridView = tvLeaseView
          end
        end
        object RzToolbar4: TRzToolbar
          Left = 0
          Top = 0
          Width = 963
          Height = 29
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsGroove
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 2
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer20
            RzToolButton14
            RzSpacer21
            RzToolButton15
            RzSpacer22
            RzToolButton16
            RzSpacer23
            RzToolButton17
            RzSpacer49
            RzToolButton18
            RzSpacer25
            cxLabel38
            cxDateEdit25
            RzSpacer26
            cxLabel39
            cxDateEdit26
            RzToolButton25
            RzSpacer33
            RzToolButton19)
          object RzSpacer20: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton14: TRzToolButton
            Left = 12
            Top = 2
            Width = 65
            ImageIndex = 1
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #28155#21152
          end
          object RzSpacer21: TRzSpacer
            Left = 77
            Top = 2
          end
          object RzToolButton15: TRzToolButton
            Left = 85
            Top = 2
            Width = 65
            ImageIndex = 3
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #20445#23384
          end
          object RzSpacer22: TRzSpacer
            Left = 150
            Top = 2
          end
          object RzToolButton16: TRzToolButton
            Left = 158
            Top = 2
            Width = 65
            ImageIndex = 2
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #21024#38500
          end
          object RzSpacer23: TRzSpacer
            Left = 223
            Top = 2
          end
          object RzToolButton17: TRzToolButton
            Left = 231
            Top = 2
            Width = 70
            ImageIndex = 4
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #34920#26684#35774#32622
          end
          object RzToolButton18: TRzToolButton
            Left = 309
            Top = 2
            Width = 65
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzToolButton19: TRzToolButton
            Left = 712
            Top = 2
            Width = 65
            ImageIndex = 8
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #36864#20986
          end
          object RzSpacer25: TRzSpacer
            Left = 374
            Top = 2
          end
          object RzSpacer26: TRzSpacer
            Left = 528
            Top = 2
            Width = 5
          end
          object RzToolButton25: TRzToolButton
            Left = 679
            Top = 2
            ImageIndex = 10
            ShowCaption = True
            UseToolbarShowCaption = False
          end
          object RzSpacer33: TRzSpacer
            Left = 704
            Top = 2
          end
          object RzSpacer49: TRzSpacer
            Left = 301
            Top = 2
          end
          object cxLabel38: TcxLabel
            Left = 382
            Top = 6
            Caption = #36215#22987#26085#26399':'
            Transparent = True
          end
          object cxDateEdit25: TcxDateEdit
            Left = 438
            Top = 4
            TabOrder = 1
            Width = 90
          end
          object cxLabel39: TcxLabel
            Left = 533
            Top = 6
            Caption = #32467#26463#26085#26399':'
            Transparent = True
          end
          object cxDateEdit26: TcxDateEdit
            Left = 589
            Top = 4
            TabOrder = 3
            Width = 90
          end
        end
      end
      object TabSheet15: TRzTabSheet
        Color = clWhite
        Caption = #24555#25463#21333#25454
      end
      object TabSheet16: TRzTabSheet
        Color = clWhite
        Caption = #21512#21516#20449#24687
        object PactInfoGrid: TcxGrid
          Left = 0
          Top = 29
          Width = 963
          Height = 457
          Align = alClient
          TabOrder = 0
          LookAndFeel.SkinName = 'Office2016Colorful'
          object tvPactView: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvPactViewCol14
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvPactViewCol12
              end
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvPactViewCol15
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #21333#20987#31579#36873#26597#35810
            FilterRow.Visible = True
            OptionsBehavior.FocusCellOnTab = True
            OptionsBehavior.GoToNextCellOnEnter = True
            OptionsData.Deleting = False
            OptionsData.DeletingConfirmation = False
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
            OptionsView.DataRowHeight = 32
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 25
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 15
            object tvPactViewCol1: TcxGridDBColumn
              Caption = #24207#21495
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Focusing = False
              Options.Sorting = False
              Width = 56
            end
            object tvPactViewCol2: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'numbers'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 110
            end
            object tvPactViewCol3: TcxGridDBColumn
              Caption = #26085#26399
              DataBinding.FieldName = 'InputDate'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol4: TcxGridDBColumn
              Caption = #24037#31243#21517#31216
              DataBinding.FieldName = 'ProjectName'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol5: TcxGridDBColumn
              Caption = #36164#36136#21333#20301
              DataBinding.FieldName = 'AptitudeCompany'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 85
            end
            object tvPactViewCol6: TcxGridDBColumn
              Caption = #39033#30446#37096
              DataBinding.FieldName = 'ProjectDepartment'
              PropertiesClassName = 'TcxTextEditProperties'
              Width = 93
            end
            object tvPactViewCol7: TcxGridDBColumn
              Caption = #24448#26469#21333#20301
              DataBinding.FieldName = 'ContactsCompany'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 130
            end
            object tvPactViewCol8: TcxGridDBColumn
              Caption = #21333#20301#31867#21035
              DataBinding.FieldName = 'CompanyType'
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol9: TcxGridDBColumn
              Caption = #24037#31181'/'#26448#26009'/'#20998#21253
              DataBinding.FieldName = 'WorkMaterialBranch'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol10: TcxGridDBColumn
              Caption = #35268#26684#21517#31216
              DataBinding.FieldName = 'NormsCaption'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol11: TcxGridDBColumn
              Caption = #25253#20215#21333#20301
              DataBinding.FieldName = 'CompanyOffer'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol12: TcxGridDBColumn
              Caption = #24037#31243#25253#20215
              DataBinding.FieldName = 'ProjectOffer'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol13: TcxGridDBColumn
              Caption = #24066#22330#25253#20215#26085#26399
              DataBinding.FieldName = 'MarketOfferDate'
              PropertiesClassName = 'TcxDateEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol14: TcxGridDBColumn
              Caption = #24066#22330#20215
              DataBinding.FieldName = 'MarketOffer'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 100
            end
            object tvPactViewCol15: TcxGridDBColumn
              Caption = #24046#39069
              DataBinding.FieldName = 'DisparityMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taCenter
              Styles.Content = DM.SumMoney
              Width = 100
            end
            object tvPactViewCol16: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'Remarks'
              PropertiesClassName = 'TcxMemoProperties'
              HeaderAlignmentHorz = taCenter
              Width = 320
            end
            object tvPactViewColumn1: TcxGridDBColumn
              Caption = #26045#24037#21333#20301
              DataBinding.FieldName = 'CreateCompany'
              Width = 70
            end
          end
          object LvPact: TcxGridLevel
            GridView = tvPactView
          end
        end
        object cxGroupBox9: TcxGroupBox
          Left = 358
          Top = 208
          Caption = #26597#35810
          TabOrder = 1
          Visible = False
          Height = 57
          Width = 500
          object Label60: TLabel
            Left = 16
            Top = 22
            Width = 52
            Height = 13
            Caption = #24448#26469#21333#20301':'
          end
          object Label61: TLabel
            Left = 852
            Top = 22
            Width = 52
            Height = 13
            Caption = #36215#22987#26085#26399':'
          end
          object Label62: TLabel
            Left = 1011
            Top = 22
            Width = 52
            Height = 13
            Caption = #32467#26463#26085#26399':'
          end
          object Label63: TLabel
            Left = 215
            Top = 22
            Width = 52
            Height = 13
            Caption = #21333#20301#31867#21035':'
          end
          object Label64: TLabel
            Left = 421
            Top = 22
            Width = 92
            Height = 13
            Caption = #24037#31181'/'#26448#26009'/'#20998#21253#65306
          end
          object Label65: TLabel
            Left = 655
            Top = 22
            Width = 52
            Height = 13
            Caption = #35268#26684#21517#31216':'
          end
          object cxTextEdit22: TcxTextEdit
            Left = 74
            Top = 20
            TabOrder = 0
            TextHint = #22238#36710#21363#21487#26597#35810
            Width = 129
          end
          object cxButton27: TcxButton
            Left = 1171
            Top = 20
            Width = 60
            Height = 25
            Caption = #26597#35810
            OptionsImage.Glyph.Data = {
              36040000424D3604000000000000360000002800000010000000100000000100
              2000000000000004000000000000000000000000000000000000000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              000000000000000000020000000E0B14308329448DFB1D2F58A5000000000000
              0000000000000000000000000000000000000000000000000000000000000000
              0000000000020000000E0D1937883C6DB2FF5BB1F9FF325196F4000000000000
              00000000000100000004000000090000000D0000000F0000000F0000000C0000
              00070000000E0F1D3C864A7CBCFF73C4FFFF467CC3FF17254485000000000000
              0002000000081C130F465A3B31BC7C5043F87F5244FF7B4E42FA57382FC11E14
              1059112142875686C2FF88D0FFFF5186C7FF142343880000000F000000010302
              02104A332C91946B5DFDC6ACA1FFE4D1C6FFEDDDD2FFE2D0C5FFC0A599FF855C
              50FF6E6B7EFF98D4F8FF5B8ECBFF152545840000000D00000002000000076046
              3DA6B39288FFE9DAD0FFDAC0A1FFCBA87AFFC49B66FFCCAA7EFFDCC2A5FFE5D2
              C6FF9A766AFF736A77FF162747850000000E00000002000000002A201D4AAE88
              7CFFEFE6DFFFCDA67CFFCDA26BFFE3C28CFFEDD5A2FFE7CD9EFFD3B182FFD0AE
              88FFE7D5CAFF885F53FF25181464000000070000000000000000755B53ACDFCE
              C9FFDDC1A8FFC99865FFE8BE83FFE9C388FFEDCA97FFEFD3A7FFF2D9B0FFD5B1
              87FFDBBEA6FFC5ACA2FF5A3D33C10000000C0000000000000000A9877CE9F8F4
              F2FFC79873FFDEAB77FFEFCDABFFF0D0B1FFEDC9A1FFECC69AFFEFCFA9FFE9C9
              A4FFC89B77FFE6D6CEFF7C5448F10000000F0000000000000000C09C90FFFDFD
              FCFFBE875FFFEDCFB9FFF5DFD2FFF2D6C1FFF1CFB4FFEDC6A4FFECC19BFFEFC8
              A6FFC08B67FFF1E6DFFF8B6154FF0000000F0000000000000000AF9186E6F9F5
              F4FFC69474FFE8CDC3FFF9E8E4FFF6DED2FFF3D4C2FFF0CBB2FFEBB78EFFE5B7
              92FFC59172FFEBDFD9FF866055EE0000000D0000000000000000876F68B0E7D9
              D4FFE2C6B7FFC89072FFFAEFF2FFF9E7E4FFF6DDD3FFF1C8B2FFEBAF88FFC98E
              6CFFDCBBAAFFD3C0B7FF6B4F46BC00000009000000000000000026201E36CCAF
              A7FAFBF8F7FFCF9F88FFC78E72FFE9CDC6FFEDC7B5FFDD9F79FFC88865FFCE9D
              84FFF5EFEBFFB39387FF2A201D52000000040000000000000000000000036454
              4F84D9C2BAFFFDFBFAFFE2C6B8FFCB977EFFC08163FFCB977DFFE0C4B4FFFAF6
              F5FFC9B0A7FF6B564EA700000009000000010000000000000000000000000202
              020762534D81CEB2A9FAEADDD8FFF9F5F4FFFFFFFFFFF9F5F4FFE9DCD7FFC8AC
              A2FC62504B900404031000000002000000000000000000000000000000000000
              000000000003241F1D3486726BADB69B91E6CCADA1FFB99C92E988736CB22822
              1F3E000000060000000100000000000000000000000000000000}
            TabOrder = 4
          end
          object cxDateEdit17: TcxDateEdit
            Left = 910
            Top = 20
            TabOrder = 2
            Width = 95
          end
          object cxDateEdit18: TcxDateEdit
            Left = 1069
            Top = 20
            TabOrder = 3
            Width = 96
          end
          object cxTextEdit23: TcxTextEdit
            Left = 273
            Top = 20
            Style.BorderStyle = ebs3D
            TabOrder = 1
            Width = 142
          end
          object cxTextEdit25: TcxTextEdit
            Left = 507
            Top = 20
            Style.BorderStyle = ebs3D
            TabOrder = 5
            Width = 142
          end
          object cxTextEdit24: TcxTextEdit
            Left = 713
            Top = 20
            Style.BorderStyle = ebs3D
            TabOrder = 6
            Width = 120
          end
        end
        object RzToolbar6: TRzToolbar
          Left = 0
          Top = 0
          Width = 963
          Height = 29
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsGroove
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 2
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer24
            RzToolButton13
            RzSpacer34
            RzToolButton26
            RzSpacer35
            RzToolButton27
            RzSpacer36
            RzToolButton28
            RzSpacer47
            RzToolButton29
            RzSpacer37
            cxLabel40
            cxDateEdit27
            RzSpacer38
            cxLabel41
            cxDateEdit28
            RzSpacer39
            RzToolButton31
            RzSpacer48
            RzToolButton30)
          object RzSpacer24: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton13: TRzToolButton
            Left = 12
            Top = 2
            Width = 65
            ImageIndex = 1
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #28155#21152
          end
          object RzSpacer34: TRzSpacer
            Left = 77
            Top = 2
          end
          object RzToolButton26: TRzToolButton
            Left = 85
            Top = 2
            Width = 65
            ImageIndex = 3
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #20445#23384
          end
          object RzSpacer35: TRzSpacer
            Left = 150
            Top = 2
          end
          object RzToolButton27: TRzToolButton
            Left = 158
            Top = 2
            Width = 65
            ImageIndex = 2
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #21024#38500
          end
          object RzSpacer36: TRzSpacer
            Left = 223
            Top = 2
          end
          object RzToolButton28: TRzToolButton
            Left = 231
            Top = 2
            Width = 70
            ImageIndex = 4
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #34920#26684#35774#32622
          end
          object RzToolButton29: TRzToolButton
            Left = 309
            Top = 2
            Width = 65
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzToolButton30: TRzToolButton
            Left = 720
            Top = 2
            Width = 65
            ImageIndex = 8
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #36864#20986
          end
          object RzSpacer37: TRzSpacer
            Left = 374
            Top = 2
          end
          object RzSpacer38: TRzSpacer
            Left = 528
            Top = 2
            Width = 5
          end
          object RzToolButton31: TRzToolButton
            Left = 687
            Top = 2
            ImageIndex = 10
            ShowCaption = True
            UseToolbarShowCaption = False
          end
          object RzSpacer39: TRzSpacer
            Left = 679
            Top = 2
          end
          object RzSpacer47: TRzSpacer
            Left = 301
            Top = 2
          end
          object RzSpacer48: TRzSpacer
            Left = 712
            Top = 2
          end
          object cxLabel40: TcxLabel
            Left = 382
            Top = 6
            Caption = #36215#22987#26085#26399':'
            Transparent = True
          end
          object cxDateEdit27: TcxDateEdit
            Left = 438
            Top = 4
            TabOrder = 1
            Width = 90
          end
          object cxLabel41: TcxLabel
            Left = 533
            Top = 6
            Caption = #32467#26463#26085#26399':'
            Transparent = True
          end
          object cxDateEdit28: TcxDateEdit
            Left = 589
            Top = 4
            TabOrder = 3
            Width = 90
          end
        end
      end
      object TabSheet11: TRzTabSheet
        Color = clWhite
        Caption = #24037#31243#27010#20917
        object cxGroupBox6: TcxGroupBox
          Left = 21
          Top = 35
          Caption = #22522#26412#20449#24687
          TabOrder = 0
          Height = 186
          Width = 652
          object cxLabel1: TcxLabel
            Left = 32
            Top = 32
            AutoSize = False
            Caption = #24037#31243#21517#31216
            Height = 17
            Width = 52
          end
          object cxLabel2: TcxLabel
            Left = 32
            Top = 61
            Caption = #32467#26500#31867#22411
          end
          object cxLabel3: TcxLabel
            Left = 330
            Top = 32
            Caption = #24037#31243#32534#21495
          end
          object cxLabel4: TcxLabel
            Left = 330
            Top = 61
            Caption = #24037#31243#22320#22336
          end
          object cxLabel5: TcxLabel
            Left = 32
            Top = 148
            Caption = #24320#24037#26085#26399
          end
          object cxLabel6: TcxLabel
            Left = 330
            Top = 148
            Caption = #31459#24037#26085#26399
          end
          object cxLabel7: TcxLabel
            Left = 32
            Top = 90
            Caption = #24314#31569#38754#31215
          end
          object cxLabel8: TcxLabel
            Left = 32
            Top = 119
            Caption = #24314#31569#20445#38505
          end
          object cxLabel10: TcxLabel
            Left = 330
            Top = 90
            Caption = #24037#31243#36896#20215
          end
          object cxDBTextEdit1: TcxDBTextEdit
            Left = 92
            Top = 32
            DataBinding.DataField = 'ProjectName'
            TabOrder = 0
            Width = 225
          end
          object cxDBTextEdit2: TcxDBTextEdit
            Left = 92
            Top = 61
            DataBinding.DataField = 'structureType'
            TabOrder = 3
            Width = 225
          end
          object cxDBTextEdit3: TcxDBTextEdit
            Left = 92
            Top = 88
            DataBinding.DataField = 'buildacreage'
            TabOrder = 5
            Width = 225
          end
          object cxDBTextEdit4: TcxDBTextEdit
            Left = 92
            Top = 119
            DataBinding.DataField = 'builsafe'
            TabOrder = 7
            Width = 225
          end
          object cxDBTextEdit5: TcxDBTextEdit
            Left = 396
            Top = 34
            DataBinding.DataField = 'ProjectNumber'
            TabOrder = 1
            Width = 230
          end
          object cxDBTextEdit6: TcxDBTextEdit
            Left = 396
            Top = 61
            DataBinding.DataField = 'ProjectAddress'
            TabOrder = 4
            Width = 230
          end
          object cxDBTextEdit7: TcxDBTextEdit
            Left = 396
            Top = 119
            DataBinding.DataField = 'LargeMoney'
            TabOrder = 18
            Width = 230
          end
          object cxLabel9: TcxLabel
            Left = 330
            Top = 119
            Caption = #22823#20889#37329#39069
          end
          object cxButton22: TcxButton
            Left = 612
            Top = 227
            Width = 75
            Height = 26
            Caption = #20445#38505#38468#20214
            TabOrder = 20
            Visible = False
          end
          object cxButton20: TcxButton
            Left = 693
            Top = 227
            Width = 67
            Height = 26
            Caption = #22270#32440
            TabOrder = 21
            Visible = False
          end
          object cxButton25: TcxButton
            Left = 766
            Top = 227
            Width = 67
            Height = 26
            Caption = #22806#32463#35777
            TabOrder = 22
            Visible = False
          end
          object cxDBCurrencyEdit1: TcxDBCurrencyEdit
            Left = 396
            Top = 88
            AutoSize = False
            DataBinding.DataField = 'ProjectCost'
            Properties.Alignment.Horz = taLeftJustify
            TabOrder = 6
            Height = 21
            Width = 230
          end
          object cxDBDateEdit1: TcxDBDateEdit
            Left = 92
            Top = 148
            DataBinding.DataField = 'StartDate'
            TabOrder = 8
            Width = 121
          end
          object cxDBDateEdit2: TcxDBDateEdit
            Left = 396
            Top = 148
            DataBinding.DataField = 'EndDate'
            TabOrder = 9
            Width = 121
          end
        end
        object cxGroupBox7: TcxGroupBox
          Left = 21
          Top = 227
          Caption = #21442#24314#21333#20301
          TabOrder = 1
          Height = 134
          Width = 652
          object cxLabel12: TcxLabel
            Left = 32
            Top = 74
            Caption = #35774#35745#21333#20301
          end
          object cxLabel13: TcxLabel
            Left = 32
            Top = 45
            Caption = #30417#29702#21333#20301
          end
          object cxLabel14: TcxLabel
            Left = 32
            Top = 16
            Caption = #24314#35774#21333#20301
          end
          object cxLabel11: TcxLabel
            Left = 336
            Top = 16
            Caption = #26045#24037#21333#20301
          end
          object cxLabel15: TcxLabel
            Left = 336
            Top = 45
            Caption = #21208#23519#21333#20301
          end
          object cxLabel16: TcxLabel
            Left = 336
            Top = 74
            Caption = #30417#31649#21333#20301
          end
          object cxButton17: TcxButton
            Left = 632
            Top = 36
            Width = 75
            Height = 26
            Caption = #25307#26631#25991#20214
            TabOrder = 6
            Visible = False
          end
          object cxButton18: TcxButton
            Left = 632
            Top = 68
            Width = 75
            Height = 26
            Caption = #25237#26631#20070
            TabOrder = 7
            Visible = False
          end
          object cxButton19: TcxButton
            Left = 632
            Top = 100
            Width = 75
            Height = 26
            Caption = #39044#31639#20070
            TabOrder = 8
            Visible = False
          end
          object cxDBTextEdit9: TcxDBTextEdit
            Left = 401
            Top = 16
            DataBinding.DataField = 'CreateCompany'
            TabOrder = 10
            Width = 225
          end
          object cxDBTextEdit10: TcxDBTextEdit
            Left = 92
            Top = 45
            DataBinding.DataField = 'OverseerCompany'
            TabOrder = 11
            Width = 225
          end
          object cxDBTextEdit8: TcxDBTextEdit
            Left = 92
            Top = 18
            DataBinding.DataField = 'ConstructCompany'
            TabOrder = 9
            Width = 225
          end
          object cxDBTextEdit11: TcxDBTextEdit
            Left = 92
            Top = 74
            DataBinding.DataField = 'DesignCompany'
            TabOrder = 13
            Width = 225
          end
          object cxDBTextEdit12: TcxDBTextEdit
            Left = 401
            Top = 45
            DataBinding.DataField = 'surveyCompany'
            TabOrder = 12
            Width = 225
          end
          object cxDBTextEdit13: TcxDBTextEdit
            Left = 401
            Top = 74
            DataBinding.DataField = 'SuperviseCompany'
            TabOrder = 14
            Width = 225
          end
          object cxLabel54: TcxLabel
            Left = 32
            Top = 100
            Caption = #36164#36136#21333#20301
          end
          object cxDBTextEdit26: TcxDBTextEdit
            Left = 92
            Top = 101
            DataBinding.DataField = 'AptitudeCompany'
            TabOrder = 16
            Width = 225
          end
        end
        object cxGroupBox8: TcxGroupBox
          Left = 21
          Top = 388
          Caption = #39033#30446#37096
          TabOrder = 2
          Height = 250
          Width = 652
          object cxLabel20: TcxLabel
            Left = 16
            Top = 25
            Caption = #26045#24037#36127#36131#20154
          end
          object cxLabel21: TcxLabel
            Left = 38
            Top = 54
            Caption = #30005#12288#35805
          end
          object cxLabel22: TcxLabel
            Left = 40
            Top = 83
            Caption = #36136#26816#21592
          end
          object cxLabel23: TcxLabel
            Left = 348
            Top = 83
            Caption = #36164#26009#21592
          end
          object cxLabel24: TcxLabel
            Left = 348
            Top = 54
            Caption = #30005#12288#35805
          end
          object cxLabel25: TcxLabel
            Left = 324
            Top = 25
            Caption = #30002#26041#36127#36131#20154
          end
          object cxLabel26: TcxLabel
            Left = 348
            Top = 181
            Caption = #23433#20840#21592
          end
          object cxLabel27: TcxLabel
            Left = 40
            Top = 210
            Caption = #30005#12288#35805
          end
          object cxLabel28: TcxLabel
            Left = 16
            Top = 181
            Caption = #30417#29702#36127#36131#20154
          end
          object cxLabel29: TcxLabel
            Left = 40
            Top = 112
            Caption = #39044#31639#21592
          end
          object cxLabel30: TcxLabel
            Left = 348
            Top = 112
            Caption = #26448#26009#21592
          end
          object cxLabel31: TcxLabel
            Left = 348
            Top = 210
            Caption = #26045#24037#21592
          end
          object cxDBTextEdit14: TcxDBTextEdit
            Left = 96
            Top = 24
            DataBinding.DataField = 'Create_DutyPeople'
            TabOrder = 12
            Width = 225
          end
          object cxDBTextEdit15: TcxDBTextEdit
            Left = 96
            Top = 53
            DataBinding.DataField = 'Create_DutyTel'
            TabOrder = 13
            Width = 225
          end
          object cxDBTextEdit16: TcxDBTextEdit
            Left = 96
            Top = 82
            DataBinding.DataField = 'Create_Quality'
            TabOrder = 14
            Width = 225
          end
          object cxDBTextEdit17: TcxDBTextEdit
            Left = 96
            Top = 111
            DataBinding.DataField = 'Create_Budget'
            TabOrder = 15
            Width = 225
          end
          object cxDBTextEdit18: TcxDBTextEdit
            Left = 395
            Top = 24
            DataBinding.DataField = 'First_DutyPeople'
            TabOrder = 16
            Width = 225
          end
          object cxDBTextEdit19: TcxDBTextEdit
            Left = 395
            Top = 53
            DataBinding.DataField = 'First_DutyTel'
            TabOrder = 17
            Width = 225
          end
          object cxDBTextEdit20: TcxDBTextEdit
            Left = 395
            Top = 82
            DataBinding.DataField = 'First_Documenter'
            TabOrder = 18
            Width = 225
          end
          object cxDBTextEdit21: TcxDBTextEdit
            Left = 395
            Top = 111
            DataBinding.DataField = 'First_Material'
            TabOrder = 19
            Width = 225
          end
          object cxDBTextEdit22: TcxDBTextEdit
            Left = 97
            Top = 180
            DataBinding.DataField = 'Supervisor_DutyPeople'
            TabOrder = 20
            Width = 225
          end
          object cxDBTextEdit23: TcxDBTextEdit
            Left = 97
            Top = 209
            DataBinding.DataField = 'Supervisor_DutyTel'
            TabOrder = 21
            Width = 225
          end
          object cxDBTextEdit24: TcxDBTextEdit
            Left = 395
            Top = 180
            DataBinding.DataField = 'Supervisor_SafetyOfficer'
            TabOrder = 22
            Width = 225
          end
          object cxDBTextEdit25: TcxDBTextEdit
            Left = 395
            Top = 209
            DataBinding.DataField = 'Supervisor_Builder'
            TabOrder = 23
            Width = 225
          end
          object cxLabel52: TcxLabel
            Left = 40
            Top = 137
            Caption = #21462#26679#21592
          end
          object cxLabel53: TcxLabel
            Left = 349
            Top = 137
            Caption = #35265#35777#21592
          end
          object cxDBTextEdit27: TcxDBTextEdit
            Left = 97
            Top = 138
            DataBinding.DataField = 'Sampling'
            TabOrder = 26
            Width = 225
          end
          object cxDBTextEdit28: TcxDBTextEdit
            Left = 395
            Top = 138
            DataBinding.DataField = 'witness'
            TabOrder = 27
            Width = 225
          end
        end
        object RzToolbar8: TRzToolbar
          Left = 0
          Top = 0
          Width = 963
          Height = 29
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsGroove
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 3
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer53
            RzToolButton39
            RzSpacer54
            RzToolButton43
            RzSpacer56
            RzToolButton40
            RzSpacer55
            RzToolButton42
            RzSpacer58
            RzToolButton45
            RzSpacer60
            RzToolButton46
            RzSpacer61
            RzToolButton47
            RzSpacer62
            RzToolButton48
            RzSpacer63
            RzToolButton41
            RzSpacer59
            RzToolButton44)
          object RzSpacer53: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton39: TRzToolButton
            Left = 12
            Top = 2
            Width = 63
            ImageIndex = 17
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #30830#23450
          end
          object RzSpacer54: TRzSpacer
            Left = 75
            Top = 2
          end
          object RzToolButton43: TRzToolButton
            Left = 83
            Top = 2
            Width = 70
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzToolButton44: TRzToolButton
            Left = 577
            Top = 2
            Width = 36
            ImageIndex = 8
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #36864#20986
          end
          object RzSpacer59: TRzSpacer
            Left = 569
            Top = 2
          end
          object RzSpacer56: TRzSpacer
            Left = 153
            Top = 2
          end
          object RzToolButton40: TRzToolButton
            Left = 161
            Top = 2
            Width = 60
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #20445#38505#38468#20214
          end
          object RzSpacer55: TRzSpacer
            Left = 221
            Top = 2
          end
          object RzToolButton42: TRzToolButton
            Left = 229
            Top = 2
            Width = 36
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #22270#32440
          end
          object RzSpacer58: TRzSpacer
            Left = 265
            Top = 2
          end
          object RzToolButton45: TRzToolButton
            Left = 273
            Top = 2
            Width = 48
            DisabledIndex = 21
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #22806#32463#35777
          end
          object RzSpacer60: TRzSpacer
            Left = 321
            Top = 2
          end
          object RzToolButton46: TRzToolButton
            Left = 329
            Top = 2
            Width = 60
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #25307#26631#25991#20214
          end
          object RzSpacer61: TRzSpacer
            Left = 389
            Top = 2
          end
          object RzToolButton47: TRzToolButton
            Left = 397
            Top = 2
            Width = 48
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #25237#26631#20070
          end
          object RzSpacer62: TRzSpacer
            Left = 445
            Top = 2
          end
          object RzToolButton48: TRzToolButton
            Left = 453
            Top = 2
            Width = 48
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #39044#31639#20070
          end
          object RzSpacer63: TRzSpacer
            Left = 501
            Top = 2
          end
          object RzToolButton41: TRzToolButton
            Left = 509
            Top = 2
            Width = 60
            ImageIndex = 21
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #32508#21512#25991#26723
          end
        end
      end
      object TabSheet12: TRzTabSheet
        Color = clWhite
        Caption = #26045#24037#26085#35760
        object RzPanel37: TRzPanel
          Left = 0
          Top = 0
          Width = 963
          Height = 486
          Align = alClient
          BorderOuter = fsNone
          TabOrder = 0
          Transparent = True
          object RzToolbar2: TRzToolbar
            AlignWithMargins = True
            Left = 3
            Top = 3
            Width = 957
            Height = 29
            BorderInner = fsNone
            BorderOuter = fsGroove
            BorderSides = [sdTop]
            BorderWidth = 0
            TabOrder = 0
            Transparent = True
            VisualStyle = vsClassic
            ToolbarControls = (
              RzSpacer5
              RzToolButton1
              RzSpacer10
              RzToolButton6
              RzSpacer6
              RzToolButton2
              RzSpacer7
              RzToolButton3
              RzSpacer8
              RzToolButton4
              RzSpacer9
              RzToolButton7
              RzSpacer11
              RzToolButton5
              RzSpacer12
              cxLabel36
              cxLabel18
              cxComboBox11
              cxLabel19
              cxComboBox12
              cxLabel32
              cxLabel33
              cxSpinEdit2
              cxLabel34
              cxSpinEdit1)
            object RzSpacer5: TRzSpacer
              Left = 4
              Top = 2
            end
            object RzToolButton1: TRzToolButton
              Left = 12
              Top = 2
              ImageIndex = 0
            end
            object RzSpacer6: TRzSpacer
              Left = 70
              Top = 2
            end
            object RzToolButton2: TRzToolButton
              Left = 78
              Top = 2
              ImageIndex = 2
            end
            object RzSpacer7: TRzSpacer
              Left = 103
              Top = 2
            end
            object RzToolButton3: TRzToolButton
              Left = 111
              Top = 2
              ImageIndex = 4
            end
            object RzSpacer8: TRzSpacer
              Left = 136
              Top = 2
            end
            object RzToolButton4: TRzToolButton
              Left = 144
              Top = 2
              ImageIndex = 6
            end
            object RzSpacer9: TRzSpacer
              Left = 169
              Top = 2
            end
            object RzToolButton5: TRzToolButton
              Left = 210
              Top = 2
              ImageIndex = 7
            end
            object RzSpacer10: TRzSpacer
              Left = 37
              Top = 2
            end
            object RzToolButton6: TRzToolButton
              Left = 45
              Top = 2
              ImageIndex = 1
            end
            object RzSpacer11: TRzSpacer
              Left = 202
              Top = 2
            end
            object RzToolButton7: TRzToolButton
              Left = 177
              Top = 2
              Hint = #38468#20214
              ImageIndex = 8
            end
            object RzSpacer12: TRzSpacer
              Left = 235
              Top = 2
              Width = 38
            end
            object cxLabel36: TcxLabel
              Left = 273
              Top = 6
              Caption = #22825#27668
              Transparent = True
            end
            object cxLabel18: TcxLabel
              Left = 301
              Top = 6
              Caption = #19978#21320':'
              Transparent = True
            end
            object cxComboBox11: TcxComboBox
              Left = 333
              Top = 4
              TabOrder = 2
              Text = 'cxComboBox11'
              Width = 50
            end
            object cxLabel19: TcxLabel
              Left = 383
              Top = 6
              Caption = #19979#21320':'
              Transparent = True
            end
            object cxComboBox12: TcxComboBox
              Left = 415
              Top = 4
              TabOrder = 4
              Text = 'cxComboBox11'
              Width = 50
            end
            object cxLabel32: TcxLabel
              Left = 465
              Top = 6
              Caption = #28201#24230'('#176'):'
              Transparent = True
            end
            object cxLabel33: TcxLabel
              Left = 510
              Top = 6
              Caption = '8'#26102':'
              Transparent = True
            end
            object cxSpinEdit2: TcxSpinEdit
              Left = 536
              Top = 4
              TabOrder = 7
              Width = 56
            end
            object cxLabel34: TcxLabel
              Left = 592
              Top = 6
              Caption = '14'#26102':'
              Transparent = True
            end
            object cxSpinEdit1: TcxSpinEdit
              Left = 624
              Top = 4
              TabOrder = 9
              Width = 56
            end
          end
          object cxGroupBox13: TcxGroupBox
            Left = 778
            Top = 35
            Align = alRight
            Caption = #26045#24037#26085#26399
            TabOrder = 1
            Height = 451
            Width = 185
            object cxLabel35: TcxLabel
              Left = 11
              Top = 23
              Caption = #26597#35810':'
            end
            object cxTextEdit37: TcxTextEdit
              Left = 49
              Top = 22
              TabOrder = 1
              Text = 'cxTextEdit37'
              Width = 112
            end
            object tv1: TTreeView
              Left = 8
              Top = 49
              Width = 170
              Height = 432
              Indent = 19
              ReadOnly = True
              RowSelect = True
              TabOrder = 2
              Items.NodeData = {
                0302000000280000000000000000000000FFFFFFFFFFFFFFFF00000000000000
                000100000001053200300031003700745E240000000000000000000000FFFFFF
                FFFFFFFFFF00000000000000000300000001033000330008672E000000000000
                0000000000FFFFFFFFFFFFFFFF000000000000000000000000010832003200E5
                6528001F661F67094E29002E0000000000000000000000FFFFFFFFFFFFFFFF00
                0000000000000000000000010832003300E56528001F661F67DB5629002C0000
                000000000000000000FFFFFFFFFFFFFFFF000000000000000000000000010732
                00380028001F661F678C4E2900280000000000000000000000FFFFFFFFFFFFFF
                FF00000000000000000000000001053200300031003800745E}
            end
          end
          object RzPageControl3: TRzPageControl
            AlignWithMargins = True
            Left = 3
            Top = 38
            Width = 772
            Height = 445
            Hint = ''
            ActivePage = TabSheet17
            Align = alClient
            TabOverlap = -4
            TabColors.Unselected = clWhite
            TabHeight = 25
            TabIndex = 6
            TabOrder = 2
            TabStyle = tsSquareCorners
            TabWidth = 65
            Transparent = True
            FixedDimension = 25
            object TabSheet28: TRzTabSheet
              Color = clWhite
              Caption = #26412#26085#35760#20107
            end
            object TabSheet33: TRzTabSheet
              Caption = #30002#26041#36890#30693
            end
            object TabSheet25: TRzTabSheet
              Color = clWhite
              Caption = #30417#29702#36890#30693
            end
            object TabSheet32: TRzTabSheet
              Caption = #25919#24220#36890#30693
            end
            object TabSheet34: TRzTabSheet
              Caption = #20250#35758#35760#24405
            end
            object TabSheet29: TRzTabSheet
              Color = clWhite
              Caption = #24037#20316#35745#21010
            end
            object TabSheet17: TRzTabSheet
              Color = clWhite
              Caption = #32771#21220#35760#24405
              ExplicitTop = 20
            end
            object TabSheet18: TRzTabSheet
              Color = clWhite
              Caption = #26426#26800#35745#26102
            end
            object TabSheet19: TRzTabSheet
              Color = clWhite
              Caption = #20998#21253#35760#24405
            end
            object TabSheet24: TRzTabSheet
              Color = clWhite
              Caption = #36164#26009#23454#39564
            end
            object TabSheet20: TRzTabSheet
              Color = clWhite
              Caption = #36136#37327#38382#39064
            end
            object TabSheet21: TRzTabSheet
              Color = clWhite
              Caption = #36827#24230#38382#39064
            end
            object TabSheet22: TRzTabSheet
              Color = clWhite
              Caption = #23433#20840#38382#39064
            end
            object TabSheet23: TRzTabSheet
              Color = clWhite
              Caption = #25991#26126#26045#24037
            end
            object TabSheet26: TRzTabSheet
              Color = clWhite
              Caption = #21464#26356#31614#35777
            end
            object TabSheet27: TRzTabSheet
              Color = clWhite
              Caption = #38382#39064#22788#29702
            end
          end
        end
      end
      object TabSheet31: TRzTabSheet
        Color = clWhite
        Caption = #25320#27454#30003#35831#21333
        object AllocateBankrollGrid: TcxGrid
          Left = 0
          Top = 29
          Width = 963
          Height = 457
          Align = alClient
          TabOrder = 0
          object tvABView: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = #165',0.00;'#165'-,0.00'
                Kind = skSum
                Column = tvABViewColumn10
              end
              item
                Kind = skCount
                Column = tvABViewColumn9
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #21333#20987#31579#36873#26597#35810
            FilterRow.Visible = True
            OptionsSelection.InvertSelect = False
            OptionsSelection.MultiSelect = True
            OptionsSelection.CellMultiSelect = True
            OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
            OptionsView.DataRowHeight = 28
            OptionsView.Footer = True
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 30
            OptionsView.Indicator = True
            OptionsView.IndicatorWidth = 20
            object tvABViewColumn1: TcxGridDBColumn
              Caption = #24207#21495
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Focusing = False
              Width = 35
            end
            object tvABViewColumn2: TcxGridDBColumn
              Caption = #32534#21495
              DataBinding.FieldName = 'numbers'
              HeaderAlignmentHorz = taCenter
              Options.Filtering = False
              Options.Sorting = False
              Width = 108
            end
            object tvABViewColumn3: TcxGridDBColumn
              Caption = #26085#26399
              DataBinding.FieldName = 'InputDate'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 141
            end
            object tvABViewColumn4: TcxGridDBColumn
              Caption = #24037#31243#21517#31216
              DataBinding.FieldName = 'ProjectName'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 126
            end
            object tvABViewColumn5: TcxGridDBColumn
              Caption = #36164#36136#21333#20301
              DataBinding.FieldName = 'AptitudeCompany'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 145
            end
            object tvABViewColumn6: TcxGridDBColumn
              Caption = #39033#30446#37096
              DataBinding.FieldName = 'ProjectDepartment'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 153
            end
            object tvABViewColumn7: TcxGridDBColumn
              Caption = #39033#30446#37096#36127#36131#20154
              DataBinding.FieldName = 'ProjectManager'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 127
            end
            object tvABViewColumn8: TcxGridDBColumn
              Caption = #24418#35937#36827#24230
              DataBinding.FieldName = 'ImageSpeed'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 118
            end
            object tvABViewColumn10: TcxGridDBColumn
              Caption = #24212#25320#37329#39069
              DataBinding.FieldName = 'AgreeMoney'
              PropertiesClassName = 'TcxCurrencyEditProperties'
              HeaderAlignmentHorz = taCenter
              Styles.Content = DM.SumMoney
              Width = 80
            end
            object tvABViewColumn9: TcxGridDBColumn
              Caption = #37329#39069#22823#20889
              DataBinding.FieldName = 'LargeMoney'
              Width = 309
            end
            object tvABViewColumn13: TcxGridDBColumn
              Caption = #25320#27454#29366#24577
              DataBinding.FieldName = 'PayStatus'
              PropertiesClassName = 'TcxComboBoxProperties'
              HeaderAlignmentHorz = taCenter
              Width = 80
            end
            object tvABViewColumn14: TcxGridDBColumn
              Caption = #23457#26680#29366#24577
              DataBinding.FieldName = 'CheckStatus'
              PropertiesClassName = 'TcxComboBoxProperties'
              HeaderAlignmentHorz = taCenter
              Width = 80
            end
            object tvABViewColumn15: TcxGridDBColumn
              Caption = #22791#27880
              DataBinding.FieldName = 'Remarks'
              PropertiesClassName = 'TcxMemoProperties'
              HeaderAlignmentHorz = taCenter
              Width = 80
            end
            object tvABViewColumn11: TcxGridDBColumn
              Caption = #22791#27880'1'
              DataBinding.FieldName = 'Remarks1'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 80
            end
            object tvABViewColumn12: TcxGridDBColumn
              Caption = #22791#27880'2'
              DataBinding.FieldName = 'Remarks2'
              PropertiesClassName = 'TcxTextEditProperties'
              HeaderAlignmentHorz = taCenter
              Width = 80
            end
            object tvABViewColumn16: TcxGridDBColumn
              Caption = #26045#24037#21333#20301
              DataBinding.FieldName = 'CreateCompany'
              Width = 70
            end
          end
          object ABLv: TcxGridLevel
            GridView = tvABView
          end
        end
        object RzToolbar7: TRzToolbar
          Left = 0
          Top = 0
          Width = 963
          Height = 29
          Images = DM.cxImageList1
          BorderInner = fsNone
          BorderOuter = fsGroove
          BorderSides = [sdTop]
          BorderWidth = 0
          GradientColorStyle = gcsCustom
          TabOrder = 1
          VisualStyle = vsGradient
          ToolbarControls = (
            RzSpacer40
            RzToolButton32
            RzSpacer41
            RzToolButton33
            RzSpacer42
            RzToolButton34
            RzSpacer43
            RzToolButton35
            RzSpacer44
            RzToolButton36
            RzSpacer46
            cxLabel42
            cxDateEdit29
            RzSpacer45
            cxLabel43
            cxDateEdit30
            RzSpacer52
            RzToolButton38
            RzSpacer50
            RzToolButton37
            RzSpacer51)
          object RzSpacer40: TRzSpacer
            Left = 4
            Top = 2
          end
          object RzToolButton32: TRzToolButton
            Left = 12
            Top = 2
            Width = 63
            ImageIndex = 1
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #28155#21152
          end
          object RzSpacer41: TRzSpacer
            Left = 75
            Top = 2
          end
          object RzToolButton33: TRzToolButton
            Left = 83
            Top = 2
            Width = 70
            ImageIndex = 3
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #20445#23384
          end
          object RzSpacer42: TRzSpacer
            Left = 153
            Top = 2
          end
          object RzToolButton34: TRzToolButton
            Left = 161
            Top = 2
            Width = 70
            ImageIndex = 2
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            Caption = #21024#38500
          end
          object RzSpacer43: TRzSpacer
            Left = 231
            Top = 2
          end
          object RzToolButton35: TRzToolButton
            Left = 239
            Top = 2
            Width = 60
            ImageIndex = 4
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #34920#26684#35774#32622
          end
          object RzToolButton36: TRzToolButton
            Left = 307
            Top = 2
            Width = 70
            ImageIndex = 5
            ShowCaption = True
            UseToolbarButtonSize = False
            UseToolbarShowCaption = False
            ToolStyle = tsDropDown
            Caption = #25253#34920
          end
          object RzToolButton37: TRzToolButton
            Left = 723
            Top = 2
            Width = 36
            ImageIndex = 8
            ShowCaption = True
            UseToolbarShowCaption = False
            Caption = #36864#20986
          end
          object RzSpacer44: TRzSpacer
            Left = 299
            Top = 2
          end
          object RzSpacer45: TRzSpacer
            Left = 531
            Top = 2
            Width = 5
          end
          object RzToolButton38: TRzToolButton
            Left = 690
            Top = 2
            ImageIndex = 10
            ShowCaption = True
            UseToolbarShowCaption = False
          end
          object RzSpacer46: TRzSpacer
            Left = 377
            Top = 2
          end
          object RzSpacer50: TRzSpacer
            Left = 715
            Top = 2
          end
          object RzSpacer51: TRzSpacer
            Left = 759
            Top = 2
          end
          object RzSpacer52: TRzSpacer
            Left = 682
            Top = 2
          end
          object cxLabel42: TcxLabel
            Left = 385
            Top = 6
            Caption = #36215#22987#26085#26399':'
            Transparent = True
          end
          object cxDateEdit29: TcxDateEdit
            Left = 441
            Top = 4
            TabOrder = 1
            Width = 90
          end
          object cxLabel43: TcxLabel
            Left = 536
            Top = 6
            Caption = #32467#26463#26085#26399':'
            Transparent = True
          end
          object cxDateEdit30: TcxDateEdit
            Left = 592
            Top = 4
            TabOrder = 3
            Width = 90
          end
        end
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 0
    Width = 269
    Height = 521
    Align = alLeft
    Color = 16250613
    ParentBackground = False
    TabOrder = 1
    object cxSplitter1: TcxSplitter
      Left = 1
      Top = 321
      Width = 267
      Height = 8
      HotZoneClassName = 'TcxMediaPlayer8Style'
      HotZone.SizePercent = 61
      AlignSplitter = salTop
      AutoSnap = True
      ResizeUpdate = True
      Control = TreeView1
    end
    object RzPageControl1: TRzPageControl
      Left = 1
      Top = 352
      Width = 267
      Height = 133
      Hint = ''
      ActivePage = TabSheet4
      Align = alClient
      ShowShadow = False
      TabOverlap = -4
      TabHeight = 22
      TabIndex = 3
      TabOrder = 1
      TabStyle = tsSquareCorners
      FixedDimension = 22
      object TabSheet1: TRzTabSheet
        Color = 16250613
        Caption = #24037#31243#26723#26696
        object TreeView2: TTreeView
          Left = 0
          Top = 0
          Width = 265
          Height = 106
          Align = alClient
          Images = DM.il1
          Indent = 19
          TabOrder = 0
          Items.NodeData = {
            0301000000220000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000
            0004000000010263684868260000000000000000000000FFFFFFFFFFFFFFFFFF
            FFFFFF00000000000000000104E55D0B7A8269B5512600000000000000000000
            00FFFFFFFFFFFFFFFFFFFFFFFF00000000000000000104A17B0674ED73C47E28
            0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000000400000001
            05A17B06745480FB7C6888220000000000000000000000FFFFFFFFFFFFFFFFFF
            FFFFFF000000000000000001023275B965220000000000000000000000FFFFFF
            FFFFFFFFFFFFFFFFFF00000000000000000102D1760674260000000000000000
            000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000001043F659C5EE890E8
            95240000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000000000
            000103E55D3057C47E260000000000000000000000FFFFFFFFFFFFFFFFFFFFFF
            FF00000000030000000104BD65E55D08540C54220000000000000000000000FF
            FFFFFFFFFFFFFFFFFFFFFF000000000200000001023275B96524000000000000
            0000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000000000000103E55D0B7AC4
            7E240000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000000000
            0001039B4F278D4655220000000000000000000000FFFFFFFFFFFFFFFFFFFFFF
            FF00000000020000000102594EB965240000000000000000000000FFFFFFFFFF
            FFFFFFFFFFFFFF00000000000000000103E55D0B7AC47E240000000000000000
            000000FFFFFFFFFFFFFFFFFFFFFFFF000000000000000001039B4F278D465522
            0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000000200000001
            02194EB965240000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000
            00000000000103E55D0B7AC47E240000000000000000000000FFFFFFFFFFFFFF
            FFFFFFFFFF000000000000000001039B4F278D4655}
        end
      end
      object TabSheet2: TRzTabSheet
        Color = 16250613
        Caption = #24037#31243#25163#32493
        object TreeView3: TTreeView
          Left = 0
          Top = 0
          Width = 265
          Height = 106
          Align = alClient
          Images = DM.il1
          Indent = 19
          TabOrder = 0
        end
      end
      object TabSheet3: TRzTabSheet
        Color = 16250613
        Caption = #24037#31243#31649#29702
        object TreeView4: TTreeView
          Left = 0
          Top = 0
          Width = 265
          Height = 106
          Align = alClient
          Images = DM.il1
          Indent = 19
          TabOrder = 0
          Items.NodeData = {
            0301000000260000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000
            00040000000104B0733A57A17B0674260000000000000000000000FFFFFFFFFF
            FFFFFFFFFFFFFF00000000000000000104BD65E55DE565D75F2A000000000000
            0000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000000000000106BD65E55DC7
            8F0B7AB08B555F220000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00
            0000000000000001021A4FAE8B220000000000000000000000FFFFFFFFFFFFFF
            FFFFFFFFFF0000000000000000010256595A7F}
        end
      end
      object TabSheet4: TRzTabSheet
        Color = 16250613
        Caption = #24037#31243#36164#26009
        object TreeView5: TTreeView
          Left = 0
          Top = 0
          Width = 265
          Height = 106
          Align = alClient
          Ctl3D = True
          Images = DM.il1
          Indent = 19
          ParentCtl3D = False
          TabOrder = 0
          Items.NodeData = {
            03010000002C0000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000
            00070000000107E55D0B7A3652A65ECA53E55D77512200000000000000000000
            00FFFFFFFFFFFFFFFFFFFFFFFF00000000000000000102FE56C6962200000000
            00000000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000000000000102C48903
            83220000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000000000
            000102895B6851220000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00
            000000000000000102288DCF91260000000000000000000000FFFFFFFFFFFFFF
            FFFFFFFFFF0000000000000000010487650E66BD65E55D260000000000000000
            000000FFFFFFFFFFFFFFFFFFFFFFFF0000000000000000010480622F67A44E41
            6D260000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF00000000000000
            000104A17B06743652A65E}
        end
      end
    end
    object TreeView1: TTreeView
      Left = 1
      Top = 30
      Width = 267
      Height = 291
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Images = DM.il1
      Indent = 19
      ParentFont = False
      RowSelect = True
      TabOrder = 2
      Items.NodeData = {
        0301000000260000000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000
        000100000001047998EE76A17B06742A0000000000000000000000FFFFFFFFFF
        FFFFFFFFFFFFFF00000000000000000106248DB75FD1824F4F855B7C69}
    end
    object RzPanel4: TRzPanel
      Left = 1
      Top = 485
      Width = 267
      Height = 35
      Align = alBottom
      BorderOuter = fsNone
      TabOrder = 3
      object Label1: TLabel
        Left = 8
        Top = 9
        Width = 28
        Height = 13
        Caption = #21517#31216':'
      end
      object cxButton1: TcxButton
        Left = 148
        Top = 5
        Width = 38
        Height = 22
        Caption = #26032#22686
        TabOrder = 0
      end
      object cxButton2: TcxButton
        Left = 188
        Top = 5
        Width = 38
        Height = 22
        Caption = #20462#25913
        TabOrder = 1
      end
      object cxButton3: TcxButton
        Left = 228
        Top = 5
        Width = 38
        Height = 22
        Caption = #21024#38500
        TabOrder = 2
      end
      object cxTextEdit1: TcxTextEdit
        Left = 42
        Top = 6
        TabOrder = 3
        Width = 103
      end
    end
    object Panel9: TPanel
      Left = 1
      Top = 329
      Width = 267
      Height = 23
      Align = alTop
      BevelOuter = bvNone
      Caption = #26045#24037#31867#21035
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
    end
    object RzToolbar5: TRzToolbar
      Left = 1
      Top = 1
      Width = 267
      Height = 29
      AutoStyle = False
      Images = DM.cxImageList1
      BorderInner = fsNone
      BorderOuter = fsNone
      BorderSides = [sdTop]
      BorderWidth = 0
      GradientColorStyle = gcsCustom
      TabOrder = 5
      VisualStyle = vsGradient
      ToolbarControls = (
        RzSpacer27
        RzToolButton20
        RzSpacer28
        RzToolButton21
        RzSpacer29
        RzToolButton22)
      object RzSpacer27: TRzSpacer
        Left = 4
        Top = 2
      end
      object RzToolButton20: TRzToolButton
        Left = 12
        Top = 2
        Width = 70
        ImageIndex = 1
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21333#20307#24037#31243
      end
      object RzSpacer28: TRzSpacer
        Left = 82
        Top = 2
      end
      object RzToolButton21: TRzToolButton
        Left = 90
        Top = 2
        Width = 70
        ImageIndex = 0
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #32534#36753
      end
      object RzSpacer29: TRzSpacer
        Left = 160
        Top = 2
      end
      object RzToolButton22: TRzToolButton
        Left = 168
        Top = 2
        Width = 70
        ImageIndex = 2
        ShowCaption = True
        UseToolbarButtonSize = False
        UseToolbarShowCaption = False
        Caption = #21024#38500
      end
    end
  end
end
