unit ufrmProjectLease;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, RzButton, RzPanel,
  Vcl.ExtCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxCore,
  cxDateUtils, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCheckBox, cxTextEdit,
  cxDropDownEdit, cxSpinEdit, cxCurrencyEdit, cxMemo, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,ufrmBaseController,
  cxGridCustomView, cxGrid, cxMaskEdit, cxCalendar, cxLabel, Data.Win.ADODB,
  Vcl.Menus, cxDBLookupComboBox, Vcl.StdCtrls, cxDBEdit, cxButtons, cxGroupBox;

type
  TfrmProjectLease = class(TfrmBaseController)
    RzPanel2: TRzPanel;
    RzPanel1: TRzPanel;
    Splitter1: TSplitter;
    RzToolbar4: TRzToolbar;
    RzSpacer20: TRzSpacer;
    RzBut4: TRzToolButton;
    RzSpacer21: TRzSpacer;
    RzBut5: TRzToolButton;
    RzSpacer22: TRzSpacer;
    RzBut6: TRzToolButton;
    RzSpacer23: TRzSpacer;
    RzBut7: TRzToolButton;
    RzBut8: TRzToolButton;
    RzBut10: TRzToolButton;
    RzSpacer25: TRzSpacer;
    RzSpacer26: TRzSpacer;
    RzBut9: TRzToolButton;
    RzSpacer33: TRzSpacer;
    RzSpacer49: TRzSpacer;
    cxLabel38: TcxLabel;
    Date1: TcxDateEdit;
    cxLabel39: TcxLabel;
    Date2: TcxDateEdit;
    Grid: TcxGrid;
    tvLeaseView: TcxGridDBTableView;
    tvLeaseCol1: TcxGridDBColumn;
    tvLeaseViewColumn2: TcxGridDBColumn;
    tvLeaseCol2: TcxGridDBColumn;
    tvLeaseCol3: TcxGridDBColumn;
    tvLeaseCol4: TcxGridDBColumn;
    tvLeaseCol5: TcxGridDBColumn;
    tvLeaseCol6: TcxGridDBColumn;
    tvLeaseCol7: TcxGridDBColumn;
    tvLeaseCol9: TcxGridDBColumn;
    tvLeaseCol10: TcxGridDBColumn;
    tvLeaseCol11: TcxGridDBColumn;
    tvLeaseCol12: TcxGridDBColumn;
    tvLeaseCol13: TcxGridDBColumn;
    tvLeaseCol14: TcxGridDBColumn;
    tvLeaseCol15: TcxGridDBColumn;
    tvLeaseCol16: TcxGridDBColumn;
    tvLeaseCol17: TcxGridDBColumn;
    tvLeaseCol18: TcxGridDBColumn;
    tvLeaseCol19: TcxGridDBColumn;
    tvLeaseCol20: TcxGridDBColumn;
    tvLeaseCol21: TcxGridDBColumn;
    tvLeaseViewColumn1: TcxGridDBColumn;
    Lv: TcxGridLevel;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    ADOMaking: TADOQuery;
    DataMaking: TDataSource;
    ADOSignName: TADOQuery;
    DataSignName: TDataSource;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    RzPanel3: TRzPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxGroupBox1: TcxGroupBox;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxLabel1: TcxLabel;
    cxDBDateEdit1: TcxDBDateEdit;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxDBDateEdit2: TcxDBDateEdit;
    cxLabel5: TcxLabel;
    cxDBSpinEdit3: TcxDBSpinEdit;
    cxDBCurrencyEdit3: TcxDBCurrencyEdit;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    cxDBCurrencyEdit4: TcxDBCurrencyEdit;
    cxGroupBox2: TcxGroupBox;
    cxLabel6: TcxLabel;
    cxDBSpinEdit4: TcxDBSpinEdit;
    cxDBCurrencyEdit2: TcxDBCurrencyEdit;
    cxLabel8: TcxLabel;
    cxLabel7: TcxLabel;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    cxLabel4: TcxLabel;
    cxDBSpinEdit2: TcxDBSpinEdit;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    procedure RzBut10Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzBut7Click(Sender: TObject);
    procedure tvLeaseViewColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure RzBut9Click(Sender: TObject);
    procedure tvLeaseCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvLeaseViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvLeaseViewEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvLeaseViewStylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tvLeaseViewKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzBut4Click(Sender: TObject);
    procedure RzBut5Click(Sender: TObject);
    procedure ADOQuery1AfterInsert(DataSet: TDataSet);
    procedure tvLeaseViewDblClick(Sender: TObject);
    procedure tvLeaseCol8GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure tvLeaseCol9PropertiesInitPopup(Sender: TObject);
    procedure tvLeaseCol9PropertiesCloseUp(Sender: TObject);
    procedure tvLeaseCol13GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure tvLeaseCol10GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure tvLeaseViewColumn2PropertiesChange(Sender: TObject);
    procedure RzBut6Click(Sender: TObject);
    procedure tvLeaseViewColumn1GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure ADOSignNameAfterOpen(DataSet: TDataSet);
    procedure ADOQuery1AfterOpen(DataSet: TDataSet);
    procedure ADOMakingAfterOpen(DataSet: TDataSet);
    procedure tvLeaseViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems2GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure Excel1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure tvLeaseCol12PropertiesPopup(Sender: TObject);
    procedure PropertiesInitPopup(Sender: TObject);
    procedure cxDBDateEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBDateEdit2PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBDateEdit2PropertiesCloseUp(Sender: TObject);
    procedure cxDBDateEdit1PropertiesCloseUp(Sender: TObject);
    procedure cxDBSpinEdit2PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBSpinEdit3PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBSpinEdit4PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBCurrencyEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBCurrencyEdit3PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvLeaseCol12PropertiesCloseUp(Sender: TObject);
    procedure cxDBSpinEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure RzToolButton1Click(Sender: TObject);
  private
    { Private declarations }
    g_ProjectName : string;
    g_PostCode    : string;
    IsSave : Boolean;
    Prefix : string;
    Suffix : string;
    procedure  SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
    function GetleaseDayNum(lpStartDate,lpEndDate : TDate ; DayNum , Deduction : Integer ; lpLeaseUnitPrice : Currency ):Boolean;
  public
    { Public declarations }
    procedure WndProc(var Message: TMessage); override;  // 第一优先权
  end;

var
  frmProjectLease: TfrmProjectLease;


implementation

uses
   uProjectFrame,global,ufunctions,uDataModule,ufrmIsViewGrid,System.DateUtils, Math ;

{$R *.dfm}

procedure TfrmProjectLease.WndProc(var Message: TMessage);
var
  pMsg : PProject;
  s : string;
  szCodeList : string;
  szTableList : array[0..1] of TTableList;
begin
  case Message.Msg of
    WM_FrameClose:
      begin
        Self.ADOQuery1.Close;
        Self.ADOSignName.Close;
        Self.ADOMaking.Close;

      end;
    WM_FrameView :
      begin
        pMsg := PProject(Message.LParam);
        g_PostCode := pMsg.dwCode;
        g_ProjectName := pMsg.dwName;

        s := 'Select *from ' + g_Table_Project_Lease + ' Where Code ="' +  g_PostCode + '"';
        with Self.ADOQuery1 do
        begin
          Close;
          SQL.Clear;
          SQL.Text := s;
          Open;
        end;

        with Self.ADOSignName do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'Select pt.SignName AS Name,pycode from '+ g_Table_Maintain_Science +
                      ' AS PT right join '+ g_Table_CompanyManage +
                      //' AS PS on PT.SignName = PS.SignName group by pt.SignName,pycode' +
                      ' AS PS on PT.SignName = PS.SignName Where ProjectName="'+
                    g_ProjectName +
                    '" group by pt.SignName,pycode';
          Open;
        end;

        with Self.ADOMaking do
        begin
          Close;
          SQL.Clear;
          SQL.Text := 'Select * from '+ g_Table_Maintain_Science + ' Where ProjectName="' + g_ProjectName + '"';
          Open;
        end;

      end;
    WM_FrameDele :
      begin
        pMsg   := PProject(Message.LParam);
        szCodeList := pMsg.dwCodeList;
        DeleteTableFile(szTableList,szCodeList);
        DM.InDeleteData(g_Table_Project_Lease,szCodeList); //清除主目录表内关联
      end;
    WM_SetGridFocus:
      begin
        SetNewGridFocus(Self.tvLeaseView,Self.tvLeaseCol7.DataBinding.FieldName);
        keybd_event(VK_RETURN,0,0,0);
      end;  
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

procedure TfrmProjectLease.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;

procedure TfrmProjectLease.tvLeaseCol10GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  List : TStringList;
  i : Integer;
  
begin
  inherited;
  {
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    List := TStringList.Create;
    try
      GetCompany(List,6);
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    finally
      List.Free;
    end;
  end;
  }
end;

procedure TfrmProjectLease.tvLeaseCol12PropertiesCloseUp(Sender: TObject);
var
  szLeaseSumMoney : Currency;
  szConveyanceSumMoney : Currency;
  szRowIndex : Integer;
  szSumMoney : Currency;
  
begin
  inherited;
  szLeaseSumMoney      := StrToCurrDef( vartostr( Self.cxDBCurrencyEdit4.EditingValue ),0);  //租赁总额
  szConveyanceSumMoney := StrToCurrDef( vartostr( Self.cxDBCurrencyEdit2.EditingValue ),0);  //运输总额
  szSumMoney := szLeaseSumMoney + szConveyanceSumMoney;

  with Self.ADOQuery1 do
  begin
    if State <> dsInactive then
    begin
      FieldByName(Self.tvLeaseCol19.DataBinding.FieldName).Value := szSumMoney;
    end;  
  end;  
end;

procedure TfrmProjectLease.tvLeaseCol12PropertiesPopup(Sender: TObject);
begin
  inherited;
//  Self.cxDBDateEdit1.SetFocus;
  if null = Self.cxDBDateEdit1.EditingValue then
  begin
     Self.cxDBDateEdit1.Date := Date;
     Self.cxDBDateEdit1.PostEditValue;
  end;

  if null = Self.cxDBDateEdit2.EditingValue then
  begin
     Self.cxDBDateEdit2.Date := Date;
     Self.cxDBDateEdit2.PostEditValue;
  end;

  if null = Self.cxDBSpinEdit4.EditingValue then
  begin
    self.cxDBSpinEdit4.EditValue := 1;
    Self.cxDBSpinEdit4.PostEditValue;
  end;  
   
end;

procedure TfrmProjectLease.tvLeaseCol13GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  List : TStringList;
  i : Integer;
  
begin
  inherited;
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    List := TStringList.Create;
    try
      GetCompany(List,0);
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    finally
      List.Free;
    end;
  end;
end;

procedure TfrmProjectLease.tvLeaseCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmProjectLease.tvLeaseCol8GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  with (Sender.Properties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    Items.Add('入库');
    Items.Add('出库');
  end;
end;

procedure TfrmProjectLease.tvLeaseCol9PropertiesCloseUp(Sender: TObject);
var
  szColContentA : Variant;
  szColContentB : Variant;
  szColContentC : Variant;
  szColContentD : Variant;
  szColContentE : Variant;
  szColContentF : Variant;
  szColContentG : Variant;

  szRowIndex:Integer;
  szColName : string;

begin
  inherited;

  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szColContentA := DataController.Values[szRowIndex,0]; //名称
      szColContentB := DataController.Values[szRowIndex,1]; //单价
      szColContentC := DataController.Values[szRowIndex,2]; //录入日期
      szColContentD := DataController.Values[szRowIndex,3]; //品牌
      szColContentE := DataController.Values[szRowIndex,4]; //型号
      szColContentF := DataController.Values[szRowIndex,5]; //规格
      szColContentG := DataController.Values[szRowIndex,6]; //单位

      if Length(szColContentA) <> 0 then
      begin
        with Self.ADOQuery1 do
        begin
          if State <> dsInactive then
          begin
            Edit;
            szColName := Self.tvLeaseCol9.DataBinding.FieldName;
            FieldByName(szColName).Value := szColContentA; //名称
            szColName := Self.tvLeaseCol14.DataBinding.FieldName;
            FieldByName(szColName).Value := szColContentB; //单价
            szColName := Self.tvLeaseCol13.DataBinding.FieldName;
            FieldByName(szColName).Value := szColContentG; //单位
            szColName := Self.tvLeaseCol10.DataBinding.FieldName;
            FieldByName(szColName).Value := szColContentF; //租赁规格

          end;
        end;
      end;

    end;
  end;

end;

procedure TfrmProjectLease.tvLeaseCol9PropertiesInitPopup(Sender: TObject);
var
  szSignName : string;
  szRowIndex : Integer;

begin
  inherited;

  szRowIndex := Self.tvLeaseView.DataController.FocusedRowIndex;
  szSignName := VarToStr( Self.tvLeaseView.DataController.Values[szRowIndex,Self.tvLeaseCol7.Index] );

  if Length(szSignName) <> 0 then
  begin
    with Self.ADOMaking do
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'Select * from '+ g_Table_Maintain_Science +
                  ' WHERE SignName="' + szSignName +
                  '" AND  ProjectName="' + g_ProjectName + '"';
      Open;
    end;
  end else
  begin
  //  Self.tvLeaseCol7.FocusWithSelection;
    ShowMessage('没有选择的往来单位，需要指定一个往来单位才可以选择租赁名！');
  end;

end;

procedure TfrmProjectLease.tvLeaseViewColumn1GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  GetCompanyInfo(AProperties);
end;

procedure TfrmProjectLease.tvLeaseViewColumn2PropertiesChange(Sender: TObject);
var
  szRowIndex : Integer;
begin
  inherited;
  szRowIndex := Self.tvLeaseView.DataController.FocusedRecordIndex;
  Self.ADOQuery1.UpdateBatch();
  Self.ADOQuery1.Requery();
  Self.tvLeaseView.Controller.FocusedRowIndex := szRowIndex;
end;

procedure TfrmProjectLease.tvLeaseViewColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  SetcxGrid(Self.tvLeaseView,1,g_Dir_Project_Lease);
end;

procedure TfrmProjectLease.tvLeaseViewDblClick(Sender: TObject);
var
  szDir : string;
begin
  inherited;
  szDir :=  Concat(g_Resources,'\' + g_Dir_Project_Lease);
  CreateEnclosure(Self.tvLeaseView.DataController.DataSource.DataSet,Handle,szdir);
end;

procedure TfrmProjectLease.tvLeaseViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
var
  szColName : string;
begin
  inherited;
  with Self.ADOQuery1 do
  begin
    if State <> dsInactive then
      begin
        if (State <> dsEdit) and (State <> dsInsert) then
        begin
          AAllow := False;
        end else
        begin
          AAllow := True;
        end;

      end;
  end;
  {
  szColName := Self.tvLeaseViewColumn2.DataBinding.FieldName;
  if AItem.Index <> Self.tvLeaseViewColumn2.Index then
  begin

  end;
  }
  if AItem.Index = Self.tvLeaseCol1.Index then
  begin
    AAllow := False;
  end;
end;

procedure TfrmProjectLease.tvLeaseViewEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
var
  szLeaseNumber : Double;
  szLeaseDayNum : Double;
  szLeasePrice  : Currency;
  szColName : string;
  szLeaseSumMoney : Currency;
  szConveyanceNumber : Double;
  szConveyancePrice  : Double;
  szConveyanceSumMoney : Currency;

begin
  inherited;
  if Key = 13 then
  begin
    {
    if AItem.Editing then
    begin
      AItem.DataBinding.DataController.UpdateData;
      with Self.ADOQuery1 do
      begin
        if State <> dsInactive then
        begin

          if (AItem.Index = Self.tvLeaseCol11.Index) or
             (AItem.Index = Self.tvLeaseCol12.Index) or
             (AItem.Index = Self.tvLeaseCol14.Index) or
             (AItem.Index = Self.tvLeaseCol16.Index) or
             (AItem.Index = Self.tvLeaseCol17.Index) or
             (AItem.Index = Self.tvLeaseCol18.Index)
             then
          begin
              //********************************************************************
              //租赁运算
              szColName := Self.tvLeaseCol11.DataBinding.FieldName;
              szLeaseNumber := FieldByName(szColName).AsFloat;
              szColName := Self.tvLeaseCol12.DataBinding.FieldName;
              szLeaseDayNum := FieldByName(szColName).AsFloat;
              szColName := Self.tvLeaseCol14.DataBinding.FieldName;
              szLeasePrice  := FieldByName(szColName).AsCurrency;
              szLeaseSumMoney := szLeaseNumber * szLeaseDayNum * szLeasePrice;
              Edit;
              szColName := Self.tvLeaseCol15.DataBinding.FieldName;
              FieldByName(szColName).Value := szLeaseSumMoney;
              Self.tvLeaseCol15.DataBinding.DataController.UpdateData;
              //********************************************************************
              //运输运算
              szColName := Self.tvLeaseCol16.DataBinding.FieldName;
              szConveyanceNumber := FieldByName(szColName).AsFloat;
              szColName := Self.tvLeaseCol17.DataBinding.FieldName; //运输单价
              szConveyancePrice  := FieldByName(szColName).AsFloat;
              szConveyanceSumMoney := szConveyanceNumber * szConveyancePrice;
              Edit;
              szColName := Self.tvLeaseCol18.DataBinding.FieldName;
              FieldByName(szColName).Value := szConveyanceSumMoney;
              Self.tvLeaseCol18.DataBinding.DataController.UpdateData;
              //********************************************************************
              //总价运算
              szColName := Self.tvLeaseCol19.DataBinding.FieldName;
              FieldByName(szColName).Value := szConveyanceSumMoney + szLeaseSumMoney;
              Self.tvLeaseCol19.DataBinding.DataController.UpdateData;
          end else
          begin

          end;
        end;
      end;

    end;
    
    begin

      
    end;
    }
    if AItem.Index = Self.tvLeaseCol19.Index then
      begin
        if Self.tvLeaseView.Controller.FocusedRow.IsLast and IsNewRow then
        begin
        
          with Self.ADOQuery1 do
          begin
            if State <> dsInactive then
            begin
              Append;
              SendMessage(Handle,WM_SetGridFocus,0,0);
            end;
          end;
             
        end;

      end;

  end;

end;

procedure TfrmProjectLease.tvLeaseViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 46 then
  begin
    Self.RzBut6.Click;
  end;
end;

procedure TfrmProjectLease.tvLeaseViewStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvLeaseViewColumn2.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;


procedure TfrmProjectLease.tvLeaseViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems2GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  t : Currency;
  szCapital : string;

begin
  inherited;
  try
    szCapital := '';
    if AValue <> null then
    begin
      t := StrToCurr( VarToStr(AValue) );
      szCapital := MoneyConvert(t);
    end else
    begin
      szCapital := '';
    end;
    Self.tvLeaseCol20.Summary.FooterFormat := szCapital;
  except
  end;

end;

procedure TfrmProjectLease.ADOMakingAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmProjectLease.ADOQuery1AfterInsert(DataSet: TDataSet);
var
  szColName : string;
  szCode : string;
begin
  inherited;
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      FieldByName('Code').Value := g_PostCode;
      szColName := Self.tvLeaseViewColumn2.DataBinding.FieldName;
      FieldByName(szColName).Value := True;
      szColName := Self.tvLeaseCol2.DataBinding.FieldName;
      szCode := Prefix + GetRowCode(g_Table_Project_Lease,szColName,Suffix,60000);
      FieldByName(szColName).Value := szCode;
      szColName := Self.tvLeaseCol3.DataBinding.FieldName;
      FieldByName(szColName).Value := Date;
      szColName := Self.tvLeaseCol4.DataBinding.FieldName;
      FieldByName(szColName).Value := g_ProjectName;

    end;

  end;

end;

procedure TfrmProjectLease.ADOQuery1AfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmProjectLease.ADOSignNameAfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmProjectLease.cxButton1Click(Sender: TObject);
begin
  inherited;
//结束日期-开始日期 = 天数 - 扣除数 = 租赁天数 * 租赁数量 * 租赁单价 = 租赁金额
//  +
//运输次数 * 单价 = 金额
  with Self.tvLeaseCol12 do
  begin
    EditValue := Self.cxDBSpinEdit1.EditValue;
    Editing   := false;
  end;
end;

procedure TfrmProjectLease.cxButton2Click(Sender: TObject);
begin
  inherited;
  with Self.tvLeaseCol12 do
  begin
    Editing   := false;
  end;
end;

function TfrmProjectLease.GetleaseDayNum(lpStartDate,lpEndDate : TDate ; DayNum , Deduction : Integer ; lpLeaseUnitPrice : Currency):Boolean;
var
  szDayNum : Integer;
  szleaseNum : Variant;
  szRowIndex : Integer;
  s : string;

begin
  inherited;

  if CompareDateTime(lpEndDate,lpStartDate) <> -1 then
  begin

    szDayNum := DaysBetween(lpEndDate,lpStartDate);

    Self.cxDBSpinEdit2.EditValue := szDayNum;
    Self.cxDBSpinEdit2.PostEditValue;

    Self.cxDBSpinEdit1.EditValue := szDayNum - Deduction;
    Self.cxDBSpinEdit1.PostEditValue;
    s := '';

    szRowIndex := Self.tvLeaseView.Controller.FocusedRowIndex;
    szleaseNum := Self.tvLeaseView.DataController.Values[szRowIndex,Self.tvLeaseCol11.Index];
    if szleaseNum <> null then
    begin
      s := VarToStr(szleaseNum);
    end;
    Self.cxDBCurrencyEdit4.EditValue := StrToIntDef(s,0) * (szDayNum - Deduction) *  lpLeaseUnitPrice;
    Self.cxDBCurrencyEdit4.PostEditValue;
  end;
end;


procedure TfrmProjectLease.cxDBDateEdit1PropertiesCloseUp(Sender: TObject);
begin
  inherited;
  Self.cxDBDateEdit1.PostEditValue;
end;

procedure TfrmProjectLease.cxDBDateEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  EndDate : Variant;
begin
  inherited;
//  EndDate := Self.cxDBDateEdit2.Date;
//  GetleaseDayNum(VarToDateTime(DisplayValue) , EndDate);

end;

procedure TfrmProjectLease.cxDBDateEdit2PropertiesCloseUp(Sender: TObject);
begin
  inherited;
  Self.cxDBDateEdit2.PostEditValue;
end;

procedure TfrmProjectLease.cxDBDateEdit2PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  StartDate : Variant;
  DeductionDayNum : string; //扣除天数
  DayNum : Variant;
  s : string;
  
begin
  inherited;
  StartDate := Self.cxDBDateEdit1.Date;
  DayNum := Self.cxDBSpinEdit1.EditingValue; //天数
  s := VarToStr(DayNum);
  DeductionDayNum := VarToStr( Self.cxDBSpinEdit3.EditingValue ); //扣除天数
  
  GetleaseDayNum(VarToDateTime(StartDate) ,
                 VarToDateTime(DisplayValue),
                 StrToIntDef(s,0) ,
                 StrToIntDef(DeductionDayNum,0),
                 StrToCurrDef(VarToStr(Self.cxDBCurrencyEdit3.EditingValue),0)
                 );
end;

procedure TfrmProjectLease.cxDBSpinEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szLeasePrice : Currency;
begin
  inherited;
//租赁天数
  szLeasePrice := StrToCurrDef( VartoStr( Self.cxDBCurrencyEdit3.EditingValue ) , 0 );
  Self.cxDBCurrencyEdit4.EditValue := StrToIntDef( VarToStr(DisplayValue) , 0 ) * szLeasePrice;
  Self.cxDBCurrencyEdit4.PostEditValue;
  
end;

procedure TfrmProjectLease.cxDBSpinEdit2PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  StartDate : TDate;
  EndDate   : TDate;
  DeductionDayNum : string; //扣除天数
  s : string;

begin
  inherited;
  StartDate := Self.cxDBDateEdit1.Date;
  EndDate   := Self.cxDBDateEdit2.Date;
  DeductionDayNum := VarToStr( Self.cxDBSpinEdit3.EditingValue );
  s := VarToStr(DisplayValue);
  GetleaseDayNum(StartDate ,
                 EndDate,
                 StrToIntDef(s , 0),
                 StrToIntDef(DeductionDayNum,0),
                 StrToCurrDef(VarToStr(Self.cxDBCurrencyEdit3.EditingValue),0)
                 );
//天数
end;

procedure TfrmProjectLease.cxDBSpinEdit3PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  StartDate : TDate;
  EndDate   : TDate;
  DayNum : Integer;
  s : string;

begin
  inherited;

  StartDate := Self.cxDBDateEdit1.Date;
  EndDate   := Self.cxDBDateEdit2.Date;
  DayNum := StrToIntDef(VarToStr( Self.cxDBSpinEdit1.EditingValue ),0);
  GetleaseDayNum(StartDate ,
                 EndDate,
                 DayNum,
                 StrToIntDef( VarToStr( DisplayValue ) ,0),
                 StrToCurrDef(VarToStr(Self.cxDBCurrencyEdit3.EditingValue),0)
                 );
//扣除天数
end;

procedure TfrmProjectLease.cxDBCurrencyEdit3PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  StartDate : TDate;
  EndDate   : TDate;
  DayNum : Integer;
  s : string;

begin
  inherited;
  StartDate := Self.cxDBDateEdit1.Date;
  EndDate   := Self.cxDBDateEdit2.Date;
  DayNum := StrToIntDef(VarToStr( Self.cxDBSpinEdit1.EditingValue ),0);
  GetleaseDayNum(StartDate ,
                 EndDate,
                 DayNum,
                 StrToIntDef( VarToStr( Self.cxDBSpinEdit3.EditingValue ) ,0),
                 StrToCurrDef(VarToStr( DisplayValue ),0)
                 );
//租赁单价
end;


procedure TfrmProjectLease.cxDBSpinEdit4PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var 
  szConveyancePrice : string; //运输单价
  
begin
  inherited;
  //运输次数
  szConveyancePrice := VarToStr( Self.cxDBCurrencyEdit1.EditingValue );
  Self.cxDBCurrencyEdit2.EditValue := StrToIntDef( VarToStr( DisplayValue),0 ) * StrToCurrDef(szConveyancePrice , 0);
  Self.cxDBCurrencyEdit2.PostEditValue;
end;

procedure TfrmProjectLease.cxDBCurrencyEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szConveyanceNumber : string;  //运输次数
  
begin
  inherited;
  //运输单价
  szConveyanceNumber := VarToStr( Self.cxDBSpinEdit4.EditingValue ); 
  Self.cxDBCurrencyEdit2.EditValue := StrToIntDef( VarToStr(DisplayValue) ,0 ) * StrToCurrDef(szConveyanceNumber , 0);
  Self.cxDBCurrencyEdit2.PostEditValue;
end;

procedure TfrmProjectLease.Excel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'机械明细表');
end;

procedure TfrmProjectLease.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  inherited;
  Action := caFree;
end;

procedure TfrmProjectLease.FormCreate(Sender: TObject);
var
  ChildFrame : TProjectFrame;
begin
  Prefix := 'ZL';
  Suffix := '00006';

  ChildFrame := TProjectFrame.Create(Self);
  ChildFrame.dwParentHandle := Handle;
  ChildFrame.Parent := Self.RzPanel2;
  ChildFrame.IsSystem := True;
  ChildFrame.FrameClick(Self);
  SetcxGrid(Self.tvLeaseView,0,g_Dir_Project_Lease);
  Self.Date1.Date := Date;
  Self.Date2.Date := Date;
end;

procedure TfrmProjectLease.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Self.RzBut10.Click;

end;

procedure TfrmProjectLease.N2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.Grid;
  DM.BasePrinterLink1.ReportTitleText := '项目名称：' + g_Projectname;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmProjectLease.PropertiesInitPopup(Sender: TObject);
begin
  inherited;
  with Self.ADOQuery1 do
  begin
    if State <> dsInactive  then
    begin
      if (State <> dsEdit) and (State <> dsInsert) then
      begin
        Edit;
      end;
    end;  
  end;  
end;

procedure TfrmProjectLease.RzBut10Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmProjectLease.RzBut4Click(Sender: TObject);
var
  szName: string;
  szCount : Integer;
  szIndex : Integer;
  szRecordCount : Integer;
  i : Integer;
  str: string;

begin
  inherited;
  with Self.ADOQuery1 do
  begin
    if Sender = Self.RzBut4 then
    begin
      if State = dsInactive then
      begin
        Application.MessageBox( '当前查询状态无效，单击项目管理在进行操作？', '提示:', MB_OKCANCEL + MB_ICONWARNING)
      end else
      begin
        First;
        Insert;
      //  Append;
        IsSave := True;
        SendMessage(Handle,WM_SetGridFocus,0,0);
      //  Self.tvLeaseCol7.FocusWithSelection;
      //  Self.tvLeaseView.Columns[4].Focused := True;
        Self.Grid.SetFocus;
      //  keybd_event(VK_RETURN,0,0,0);
      end;

    end;
  end;

end;

procedure TfrmProjectLease.RzBut5Click(Sender: TObject);
begin
  inherited;
  IsDeleteEmptyData(Self.ADOQuery1 ,
                    g_Table_Project_Lease ,
                    Self.tvLeaseCol7.DataBinding.FieldName);
  IsSave := False;
end;

procedure TfrmProjectLease.RzBut6Click(Sender: TObject);
begin
  inherited;
  DeleteSelection(Self.tvLeaseView,'',g_Table_Project_Lease,
  Self.tvLeaseCol2.DataBinding.FieldName);
  Self.ADOQuery1.Requery();
end;

procedure TfrmProjectLease.RzBut7Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
begin
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_Dir_Project_Lease;
    Child.ShowModal;
    SetcxGrid(Self.tvLeaseView,0,g_Dir_Project_Lease);
  finally
    Child.Free;
  end;

end;

procedure TfrmProjectLease.RzBut9Click(Sender: TObject);
var
  szColName : string;
  sqltext : string;

begin
  szColName := Self.tvLeaseCol3.DataBinding.FieldName;
  sqltext := 'select *from ' + g_Table_Project_Lease + ' where Code= "' + g_PostCode + ' " and ' + szColName +' between :t1 and :t2';
  SearchDateRange(Self.Date1.Text,Self.Date2.Text,sqltext,Self.ADOQuery1);

end;

procedure TfrmProjectLease.RzToolButton1Click(Sender: TObject);
begin
  inherited;
  ADOIsEdit( tvLeaseViewColumn2.EditValue , Self.ADOQuery1)
end;

end.
