unit ufrmProjectMechanics;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.ComCtrls, RzButton,
  RzPanel, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxCore,
  cxDateUtils, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxTextEdit, cxCheckBox,
  cxDropDownEdit, cxSpinEdit, cxCurrencyEdit, cxMemo, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,ufrmBaseController,
  cxGridCustomView, cxGrid, cxMaskEdit, cxCalendar, cxLabel, Data.Win.ADODB,
  Vcl.Menus, Vcl.StdCtrls, cxButtons, cxDBLookupComboBox, cxDBEdit, cxGroupBox,
  cxDBLabel, System.Actions, Vcl.ActnList;

type
  TfrmProjectMechanics = class(TfrmBaseController)
    RzPanel2: TRzPanel;
    Splitter1: TSplitter;
    RzPanel1: TRzPanel;
    RzToolbar11: TRzToolbar;
    RzSpacer88: TRzSpacer;
    RzToolButton66: TRzToolButton;
    RzSpacer89: TRzSpacer;
    RzToolButton67: TRzToolButton;
    RzSpacer90: TRzSpacer;
    RzToolButton68: TRzToolButton;
    RzSpacer91: TRzSpacer;
    RzToolButton69: TRzToolButton;
    RzToolButton70: TRzToolButton;
    RzSpacer92: TRzSpacer;
    RzToolButton71: TRzToolButton;
    RzSpacer93: TRzSpacer;
    RzSpacer94: TRzSpacer;
    RzSpacer95: TRzSpacer;
    RzSpacer96: TRzSpacer;
    RzToolButton72: TRzToolButton;
    RzSpacer97: TRzSpacer;
    RzToolButton73: TRzToolButton;
    RzSpacer98: TRzSpacer;
    RzSpacer99: TRzSpacer;
    RzToolButton74: TRzToolButton;
    cxLabel48: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel49: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    Grid: TcxGrid;
    tvMechanicsGrid: TcxGridDBTableView;
    tvMechanicsGridColumn1: TcxGridDBColumn;
    tvMechanicsGridColumn5: TcxGridDBColumn;
    ColMechanicsGrid1: TcxGridDBColumn;
    ColMechanicsGrid2: TcxGridDBColumn;
    ColMechanicsGrid3: TcxGridDBColumn;
    ColMechanicsGrid4: TcxGridDBColumn;
    ColMechanicsGrid5: TcxGridDBColumn;
    ColMechanicsGrid12: TcxGridDBColumn;
    ColMechanicsGrid13: TcxGridDBColumn;
    ColMechanicsGrid6: TcxGridDBColumn;
    ColMechanicsGrid14: TcxGridDBColumn;
    ColMechanicsGrid15: TcxGridDBColumn;
    ColMechanicsGrid7: TcxGridDBColumn;
    ColMechanicsGrid8: TcxGridDBColumn;
    ColMechanicsGrid9: TcxGridDBColumn;
    ColMechanicsGrid10: TcxGridDBColumn;
    ColMechanicsGrid11: TcxGridDBColumn;
    tvMechanicsGridColumn2: TcxGridDBColumn;
    tvMechanicsGridColumn3: TcxGridDBColumn;
    tvMechanicsGridColumn4: TcxGridDBColumn;
    Lv: TcxGridLevel;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    RzPanel27: TRzPanel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    cxDateEdit3: TcxDateEdit;
    cxDateEdit4: TcxDateEdit;
    cxButton16: TcxButton;
    cxCurrencyEdit2: TcxCurrencyEdit;
    ADOSignName: TADOQuery;
    DataSignName: TDataSource;
    Print: TPopupMenu;
    Excel1: TMenuItem;
    N2: TMenuItem;
    RzPanel3: TRzPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    tvMechanicsGridColumn6: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    cxLabel7: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel1: TcxLabel;
    cxGroupBox2: TcxGroupBox;
    cxLabel6: TcxLabel;
    cxDBCurrencyEdit2: TcxDBCurrencyEdit;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxDBCurrencyEdit3: TcxDBCurrencyEdit;
    cxLabel8: TcxLabel;
    cxDBCurrencyEdit4: TcxDBCurrencyEdit;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxDBSpinEdit2: TcxDBSpinEdit;
    ActionList1: TActionList;
    PopupEditClose: TAction;
    cxDBSpinEdit3: TcxDBSpinEdit;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    tvMechanicsGridColumn7: TcxGridDBColumn;
    cxDBSpinEdit4: TcxDBSpinEdit;
    cxDBCurrencyEdit5: TcxDBCurrencyEdit;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    cxLabel11: TcxLabel;
    cxLabel12: TcxLabel;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure RzToolButton71Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure RzToolButton66Click(Sender: TObject);
    procedure RzToolButton73Click(Sender: TObject);
    procedure RzToolButton72Click(Sender: TObject);
    procedure tvMechanicsGridColumn1GetDisplayText(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AText: string);
    procedure tvMechanicsGridColumnSizeChanged(Sender: TcxGridTableView;
      AColumn: TcxGridColumn);
    procedure RzToolButton69Click(Sender: TObject);
    procedure cxButton16Click(Sender: TObject);
    procedure tvMechanicsGridEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvMechanicsGridEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvMechanicsGridKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tvMechanicsGridStylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure tvMechanicsGridColumn5PropertiesChange(Sender: TObject);
    procedure ADOQuery1AfterInsert(DataSet: TDataSet);
    procedure ColMechanicsGrid15GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure ColMechanicsGrid5GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure RzToolButton74Click(Sender: TObject);
    procedure tvMechanicsGridDblClick(Sender: TObject);
    procedure tvMechanicsGridColumn4GetPropertiesForEdit(
      Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
      var AProperties: TcxCustomEditProperties);
    procedure tvMechanicsGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure Excel1Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxDBSpinEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBSpinEdit2PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBCurrencyEdit3PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBSpinEdit3PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure cxDBCurrencyEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure PropertiesInitPopup(Sender: TObject);
    procedure ADOQuery1AfterOpen(DataSet: TDataSet);
    procedure RzToolButton1Click(Sender: TObject);
  private
    { Private declarations }
    g_ProjectName : string;
    g_PostCode    : string;
    IsSave : Boolean;
    Prefix : string;
    Suffix : string;
    procedure  SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;lpSuffix:string);
    function GetSumMoney(lpWorkDays , lpBicycleVolume  : Double ;
       lpUnitPrice : Currency;lpOvertimer : Double ; lpWorkUnitPrice : Currency ):Boolean;
  public
    { Public declarations }
    procedure WndProc(var Message: TMessage); override;  // 第一优先权
  end;

var
  frmProjectMechanics: TfrmProjectMechanics;

implementation

uses
   uProjectFrame,global,ufunctions,uDataModule,ufrmIsViewGrid,System.DateUtils, Math ;

{$R *.dfm}

procedure TfrmProjectMechanics.WndProc(var Message: TMessage);
var
  pMsg : PProject;
  s : string;
  szCodeList : string;
  szTableList : array[0..1] of TTableList;
begin
  case Message.Msg of
    WM_FrameClose:
    begin
      Self.ADOQuery1.Close;
      Self.ADOSignName.Close;
    end;
    WM_FrameView :
    begin
      pMsg := PProject(Message.LParam);
      g_PostCode := pMsg.dwCode;
      g_ProjectName := pMsg.dwName;

      s := 'Select *from ' + g_Table_Project_Mechanics + ' Where Code ="' +  g_PostCode + '"';
      with Self.ADOQuery1 do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Open;
      end;
      {
      with Self.ADOSignName do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select * from ' + g_Table_CompanyManage;
        Open;
      end;
      }
      with Self.ADOSignName do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select pt.SignName AS Name,pycode from '+ g_Table_Maintain_Science +
                    ' AS PT left join '+ g_Table_CompanyManage +
                    ' AS PS on PT.SignName = PS.SignName Where ProjectName="'+
                    g_ProjectName +
                    '" group by pt.SignName,pycode';
        Open;
      end;

    end;
    WM_FrameDele :
    begin
      pMsg   := PProject(Message.LParam);
      szCodeList := pMsg.dwCodeList;
      DeleteTableFile(szTableList,szCodeList);
      DM.InDeleteData(g_Table_Project_Mechanics,szCodeList); //清除主目录表内关联
    end;
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

procedure TfrmProjectMechanics.SetcxGrid(lpGridView : TcxGridDBTableView ; index : Byte ;
lpSuffix:string);
var
  i : Integer;
begin
  for I := 0 to lpGridView.ColumnCount - 1 do
  begin
    Application.ProcessMessages;
    SetGrid(lpGridView.Columns[i],index, Self.Name + lpSuffix);
  end;
end;


procedure TfrmProjectMechanics.tvMechanicsGridColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmProjectMechanics.tvMechanicsGridColumn4GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
  inherited;
  GetCompanyInfo(AProperties);
end;

procedure TfrmProjectMechanics.tvMechanicsGridColumn5PropertiesChange(
  Sender: TObject);
var
  szRowIndex : Integer;
begin
  inherited;
  szRowIndex := Self.tvMechanicsGrid.DataController.FocusedRecordIndex;
  Self.ADOQuery1.UpdateBatch();
  Self.ADOQuery1.Requery();
  Self.tvMechanicsGrid.Controller.FocusedRowIndex := szRowIndex;
end;

procedure TfrmProjectMechanics.tvMechanicsGridColumnSizeChanged(
  Sender: TcxGridTableView; AColumn: TcxGridColumn);
begin
  inherited;
  SetcxGrid(Self.tvMechanicsGrid,1, g_Dir_Project_Mechanics );
end;

procedure TfrmProjectMechanics.tvMechanicsGridDblClick(Sender: TObject);
var
  szDir : string;
begin
  inherited;
  szDir := Concat(g_Resources,'\'+ g_Dir_Project_Mechanics);
  CreateEnclosure(Self.tvMechanicsGrid.DataController.DataSource.DataSet,Handle,szDir);
end;

procedure TfrmProjectMechanics.tvMechanicsGridEditing(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  var AAllow: Boolean);
var
  szColName : string;
begin
  inherited;
  with Self.ADOQuery1 do
  begin
    if State <> dsInactive then
      begin
        if (State <> dsEdit) and (State <> dsInsert) then
        begin
          AAllow := False;
        end else
        begin
          AAllow := True;
        end;

      end;
  end;
  {
  szColName := Self.tvMechanicsGridColumn5.DataBinding.FieldName;
  if AItem.Index <> Self.tvMechanicsGridColumn5.Index then
  begin

  end;
  }
  if (AItem.Index = Self.tvMechanicsGridColumn1.Index) or
     (AItem.Index = Self.ColMechanicsGrid1.Index) then
  begin
    AAllow := False;
  end;
end;

procedure TfrmProjectMechanics.tvMechanicsGridEditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
var
  szTotal : Currency;
  szDaysCount : Single;
  szUnitPrice : Currency;
  szOvertimer : Single;

  szFieldNameUnitPrice : string;
  szFieldNameWorkTotal : string;
  szFieldNameWorkDays  : string;
  szFieldNameOvertimer : string;
  szFieldNameWorkUnitPrice:string;
  szBicycleVolume : Double;
  szTotalVolume : Double;
  szEditValue : Variant;

begin
  inherited;
  if (Key = 13) then
  begin

      try
        {
      //  Self.tvMechanicsGrid.DataController.UpdateData;
        szEditValue := AEdit.EditingValue;
        with Self.ADOQuery1 do
        begin

          szFieldNameWorkTotal := Self.ColMechanicsGrid10.DataBinding.FieldName; //总额
          szFieldNameWorkDays  := Self.ColMechanicsGrid6.DataBinding.FieldName; //取出数量/车

          szDaysCount := FieldByName(szFieldNameWorkDays).AsFloat; //正常数量/车
          if (State = dsEdit) or (State = dsinsert) then
          begin

            if (AItem.Name = Self.ColMechanicsGrid6.Name) or (AItem.Name = Self.ColMechanicsGrid13.Name) then
            begin
              szBicycleVolume := FieldByName(Self.ColMechanicsGrid13.DataBinding.FieldName).AsFloat; //单车方量
              szTotalVolume   := szBicycleVolume * szDaysCount;
              FieldByName(Self.ColMechanicsGrid14.DataBinding.FieldName).AsFloat := szTotalVolume;// StrToIntDef(VarToStr(szEditValue),0);
            end;

            if AItem.Name = Self.ColMechanicsGrid7.Name then
            begin
              szTotal := szDaysCount * StrToIntDef(VarToStr( szEditValue ),0);
              FieldByName(szFieldNameWorkTotal).AsCurrency := szTotal;
            end;

            if AItem.Name = Self.ColMechanicsGrid9.Name then
            begin

              szFieldNameUnitPrice     := Self.ColMechanicsGrid7.DataBinding.FieldName; //单价
              szFieldNameOvertimer     := Self.ColMechanicsGrid8.DataBinding.FieldName; //机械用时
              szFieldNameWorkUnitPrice := Self.ColMechanicsGrid9.DataBinding.FieldName; //机械用时单价

              szUnitPrice := FieldByName(szFieldNameUnitPrice).AsCurrency;
              szOvertimer := FieldByName(szFieldNameOvertimer).AsFloat;

              FieldByName(szFieldNameWorkTotal).AsCurrency := (szDaysCount * szUnitPrice) + (szOvertimer * StrToIntDef(VarToStr( szEditValue ),0) );

            end;

          end;

        end;
        }
        if AItem.Index = Self.ColMechanicsGrid10.Index then
        begin
          if Self.tvMechanicsGrid.Controller.FocusedRow.IsLast and IsNewRow then
          begin
            with Self.ADOQuery1 do
            begin
              if State <> dsInactive then
              begin
                First;
                Insert;
                Self.ColMechanicsGrid2.FocusWithSelection;
              end;

            end;

          end;
        end;
      except
        on e:Exception do
        begin
        end;

      end;

  end;

end;

procedure TfrmProjectMechanics.tvMechanicsGridKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  inherited;
  if Key =  46 then
  begin
    Self.RzToolButton68.Click;
  end;
end;

procedure TfrmProjectMechanics.tvMechanicsGridStylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  szIndex : Integer;
  Value : Variant;
begin
  inherited;
  Value := ARecord.Values[Self.tvMechanicsGridColumn5.Index];
  if Value = False then
  begin
    AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmProjectMechanics.tvMechanicsGridTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
var
  t : Currency;
  szCapital : string;

begin
  inherited;
  try
    szCapital := '';
    if AValue <> null then
    begin
      t := StrToCurr( VarToStr(AValue) );
      szCapital := MoneyConvert(t);
    end else
    begin
      szCapital := '';
    end;
    Self.ColMechanicsGrid11.Summary.FooterFormat := szCapital;
  except
  end;

end;

procedure TfrmProjectMechanics.ADOQuery1AfterInsert(DataSet: TDataSet);
var
  szConName , szNumbers : string;
  szCode : string;
begin
  inherited;
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      FieldByName('Code').Value := g_PostCode;
      //立方
      szConName := Self.ColMechanicsGrid15.DataBinding.FieldName;
      FieldByName( szConName ).Value := 'm3';
      szConName := Self.ColMechanicsGrid3.DataBinding.FieldName;
      FieldByName( szConName ).Value := g_ProjectName;
      szConName := Self.ColMechanicsGrid1.DataBinding.FieldName;
      szCode := Prefix + GetRowCode(g_Table_Project_Mechanics,szConName,Suffix,70000);
      FieldByName( szConName ).Value := szCode;
      szConName := Self.ColMechanicsGrid2.DataBinding.FieldName;
      FieldByName( szConName ).Value := Date;
      szConName := Self.tvMechanicsGridColumn5.DataBinding.FieldName;
      FieldByName(szConName).Value := True;

    end;
  end;
end;

procedure TfrmProjectMechanics.ADOQuery1AfterOpen(DataSet: TDataSet);
begin
  inherited;
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmProjectMechanics.ColMechanicsGrid15GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  List : TStringList;
  i : Integer;

begin
  inherited;
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    List := TStringList.Create;
    try
      GetCompany(List,0);
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    finally
      List.Free;
    end;
  end;
end;

procedure TfrmProjectMechanics.ColMechanicsGrid5GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
var
  List : TStringList;
  i : Integer;
begin
  inherited;
  with (AProperties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    List := TStringList.Create;
    try
      GetCompany(List,6);
      for I := 0 to List.Count-1 do Items.Add(List.Strings[i]);
    finally
      List.Free;
    end;
  end;
end;

procedure TfrmProjectMechanics.cxButton16Click(Sender: TObject);
var
  szStartTime : TDateTime;
  szEndTime   : TDateTime;
  szValue : Integer;
  m : Currency;
  Minute : Double;

begin
  inherited;
  m := Self.cxCurrencyEdit2.Value;
  szStartTime := Self.cxDateEdit3.Date;
  szEndTime   := Self.cxDateEdit4.Date;
  if CompareDateTime(szEndTime,szStartTime) <> -1 then
  begin

    if m <> 0 then
    begin
      Minute := MinuteSpan(szEndTime , szStartTime); //得到总分钟数
      m := m / 60;
      Label53.Caption := FloatToStr(Math.RoundTo( m * Minute , -2)) + ' 元';

    end;
    Label50.Caption := getTaskTimeStr(szEndTime,szStartTime,0);
    Label48.Caption := getDateTimeStr(szEndTime,szStartTime);
    if Length( Label48.Caption ) = 0 then Label48.Caption := '无';

  end
  else
  begin
    Application.MessageBox('开始时间不能大于结束时间,无法计算正确计算加班时间',m_title,MB_OK + MB_ICONHAND);
  end;
end;

procedure TfrmProjectMechanics.cxButton1Click(Sender: TObject);
begin
  inherited;
//单车方量 * 数量/车 = 合计方量
//数量/车 * 单价 = 运费 + 机械用时 * 机械用时单价  = 金额
  with Self.ColMechanicsGrid6 do
  begin
    EditValue := cxDBSpinEdit1.EditValue;
    Editing   := false;
  end;

end;

procedure TfrmProjectMechanics.cxButton2Click(Sender: TObject);
begin
  inherited;
  Self.ColMechanicsGrid6.Editing := False;
end;

function TfrmProjectMechanics.GetSumMoney(lpWorkDays , lpBicycleVolume  : Double ;
lpUnitPrice : Currency;lpOvertimer : Double ; lpWorkUnitPrice : Currency ):Boolean;
var
  SumValue : Double;
  szFreight: Double;
  szTimerCost : Double;

begin
  SumValue := lpWorkDays * lpBicycleVolume;

  Self.cxDBSpinEdit4.EditValue := SumValue; //合计方量
  Self.cxDBSpinEdit4.PostEditValue;

  szFreight:= lpWorkDays * lpUnitPrice;  //运费;

  Self.cxDBCurrencyEdit4.EditValue := szFreight;
  Self.cxDBCurrencyEdit4.PostEditValue;

  szTimerCost:= lpOvertimer * lpWorkUnitPrice; //用时成本
  Self.cxDBCurrencyEdit2.EditValue := szTimerCost;
  Self.cxDBCurrencyEdit2.PostEditValue;

  Self.cxDBCurrencyEdit5.EditValue := szTimerCost + szFreight;
  Self.cxDBCurrencyEdit5.PostEditValue;
end;

procedure TfrmProjectMechanics.cxDBCurrencyEdit1PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var
  szWorkDays  : Double;
  szBicycleVolume , B : Double;
  szUnitPrice : Currency;
  szOvertimer : Double;
  szWorkUnitPrice : Currency;

begin
  inherited;
  //机械用时
  szWorkDays      := StrToCurrDef(VarToStr( Self.cxDBSpinEdit1.EditingValue ),0); //数量/车
  szUnitPrice     := StrToCurrDef(VarToStr( Self.cxDBCurrencyEdit3.EditingValue ) , 0 ); //单价
  szBicycleVolume := strTocurrdef(VarToStr( Self.cxDBSpinEdit2.EditingValue ),0); //单车方量

  szOvertimer     := StrToCurrDef(VarToStr( Self.cxDBSpinEdit3.EditingValue ),0); //机械用时
  szWorkUnitPrice := StrToCurrDef(VarToStr( DisplayValue ) , 0 );//用时单价

  GetSumMoney(szWorkDays,szBicycleVolume,szUnitPrice,szOvertimer,szWorkUnitPrice);
end;

procedure TfrmProjectMechanics.cxDBCurrencyEdit3PropertiesValidate(
  Sender: TObject; var DisplayValue: Variant; var ErrorText: TCaption;
  var Error: Boolean);
var
  szWorkDays  : Double;
  szBicycleVolume , B : Double;
  szUnitPrice : Currency;
  szOvertimer : Double;
  szWorkUnitPrice : Currency;

begin
  inherited;
  //单价
  szWorkDays      := StrToCurrDef(VarToStr( Self.cxDBSpinEdit1.EditingValue ),0); //数量/车
  szUnitPrice     := StrToCurrDef(VarToStr( DisplayValue ) , 0 ); //单价
  szBicycleVolume := strTocurrdef(VarToStr( Self.cxDBSpinEdit2.EditingValue ),0); //单车方量

  szOvertimer     := StrToCurrDef(VarToStr( Self.cxDBSpinEdit3.EditingValue ),0); //机械用时
  szWorkUnitPrice := StrToCurrDef(VarToStr( Self.cxDBCurrencyEdit1.EditingValue ) , 0 );//用时单价

  GetSumMoney(szWorkDays,szBicycleVolume,szUnitPrice,szOvertimer,szWorkUnitPrice);
  //单车方量 * 数量/车 = 合计方量
end;

procedure TfrmProjectMechanics.cxDBSpinEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : Variant;
  szWorkDays  : Double;
  szBicycleVolume , B : Double;
  szUnitPrice : Currency;
  szOvertimer : Double;
  szWorkUnitPrice : Currency;

begin
  inherited;
  //数量/车
  szWorkDays      := StrToCurrDef(VarToStr( DisplayValue ),0); //数量/车
  szUnitPrice     := StrToCurrDef(VarToStr( Self.cxDBCurrencyEdit3.EditingValue ) , 0 ); //单价
  szBicycleVolume := strTocurrdef(VarToStr( Self.cxDBSpinEdit2.EditingValue ),0); //单车方量

  szOvertimer     := StrToCurrDef(VarToStr( Self.cxDBSpinEdit3.EditingValue ),0); //机械用时
  szWorkUnitPrice := StrToCurrDef(VarToStr( Self.cxDBCurrencyEdit1.EditingValue ) , 0 );//用时单价

  GetSumMoney(szWorkDays,szBicycleVolume,szUnitPrice,szOvertimer,szWorkUnitPrice);

  //单车方量 * 数量/车 = 合计方量
end;

procedure TfrmProjectMechanics.cxDBSpinEdit2PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : Variant;
  szWorkDays  : Double;
  szBicycleVolume , B : Double;
  szUnitPrice : Currency;
  szOvertimer : Double;
  szWorkUnitPrice : Currency;

begin
  inherited;
  //单车方量
  szWorkDays      := StrToCurrDef(VarToStr( Self.cxDBSpinEdit1.EditingValue ),0); //数量/车
  szUnitPrice     := StrToCurrDef(VarToStr( Self.cxDBCurrencyEdit3.EditingValue ) , 0 ); //单价
  szBicycleVolume := strTocurrdef(VarToStr( DisplayValue ),0); //单车方量

  szOvertimer     := StrToCurrDef(VarToStr( Self.cxDBSpinEdit3.EditingValue ),0); //机械用时
  szWorkUnitPrice := StrToCurrDef(VarToStr( Self.cxDBCurrencyEdit1.EditingValue ) , 0 );//用时单价

  GetSumMoney(szWorkDays,szBicycleVolume,szUnitPrice,szOvertimer,szWorkUnitPrice);
end;

procedure TfrmProjectMechanics.cxDBSpinEdit3PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  szWorkDays  : Double;
  szBicycleVolume , B : Double;
  szUnitPrice : Currency;
  szOvertimer : Double;
  szWorkUnitPrice : Currency;

begin
  inherited;
  //机械用时
  szWorkDays      := StrToCurrDef(VarToStr( Self.cxDBSpinEdit1.EditingValue ),0); //数量/车
  szUnitPrice     := StrToCurrDef(VarToStr( Self.cxDBCurrencyEdit3.EditingValue ) , 0 ); //单价
  szBicycleVolume := strTocurrdef(VarToStr( Self.cxDBSpinEdit2.EditingValue ),0); //单车方量

  szOvertimer     := StrToCurrDef(VarToStr( DisplayValue ),0); //机械用时
  szWorkUnitPrice := StrToCurrDef(VarToStr( Self.cxDBCurrencyEdit1.EditingValue ) , 0 );//用时单价

  GetSumMoney(szWorkDays,szBicycleVolume,szUnitPrice,szOvertimer,szWorkUnitPrice);
end;

procedure TfrmProjectMechanics.Excel1Click(Sender: TObject);
begin
  inherited;
  CxGridToExcel(Self.Grid,'土石方明细表');
end;

procedure TfrmProjectMechanics.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmProjectMechanics.FormCreate(Sender: TObject);
var
  ChildFrame : TProjectFrame;
begin
  inherited;
  Prefix := 'JX';
  Suffix := '00007';

  ChildFrame := TProjectFrame.Create(Self);
  ChildFrame.dwParentHandle := Handle;
  ChildFrame.IsSystem := True;
  ChildFrame.Parent := Self.RzPanel2;
  ChildFrame.FrameClick(Self);
  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
  Self.cxDateEdit3.Date := Date;
  Self.cxDateEdit4.Date := Date;
  SetcxGrid(Self.tvMechanicsGrid,0, g_Dir_Project_Mechanics );
end;

procedure TfrmProjectMechanics.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
  begin
    Self.RzToolButton71.Click;
  end;
end;

procedure TfrmProjectMechanics.N2Click(Sender: TObject);
begin
  inherited;
  DM.BasePrinterLink1.Component := Self.Grid;
  DM.BasePrinterLink1.ReportTitleText := '项目名称：' + g_Projectname;
  DM.BasePrinter.Preview(True, nil);
end;

procedure TfrmProjectMechanics.PropertiesInitPopup(Sender: TObject);
begin
  inherited;
  with Self.ADOQuery1 do
  begin
    if State <> dsInactive  then
    begin
      if (State <> dsEdit) and (State <> dsInsert) then
      begin
        Edit;
      end;
    end;
  end;
end;

procedure TfrmProjectMechanics.RzToolButton1Click(Sender: TObject);
begin
  inherited;
  {
  if tvMechanicsGridColumn5.EditValue then
  begin
    with Self.ADOQuery1 do
    begin
      if State <> dsInactive then
        begin
          if (State <> dsEdit) and ( State <> dsInsert ) then
             Edit;
        end;
    end;
  end else
  begin
    Application.MessageBox( '冻结状态下不可以编辑！', '提示:', MB_OK + MB_ICONWARNING)
  end;
  }
  ADOIsEdit( tvMechanicsGridColumn5.EditValue , Self.ADOQuery1)
end;

procedure TfrmProjectMechanics.RzToolButton66Click(Sender: TObject);
var
  szName : string;
  szIndex: Integer;
  szNumbers : string;
  szRecordCount : Integer;

begin
  inherited;

  with Self.ADOQuery1 do
  begin
    if State = dsInactive then
    begin
      Application.MessageBox( '当前查询状态无效，单击项目管理在进行操作？', '提示:', MB_OKCANCEL + MB_ICONWARNING)
    end else
    begin

      if Sender = Self.RzToolButton66 then
      begin
        //添加
        First;
        Insert;
      //  Append;
        Self.ColMechanicsGrid4.FocusWithSelection;
        Self.Grid.SetFocus;
        keybd_event(VK_RETURN,0,0,0);
        IsSave := True;

      end else
      if Sender = Self.RzToolButton67 then
      begin
        IsDeleteEmptyData(Self.ADOQuery1 ,
              g_Table_Project_Mechanics ,
              Self.ColMechanicsGrid4.DataBinding.FieldName);
        IsSave := False;
      end else
      if Sender = Self.RzToolButton68 then
      begin
        //删除
        DeleteSelection(Self.tvMechanicsGrid,'',g_Table_Project_Mechanics,
        Self.ColMechanicsGrid1.DataBinding.FieldName);
        Self.ADOQuery1.Requery;
      end;
    end;
  end;
end;


procedure TfrmProjectMechanics.RzToolButton69Click(Sender: TObject);
var
  Child : TfrmIsViewGrid;
  i : Integer;
begin
  inherited;
  Child := TfrmIsViewGrid.Create(Application);
  try
    Child.g_fromName := Self.Name + g_Dir_Project_Mechanics;
    Child.ShowModal;
    SetcxGrid(Self.tvMechanicsGrid,0,g_Dir_Project_Mechanics);
  finally
    Child.Free;
  end;
end;

procedure TfrmProjectMechanics.RzToolButton71Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmProjectMechanics.RzToolButton72Click(Sender: TObject);
var
  szColName : string;
  sqltext : string;

begin
  inherited;
  szColName := Self.ColMechanicsGrid3.DataBinding.FieldName;
  sqltext := 'select *from ' + g_Table_Project_Mechanics + ' where Code= "' + g_PostCode + ' " and ' + szColName +' between :t1 and :t2';
  SearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit2.Text,sqltext,Self.ADOQuery1);
end;

procedure TfrmProjectMechanics.RzToolButton73Click(Sender: TObject);
var
  s : string;
begin
  inherited;
  s := 'ColligateFile';
  CreateOpenDir(Handle,Concat(g_Resources,g_Dir_Project_Mechanics + '\' + s),True);
end;

procedure TfrmProjectMechanics.RzToolButton74Click(Sender: TObject);
begin
  inherited;
  with RzPanel27 do
  begin
    if Visible then
    begin
      Visible := False;
    end else
      Visible := True;
  end;

end;

end.
