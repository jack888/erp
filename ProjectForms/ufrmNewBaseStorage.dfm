object frmNewBaseStorage: TfrmNewBaseStorage
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = #26032#20179#24211#22522#31867
  ClientHeight = 539
  ClientWidth = 1175
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Visible = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 274
    Top = 0
    Width = 10
    Height = 539
    Color = 16250613
    ParentColor = False
    ResizeStyle = rsUpdate
    ExplicitTop = -2
    ExplicitHeight = 571
  end
  object RzPanel1: TRzPanel
    Left = 284
    Top = 0
    Width = 891
    Height = 539
    Align = alClient
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdLeft, sdRight]
    TabOrder = 0
    object RzPanel8: TRzPanel
      Left = 1
      Top = 0
      Width = 889
      Height = 539
      Align = alClient
      BorderInner = fsFlat
      BorderOuter = fsNone
      TabOrder = 0
      object Grid: TcxGrid
        Left = 1
        Top = 65
        Width = 887
        Height = 473
        Align = alClient
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        LookAndFeel.SkinName = 'Office2016Colorful'
        object tvView: TcxGridDBTableView
          PopupMenu = RightMenu
          OnDblClick = tvViewDblClick
          Navigator.Buttons.CustomButtons = <>
          OnEditing = tvViewEditing
          DataController.DataSource = DataMaster
          DataController.Summary.DefaultGroupSummaryItems = <
            item
              Format = #25968#20540':0.########;-0.########'
              Kind = skSum
              Column = tvViewColumn7
            end
            item
              Format = #25968#20540':0.########;-0.########'
              Kind = skSum
              Position = spFooter
              Column = tvViewColumn7
            end
            item
              Format = #25968#37327':0.###;-0.###'
              Kind = skSum
              Column = tvViewColumn8
            end
            item
              Format = #25968#37327':0.###;-0.###'
              Kind = skSum
              Position = spFooter
              Column = tvViewColumn8
            end
            item
              Format = #37329#39069':'#165',0.00;'#165'-,0.00'
              Kind = skSum
              Position = spFooter
              Column = tvViewColumn12
            end
            item
              Format = #37329#39069':'#165',0.00;'#165'-,0.00'
              Kind = skSum
              Column = tvViewColumn12
            end>
          DataController.Summary.FooterSummaryItems = <
            item
              Format = #37329#39069':'#165',0.00;'#165'-,0.00'
              Kind = skSum
              OnGetText = tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
              Column = tvViewColumn12
            end
            item
              Format = #25968#20540':0.########;-0.########'
              Kind = skSum
              Column = tvViewColumn7
            end
            item
              Format = #25968#37327':0.###;-0.###'
              Kind = skSum
              Column = tvViewColumn8
            end
            item
              Format = #165',0.00;'#165'-,0.00'
              Kind = skSum
              Column = tvViewColumn12
            end
            item
              Format = #22823#20889':'
              Kind = skCount
              Column = tvViewColumn13
            end>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #21333#20987#31579#36873#26597#35810
          FilterRow.Visible = True
          OptionsBehavior.FocusCellOnTab = True
          OptionsBehavior.FocusFirstCellOnNewRecord = True
          OptionsBehavior.GoToNextCellOnEnter = True
          OptionsBehavior.FocusCellOnCycle = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.DataRowSizing = True
          OptionsData.Deleting = False
          OptionsData.DeletingConfirmation = False
          OptionsSelection.InvertSelect = False
          OptionsSelection.MultiSelect = True
          OptionsSelection.CellMultiSelect = True
          OptionsView.NoDataToDisplayInfoText = '<'#27809#26377#20219#20309#25968#25454#35760#24405'>'
          OptionsView.DataRowHeight = 23
          OptionsView.Footer = True
          OptionsView.GroupByBox = False
          OptionsView.GroupRowHeight = 25
          OptionsView.HeaderFilterButtonShowMode = fbmButton
          OptionsView.HeaderHeight = 25
          OptionsView.Indicator = True
          OptionsView.IndicatorWidth = 20
          object tvViewColumn24: TcxGridDBColumn
            Caption = #24207#21495
            PropertiesClassName = 'TcxTextEditProperties'
            Properties.Alignment.Horz = taCenter
            OnGetDisplayText = tvViewColumn24GetDisplayText
            HeaderAlignmentHorz = taCenter
            MinWidth = 40
            Options.Filtering = False
            Options.Sorting = False
            Width = 40
          end
          object tvViewColumn28: TcxGridDBColumn
            Caption = #29366#24577
            DataBinding.FieldName = 'status'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.Alignment = taCenter
            HeaderAlignmentHorz = taCenter
            Width = 40
          end
          object tvViewColumn1: TcxGridDBColumn
            Caption = #32534#21495
            DataBinding.FieldName = 'Numbers'
            HeaderAlignmentHorz = taCenter
            Width = 131
          end
          object tvViewColumn2: TcxGridDBColumn
            Caption = #26085#26399
            DataBinding.FieldName = 'cxDate'
            PropertiesClassName = 'TcxDateEditProperties'
            Properties.Alignment.Horz = taCenter
            HeaderAlignmentHorz = taCenter
            Width = 80
          end
          object tvViewColumn3: TcxGridDBColumn
            Caption = #24037#31243#21517#31216
            DataBinding.FieldName = 'ProjectName'
            Visible = False
            Width = 109
          end
          object tvViewColumn15: TcxGridDBColumn
            Caption = #20379#24212#21333#20301
            DataBinding.FieldName = 'SignName'
            PropertiesClassName = 'TcxPopupEditProperties'
            Properties.PopupControl = RzPanel13
            Properties.OnInitPopup = tvViewColumn15PropertiesInitPopup
            RepositoryItem = DM.SignNameBox
            Width = 91
          end
          object tvViewColumn17: TcxGridDBColumn
            Caption = #36141#36135#21333#20301
            DataBinding.FieldName = 'BuyingUnit'
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.OnInitPopup = tvViewColumn5PropertiesInitPopup
            RepositoryItem = DM.CompanyList
            HeaderAlignmentHorz = taCenter
            Width = 92
          end
          object tvViewColumn26: TcxGridDBColumn
            Caption = #31867#22411
            DataBinding.FieldName = 'ProjectDepartment'
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.ImmediateDropDownWhenActivated = True
            Width = 80
          end
          object tvViewColumn16: TcxGridDBColumn
            Caption = #21046#34920#31867#21035
            DataBinding.FieldName = 'TabTypes'
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.ImmediateDropDownWhenActivated = True
            Properties.Items.Strings = (
              #20837#24211
              #20986#24211)
            Properties.ReadOnly = False
            Options.Editing = False
            Width = 101
          end
          object tvViewColumn4: TcxGridDBColumn
            Caption = #26448#26009'\'#21517#31216
            DataBinding.FieldName = 'StuffName'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.DropDownListStyle = lsEditList
            Properties.DropDownWidth = 500
            Properties.ImmediateDropDownWhenActivated = True
            Properties.KeyFieldNames = 'MakingCaption'
            Properties.ListColumns = <
              item
                Caption = #26448#26009#21517#31216
                HeaderAlignment = taCenter
                Width = 150
                FieldName = 'MakingCaption'
              end
              item
                Caption = #22411#21495
                HeaderAlignment = taCenter
                Width = 100
                FieldName = 'ModelIndex'
              end
              item
                Caption = #35268#26684
                HeaderAlignment = taCenter
                Width = 100
                FieldName = 'spec'
              end
              item
                Caption = #21333#20301
                HeaderAlignment = taCenter
                Width = 60
                FieldName = 'Company'
              end>
            Properties.ListSource = DataMaking
            Properties.OnCloseUp = tvViewColumn4PropertiesCloseUp
            Properties.OnInitPopup = tvViewColumn4PropertiesInitPopup
            HeaderAlignmentHorz = taCenter
            Width = 124
          end
          object tvViewColumn5: TcxGridDBColumn
            Caption = #35268#26684
            DataBinding.FieldName = 'cxSpec'
            PropertiesClassName = 'TcxLookupComboBoxProperties'
            Properties.ImmediateDropDownWhenActivated = True
            Properties.KeyFieldNames = 'spec'
            Properties.ListColumns = <
              item
                Caption = #35268#26684
                FieldName = 'spec'
              end>
            Properties.ListSource = DataSpec
            HeaderAlignmentHorz = taCenter
            Width = 72
          end
          object tvViewColumn21: TcxGridDBColumn
            Caption = #31199#36161#35268#26684
            DataBinding.FieldName = 'UnitTypes'
            PropertiesClassName = 'TcxComboBoxProperties'
            Visible = False
            HeaderAlignmentHorz = taCenter
            Width = 96
          end
          object tvViewColumn11: TcxGridDBColumn
            Caption = #35745#31639#20844#24335
            DataBinding.FieldName = 'CalculatingFormula'
            PropertiesClassName = 'TcxMemoProperties'
            Properties.CharCase = ecUpperCase
            Properties.ScrollBars = ssVertical
            Visible = False
            HeaderAlignmentHorz = taCenter
            Width = 183
          end
          object tvViewColumn7: TcxGridDBColumn
            Caption = #35745#31639#25968#20540
            DataBinding.FieldName = 'NumericalValue'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.DecimalPlaces = 8
            Properties.DisplayFormat = '0.########;-0.########'
            Visible = False
            Width = 112
          end
          object tvViewColumn6: TcxGridDBColumn
            Caption = #21464#37327#25968
            DataBinding.FieldName = 'VariableCount'
            PropertiesClassName = 'TcxSpinEditProperties'
            Properties.DisplayFormat = '0.########;-0.########'
            Properties.EditFormat = '0.########;-0.########'
            Properties.ValueType = vtFloat
            Visible = False
            HeaderAlignmentHorz = taCenter
            Width = 108
          end
          object tvViewColumn8: TcxGridDBColumn
            Caption = #25968#37327
            DataBinding.FieldName = 'Total'
            PropertiesClassName = 'TcxPopupEditProperties'
            Properties.PopupControl = RzPanel3
            Properties.OnCloseUp = tvViewColumn8PropertiesCloseUp
            Properties.OnInitPopup = tvViewColumn8PropertiesInitPopup
            Properties.OnPopup = tvViewColumn8PropertiesPopup
            HeaderAlignmentHorz = taCenter
            Width = 110
          end
          object tvViewColumn9: TcxGridDBColumn
            Caption = #21333#20301
            DataBinding.FieldName = 'Company'
            PropertiesClassName = 'TcxComboBoxProperties'
            Properties.ImmediateDropDownWhenActivated = True
            OnGetPropertiesForEdit = tvViewColumn9GetPropertiesForEdit
            Width = 60
          end
          object tvViewColumn10: TcxGridDBColumn
            Caption = #21333#20215
            DataBinding.FieldName = 'UnitPrice'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.Alignment.Horz = taRightJustify
            Properties.OnValidate = tvViewColumn10PropertiesValidate
            HeaderAlignmentHorz = taCenter
            Width = 73
          end
          object tvViewColumn12: TcxGridDBColumn
            Caption = #37329#39069
            DataBinding.FieldName = 'SumMoney'
            PropertiesClassName = 'TcxCurrencyEditProperties'
            Properties.Alignment.Horz = taRightJustify
            HeaderGlyphAlignmentHorz = taRightJustify
            Styles.Content = DM.SumMoney
            Width = 144
          end
          object tvViewColumn14: TcxGridDBColumn
            Caption = #24037#22320#31614#25910
            DataBinding.FieldName = 'Supplier'
            Visible = False
            Width = 80
          end
          object tvViewColumn25: TcxGridDBColumn
            Caption = #36164#36136#21333#20301
            DataBinding.FieldName = 'AptitudeCompany'
            PropertiesClassName = 'TcxTextEditProperties'
            Visible = False
            Width = 80
          end
          object tvViewColumn27: TcxGridDBColumn
            Caption = #26045#24037#21333#20301
            DataBinding.FieldName = 'CreateCompany'
            PropertiesClassName = 'TcxComboBoxProperties'
            Visible = False
            Width = 70
          end
          object tvViewColumn13: TcxGridDBColumn
            Caption = #22791#27880
            DataBinding.FieldName = 'Remarks'
            PropertiesClassName = 'TcxMemoProperties'
            Properties.Alignment = taLeftJustify
            Properties.ScrollBars = ssVertical
            Width = 230
          end
        end
        object Lv: TcxGridLevel
          GridView = tvView
        end
      end
      object RzToolbar13: TRzToolbar
        Left = 1
        Top = 36
        Width = 887
        Height = 29
        AutoStyle = False
        Images = DM.cxImageList1
        BorderInner = fsNone
        BorderOuter = fsNone
        BorderSides = [sdBottom]
        BorderWidth = 0
        Caption = #26597#35810
        GradientColorStyle = gcsCustom
        TabOrder = 1
        VisualStyle = vsGradient
        ToolbarControls = (
          RzToolButton1
          RzSpacer30
          RzToolButton2
          RzSpacer2
          RzToolButton90
          RzSpacer120
          RzToolButton85
          RzSpacer113
          btnGridSet
          RzSpacer10
          RzToolButton3
          RzSpacer3
          RzToolButton87
          RzSpacer1
          RzToolButton88
          RzSpacer4)
        object RzSpacer30: TRzSpacer
          Left = 66
          Top = 2
        end
        object RzToolButton85: TRzToolButton
          Left = 214
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          SelectionFrameColor = clBtnShadow
          ImageIndex = 51
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #21024#38500
          OnClick = RzToolButton85Click
        end
        object RzSpacer113: TRzSpacer
          Left = 276
          Top = 2
        end
        object RzToolButton87: TRzToolButton
          Left = 437
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          SelectionFrameColor = clBtnShadow
          DropDownMenu = Print
          ImageIndex = 5
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          ToolStyle = tsDropDown
          Caption = #25253#34920
        end
        object RzToolButton88: TRzToolButton
          Left = 507
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          SelectionFrameColor = clBtnShadow
          ImageIndex = 8
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #36864#20986
          OnClick = RzToolButton88Click
        end
        object RzToolButton90: TRzToolButton
          Left = 144
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          SelectionFrameColor = clBtnShadow
          ImageIndex = 3
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #20445#23384
          OnClick = RzToolButton90Click
        end
        object RzSpacer120: TRzSpacer
          Left = 206
          Top = 2
        end
        object RzSpacer1: TRzSpacer
          Left = 499
          Top = 2
        end
        object RzSpacer10: TRzSpacer
          Left = 359
          Top = 2
        end
        object btnGridSet: TRzToolButton
          Left = 284
          Top = 2
          Width = 75
          ImageIndex = 4
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #34920#26684#35774#32622
          OnClick = btnGridSetClick
        end
        object RzSpacer2: TRzSpacer
          Left = 136
          Top = 2
        end
        object RzToolButton1: TRzToolButton
          Left = 4
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          ImageIndex = 37
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #26032#22686
          OnClick = RzToolButton1Click
        end
        object RzToolButton2: TRzToolButton
          Left = 74
          Top = 2
          Width = 62
          SelectionColorStop = 16119543
          ImageIndex = 0
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          Caption = #32534#36753
          Enabled = False
          OnClick = RzToolButton2Click
        end
        object RzSpacer3: TRzSpacer
          Left = 429
          Top = 2
        end
        object RzToolButton3: TRzToolButton
          Left = 367
          Top = 2
          Width = 62
          GradientColorStyle = gcsCustom
          ImageIndex = 59
          ShowCaption = True
          UseToolbarButtonSize = False
          UseToolbarShowCaption = False
          UseToolbarVisualStyle = False
          VisualStyle = vsGradient
          Caption = #26597#35810
          OnClick = RzToolButton3Click
        end
        object RzSpacer4: TRzSpacer
          Left = 569
          Top = 2
        end
      end
      object RzPanel5: TRzPanel
        Left = 1
        Top = 1
        Width = 887
        Height = 35
        Align = alTop
        BorderInner = fsFlat
        BorderOuter = fsNone
        BorderSides = [sdBottom]
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
    end
  end
  object RzPanel2: TRzPanel
    Left = 0
    Top = 0
    Width = 274
    Height = 539
    Align = alLeft
    BorderInner = fsFlat
    BorderOuter = fsNone
    BorderSides = [sdLeft, sdRight]
    TabOrder = 1
  end
  object RzPanel13: TRzPanel
    Left = 532
    Top = 151
    Width = 266
    Height = 176
    TabOrder = 2
    Visible = False
    object cxButton6: TcxButton
      Left = 46
      Top = 130
      Width = 67
      Height = 25
      Caption = #30830#23450
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      OptionsImage.ImageIndex = 17
      OptionsImage.Images = DM.cxImageList1
      TabOrder = 0
      OnClick = cxButton6Click
    end
    object cxButton7: TcxButton
      Left = 159
      Top = 130
      Width = 67
      Height = 25
      Caption = #21462#28040
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      OptionsImage.ImageIndex = 51
      OptionsImage.Images = DM.cxImageList1
      TabOrder = 1
      OnClick = cxButton7Click
    end
    object cxGroupBox1: TcxGroupBox
      Left = 7
      Top = 13
      Caption = #21333#20301
      TabOrder = 2
      Height = 102
      Width = 243
      object Label5: TLabel
        Left = 19
        Top = 65
        Width = 52
        Height = 13
        Caption = #24448#26469#21333#20301':'
      end
      object Label1: TLabel
        Left = 17
        Top = 25
        Width = 52
        Height = 13
        Caption = #20379#24212#21333#20301':'
      end
      object cxDBLookupComboBox1: TcxDBLookupComboBox
        Left = 75
        Top = 24
        DataBinding.DataField = 'SignName'
        DataBinding.DataSource = DataMaster
        Properties.KeyFieldNames = 'Name'
        Properties.ListColumns = <
          item
            Caption = #21517#31216
            FieldName = 'Name'
          end
          item
            Caption = #31616#30721
            FieldName = 'pycode'
          end>
        Properties.ListSource = DataSignName
        TabOrder = 0
        Width = 140
      end
      object cxDBLookupComboBox2: TcxDBLookupComboBox
        Left = 75
        Top = 64
        RepositoryItem = DM.SignNameBox
        DataBinding.DataField = 'SignName'
        DataBinding.DataSource = DataMaster
        Properties.ListColumns = <>
        TabOrder = 1
        Width = 140
      end
    end
  end
  object RzPanel4: TRzPanel
    Left = 301
    Top = 151
    Width = 227
    Height = 140
    TabOrder = 3
    Visible = False
    object Bevel1: TBevel
      Left = 0
      Top = 77
      Width = 225
      Height = 2
    end
    object cxDBTextEdit3: TcxDBTextEdit
      Left = 82
      Top = 15
      DataBinding.DataField = 'cxSpec'
      DataBinding.DataSource = DataMaster
      TabOrder = 0
      Width = 103
    end
    object cxLabel6: TcxLabel
      Left = 19
      Top = 15
      Caption = #35268#12288#12288#26684':'
      Style.TextColor = clBlue
    end
    object cxLabel7: TcxLabel
      Left = 19
      Top = 42
      Caption = #26448#26009#35268#26684':'
    end
    object cxDBComboBox1: TcxDBComboBox
      Left = 81
      Top = 42
      DataBinding.DataField = 'cxSpec'
      DataBinding.DataSource = DataMaster
      Properties.ImmediateDropDownWhenActivated = True
      Properties.OnInitPopup = cxDBComboBox1PropertiesInitPopup
      TabOrder = 3
      Width = 104
    end
    object cxButton1: TcxButton
      Left = 37
      Top = 94
      Width = 67
      Height = 25
      Caption = #30830#23450
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      OptionsImage.ImageIndex = 17
      OptionsImage.Images = DM.cxImageList1
      TabOrder = 4
      OnClick = cxButton1Click
    end
    object cxButton2: TcxButton
      Left = 134
      Top = 94
      Width = 67
      Height = 25
      Caption = #21462#28040
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      OptionsImage.ImageIndex = 51
      OptionsImage.Images = DM.cxImageList1
      TabOrder = 5
      OnClick = cxButton2Click
    end
  end
  object RzPanel3: TRzPanel
    Left = 804
    Top = 99
    Width = 342
    Height = 316
    BorderShadow = clBlack
    TabOrder = 4
    Visible = False
    object Bevel2: TBevel
      Left = 10
      Top = 40
      Width = 319
      Height = 2
    end
    object cxGroupBox4: TcxGroupBox
      Left = 10
      Top = 48
      Caption = #38050#31563#35745#31639#22120
      ParentBackground = False
      TabOrder = 1
      Height = 81
      Width = 319
      object Label23: TLabel
        Left = 163
        Top = 25
        Width = 52
        Height = 13
        Caption = #20214#12288#12288#25968':'
      end
      object Label24: TLabel
        Left = 24
        Top = 51
        Width = 47
        Height = 13
        Caption = ' '#21333#26681'/'#31859':'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label26: TLabel
        Left = 171
        Top = 51
        Width = 44
        Height = 13
        Caption = #21333#20214'/'#26681':'
      end
      object Label3: TLabel
        Left = 19
        Top = 25
        Width = 52
        Height = 13
        Caption = #38050#31563#35268#26684':'
      end
      object cxSpinEdit1: TcxSpinEdit
        Left = 221
        Top = 22
        Properties.EditFormat = '0.##;-0.##'
        Properties.ValueType = vtFloat
        Properties.OnChange = cxSpinEdit1PropertiesChange
        TabOrder = 1
        Width = 80
      end
      object cxSpinEdit2: TcxSpinEdit
        Left = 77
        Top = 49
        Properties.DisplayFormat = '0.##;-0.##'
        Properties.EditFormat = '0.##;-0.##'
        Properties.ValueType = vtFloat
        TabOrder = 2
        Value = 9.000000000000000000
        Width = 76
      end
      object cxSpinEdit5: TcxSpinEdit
        Left = 221
        Top = 49
        Properties.DisplayFormat = '0.##;-0.##'
        Properties.EditFormat = '0.##;-0.##'
        Properties.ValueType = vtFloat
        Properties.OnChange = cxSpinEdit5PropertiesChange
        TabOrder = 3
        Width = 80
      end
      object cxComboBox1: TcxComboBox
        Left = 77
        Top = 22
        Properties.OnCloseUp = cxComboBox1PropertiesCloseUp
        Style.BorderStyle = ebs3D
        TabOrder = 0
        Width = 72
      end
    end
    object cxGroupBox2: TcxGroupBox
      Left = 10
      Top = 135
      Caption = #25968#37327#35745#31639#22120
      TabOrder = 2
      Height = 122
      Width = 319
      object Label4: TLabel
        Left = 18
        Top = 30
        Width = 52
        Height = 13
        Caption = #35745#31639#20844#24335':'
      end
      object Label2: TLabel
        Left = 30
        Top = 82
        Width = 40
        Height = 13
        Caption = #21464#37327#25968':'
      end
      object Label6: TLabel
        Left = 170
        Top = 82
        Width = 52
        Height = 13
        Caption = #35745#31639#25968#20540':'
      end
      object cxDBMemo2: TcxDBMemo
        Left = 76
        Top = 30
        DataBinding.DataField = 'CalculatingFormula'
        DataBinding.DataSource = DataMaster
        Properties.OnChange = cxDBMemo2PropertiesChange
        TabOrder = 2
        Height = 43
        Width = 229
      end
      object cxDBSpinEdit2: TcxDBSpinEdit
        Left = 76
        Top = 79
        DataBinding.DataField = 'VariableCount'
        DataBinding.DataSource = DataMaster
        Properties.DisplayFormat = '0.#######;-0.#######'
        Properties.EditFormat = '0.#######;-0.#######'
        Properties.ValueType = vtFloat
        Properties.OnChange = cxDBSpinEdit2PropertiesChange
        TabOrder = 0
        Width = 80
      end
      object cxDBSpinEdit3: TcxDBSpinEdit
        Left = 228
        Top = 79
        DataBinding.DataField = 'NumericalValue'
        DataBinding.DataSource = DataMaster
        Properties.DisplayFormat = '0.#######;-0.#######'
        Properties.EditFormat = '0.#######;-0.#######'
        Properties.ValueType = vtFloat
        Properties.OnChange = cxDBSpinEdit3PropertiesChange
        TabOrder = 1
        Width = 77
      end
    end
    object cxLabel1: TcxLabel
      Left = 12
      Top = 13
      Caption = #25968#37327':'
    end
    object cxDBSpinEdit1: TcxDBSpinEdit
      Left = 50
      Top = 13
      DataBinding.DataField = 'Total'
      DataBinding.DataSource = DataMaster
      Properties.DisplayFormat = '0.#######;-0.#######'
      Properties.EditFormat = '0.#######;-0.#######'
      Properties.ValueType = vtFloat
      Properties.OnValidate = cxDBSpinEdit1PropertiesValidate
      TabOrder = 0
      Width = 103
    end
    object cxLabel2: TcxLabel
      Left = 159
      Top = 13
      Caption = #23567#25968#20445#30041#20301#25968':'
    end
    object cxComboBox2: TcxComboBox
      Left = 245
      Top = 13
      Properties.Items.Strings = (
        '1'#20301
        '2'#20301
        '3'#20301
        '4'#20301
        '5'#20301
        '6'#20301
        '7'#20301)
      Properties.OnCloseUp = cxComboBox2PropertiesCloseUp
      Style.BorderStyle = ebs3D
      TabOrder = 5
      Text = '3'#20301
      Width = 66
    end
    object cxButton3: TcxButton
      Left = 77
      Top = 270
      Width = 67
      Height = 25
      Caption = #30830#23450
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      OptionsImage.ImageIndex = 17
      OptionsImage.Images = DM.cxImageList1
      TabOrder = 6
      OnClick = cxButton3Click
    end
    object cxButton4: TcxButton
      Left = 222
      Top = 270
      Width = 67
      Height = 25
      Caption = #21462#28040
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      OptionsImage.ImageIndex = 51
      OptionsImage.Images = DM.cxImageList1
      TabOrder = 7
      OnClick = cxButton4Click
    end
  end
  object Master: TClientDataSet
    Aggregates = <>
    Params = <>
    AfterInsert = MasterAfterInsert
    AfterPost = MasterAfterPost
    AfterDelete = MasterAfterDelete
    Left = 64
    Top = 312
  end
  object DataMaster: TDataSource
    DataSet = Master
    Left = 64
    Top = 368
  end
  object ADOMaking: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 160
    Top = 152
  end
  object DataMaking: TDataSource
    DataSet = ADOMaking
    Left = 160
    Top = 208
  end
  object Print: TPopupMenu
    Images = DM.cxImageList1
    Left = 168
    Top = 96
    object Excel1: TMenuItem
      Caption = #36755#20986'Excel'
      OnClick = Excel1Click
    end
    object N2: TMenuItem
      Caption = #25171#21360#25253#34920
      ImageIndex = 6
      OnClick = N2Click
    end
  end
  object RightMenu: TPopupMenu
    Images = DM.cxImageList1
    Left = 384
    Top = 120
    object MenuItem1: TMenuItem
      Caption = #26032#22686
    end
    object MenuItem2: TMenuItem
      Caption = #32534#36753
      ImageIndex = 6
    end
    object N1: TMenuItem
      Caption = #20445#23384
    end
    object N3: TMenuItem
      Caption = #21024#38500
    end
  end
  object DataSignName: TDataSource
    DataSet = ADOSignName
    Left = 163
    Top = 318
  end
  object ADOSignName: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 160
    Top = 264
  end
  object DataSpec: TDataSource
    DataSet = ADOSpec
    Left = 168
    Top = 464
  end
  object ADOSpec: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 168
    Top = 416
  end
end
