program Project;

{$R 'uac.res' 'uac.rc'}

uses
  Forms,
  Winapi.Windows,
  ufrmMain in 'Forms\ufrmMain.pas' {frmMain},
  ufrmlogin in 'Forms\ufrmlogin.pas' {frmlogin},
  ufrmsystem in 'Forms\ufrmsystem.pas' {frmsystem},
  uDataModule in 'Forms\uDataModule.pas' {DM: TDataModule},
  global in 'Public\global.pas',
  TreeFillThrd in 'Public\TreeFillThrd.pas',
  TreeUtils in 'Public\TreeUtils.pas',
  ufrmSort in 'Forms\ufrmSort.pas' {frmSort},
  DBGridEhToExcel in 'Public\DBGridEhToExcel.pas',
  ufrmBackups in 'Forms\ufrmBackups.pas' {frmBackups},
  ufrmAccounts in 'Forms\ufrmAccounts.pas' {frmAccounts},
  NewDateTimePicker in 'Public\NewDateTimePicker.pas',
  ufrmAddConcrete in 'Forms\ufrmAddConcrete.pas' {frmAddConcrete},
  ufrmN1 in 'Page\ufrmN1.pas' {frmWorkWeight},
  ufrmReadyMoney in 'Forms\ufrmReadyMoney.pas' {frmReadyMoney},
  ufrmManage in 'Forms\ufrmManage.pas' {frmManage},
  vsk in 'Public\vsk.pas',
  ufrmBorrowMoney in 'Forms\ufrmBorrowMoney.pas' {frmBorrowMoney},
  ufrmBorrowPost in 'Forms\ufrmBorrowPost.pas' {frmBorrowPost},
  ufrmGiveBorrowMoney in 'Forms\ufrmGiveBorrowMoney.pas' {frmGiveBorrowMoney},
  ufrmQualityManage in 'Forms\ufrmQualityManage.pas' {frmQualityManage},
  ufrmTenderManage in 'Forms\ufrmTenderManage.pas' {frmTenderManage},
  ufrmCompleteManage in 'Forms\ufrmCompleteManage.pas' {frmCompleteManage},
  ufunctions in 'Public\ufunctions.pas',
  FileTree in 'Public\FileTree.pas',
  UnitExpCalc in 'Public\UnitExpCalc.pas',
  ufrmConstructManage in 'Forms\ufrmConstructManage.pas' {frmConstructManage},
  ufrmConstructPost in 'Forms\ufrmConstructPost.pas' {frmConstructPost},
  ufrmRebarPost in 'Forms\ufrmRebarPost.pas' {frmRebarPost},
  ufrmPostPact in 'Forms\ufrmPostPact.pas' {frmPostPact},
  ufrmPostAccounts in 'Forms\ufrmPostAccounts.pas' {frmPostAccounts},
  ufrmBorrowEdit in 'Forms\ufrmBorrowEdit.pas' {frmBorrowEdit},
  ufrmCalc in 'Forms\ufrmCalc.pas' {frmCalc},
  MD5 in 'Public\MD5.pas',
  ufrmReceivablesAccount in 'Forms\ufrmReceivablesAccount.pas' {frmReceivablesAccount},
  ufrmBorrowAccount in 'Forms\ufrmBorrowAccount.pas' {frmBorrowAccount},
  ufrmTurnover in 'Forms\ufrmTurnover.pas' {frmTurnover},
  ufrmEnterStorage in 'Forms\ufrmEnterStorage.pas' {frmEnterStorage},
  ufrmBaseController in 'Forms\ufrmBaseController.pas' {frmBaseController},
  ufrmIsViewGrid in 'Forms\ufrmIsViewGrid.pas' {frmIsViewGrid},
  ufrmFlowingWaterAccount in 'Forms\ufrmFlowingWaterAccount.pas' {frmFlowingWaterAccount},
  uMaterialStorage in 'Forms\uMaterialStorage.pas' {frmMaterialStorage},
  ufrmMaterialStoragePost in 'Forms\ufrmMaterialStoragePost.pas' {frmMaterialStoragePost},
  ufrmCalcExpression in 'Forms\ufrmCalcExpression.pas' {frmCalcExpression},
  ufrmReconciliationTable in 'Forms\ufrmReconciliationTable.pas' {frmReconciliationTable},
  ufrmDetailed in 'Forms\ufrmDetailed.pas' {frmPayDetailed},
  ufrmDatum in 'Forms\ufrmDatum.pas' {frmDatum},
  ufrmEmployee in 'Forms\ufrmEmployee.pas' {frmEmployee},
  UtilsTree in 'Public\UtilsTree.pas',
  FillThrdTree in 'Public\FillThrdTree.pas',
  ufrmCompanyManage in 'Forms\ufrmCompanyManage.pas' {frmCompanyManage},
  CreateOrderNum in 'Public\CreateOrderNum.pas',
  ufrmMakings in 'Forms\ufrmMakings.pas' {frmMakings},
  ufrmSpareMoney in 'Forms\ufrmSpareMoney.pas' {frmSpareMoney},
  ufrmSection in 'Forms\ufrmSection.pas' {frmSection},
  ufrmWorkType in 'Forms\ufrmWorkType.pas' {frmWorkType},
  ufrmProjectWork in 'ProjectForms\ufrmProjectWork.pas' {frmProjectWork},
  ufrmProjectSubcontract in 'ProjectForms\ufrmProjectSubcontract.pas' {frmProjectSubcontract},
  ufrmProjectFinance in 'ProjectForms\ufrmProjectFinance.pas' {frmProjectFinance},
  ufrmProjectSanction in 'ProjectForms\ufrmProjectSanction.pas' {frmProjectSanction},
  ufrmProjectLease in 'ProjectForms\ufrmProjectLease.pas' {frmProjectLease},
  ufrmProjectMechanics in 'ProjectForms\ufrmProjectMechanics.pas' {frmProjectMechanics},
  ufrmProjectManage in 'ProjectForms\ufrmProjectManage.pas' {frmProjectManage},
  ufrmMaintainProject in 'Maintain\ufrmMaintainProject.pas' {frmMaintainProject},
  cxCustomData in 'Library\cxCustomData.pas',
  cxDataUtils in 'Library\cxDataUtils.pas',
  cxLookupEdit in 'Library\cxLookupEdit.pas',
  uProjectFrame in 'Frames\uProjectFrame.pas' {ProjectFrame: TFrame},
  ufrmProjectConcrete in 'ProjectForms\ufrmProjectConcrete.pas' {frmConcrete},
  ufrmJobList in 'Maintain\ufrmJobList.pas' {frmJobList},
  ufrmCostSubject in 'Maintain\ufrmCostSubject.pas' {frmCostSubject},
  ufrmSystemSet in 'System\ufrmSystemSet.pas' {frmSystemSet},
  ufrmRevenueAccount in 'Account\ufrmRevenueAccount.pas' {frmRevenueAccount},
  ufrmRunningAccount in 'Account\ufrmRunningAccount.pas' {frmRunningAccount},
  ufrmDetailList in 'Account\ufrmDetailList.pas' {frmDetailList},
  ufrmBaseRunningAccount in 'Account\ufrmBaseRunningAccount.pas' {frmBaseRunningAccount},
  ufrmPayDetailedPost in 'Account\ufrmPayDetailedPost.pas' {frmPayDetailedPost},
  ufrmFinalCost in 'Account\ufrmFinalCost.pas' {frmFinalCost},
  uBaseProject in 'FinalCost\uBaseProject.pas' {frmBaseProject},
  ufrmBuild in 'Company\ufrmBuild.pas' {frmBuild},
  ufrmExpend in 'Company\ufrmExpend.pas' {frmExpend},
  ufrmBaseSanction in 'Base\ufrmBaseSanction.pas' {frmBaseSanction},
  ufrmCompanySanction in 'Company\ufrmCompanySanction.pas' {frmCompanySanction},
  ufrmCompanyWork in 'Company\ufrmCompanyWork.pas' {frmCompanyWork},
  ufrmCompanyfiles in 'Company\ufrmCompanyfiles.pas' {frmCompanyfiles},
  ufrmBaseStorage in 'Company\ufrmBaseStorage.pas' {frmBaseStorage},
  ufrmCompanyStorageEntry in 'Company\ufrmCompanyStorageEntry.pas' {frmCompanyStorageEntry},
  ufrmCompanyFormStorage in 'Company\ufrmCompanyFormStorage.pas' {frmCompanyFormStorage},
  ufrmProjectPurchase in 'ProjectForms\ufrmProjectPurchase.pas' {frmProjectPurchase},
  ufrmPurchaseDetailed in 'ProjectForms\ufrmPurchaseDetailed.pas' {frmPurchaseDetailed},
  ufrmBasePurchase in 'Purchase\ufrmBasePurchase.pas' {frmBasePurchase},
  ufrmBasePurchases in 'ProjectForms\ufrmBasePurchases.pas' {Form3},
  ufrmStorageManage in 'ProjectForms\ufrmStorageManage.pas' {frmStorageManage},
  ufrmRevenueProject in 'Forms\ufrmRevenueProject.pas' {frmRevenueProject},
  ufrmFinalAccount in 'Account\ufrmFinalAccount.pas' {frmFinalAccount},
  UnitADODataSet in 'Public\UnitADODataSet.pas',
  ufrmCompanyFrame in 'Frames\ufrmCompanyFrame.pas' {frmCompanyFrame: TFrame},
  UnitADO in 'Public\UnitADO.pas',
  ufrmEnclosure in 'Forms\ufrmEnclosure.pas' {frmEnclosure},
  ufrmEntryStorage in 'ProjectForms\ufrmEntryStorage.pas' {frmEntryStorage},
  ufrmNewBaseStorage in 'ProjectForms\ufrmNewBaseStorage.pas' {frmNewBaseStorage},
  ufrmOutStorage in 'ProjectForms\ufrmOutStorage.pas' {frmOutStorage},
  ufrmReturnStorage in 'ProjectForms\ufrmReturnStorage.pas' {frmReturnStorage},
  ufrmFlowingFrame in 'Frames\ufrmFlowingFrame.pas' {frmFlowingFrame: TFrame},
  ufrmStorageBase in 'Company\ufrmStorageBase.pas' {frmStorageBase},
  ufrmPostStorage in 'Company\ufrmPostStorage.pas' {frmPostStorage},
  ufrmReport in 'Forms\ufrmReport.pas' {frmReport},
  ufrmSumDetailed in 'Forms\ufrmSumDetailed.pas' {frmSumDetailed},
  ufrmContract in 'Forms\ufrmContract.pas' {frmContract},
  ufrmInputform in 'Forms\ufrmInputform.pas' {frmInputformBase},
  ufrmFastReturnGoods in 'Forms\ufrmFastReturnGoods.pas' {frmFastReturnGoods},
  ufrmFastGiveBack in 'Forms\ufrmFastGiveBack.pas' {frmFastGiveBack},
  ufrmFastGooutStorage in 'Forms\ufrmFastGooutStorage.pas' {frmFastGooutStorage},
  ufrmFastEnterStorage in 'Forms\ufrmFastEnterStorage.pas' {frmFastEnterStorage},
  ufrmStorageTreeView in 'Forms\ufrmStorageTreeView.pas' {frmStorageTreeView},
  ufastContrctEnterStorage in 'Forms\ufastContrctEnterStorage.pas' {fastContrctEnterStorage},
  ufastContrctGooutStorage in 'Forms\ufastContrctGooutStorage.pas' {fastContrctGooutStorage},
  ufrmGroupUnitPrice in 'Forms\ufrmGroupUnitPrice.pas' {frmGroupUnitPrice},
  ufrmfastContrctReturnGoods in 'Forms\ufrmfastContrctReturnGoods.pas' {frmfastContrctReturnGoods},
  ufrmFastContractGiveBack in 'Forms\ufrmFastContractGiveBack.pas' {frmFastContractGiveBack},
  ufrmInputFiles in 'Forms\ufrmInputFiles.pas' {frmInputFiles},
  ufrmQuotationlib in 'Forms\ufrmQuotationlib.pas' {frmQuotationlib},
  ufrmGoodsPrice in 'Forms\ufrmGoodsPrice.pas' {frmGoodsPrice},
  ufrmCalcTool in 'Forms\ufrmCalcTool.pas' {frmCalcTool},
  ufrmSystemConfig in 'Forms\ufrmSystemConfig.pas' {frmSystemConfig},
  uIStorage in 'Interface\uIStorage.pas',
  uMsg in 'Public\uMsg.pas',
  ufrmContractInputBase in 'Forms\ufrmContractInputBase.pas' {frmContractInputBase},
  ufrmContrctGoodsList in 'Forms\ufrmContrctGoodsList.pas' {frmContrctGoodsList},
  ufrmContrctList in 'Forms\ufrmContrctList.pas' {frmContrctList},
  ufrmCompanyInfo in 'Forms\ufrmCompanyInfo.pas' {frmCompanyInfo},
  SPComm in 'Public\SPComm.pas',
  ufrmSerialPort in 'Forms\ufrmSerialPort.pas' {frmSerialPort},
  ufrmMakingsPost in 'Forms\ufrmMakingsPost.pas' {frmMakingsPost},
  ufrmStyleManage in 'Forms\ufrmStyleManage.pas' {frmStyleManage},
  ufrmCustomer in 'Forms\ufrmCustomer.pas' {frmCustomer},
  ufrmCustomerPlate in 'Forms\ufrmCustomerPlate.pas' {frmCustomerPlate},
  ufrmCustomerPost in 'Forms\ufrmCustomerPost.pas' {frmCustomerPost},
  ufrmCustomerProject in 'Forms\ufrmCustomerProject.pas' {frmCustomerProject},
  ufrmCustomerSummary in 'Forms\ufrmCustomerSummary.pas' {frmCustomerSummary},
  ufrmCustomerAmount in 'Forms\ufrmCustomerAmount.pas' {frmCustomerAmount},
  ufrmCustomerCostCalc in 'Forms\ufrmCustomerCostCalc.pas' {frmCustomerCostCalc},
  ufrmCustomerSummaryU in 'Forms\ufrmCustomerSummaryU.pas' {frmCustomerSummaryU},
  uGUID in 'Public\uGUID.pas';

var
  hw : HWND;
  gt : Integer;


{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  hw := CreateMutex(nil,False,'Accounting'); {创建互斥体对象}
  gt := GetLastError;
  if gt <> Error_ALREADY_EXISTS then     {如果没有发现互斥体对象}
  begin
    Application.Title := '美诺记账本';
  //  Application.CreateForm(TfrmMain, frmMain);
    Application.CreateForm(TfrmMain, frmMain);
    Application.CreateForm(TDM, DM);
    frmlogin := Tfrmlogin.Create(Application);
    try
      frmlogin.Update;
      frmlogin.ShowModal;
      if not frmlogin.IsLogin then
      begin
        Application.Terminate;
      end else
      begin
        Application.Run;
      end;

    finally
      frmlogin.Free;
    end;

  end else
  begin
    Application.MessageBox('程序已经在运行中...','提示',MB_OK + MB_ICONERROR);
    ReleaseMutex(hw);{释放互斥体}
    Application.Terminate;
  end;

end.
