unit uCollectChild;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, DBGridEhGrouping, ToolCtrlsEh, DBGridEhToolCtrls,
  DynVarsEh, MemTableDataEh, Db, ADODB, DataDriverEh, ADODataDriverEh,
  MemTableEh, ComCtrls,RzPanel, RzTabs, RzTreeVw, RzRadGrp, RzSplit, Spin, RzButton,
  jpeg,ufrmPicture, Mask, RzEdit, ImgList, Buttons,TreeUtils, ExtDlgs, DBCGrids,
  GridsEh, DBAxisGridsEh, DBGridEh, ExtCtrls, PrnDbgeh, RzDTP, RzDBDTP, sPanel,
  Menus,EhLibADO,Math;

type
  TfrmCollectChild = class(TForm)
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure TreeView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RzBitBtn2Click(Sender: TObject);
    procedure N2Click(Sender: TObject);
    procedure QryCalcFields(DataSet: TDataSet);
    procedure TreeView1CustomDrawItem(Sender: TCustomTreeView; Node: TTreeNode;
      State: TCustomDrawState; var DefaultDraw: Boolean);
    procedure TreeView1DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure TreeView1DragDrop(Sender, Source: TObject; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
    PactCode : string;
  end;

var
  frmCollectChild: TfrmCollectChild;
  g_frmPicture : TfrmPicture;
  FContractTree : TTreeUtils;
  g_Paid : Currency;
  g_Sum_Money : Currency;


  
implementation

{$R *.dfm}

uses
  vsk,
  ufrmMain,
  uDataModule,
  global,
  TreeFillThrd,
  ufrmInput,
  ufrmAddPact,
  DBGridEhToExcel,
  ufrmAccounts,
  ShellAPI,
  PrViewEh,
  Printers,
  ufrmBranchInput,
  ufrmTradingAccount;  //ufrmBranchlnput


procedure TfrmCollectChild.TreeView1CustomDrawItem(Sender: TCustomTreeView;
  Node: TTreeNode; State: TCustomDrawState; var DefaultDraw: Boolean);
begin
  if (Node.Level = 1) then  {and (该叶子节点满足某条件)}
  begin
    if (cdsSelected in State) then
     begin
        with Sender.Canvas do
        begin
          Brush.Color := clGradientActiveCaption;
          Font.Color :=clRed;
        //  Font.Style:=[fsBold];
        end;
     end
     else
     begin
       Sender.Canvas.Font.Color := clBlue;
     end;

  end;
  DefaultDraw := True
end;

procedure TfrmCollectChild.TreeView1DragDrop(Sender, Source: TObject; X,
  Y: Integer);
var TargetNode, SourceNode: TTreeNode;
begin
  TargetNode := TreeView1.DropTarget;
  SourceNode := TreeView1.Selected;
  if vsk.Box('您确认要移动合同吗？', '提示', MB_ICONQUESTION
    + MB_YESNO) = IDYES then
    if FContractTree.ModifyNodePID(PNodeData(TargetNode.Data)^.Index, SourceNode) then
       FContractTree.FillTree(g_Parent,g_pacttype);
end;

procedure TfrmCollectChild.TreeView1DragOver(Sender, Source: TObject; X,
  Y: Integer; State: TDragState; var Accept: Boolean);
begin
  if Source = TreeView1 then Accept := True
  else Accept := False;
end;

procedure TfrmCollectChild.TreeView1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then
  begin
    Self.RzBitBtn17.Click;
  end;
end;

procedure TfrmCollectChild.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
//  Action := caMinimize;
//  frmMain.OpenInAccounts;
end;

procedure TfrmCollectChild.FormCreate(Sender: TObject);
begin
//  frmCollectChild := Self;
end;

procedure TfrmCollectChild.N2Click(Sender: TObject);
begin
  Self.RzBitBtn18.Click;
end;

procedure TfrmCollectChild.QryCalcFields(DataSet: TDataSet);
begin
//  DataSet.FieldByName('sumMoney').Value := 888;
end;

procedure TfrmCollectChild.RzBitBtn2Click(Sender: TObject);
begin
  Close;
end;

end.
